<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?
use Bitrix\Main\Application;
use Tairai\DependencyInjection\Container;

$request = Application::getInstance()->getContext()->getRequest();
$isOnlineRecordPage = $page === 'online-record';
?>
    <?php if (!$isOnlineRecordPage) : ?>
    <div class="global-footer">
				<div class="inner-wrapper">
					<div class="gf-overflow">
						<div class="gf-left">
							<div class="gf-phone">
								<?$APPLICATION->IncludeComponent("bitrix:main.include", "phone_footer", 
									Array(
										"AREA_FILE_SHOW" => "file", 
										"PATH" => $phoneFilePath
									)
								);?>
								<div class="gfp-info">
								<?$APPLICATION->IncludeComponent("bitrix:main.include", "", 
									Array(
										"AREA_FILE_SHOW" => "file", 
										"PATH" => "local/templates/tairai/paths/phone_sub_text.php" 
									)
									);?>
								</div>
								<!--<div class="gfp-time">
								<?/*$APPLICATION->IncludeComponent("bitrix:main.include", "",
									Array(
										"AREA_FILE_SHOW" => "file", 
										"PATH" => "local/templates/tairai/paths/day_time.php" 
									)
									);*/?>
								</div>-->
							</div>
							<div class="gf-service-links">
								<div class="gfsl-item">
									<div class="gf-i-title">Задайте нам вопрос</div>
									<?$APPLICATION->IncludeComponent("bitrix:main.include", "mail",
										Array(
											"AREA_FILE_SHOW" => "file",
											"PATH" => "/local/templates/tairai/paths/mail.php"
										)
									);?>
								</div>
							</div>
							<div class="gf-l-bottom">
								<?$APPLICATION->IncludeComponent("bitrix:menu", "socials",
									Array(
										"ROOT_MENU_TYPE" => "socials",
										"MAX_LEVEL" => "1",
										"CHILD_MENU_TYPE" => "",
										"USE_EXT" => "N",
										"DELAY" => "N",
										"ALLOW_MULTI_SELECT" => "N",
										"MENU_CACHE_TYPE" => "A",
										"MENU_CACHE_TIME" => "3600",
										"MENU_CACHE_USE_GROUPS" => "N",
										"MENU_CACHE_GET_VARS" => array(),
									)
								);?>
								<a href="/online-recording/" class="button">Записаться на сеанс</a>
							</div>
						</div>
						<div class="gf-right">
							<div class="gfr-links">
								<div class="gfr-col">
									<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom_col",
										Array(
											"ROOT_MENU_TYPE" => "bottom_left_col",
											"MAX_LEVEL" => "1",
											"CHILD_MENU_TYPE" => "",
											"USE_EXT" => "N",
											"DELAY" => "N",
											"ALLOW_MULTI_SELECT" => "N",
											"MENU_CACHE_TYPE" => "A",
											"MENU_CACHE_TIME" => "3600",
											"MENU_CACHE_USE_GROUPS" => "N",
											"MENU_CACHE_GET_VARS" => array(),
										)
									);?>
								</div>
								<div class="gfr-col">
									<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom_col",
										Array(
											"ROOT_MENU_TYPE" => "bottom_right_col",
											"MAX_LEVEL" => "1",
											"CHILD_MENU_TYPE" => "",
											"USE_EXT" => "N",
											"DELAY" => "N",
											"ALLOW_MULTI_SELECT" => "N",
											"MENU_CACHE_TYPE" => "A",
											"MENU_CACHE_TIME" => "3600",
											"MENU_CACHE_USE_GROUPS" => "N",
											"MENU_CACHE_GET_VARS" => array(),
										)
									);?>
									<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom_col_small",
										Array(
											"ROOT_MENU_TYPE" => "bottom_right_col_small",
											"MAX_LEVEL" => "1",
											"CHILD_MENU_TYPE" => "",
											"USE_EXT" => "N",
											"DELAY" => "N",
											"ALLOW_MULTI_SELECT" => "N",
											"MENU_CACHE_TYPE" => "A",
											"MENU_CACHE_TIME" => "3600",
											"MENU_CACHE_USE_GROUPS" => "N",
											"MENU_CACHE_GET_VARS" => array(),
										)
									);?>
								</div>
							</div>
						</div>
                        <div class="float-clear"></div>
					</div>
					<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom",
						Array(
							"ROOT_MENU_TYPE" => "bottom",
							"MAX_LEVEL" => "1",
							"CHILD_MENU_TYPE" => "",
							"USE_EXT" => "N",
							"DELAY" => "N",
							"ALLOW_MULTI_SELECT" => "N",
							"MENU_CACHE_TYPE" => "A",
							"MENU_CACHE_TIME" => "3600",
							"MENU_CACHE_USE_GROUPS" => "N",
							"MENU_CACHE_GET_VARS" => array(),
						)
					);?>
				</div>
				<div class="gf-image"></div>
			</div>
    <?php endif; ?>
		</div>
	</div>

	<div class="hidden-content">

	</div>

	<div class="modal">
		<div class="modal-inner">
			<a data-modal-close></a>
			<div class="modal-content"></div>
		</div>
	</div>
    <?if (!$isOnlineRecordPage && $request["REGISTER_SUCCESS"] == "Y"):?>
        <div id="modal-thanks" class="modal-self is-show">
            <div class="thanks-modal">
                <div class="sale-text">Спасибо!</div>
                <h2>Вы успешно зарегистрированы!</h2>
            </div>
        </div>
    <?endif;?>

<script src="https://www.google.com/recaptcha/api.js?render=explicit&hl=ru" defer></script>
<?php
    $jsAssets = Container::getInstance()->getAssetManager()->getJsAssets($page);
    foreach ($jsAssets as $jsAsset):
?>
    <script type="text/javascript" src="<?= $jsAsset ?>" defer></script>'
<?
    endforeach;
?>
<script>
// select salon
setTimeout(function () {
	jQuery('.metro__check-single.selected').click();
}, 1000);
setTimeout(function () {
if ($(".metro__check-single.selected").length){
	jQuery('.metro__check-single.selected').click();
}
}, 2000);

setTimeout(function () {
var url_string = window.location.href; //window.location.href
var url = new URL(url_string);
var duration = url.searchParams.get("duration");
console.log(duration);
$('.services-select__select option[value=' + duration + ']').prop('selected', true);
}, 3000);




</script>
<script>
    document.addEventListener('DOMContentLoaded', function(event) {
        if (appConfig.mobileVersion) {
            const menuCnt = document.querySelector('.js-mobile-menu');
            const openMenuBtn = container.querySelector('.js-mobile-menu-open');
            const closeMenuBtn = document.querySelector('.js-mobile-menu-close');
            openMenuBtn.addEventListener('click', () => {
                document.body.classList.add('one-screen');
            });
            closeMenuBtn.addEventListener('click', () => {
                document.body.classList.remove('one-screen');
            });
        }
    });
</script>
<script type="text/javascript">
    (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter38097465 = new Ya.Metrika({ id:38097465, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true, trackHash:true, ut:"noindex", ecommerce:"dataLayer" }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");
</script>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&amp;l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-KK7ZQ5');
</script>
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KK7ZQ5" height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<script type="text/javascript">
    var __cs = __cs || [];
    __cs.push(["setCsAccount", "tIglgOHyX9gq1d9_qQLZvUp27s4RsGDW"]);
</script>
<script type="text/javascript" async src="//app.comagic.ru/static/cs.min.js"></script>

<!-- Rating@Mail.ru counter -->
<script type="text/javascript">
var _tmr = window._tmr || (window._tmr = []);
_tmr.push({id: "3125273", type: "pageView", start: (new Date()).getTime(), pid: "USER_ID"});
(function (d, w, id) {
  if (d.getElementById(id)) return;
  var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true; ts.id = id;
  ts.src = "https://top-fwz1.mail.ru/js/code.js";
  var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
  if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
})(document, window, "topmailru-code");
</script><noscript><div>
<img src="https://top-fwz1.mail.ru/counter?id=3125273;js=na" style="border:0;position:absolute;left:-9999px;" alt="Top.Mail.Ru" />
</div></noscript>
<!-- //Rating@Mail.ru counter -->

<!-- Rating@Mail.ru counter dynamic remarketing appendix -->
<script type="text/javascript">
var _tmr = _tmr || [];
_tmr.push({
    type: 'itemView',
    productid: 'VALUE',
    pagetype: 'VALUE',
    list: 'VALUE',
    totalvalue: 'VALUE'
});
</script>
<!-- // Rating@Mail.ru counter dynamic remarketing appendix -->

</body>
</html>
<?
// проставляем по необходимости город
foreach (['keywords', 'description', 'title'] as $prop) {
    $value = $APPLICATION->GetProperty($prop);
	$APPLICATION->SetPageProperty($prop, str_replace(['[город]', '{{city}}'], $GLOBALS['CITY_NAME'], $value));
}
