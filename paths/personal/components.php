<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// echo '<script>var log_php_object = '.json_encode($arResult).';</script>';
?>


<script type="text/x-template" v-pre="" id="js-change-password-template">
	<div class="lk-change-password">
		<transition-group name="slide-fade--m">
			<div class="lkcp-msg" v-if="isActiveState === 'success'" key="s1">
				<p>Пароль успешно изменен</p>
			</div>
			<div class="lkcp-msg" v-if="isActiveState === 'fail'" key="f1">
				<p>{{ msg }}</p>
			</div>
			<div class="lkcp-link" v-if="(isActiveState === 'passive' || isActiveState === 'success')" :key="'p1'">
				<a href="#" class="is-link" @click.prevent="changeState('active')">Изменить пароль</a>
			</div>
			<div class="lkcp-link" v-if="(isActiveState === 'active' || isActiveState === 'fail')" :key="'a1'">
				<a href="#" class="is-link" @click.prevent="changeState('passive')">Отменить изменение пароля</a>
			</div>
			<form action="" v-if="(isActiveState === 'active' || isActiveState === 'fail')" :key="'a2'" @submit.prevent="submit" ref="form">
				<div class="input-element" :class="{ 'has-error' : errors.has('OLD_PASSWORD') }">
					<label>
						  <span class="form-item-label">
							Старый пароль
						  </span>
						<span class="form-item-error">Проверьте правильность ввода</span>
						<input placeholder="Поле ввода пароля" v-model="oldPas" v-validate="'required|min:6'" type="password" name="OLD_PASSWORD"/>
					</label>
				</div>
				<div class="input-element" :class="{ 'has-error' : errors.has('NEW_PASSWORD') }">
					<label>
					  <span class="form-item-label">
						Новый пароль
					  </span>
						<span class="form-item-error">Проверьте правильность ввода</span>
						<input placeholder="Поле ввода пароля" v-model="newPas"  v-validate="'required|min:6'" type="password" name="NEW_PASSWORD"/>
					</label>
				</div>
				<div class="input-element" :class="{ 'has-error' : errors.has('REPEAT_PASSWORD') }">
					<label>
						  <span class="form-item-label">
							Повторите новый пароль
						  </span>
						<span class="form-item-error">Проверьте правильность ввода</span>
						<input placeholder="Поле ввода пароля" v-model="repeatPas" v-validate="'required|min:6|confirmed:NEW_PASSWORD'" type="password" name="REPEAT_PASSWORD"/>
					</label>
				</div>
				<div class="lkcp-btn" :class="{ 'load' : isLoad }">
					<button class="button solid" :disabled="isLoad">Сохранить</button>
				</div>
			</form>
		</transition-group>
	</div>
</script>

<script type="text/x-template" v-pre="" id="js-pd-phone-template">
	<div class="lkpd-item">
		<transition-group name="slide-fade--m">
			<h2 key="phone1">Контактный телефон</h2>

			<div class="lkpd-confirm" v-if="showConfirmationMessage" key="phone2">
				{{ confirmationResultMessage }}
			</div>

			<div class="lkpd-confirm" v-if="!isPhoneConfirmed && !isConfirmationMode" key="phone3">
				Ваш телефон не подтвержден
			</div>
			<div class="lkpd-confirm" v-if="isPhoneConfirmed && !isConfirmationMode" key="phone4">
				Ваш телефон подтвержден
			</div>

			<div class="lkpd-data" v-if="!isChangePhoneMode" key="phone5">
				{{ phone }}
			</div>
			<form action="" v-if="isChangePhoneMode" ref="confirmationPhoneForm" key="phone6" class="lkpd-form">
				<div class="input-element" :class="{ 'has-error' : errors.has('phone') }">
					<label>
						<span class="form-item-error">Проверьте правильность ввода</span>
                        <the-mask mask="+7 (###) ###-##-##"
                                  :masked="true"
                                  name="phone"
                                  type="tel"
                                  v-model="newPhone"
                                  v-validate="{ required: true, regex: /^\+7\s\(([0-9]{3})\)\s([0-9]{3})-[0-9]{2}-[0-9]{2}$/ }"
                                  placeholder="Введите ваш номер телефона">
                        </the-mask>
					</label>
				</div>
			</form>
			<form action="" v-if="isConfirmationMode" ref="confirmForm" key="phone7"  class="lkpd-form">
				<p>{{ smsSendResultMessage }}</p>
				<div class="input-element" :class="{ 'has-error' : errors.has('CODE') }" >
					<label>
						<span class="form-item-error">Проверьте правильность ввода</span>
						<input type="text" name="CODE" v-model="code" placeholder="Введите код из смс" v-validate="'required'">
					</label>
				</div>
			</form>
			<div class="lkpd-action" v-if="!isChangePhoneMode && !isConfirmationMode" key="phone8">
				<a href="#" class="is-link" @click.prevent="changePhone">Изменить</a>
			</div>
			<div class="lkpd-action" v-if="isChangePhoneMode" key="phone9">
				<a href="#" class="is-link"  @click.prevent="savePhone">Сохранить</a>
			</div>
			<div class="lkpd-action" v-if="isChangePhoneMode" key="phone10">
				<a href="#" class="is-link"  @click.prevent="cancelSaving">Отменить</a>
			</div>
			<div class="lkpd-action" v-if="!isPhoneConfirmed && !isChangePhoneMode && !isConfirmationMode" key="phone11">
				<a href="#" class="is-link" @click.prevent="confirmPhone">Подтвердить</a>
			</div>
			<div class="lkpd-action mar-10" v-if="isConfirmationMode" key="phone13">
				<a href="#" class="is-link" @click.prevent="sendSmsAgain">Отправить смс еще раз</a>
			</div>
			<div class="lkpd-action" v-if="isConfirmationMode" key="phone12">
				<a href="#" class="button" @click.prevent="sendSms">Отправить код</a>
			</div>
		</transition-group>
	</div>
</script>

<script type="text/x-template" v-pre="" id="js-pd-email-template">
	<div class="lkpd-item">
		<transition-group name="slide-fade--m">
			<h2 key="email1">E-mail</h2>

			<div class="lkpd-confirm" v-if="showMsg" key="email2">
				{{ msg }}
			</div>

			<div class="lkpd-confirm" v-if="email && !confirm && !isConfirm && !isEdit" key="email3">
				Ваш e-mail не подтвержден
			</div>
			<div class="lkpd-confirm" v-if="email && confirm && !isConfirm && !isEdit" key="email4">
				Ваш e-mail подтвержден
			</div>

			<div class="lkpd-data" v-if="email && !isEdit" key="email5">
				{{ email }}
			</div>
			<form action="" v-if="isEdit" ref="form" key="email6">
				<div class="input-element" :class="{ 'has-error' : errors.has('email') }">
					<label>
						<span class="form-item-error">Проверьте правильность ввода</span>
						<input type="text" v-model="newEmail" name="email" v-validate="'required|email'">
					</label>
				</div>
			</form>

            <div class="lkpd-action" v-if="!email && !isEdit && !isConfirm" key="email7">
                <a href="#" class="is-link" @click.prevent="change">Добавить</a>
            </div>
			<div class="lkpd-action" v-if="email && !isEdit && !isConfirm" key="email7">
				<a href="#" class="is-link" @click.prevent="change">Изменить</a>
			</div>
			<div class="lkpd-action" v-if="isEdit && !isConfirm" key="email8">
				<a href="#" class="is-link"  @click.prevent="save">Сохранить</a>
			</div>
			<div class="lkpd-action" v-if="isEdit && !isConfirm" key="email9">
				<a href="#" class="is-link"  @click.prevent="unsave">Отменить</a>
			</div>

			<div class="lkpd-action" v-if="email && !confirm && !isConfirm && !isEdit" key="email10">
				<a href="#" class="is-link" @click.prevent="toConfirm">Подтвердить</a>
			</div>
			<div class="lkpd-action" v-if="isConfirm && !isEdit" key="email11">
				<a href="#" class="is-link" @click.prevent="sendEmail">Отправить письмо ещё раз</a>
			</div>
		</transition-group>
	</div>
</script>

