<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// echo '<script>var log_php_object = '.json_encode($arResult).';</script>';
GLOBAL $USER;

$code_not_correct = false;

if (!$USER->IsAuthorized()):
	// форма Регистрации
	if (isset($_REQUEST["register_submit_button"])) {
        $doubleEmail = false;
        if (!empty($_REQUEST["REGISTER"]["EMAIL"])) {
            $rsUsers = CUser::GetList(($by = "personal_country"), ($order = "desc"), Array("EMAIL" => $_REQUEST["REGISTER"]["EMAIL"]));
            if ($arUser = $rsUsers->Fetch()) {
                $doubleEmail = true;
                ?>
                <div id="modal-thanks" class="modal-self is-show">
                    <div class="thanks-modal">
                        <div class="sale-text">Произошла ошибка</div>
                        <h2>Пользователь с таким email уже существует</h2>
                    </div>
                </div>
                <?
            }
        }
        if (!$doubleEmail) {
            if (phone_confrim_code_check($_REQUEST["SMS_CODE"], $_REQUEST["REGISTER"]["PERSONAL_PHONE"]) == 1) {
                $API_1C = new Api_1C();
                $phone = $API_1C->PhoneConvert($_REQUEST["REGISTER"]["PERSONAL_PHONE"]);
                //$_REQUEST["REGISTER"]["LOGIN"] = $phone;
                $params = Array(
	                "PHONE" => $_REQUEST["REGISTER"]["PERSONAL_PHONE"]
                , "NAME" => $_REQUEST["REGISTER"]["LAST_NAME"] . " " . $_REQUEST["REGISTER"]["NAME"] . " " . $_REQUEST["REGISTER"]["SECOND_NAME"]
                , "BIRTHDAY" => date('Y-m-d', strtotime($_REQUEST["BIRTHDAY"]))
                , "EMAIL" => $_REQUEST["REGISTER"]["EMAIL"]
                );
	            if($user_info = $API_1C->GetInfoByPhone($phone)) {
		            $params["ID"] = $user_info;
	            }
		        $_REQUEST["REGISTER"]["LOGIN"] = $API_1C->Registration($params);
                if (is_null($_REQUEST["REGISTER"]["LOGIN"])):?>
                <div id="reviews_form" class="modal-self is-show">
                    <div class="thanks-modal">
                        <div class="sale-text">Произошла ошибка</div>
                        <div class="is-styled">
                            <?=$API_1C->GetError();?>
                        </div>
                    </div>
                </div>
                <?else:
                    $_REQUEST["REGISTER"]["CONFIRM_PASSWORD"] = $_REQUEST["REGISTER"]["PASSWORD"];
                    $_REQUEST["REGISTER"]["UF_REFERER"] = Ref::getRef();
                    $APPLICATION->IncludeComponent("bitrix:main.register", "register",
                        Array(
                            "USER_PROPERTY_NAME" => "",
                            "SEF_MODE" => "Y",
                            "SHOW_FIELDS" => Array("PERSONAL_PHONE", "EMAIL", "NAME", "LAST_NAME", "SECOND_NAME", 'PERSONAL_BIRTHDAY', 'UF_REFERER'),
                            "REQUIRED_FIELDS" => Array("PERSONAL_PHONE"),
                            "AUTH" => "Y",
                            "USE_BACKURL" => "Y",
                            "SUCCESS_PAGE" => "?REGISTER_SUCCESS=Y",
                            "SET_TITLE" => "N",
                            "USER_PROPERTY" => Array(),
                            "SEF_FOLDER" => "/",
                            "VARIABLE_ALIASES" => Array()
                        )
                    );

                endif;
            } else {
                $code_not_correct = true;
            }
        }
	}
	?>

	<div id="sign-in-modal" class="modal-content-inner modal-self<?if ($code_not_correct):?> is-show<?endif;?>">
		<div class="auth-modal is-styled">
			<form data-is="registration" method="post" action="" name="regform" enctype="multipart/form-data" novalidate>
				<div class="title-h1">
					<span>Регистрация</span>
				</div>
				<div class="input-element">
					<label>
						<span class="form-item-label">
							Фамилия <span class="required">*</span>
						</span>
						<span class="form-item-error">Проверьте правильность ввода</span>
						<input type="text" name="REGISTER[LAST_NAME]" value="<?=@$_REQUEST["REGISTER[LAST_NAME]"];?>" data-validation="text" />
					</label>
				</div>
				<div class="input-element">
					<label>
						<span class="form-item-label">
							Имя <span class="required">*</span>
						</span>
						<span class="form-item-error">Проверьте правильность ввода</span>
						<input type="text"  name="REGISTER[NAME]" value="<?=@$_REQUEST["REGISTER[NAME]"];?>" data-validation="text"/>
					</label>
				</div>
				<div class="input-element">
					<label>
						<span class="form-item-label">
							Отчество
						</span>
						<span class="form-item-error">Проверьте правильность ввода</span>
						<input type="text" name="REGISTER[SECOND_NAME]"  value="<?=@$_REQUEST["REGISTER[SECOND_NAME]"];?>"/>
					</label>
				</div>
				<div class="input-element is-complex js-registration-phone-cnt">
					<label>
						<span class="form-item-label">
							Телефон <span class="required">*</span><span class="clarification required">Ваш будущий логин</span>
						</span>
                        <div class="input-element__complex">
                            <input type="tel" name="REGISTER[PERSONAL_PHONE]" placeholder="+7 (   )   -  -  "  value="<?=@$_REQUEST["REGISTER[PERSONAL_PHONE]"];?>" data-validation="phone" class="phone-mask js-registration-phone" />
                            <span class="form-item-error">Проверьте правильность ввода</span>
                        </div>
						<span class="footnote">на&nbsp;указанный номер телефона будет выслано смс-сообщение с&nbsp;кодом для&nbsp;подтверждения регистрации</span>
					</label>
				</div>
				<div class="input-element">
					<label>
						<span class="form-item-label">
							Пароль <span class="required">*</span>
						</span>
						<span class="form-item-error">Проверьте правильность ввода</span>
						<input type="password" name="REGISTER[PASSWORD]"  value="<?=@$_REQUEST["REGISTER[PASSWORD]"];?>" data-validation="password"/>
					</label>
				</div>
				<div class="input-element">
					<label>
						<span class="form-item-label">
							E-mail
						</span>
						<span class="form-item-error">Проверьте правильность ввода</span>
						<input type="text" name="REGISTER[EMAIL]" value="<?=@$_REQUEST["REGISTER[EMAIL]"];?>" data-validation="emailOption"/>
					</label>
				</div>
				<div class="input-element" data-is="datepicker">
					<label>
						<span class="form-item-label">
						   День рождения
						</span>
						<span class="form-item-error">Проверьте правильность ввода</span>
						<div class="date-input">
							<input type="date"
                                   name="REGISTER[PERSONAL_BIRTHDAY]"
                                   value="<?=@$_REQUEST["REGISTER[PERSONAL_BIRTHDAY]"];?>"
                                   class="js-flatpickr-bday"
                                   data-validation="dateOption" />
						</div>
					</label>
				</div>

				<div class="paragraph">Для регистрации необходимо подтвердить свой телефон</div>
                <div class="registration-actions">
                    <button class="button solid js-send" type="button">Получить код</button>
                </div>
				<div class="input-element<?if ($code_not_correct) echo " has-error";?>">
					<label>
						<span class="form-item-label">
							Введите код из смс
						</span>
						<span class="form-item-error">Проверьте правильность ввода</span>
						<input type="text" name="SMS_CODE" data-validate="text" class="js-registration-code"/>
					</label>
				</div>
				<div class="registration__error-msg js-error-msg"></div>

                <div class="element-checkbox">
                    <label>
                        <input type="checkbox" name="condition" class="js-registration-condition">
                        <i></i>
                        <a href="/agreement" target="_blank">Согласие на обработку персональных данных</a>
                    </label>
                </div>
				<div class="registration-actions">
            		<input type="hidden" name="register_submit_button" value="Регистрация">
					<button class="button js-submit" type="submit" disabled>Отправить</button>
				</div>
			</form>
		</div>
	</div>
		
	

	<?
	// форма Авторизация
	$auth_error = false;
	if (isset($_REQUEST["Login"])) {
		GLOBAL $API_1C;
        $API_1C = new Api_1C();
		$login = $API_1C->Login(Array("PHONE" => $_REQUEST["USER_PHONE"]));
		if (is_null($login)) {
			$auth_error = true;
		} else {
		    $phone = $API_1C->PhoneConvert($_REQUEST["USER_PHONE"]);
			$arAuthResult = $USER->Login($phone, $_REQUEST["USER_PASSWORD"], "Y");
			$APPLICATION->arAuthResult = $arAuthResult;
			if ($arAuthResult === true) {
				LocalRedirect($APPLICATION->GetCurPage(false));
			} else {
				$auth_error = true;
			}
		}
	}
	?>
	<div id="login-in-modal" class="modal-content-inner<?if ($auth_error) echo " modal-self is-show";?>" >
		<div class="auth-modal is-styled">
			<form data-is="formValidation" name="form_auth" method="post" novalidate data-ya-target="vhod_kabinet">
				<div class="title-h1">
					<span>Авторизация</span>
				</div>
                <? if($_REQUEST['success_change_password'] == 'yes'): ?>
                    <div class="anotation">
                        <p>Теперь вы можете войти используя новый пароль</p>
                    </div>
                <? endif ?>
				<div class="input-element">
					<label>
						<span class="form-item-label">
							Телефон <span class="required">*</span>
						</span>
                        <div class="input-element__complex">
                            <input type="tel" data-validation="phone" placeholder="+7 (   )   -  -  " class="phone-mask" name="USER_PHONE" value="<?=@$_POST["USER_PHONE"];?>" />
                            <span class="form-item-error">Проверьте правильность ввода</span>
                        </div>
					</label>
				</div>
				<div class="input-element<?if ($auth_error) echo " has-error";?>">
					<label>
						<span class="form-item-label">
							Пароль <span class="required">*</span>
						</span>
                        <div class="input-element__complex">
                            <input type="password" name="USER_PASSWORD" data-validation="password" />
                            <span class="form-item-error">Сочетание телефон/пароль - неверно</span>
                        </div>
					</label>
				</div>
				<div class="sign-in__btn mar-50">
					<input type="hidden" name="Login" value="Войти">
					<button class="button" type="submit">Войти</button>
				</div>
                <div class="cntr">
                    <a class="is-link" href="/personal/?forgot_password=yes">Забыли пароль?</a>
                </div>
			</form>
		</div>
	</div>
<?endif;?>
