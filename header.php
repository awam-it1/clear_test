<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
IncludeTemplateLangFile(__FILE__);
if (!empty($_GET['ref'])) $_SESSION['ref'] = (int)$_GET['ref'];
use Tairai\DependencyInjection\Container;

?><!DOCTYPE html>
<![if IE]>
<html class="ie">
<![endif]>
<![if !IE]>
<html>
<![endif]>
<head>
    <meta charset="utf-8">
    <title><?$APPLICATION->ShowTitle()?></title>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <?$APPLICATION->ShowHead()?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" id="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="cmsmagazine" content="9fd15f69c95385763dcf768ea3b67e22">
    <script>
        var appConfig = {
                'mobileVersion':false,
                'desktopVersion':true,
                'startupMessage':{
                    'title':false,
                    'message':false
                }
            },
            google = {};

        /* viewpoint*/
        (function(){var a,b,c,d,e,f,g,h,i,j;a=window.device,window.device={},c=window.document.documentElement,j=window.navigator.userAgent.toLowerCase(),device.ios=function(){return device.iphone()||device.ipod()||device.ipad()},device.iphone=function(){return d("iphone")},device.ipod=function(){return d("ipod")},device.ipad=function(){return d("ipad")},device.android=function(){return d("android")},device.androidPhone=function(){return device.android()&&d("mobile")},device.androidTablet=function(){return device.android()&&!d("mobile")},device.blackberry=function(){return d("blackberry")||d("bb10")||d("rim")},device.blackberryPhone=function(){return device.blackberry()&&!d("tablet")},device.blackberryTablet=function(){return device.blackberry()&&d("tablet")},device.windows=function(){return d("windows")},device.windowsPhone=function(){return device.windows()&&d("phone")},device.windowsTablet=function(){return device.windows()&&d("touch")&&!device.windowsPhone()},device.fxos=function(){return(d("(mobile;")||d("(tablet;"))&&d("; rv:")},device.fxosPhone=function(){return device.fxos()&&d("mobile")},device.fxosTablet=function(){return device.fxos()&&d("tablet")},device.meego=function(){return d("meego")},device.cordova=function(){return window.cordova&&"file:"===location.protocol},device.nodeWebkit=function(){return"object"==typeof window.process},device.mobile=function(){return device.androidPhone()||device.iphone()||device.ipod()||device.windowsPhone()||device.blackberryPhone()||device.fxosPhone()||device.meego()},device.tablet=function(){return device.ipad()||device.androidTablet()||device.blackberryTablet()||device.windowsTablet()||device.fxosTablet()},device.desktop=function(){return!device.tablet()&&!device.mobile()},device.portrait=function(){return window.innerHeight/window.innerWidth>1},device.landscape=function(){return window.innerHeight/window.innerWidth<1},device.noConflict=function(){return window.device=a,this},d=function(a){return-1!==j.indexOf(a)},f=function(a){var b;return b=new RegExp(a,"i"),c.className.match(b)},b=function(a){return f(a)?void 0:c.className+=" "+a},h=function(a){return f(a)?c.className=c.className.replace(a,""):void 0},device.ios()?device.ipad()?b("ios ipad tablet"):device.iphone()?b("ios iphone mobile"):device.ipod()&&b("ios ipod mobile"):b(device.android()?device.androidTablet()?"android tablet":"android mobile":device.blackberry()?device.blackberryTablet()?"blackberry tablet":"blackberry mobile":device.windows()?device.windowsTablet()?"windows tablet":device.windowsPhone()?"windows mobile":"desktop":device.fxos()?device.fxosTablet()?"fxos tablet":"fxos mobile":device.meego()?"meego mobile":device.nodeWebkit()?"node-webkit":"desktop"),device.cordova()&&b("cordova"),e=function(){return device.landscape()?(h("portrait"),b("landscape")):(h("landscape"),b("portrait"))},i="onorientationchange"in window,g=i?"orientationchange":"resize",window.addEventListener?window.addEventListener(g,e,!1):window.attachEvent?window.attachEvent(g,e):window[g]=e,e()}).call(this);
        (function (a ,d, vp) {
            var w = screen.width,
                h = screen.height;
            if (w < a || h < a || device.mobile()) {
                if (w > a) document.getElementById(vp).setAttribute('content', 'width=' + a);
                appConfig.mobileVersion = true;
            }
            else document.getElementById(vp).setAttribute('content','width=' + d);
            appConfig.desktopVersion = !appConfig.mobileVersion;
        })(740, 1200, 'viewport');
    </script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-833723846"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'AW-833723846');
    </script>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.2.1/dist/jquery.min.js" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/g/swiper@3.4.2" defer></script>
	<script src="https://cdn.jsdelivr.net/npm/jquery.maskedinput@1.4.1/src/jquery.maskedinput.js" type="text/javascript"></script>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400&amp;subset=cyrillic" rel="stylesheet">
    <?php
        $cssAssets = Container::getInstance()->getAssetManager()->getCssAssets();
        foreach ($cssAssets as $cssAsset):
    ?>
        <link rel="stylesheet" href="<?= $cssAsset ?>" />
    <?php
        endforeach;
    ?>
	<style>
	html .s_filter label{
		margin-right:10px;
	}
	html .s_filter input[type="checkbox"]{
	    display: none;	
	}
	</style>
</head>
<!--<div class="line" style="position: fixed; z-index: 999; width: 2px; height: 100vh; top: 0; left: calc(50% - 1px); background-color: red;"></div>-->
<body id="body" class="page-<?=$page?>" data-page="<?=$page?>">
<? if ($USER->IsAdmin()) { ?>
    <? $APPLICATION->ShowPanel() ?>
<? } ?>
<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '2041260419256978');
    fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=2041260419256978&ev=PageView&noscript=1"></noscript>
<!-- End Facebook Pixel Code -->
<?/*
<!-- Rating@Mail.ru counter -->
<script type="text/javascript">
    var _tmr = window._tmr || (window._tmr = []);
    _tmr.push({id: "3039765", type: "pageView", start: (new Date()).getTime(), pid: "USER_ID"});
    (function (d, w, id) {
        if (d.getElementById(id)) return;
        var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true; ts.id = id;
        ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
        var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
        if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
    })(document, window, "topmailru-code");
</script>
<noscript><div><img src="//top-fwz1.mail.ru/counter?id=3039765;js=na" style="border:0;position:absolute;left:-9999px;" alt=""></div></noscript>
<!-- //Rating@Mail.ru counter -->

<!-- Rating@Mail.ru counter dynamic remarketing appendix -->
<script type="text/javascript">
    var _tmr = _tmr || [];
    _tmr.push({
        type: 'itemView',
        productid: 'VALUE',
        pagetype: 'VALUE',
        list: 'VALUE',
        totalvalue: 'VALUE'
    });
</script>
<!-- // Rating@Mail.ru counter dynamic remarketing appendix -->
<?$APPLICATION->ShowPanel();?>

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(38992940, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
    });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/38992940" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
*/?>
<div class="main">
    <div class="global-content">
        <?$APPLICATION->IncludeComponent('bitrix:news.list', 'top_panel', [
            'IBLOCK_TYPE' => IBLOCK_TYPE_home,
            'IBLOCK_ID' => 31,
            'NEWS_COUNT' => '',
            'SORT_BY1' => 'SORT',
            'SORT_ORDER1' => 'ASC',
            'SORT_BY2' => '',
            'SORT_ORDER2' => '',
            'FILTER_NAME' => '',
            'FIELD_CODE' => ['*'],
            'PROPERTY_CODE' => ['FIRST', 'SECOND', 'THIRD'],
            'CHECK_DATES' => 'Y',
            'DETAIL_URL' => '',
            'AJAX_MODE' => 'N',
            'AJAX_OPTION_JUMP' => 'N',
            'AJAX_OPTION_STYLE' => 'Y',
            'AJAX_OPTION_HISTORY' => 'N',
            'CACHE_TYPE' => 'N',
            'CACHE_TIME' => '36000000',
            'CACHE_FILTER' => 'N',
            'CACHE_GROUPS' => 'Y',
            'PREVIEW_TRUNCATE_LEN' => '',
            'ACTIVE_DATE_FORMAT' => 'd F Y',
            'SET_STATUS_404' => 'N',
            'SET_TITLE' => 'N',
            'INCLUDE_IBLOCK_INTO_CHAIN' => 'N',
            'ADD_SECTIONS_CHAIN' => 'N',
            'HIDE_LINK_WHEN_NO_DETAIL' => 'N',
            'PARENT_SECTION' => '',
            'PARENT_SECTION_CODE' => '',
            'INCLUDE_SUBSECTIONS' => 'Y',
            'DISPLAY_DATE' => 'Y',
            'DISPLAY_NAME' => 'Y',
            'DISPLAY_PICTURE' => 'Y',
            'DISPLAY_PREVIEW_TEXT' => 'Y',
            'PAGER_TEMPLATE' => 'pagination',
            'DISPLAY_TOP_PAGER' => 'N',
            'DISPLAY_BOTTOM_PAGER' => 'N',
            'PAGER_TITLE' => '',
            'PAGER_SHOW_ALWAYS' => 'N',
            'PAGER_DESC_NUMBERING' => 'N',
            'PAGER_DESC_NUMBERING_CACHE_TIME' => '36000',
            'PAGER_SHOW_ALL' => 'Y',
        ]);?>
        <div class="global-header">
            <div class="inner-wrapper">
                <section class="tool-bar">
                    <?$APPLICATION->IncludeComponent('bitrix:menu', 'top_menu', [
                        'ROOT_MENU_TYPE' => 'top',
                        'MAX_LEVEL' => '1',
                        'CHILD_MENU_TYPE' => '',
                        'USE_EXT' => 'N',
                        'DELAY' => 'N',
                        'ALLOW_MULTI_SELECT' => 'N',
                        'MENU_CACHE_TYPE' => 'A',
                        'MENU_CACHE_TIME' => '3600',
                        'MENU_CACHE_USE_GROUPS' => 'N',
                        'MENU_CACHE_GET_VARS' => [],
                    ]);?>
                    <div class="tb-right-area">
                        <?//Проверим есть ли города для которых указан свой телефон, и подключим область с ним
                        $phoneFilePath = '/local/templates/tairai/paths/'.$CITY_CODE.'_phone.php';
                        if (!in_array($CITY_CODE, getCityPhoneIgnoreComagic()) || !file_exists($_SERVER['DOCUMENT_ROOT'].$phoneFilePath)) {
                            $phoneFilePath = '/local/templates/tairai/paths/phone.php';
                        }
                        $APPLICATION->IncludeComponent('bitrix:main.include', 'phone_top', [
                            'AREA_FILE_SHOW' => 'file',
                            'PATH' => $phoneFilePath,
                        ]);?>
                        <?$APPLICATION->IncludeComponent('bitrix:city.select', 'header', [
                            'IBLOCK_ID' => CITIES_IBLOCK,
                        ]);?>
                        <?/*<div class="tra-auth mobile-hidden" data-is="auth.js">
                            <span class="tb-inner-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 13 13"><path fill="#d4be6f" d="M0 6.5a6.5 6.5 0 1 1 13 0 6.5 6.5 0 0 1-13 0z"/><path fill="#3c0036" d="M8.11 4.54C8.14 3.67 7.41 3 6.56 3 5.71 3 4.95 3.69 5 4.54c.02.18.18 1.03.18 1.03.13.74.62 1.36 1.38 1.36.75 0 1.24-.62 1.37-1.38l.18-1.01zM10.53 9.62H3l.18-1.34c.08-.45.47-.71.92-.81L6.76 7l2.67.47c.44.1.83.36.9.81z"/><path fill="#d7c091" d="M0 6.5a6.5 6.5 0 1 1 13 0 6.5 6.5 0 0 1-13 0z"/><path fill="#3c0036" d="M8.11 4.54C8.14 3.67 7.41 3 6.56 3 5.71 3 4.95 3.69 5 4.54c.02.18.18 1.03.18 1.03.13.74.62 1.36 1.38 1.36.75 0 1.24-.62 1.37-1.38l.18-1.01zM10.53 9.62H3l.18-1.34c.08-.45.47-.71.92-.81L6.76 7l2.67.47c.44.1.83.36.9.81z"/></svg>
                            </span>
                            
                            <?if ($GLOBALS['USER']->IsAuthorized()) :?>
                                <a href="/personal/"><?=$GLOBALS['USER']->GetFullName();?></a><span>/</span><a href="/logout/">Выход</a>
                            <?else :?>
                                <a href="#login-in-modal" data-modal-open>Вход</a><span>/</span><a href="#sign-in-modal" data-modal-open>Регистрация</a>
                            <?endif;?>
                            
                        </div>*/?>
                    </div>
                    <div class="mobile-visible" data-is="mobileMenu">
                        <a href="#" class="mobile-menu-btn js-mobile-menu-open">
                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="12" viewBox="0 0 18 12">
                                <defs>
                                    <linearGradient id="mobile-menu-btn__a" x1="291" x2="291" y1="22" y2="33.87" gradientUnits="userSpaceOnUse">
                                        <stop offset="0" stop-color="#ffedd9"/>
                                        <stop offset="1" stop-color="#c7b179"/>
                                    </linearGradient>
                                </defs>
                                <path fill="url(#mobile-menu-btn__a)" d="M283 22h16a1 1 0 0 1 0 2h-16a1 1 0 0 1 0-2zm0 5h16a1 1 0 0 1 0 2h-16a1 1 0 0 1 0-2zm0 5h16a1 1 0 0 1 0 2h-16a1 1 0 0 1 0-2z" transform="translate(-282 -22)"/>
                            </svg>
                        </a>
                    </div>
                </section>
                <section class="main-bar">
                    <div class="mb-left">
                        <a<?=$APPLICATION->GetCurPage(false) != '/' ? ' href="/"' : ''?> class="site-logo">
                            <img src="/dist/img/site-logo.png" srcset="/dist/img/site-logo.png 1x, /dist/img/site-logo@2x.png 2x, /dist/img/site-logo@3x.png 3x">
                        </a>
                        <nav class="main-menu mobile-hidden">
                            <?$APPLICATION->IncludeComponent('bitrix:menu', 'top_menu_2', [
                                'ROOT_MENU_TYPE' => 'top_2',
                                'MAX_LEVEL' => '1',
                                'CHILD_MENU_TYPE' => '',
                                'USE_EXT' => 'N',
                                'DELAY' => 'N',
                                'ALLOW_MULTI_SELECT' => 'N',
                                'MENU_CACHE_TYPE' => 'A',
                                'MENU_CACHE_TIME' => '3600',
                                'MENU_CACHE_USE_GROUPS' => 'N',
                                'MENU_CACHE_GET_VARS' => [],
                            ]);?>
                        </nav>
                    </div>
                    <div class="mb-right">
                        <a href="/search/" class="mb-search mobile-hidden">
                            <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 16 16"><path d="M15.7,14.3l-3.105-3.105C13.473,10.024,14,8.576,14,7c0-3.866-3.134-7-7-7S0,3.134,0,7s3.134,7,7,7 c1.576,0,3.024-0.527,4.194-1.405L14.3,15.7c0.184,0.184,0.38,0.3,0.7,0.3c0.553,0,1-0.447,1-1C16,14.781,15.946,14.546,15.7,14.3z M2,7c0-2.762,2.238-5,5-5s5,2.238,5,5s-2.238,5-5,5S2,9.762,2,7z"/></svg>
                        </a>
                        <div class="mb-button-group">
                            <a href="/online-recording/" class="button">Записаться</a>
                            <a href="/kupit-sertifikat/" class="button">Купить сертификат</a>
                        </div>
                    </div>
                </section>
            </div>
        </div>

        <div class="mobile-menu js-mobile-menu">
            <div class="mobile-menu-inner">
                <div class="mobile-menu__ctrl">
                    <a href="/search/" class="mobile-menu__search">
                        <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 16 16"><path fill="#fff" d="M15.7,14.3l-3.105-3.105C13.473,10.024,14,8.576,14,7c0-3.866-3.134-7-7-7S0,3.134,0,7s3.134,7,7,7 c1.576,0,3.024-0.527,4.194-1.405L14.3,15.7c0.184,0.184,0.38,0.3,0.7,0.3c0.553,0,1-0.447,1-1C16,14.781,15.946,14.546,15.7,14.3z M2,7c0-2.762,2.238-5,5-5s5,2.238,5,5s-2.238,5-5,5S2,9.762,2,7z"/></svg>
                    </a>
                    <a href="#" class="mobile-menu__close js-mobile-menu-close">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><defs><linearGradient id="a" x1="294" x2="294" y1="19" y2="32.84" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#ffedd9"/><stop offset="1" stop-color="#c7b179"/></linearGradient></defs><path fill="url(#a)" d="M286.62 29.2c-.73.73-.83 1.81-.23 2.41.6.6 1.68.5 2.41-.23l4.2-4.2 4.2 4.2c.73.73 1.81.83 2.41.23.6-.6.5-1.68-.23-2.41l-4.2-4.2 4.2-4.2c.73-.73.83-1.81.23-2.41-.6-.6-1.68-.5-2.41.23l-4.2 4.2-4.2-4.2c-.73-.73-1.81-.83-2.41-.23-.6.6-.5 1.68.23 2.41l4.2 4.2z" transform="translate(-285 -17)"/></svg>
                    </a>
                </div>
                <?$APPLICATION->IncludeComponent('bitrix:city.select', 'mobile-header', [
                    'IBLOCK_ID' => CITIES_IBLOCK,
                ]);?>
                <div class="mobile-menu__item">
                    <nav class="mobile-menu__links">
                        <?$APPLICATION->IncludeComponent('bitrix:menu', 'top_menu_2', [
                            'ROOT_MENU_TYPE' => 'top_2',
                            'MAX_LEVEL' => '1',
                            'CHILD_MENU_TYPE' => '',
                            'USE_EXT' => 'N',
                            'DELAY' => 'N',
                            'ALLOW_MULTI_SELECT' => 'N',
                            'MENU_CACHE_TYPE' => 'A',
                            'MENU_CACHE_TIME' => '3600',
                            'MENU_CACHE_USE_GROUPS' => 'N',
                            'MENU_CACHE_GET_VARS' => [],
                        ]);?>
                    </nav>
                </div>
                <div class="mobile-menu__item">
                    <nav class="mobile-menu__links is-small">
                        <?$APPLICATION->IncludeComponent('bitrix:menu', 'top_menu_2', [
                            'ROOT_MENU_TYPE' => 'top',
                            'MAX_LEVEL' => '1',
                            'CHILD_MENU_TYPE' => '',
                            'USE_EXT' => 'N',
                            'DELAY' => 'N',
                            'ALLOW_MULTI_SELECT' => 'N',
                            'MENU_CACHE_TYPE' => 'A',
                            'MENU_CACHE_TIME' => '3600',
                            'MENU_CACHE_USE_GROUPS' => 'N',
                            'MENU_CACHE_GET_VARS' => [],
                        ]);?>
                    </nav>
                </div>
                <?$APPLICATION->IncludeComponent('bitrix:main.include', 'phone_top_mobile', [
                    'AREA_FILE_SHOW' => 'file',
                    'PATH' => '/local/templates/tairai/paths/phone.php',
                ]);?>
                <?/*<div class="mobile-menu__item">
                    <div class="tra-auth" data-is="auth.js">
                        <span class="tb-inner-icon">
                           <svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 13 13">
                               <path fill="#d4be6f" d="M0 6.5a6.5 6.5 0 1 1 13 0 6.5 6.5 0 0 1-13 0z"/>
                               <path fill="#3c0036" d="M8.11 4.54C8.14 3.67 7.41 3 6.56 3 5.71 3 4.95 3.69 5 4.54c.02.18.18 1.03.18 1.03.13.74.62 1.36 1.38 1.36.75 0 1.24-.62 1.37-1.38l.18-1.01zM10.53 9.62H3l.18-1.34c.08-.45.47-.71.92-.81L6.76 7l2.67.47c.44.1.83.36.9.81z"/>
                               <path fill="#d7c091" d="M0 6.5a6.5 6.5 0 1 1 13 0 6.5 6.5 0 0 1-13 0z"/>
                               <path fill="#3c0036" d="M8.11 4.54C8.14 3.67 7.41 3 6.56 3 5.71 3 4.95 3.69 5 4.54c.02.18.18 1.03.18 1.03.13.74.62 1.36 1.38 1.36.75 0 1.24-.62 1.37-1.38l.18-1.01zM10.53 9.62H3l.18-1.34c.08-.45.47-.71.92-.81L6.76 7l2.67.47c.44.1.83.36.9.81z"/>
                           </svg>
                        </span>
                            
                        <?if ($GLOBALS['USER']->IsAuthorized()) :?>
                            <a href="/personal/"><?=$GLOBALS['USER']->GetFullName();?></a><span>/</span><a href="/logout/">Выход</a>
                        <?else :?>
                            <a href="#login-in-modal" data-modal-open>Вход</a><span>/</span><a href="#sign-in-modal" data-modal-open>Регистрация</a>
                        <?endif;?>
                    </div>
                </div>*/?>
            </div>
        </div>
        <?$APPLICATION->IncludeFile('include/auth.php');?>
