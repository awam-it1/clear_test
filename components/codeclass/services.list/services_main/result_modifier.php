<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$arResult['JS_DATA'] = [
    'sections' => array_values(array_map(function ($item) use ($arResult) {
        $url = CIBlock::ReplaceDetailUrl(
            $arResult['IBLOCK']['DETAIL_PAGE_URL'],
            ['SECTION_CODE' => $item['CODE']],
            true, false
        );
        return [
            'id' => $item['ID'],
            'name' => trim(str_replace('для женщин и мужчин', '', $item['NAME'])),
            'code' => $item['CODE'],
            'sort' => $item['SORT'],
            'url' => $url,
            'active' => $item['SELECTED'] == 'Y',
        ];
    }, $arResult['SECTIONS'])),
    'categories' => array_values(array_map(function ($item) {
        return [
            'id' => $item['ID'],
            'name' => $item['NAME'],
            'count' => $item['COUNT'],
        ];
    }, $arResult['CATEGORIES'])),
    'services' => array_values(array_map(function ($item) use ($arResult) {
        $img = '/local/include/img/stub.jpg';
        if (!empty($item['UF_PICTURES'][0])) {
            $img = CFile::ResizeImageGet($item['UF_PICTURES'][0], ['width' => 335, 'height' => 240])['src'];
        }
        $url = CIBlock::ReplaceDetailUrl(
            $arResult['IBLOCK']['DETAIL_PAGE_URL'],
            ['SECTION_CODE' => $item['CODE']],
            true, false
        );
        return [
            'id' => $item['ID'],
            'uid' => $item['UF_ID1C'],
            'section' => $item['TOP_SECTION_ID'],
            'categories' => array_keys($item['UF_CATEGORIES']),
            'img' => $img,
            'link' => $url,
            'sheduleLink' => '/online-recording/?s0=' . $item['ID'],
            'priceText' => ($item['CITY_PRICE'] ?: 'от ' . $item['MIN_CITY_PRICE']) . ' руб.',
            'price' => $item['MIN_CITY_PRICE'],
            'name' => $item['NAME'],
            'short' => $item['UF_SHORT'],
            'sort' => $item['SORT'] ?: 500,
        ];
    }, $arResult['SERVICES'])),
];

array_unshift($arResult['JS_DATA']['categories'], [
    'id' => 0,
    'name' => 'Все',
    'count' => 999,
]);

$section = array_values(array_filter($arResult['JS_DATA']['sections'], function($e){
    return $e['active'] == 'Y';
}));
$arResult['HEADER_H1'] = $section[0]['name'] ?? 'Услуги';
