<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>
<script>
    window.TAIRAI = window.TAIRAI || {} ;
    window.TAIRAI.pageId = '<?=ccHelpers::city('ID')?>-city-services-list';
    window.TAIRAI.servicesData = <?=json_encode($arResult['JS_DATA'], JSON_UNESCAPED_UNICODE)?>;
</script>

<div class="content is-styled">
    <?$APPLICATION->IncludeComponent('bitrix:breadcrumb', 'breadcrumb', [
        'START_FROM' => 0,
        'PATH' => '',
        'SITE_ID' => 's1',
    ], $component);?>

    <div id="js-vue-app-page">
        <div class="inner-wrapper page-services-city" v-cloak>
            <h1><?=htmlspecialchars($arResult['HEADER_H1'])?> в <?=getNewFormText($_SESSION['CURRENT_CITY_NAME']);?></h1>
            <div class="sections">
                <div v-for="item in sections" class="item">
                    <a :href="item.url" :class="{active: item.active}">{{ item.name }}</a>
                </div>
            </div>
            <?if (!empty($arResult['SECTION'])):?>
            <div class="categories">
                <label v-for="item in categories" :class="{active: item.id == activeCategory}" class="item">
                    <input type="radio" name="categoryItem" v-model="activeCategory" :value="item.id">
                    {{ item.name }}
                </label>
            </div>
            <?endif?>
            <div class="shop__inner">
                <section class="shop__section">
                    <div class="shop__section-inner">
                        <div class="shop__goods is-full">
                            <div class="shop__goods-inner">
                                <div v-for="item in showServices" class="shop__good">
                                    <div class="shop__good-card inews-item">
                                        <div class="inews-image" :style="'background-image: url(' + item.img + ')'">
                                            <div class="inews-date">
                                                {{ item.priceText }}
                                            </div>
                                        </div>
                                        <div class="shop__good-info">
                                            <a :href="item.link" class="shop__good-name">{{ item.name }}</a>
                                            <div class="inews-text" v-html="item.short"></div>
                                        </div>
                                    </div>
                                    <div class="shop__good-actions is-right">
                                        <a :href="item.sheduleLink" class="is-link">Записаться</a>
                                    </div>
                                </div>
                            </div>
                            <div class="shop__goods-more" v-if="showBtnMore">
                                <button class="button" @click.prevent="showMore">Показать еще</button>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>

<?if (empty($arResult['SECTION'])):?>
<div class="inner-wrapper min-width">
    <div class="is-styled mar-50">
        <p><span style="font-weight: 400;">Каждая СПА-программа салона ТАЙРАЙ &mdash;</span> <span style="font-weight: 400;">это несколько часов блаженства, умиротворения и роскошного ухода. Мы соединили тайские традиции релакса с европейскими стандартами уходовых процедур, создав более 10 эффективных, насыщенных яркими ощущениями и позитивными эмоциями SPA-комплексов.</span></p>
        <p><span style="font-weight: 400;">Для салонов ТАЙРАЙ в Москве СПА-программы разработаны с учетом основных проблем жителей мегаполиса:</span></p>
        <ul>
            <li style="font-weight: 400;"><span style="font-weight: 400;">комплексные и </span><span style="font-weight: 400;">anti-age</span><span style="font-weight: 400;"> процедуры для лица;</span></li>
            <li style="font-weight: 400;"><span style="font-weight: 400;">моделирующие программы для коррекции объемов тела и устранения целлюлита;</span></li>
            <li style="font-weight: 400;"><span style="font-weight: 400;">релаксационные и тонизирующие СПА-сеансы для поддержания иммунитета, улучшения работоспособности и стрессоустойчивости;</span></li>
            <li style="font-weight: 400;"><span style="font-weight: 400;">общеукрепляющие массажные СПА с тайскими маслами и травами. </span></li>
        </ul>
        <p><strong>СПА-программы для женщин и мужчин</strong></p>
        <p><span style="font-weight: 400;">СПА-программы для мужчин и женщин учитывают нюансы обмена веществ, особенности мужской и женской физиологии. </span></p>
        <p><span style="font-weight: 400;">В СПА-программах для женщин делается акцент на интенсивный уход за уставшей и возрастной кожей, эстетическую и чувственную составляющие. Основные компоненты лучших СПА-программ для женщин &mdash;</span> <span style="font-weight: 400;">ароматный шоколад, нежнейшие тайские масла, душистые травы и экстракты. &nbsp;</span></p>
        <p><span style="font-weight: 400;">СПА для мужчин ориентированы на экспресс-восстановление энергетического баланса, тонизирование и восстанавливающий уход. В составе типично мужских комплексов главную роль играет стимулирующий спортивный или фут-массаж, бодрящие травяные ароматы и экстракты. </span></p>
        <p><span style="font-weight: 400;">СПА-программы в салоне ТАЙРАЙ различаются между собой по продолжительности и этапам. Мы предлагаем программы, включающие распаривание в кедровой бочке, очищение скрабом или пилинг, обертывание или маску и, конечно же, тонизирующие и релакс-массажи всего тела или отдельных зон.</span></p>
        <p><span style="font-weight: 400;">Какую бы программу вы ни выбрали, СПА в ТАЙРАЙ начинается с волшебства и не заканчивается с завершением сеанса. Дивные ароматы экзотической Азии, легкость тела и бодрость духа остаются с вами надолго!</span></p>
    </div>
</div>
<?endif?>
