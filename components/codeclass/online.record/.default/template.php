<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>
<script>
    window.TAIRAI = window.TAIRAI || {} ;
    window.TAIRAI.pageId = '<?=ccHelpers::city('ID')?>-online-record';
    window.TAIRAI.vueData = <?=json_encode($arResult)?>;
    window.TAIRAI.mapKey = '<?=ProjOptions::GMAP_KEY?>';
    window.TAIRAI.mapStyle = <?=ProjOptions::GMAP_STYLE?>;
    window.TAIRAI.mapCenter = <?=json_encode(ProjOptions::GMAP_CENTER)?>;

</script>
<style>
    #mapContainer {
        height: 60vh;
    }
</style>
<div class="content is-styled">
    <?$APPLICATION->IncludeComponent('bitrix:breadcrumb', 'breadcrumb', [
        'START_FROM' => 0,
        'PATH' => '',
        'SITE_ID' => 's1',
    ], $component);?>
    <div id="js-vue-app-page" v-cloak>
        <div class="all_records-wrap">
            <div class="all_record-steps">
                <div class="single_step single_step-01" :class="stepClass(1)">
                    <div class="single_step-header">
                        <div class="single_step-no" @click="toStepSalon">1</div>
                        <div class="single_step-name" v-if="step < 2">Выберите салон</div>
                        <div class="single_step-name" v-if="step > 1">
                            <div class="ok_name">Салон: </div>
                            <div class="ok_info" :class="{metro: currentSalon.subway}">
                                {{ currentSalon.name }}
                                <span v-if="currentSalon.subway">({{ currentSalon.address }})</span>
                            </div>
                        </div>
                        <div class="single_step-pipka" @click="toStepSalon"></div>
                    </div>
                    <div class="single_step-body" v-if="step === steps.SALON">
                        <div class="single_step-body--tabs-wrap">
                            <div class="single_step-body--tabs-tabs">
                                <div class="single_step-body--tab-tab" :class="{active: tabsSalons === 0}" @click.prevent="tabsSalons=0" data-tab="1">Список салонов</div>
                                <div class="single_step-body--tab-tab" :class="{active: tabsSalons === 1}" @click.prevent="tabsSalons=1" data-tab="2">Выбрать салон на карте</div>
                            </div>
                            <div class="violet-stripe"></div>
                            <div class="map_sniffer" v-if="canGetGeo">
                                <span class="map_sniffer-salon" @click.prevent="getGeo">Определить ближайший к вам салон</span>
                                <span class="map_sniffer-info">(браузер попросит у вас разрешение на определение геолокации)</span>
                            </div>
                            <div class="search__form">
                                <div class="input-group">
                                    <div class="input-search-addon">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 16 16" fill="currentColor">
                                            <path d="M15.7,14.3l-3.105-3.105C13.473,10.024,14,8.576,14,7c0-3.866-3.134-7-7-7S0,3.134,0,7s3.134,7,7,7 c1.576,0,3.024-0.527,4.194-1.405L14.3,15.7c0.184,0.184,0.38,0.3,0.7,0.3c0.553,0,1-0.447,1-1C16,14.781,15.946,14.546,15.7,14.3z M2,7c0-2.762,2.238-5,5-5s5,2.238,5,5s-2.238,5-5,5S2,9.762,2,7z"></path>
                                        </svg>
                                    </div>
                                    <input
                                        class="input__search"
                                        type="text"
                                        placeholder="Введите метро или город нахождения салона"
                                        :value="salonsFilter"
                                        @input="event => salonsFilter = event.target.value"
                                    />
                                </div>
                            </div> <?/*<div v-if="activeSalon === salon.id" @click="$refs.fileInput.click()" > {{ salon.id }}</div>*/?>
                            <div class="single_step-body--tabs-info">
                                <div class="single_step-body--tab-info" :class="{active: tabsSalons === 0}" data-tab="1">
                                    <div class="metro__check-wrapper">
                                        <div class="metro__check-single" v-for="salon in visibleSalons" :class="{no_metro: !salon.subway, metro: salon.subway, selected: salon.id === activeSalon}" @click.prevent="activeSalon = salon.id; toStepServices();">
                                            <div class="metro__check-single-name">
                                                {{ salon.name }} 
                                                <span v-if="salon.subway">({{ salon.address }})</span>
                                            </div>
                                            <div class="metro__check-single-length" v-if="salon.distance">{{ salon.distance }}</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="single_step-body--tab-info" :class="{active: tabsSalons === 1}" data-tab="2">
                                    <div class="record__map" id="mapContainer"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="single_step single_step-02" :class="stepClass(2)">
                    <div class="single_step-header">
                        <div class="single_step-no" @click="toStepServices">2</div>
                        <div class="single_step-name" v-if="step < steps.ROOM">Услуги</div>
                        <div class="single_step-name service__info" v-if="step > steps.SERVICE">
                            <div class="service__info-all">
                                <div class="ok_name">Услуги: </div>
                                <div class="ok_info" :class="{dual: isDual}">
                                    <div class="firstClient">
                                        <div class="caption">Первый клиент</div>
                                        <div class="service__info-single" v-for="item in listServices0">
                                            <div class="name">{{ item.name }}</div>
                                            <div class="time">{{ item.time }}</div>
                                            <div class="price">{{ item.price }} руб.</div>
                                        </div>
                                    </div>
                                    <div class="secondClient">
                                        <div class="caption">Второй клиент</div>
                                        <div class="service__info-single" v-for="item in listServices1">
                                            <div class="name">{{ item.name }}</div>
                                            <div class="time">{{ item.time }}</div>
                                            <div class="price">{{ item.price }} руб.</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="summ__info">
                                <div class="summ__info-name">Итог:</div>
                                <div class="summ__info-price">{{ servicesSumFull }} руб.</div>
                            </div>
                        </div>
                        <div class="single_step-pipka" @click="toStepServices"></div>
                    </div>
                    <div class="single_step-body" v-if="step === steps.SERVICE">
                        <div class="order__programms">
                            <div class="two__persons-wrapper">
                            <label class="two__persons-check">
                              <input type="checkbox" v-model="isDual">
                              <div><span></span></div>
                              <div class="two__persons-info">Идём вдвоём</div>
                            </label>
                          </div>
                            <div class="programm_filters">
                                <div class="filter filter-1">
                                    <div class="s_filter" v-for="(name, value) in services.sections">
                                        <label>{{ name }}
                                            <input type="checkbox" @change="onChangeActiveServeSection(value)" :checked="value === activeServSection">
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="filter filter-2" v-if="!!activeServSection">
                                    <label v-for="(name, value) in visibleCategories">
                                        <input type="radio" :value="value" v-model="activeServCategory">
                                        <div class="name">{{ name }}</div>
                                    </label>
                                </div>
                            </div>
                            <div class="two__persons_tabs" v-if="isDual">
                              <label>
                                <input type="radio" value="0" v-model="tabsPerson">
                                <div class="name">Первый клиент</div>
                              </label>
                              <label>
                                <input type="radio" value="1" v-model="tabsPerson">
                                <div class="name">Второй клиент</div>
                              </label>
                            </div>
                            <div class="violet-stripe-55"></div>
                            <div class="order__services-list">
                                <div class="order__services-single" v-for="item in visibleServices">
                                    <div class="order__services-single-left">
                                        <div class="order__services-img">
                                            <img :src="item.main_image.src">
                                        </div>
                                        <div class="order__services-ni">
                                            <div class="order__services-name">{{ item.name }}</div>
                                            <div class="order__services-info">{{ item.uf_short }}</div>
                                        </div>
                                    </div>
                                    <div class="order__services-right">
                                        <div class="order__services-long simple-select">
                                            <div class="services-select" v-if="item.items.length > 1">
                                                <select v-model="item.selected_item" class="services-select__select">
                                                    <option v-for="variant in item.items" :value="variant.id" <?/*:class="{checked: variant.id === duration.id}"*/?>>{{ variant.human_period }}</option>
                                                </select>
                                            </div>
											<?/**
											<select name="duration" @change="changeDuration" v-model="activeProcedure.activeDuration">
												<option value="-1" disabled>Выберите из списка</option>
												<option v-for="duration in procedureDurations"  :value="duration.id" v-html="duration.name"></option>
											</select>
											<?/**/?>
                                            <span class="single" v-else="">{{ item.items[0].human_period }}</span>
                                        </div>
                                        <div class="order__services-price">{{ selectedPrice(item) }} руб.</div>
                                        <div class="order__services-add" :class="{checked: selectedSet(item)}" @click.prevent="selectService(item)">Добавить</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="dop__service" v-if="hasLinkedServices()">
                            <div class="dop__service-header">Эту услугу прекрасно дополнят</div>
                            <div class="violet-stripe-2"></div>
                            <div class="order__services-list">

                                <div class="order__services-single" v-for="linkedItem in linkedServices">
                                    <div class="order__services-single-left">
                                        <div class="order__services-img">
                                            <img :src="linkedItem.main_image">
                                        </div>
                                        <div class="order__services-ni">
                                            <div class="order__services-name">{{ linkedItem.name }}</div>
                                            <div class="order__services-info">{{ linkedItem.uf_short }}</div>
                                        </div>
                                    </div>
                                    <div class="order__services-right">
                                        <div class="order__services-long simple-select">
                                            <div class="services-select" v-if="linkedItem.items.length > 1">
                                                <select v-model="linkedItem.selected_item" class="js-choice services-select__select">
                                                    <option v-for="variant in linkedItem.items" :value="variant.id">{{ variant.human_period }}</option>
                                                </select>
                                            </div>
                                            <span class="single" v-else="">{{ linkedItem.items[0].human_period }}</span>
                                        </div>
                                        <div class="order__services-price">{{ selectedPrice(linkedItem) }} руб.</div>
                                        <div class="order__services-add" :class="{checked: selectedSet(linkedItem)}" @click.prevent="selectService(linkedItem)">Добавить</div>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <div class="next__step-wrapper" v-if="readyStepServices()">
                            <div class="next-step next-datetime" @click="toStepDateTime">Далее</div>
                        </div>
                    </div>
                </div>

                <div class="single_step single_step-03a" :class="stepClass(3)" v-if="isDual">
                    <div class="single_step-header">
                        <div class="single_step-no" @click="toStepRoom">3</div>
                        <div class="single_step-name" v-if="step < steps.DATE_TIME">Выберите вид комнаты</div>
                        <div class="single_step-name" v-if="step > steps.ROOM">
                            <div class="ok_name">Вид комнаты: </div>
                            <div class="ok_info date-time"><span>{{ roomNames[activeRoom] }}</span></div>
                        </div>
                        <div class="single_step-pipka" @click="toStepRoom"></div>
                    </div>
                    <div class="single_step-body" v-if="step === steps.ROOM">
                        <div class="info-room-type">При выборе определенного вида комнаты некоторые даты и время будут недоступны</div>
                        <div class="room-types">
                            <label class="room-type" v-for="(name, index) in roomNames">
                                <input type="radio" name="roomtype" :value="index" v-model="activeRoom">
                                <div class="name" v-if="needOverPay(index)">
                                    <p class="room-name-line">{{ name }}</p>
                                    <p class="room-name-line">
                                        <span class="room-name-line--italic">+ 1000 руб/час</span>
                                        <span class="info-room-footnote">*</span>
                                    </p>
                                </div>
                                <div class="name" v-else="">{{ name }}</div>
                            </label>
                        </div>
                        <div class="info-room-remark" v-if="needRoomRemark()">
                            <span class="info-room-footnote">*</span>
                            {{ roomRemark }}
                        </div>
                        <div class="next__step-wrapper"><div class="next-step" v-if="readyStepRoom()" @click="toStepDateTime">Далее</div></div>
                    </div>
                </div>

                <div class="single_step single_step-03" :class="stepClass(4)">
                    <div class="single_step-header">
                        <div class="single_step-no" @click="toStepDateTime">{{ isDual ? 4 : 3 }}</div>
                        <div class="single_step-name" v-if="step < steps.PERSON">Дата и время записи</div>
                        <div class="single_step-name" v-if="step > steps.DATE_TIME">
                            <div class="ok_name">Дата и время записи: </div>
                            <div class="ok_info date-time"><span>{{ captionDayTime }}</span> <span>{{ captionMaster }}</span></div>
                        </div>
                        <div class="single_step-pipka" @click="toStepDateTime"></div>
                    </div>
                    <div class="single_step-body" v-if="step === steps.DATE_TIME">
                        <div class="choose__master" v-if="!tabsMasters && !isDual">
                            <div class="choose__master-link" @click.prevent="showMasters">Хочу выбрать мастера</div>
                        </div>
                        <div class="masters-block" v-if="tabsMasters && !isDual">
                            <div class="info-master">При выборе мастера некоторые даты и время будут недоступны</div>
                            <div class="masters-all">
                                <div
                                    class="masters-single"
                                    v-for="master in masters"
                                    :class="{checked: selectedMaster(master)}"
                                    @click.prevent="selectMaster(master)"
                                    v-html="master.name"
                                >
                                </div>
                            </div>
                        </div>
                        <div class="calendar-wrapper">
                            <div class="calendar-calendar">
                                <div class="calendar-calendar-header">
                                    <div class="calendar-prev--month" @click.prevent="prevMonth"></div>
                                    <div class="calendar-name">{{ calendarCaption }}</div>
                                    <div class="calendar-name-mobile">{{ calendarCaptionMobile }}</div>
                                    <div class="calendar-next--month" @click.prevent="nextMonth"></div>
                                </div>
                                <div class="calendar-tables-wrap">
                                    <div class="table-container">
                                        <table class="order_calendar">
                                            <thead>
                                                <tr>
                                                    <th>ПН</th><th>ВТ</th><th>СР</th><th>ЧТ</th><th>ПТ</th><th>СБ</th><th>ВС</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="week in visibleCalendar[0]">
                                                    <td v-for="day in week" :class="{disabled: !day.enable, selected: day.selected, today: day.today, holiday: day.holiday}" @click.prevent="selectDay(day)">
                                                        {{ day.date }}
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="table-container">
                                        <table class="order_calendar">
                                            <thead>
                                                <tr>
                                                    <th>ПН</th><th>ВТ</th><th>СР</th><th>ЧТ</th><th>ПТ</th><th>СБ</th><th>ВС</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr v-for="week in visibleCalendar[1]">
                                                    <td v-for="day in week" :class="{disabled: !day.enable, selected: day.selected, today: day.today, holiday: day.holiday}" @click.prevent="selectDay(day)">
                                                        {{ day.date }}
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="calendar-time">
                                <div class="calendar-time-header">Время записи</div>
                                <div class="calendar-time-times--wrap">
                                    <div class="calendar-time-times-single" v-for="cell in visibleTimes" :class="{unavailable: !cell.enable, active: cell.selected, 'lucky-time': cell.lucky}" @click.prevent="selectTime(cell)">
                                        {{ cell.caption }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="float-clear"></div>
                        <div class="lucky__hours-info">Акция "Счастливые часы" - скидка на утренние часы с 10 до 15 часов в будни</div>
                        <div class="next__step-wrapper"><div class="next-step" v-if="readyStepDateTime()" @click="toStepPerson">Далее</div></div>
                    </div>
                </div>

                <div class="single_step single_step-04" :class="stepClass(5)">
                    <div class="single_step-header">
                        <div class="single_step-no" @click="toStepPerson">{{ isDual ? 5 : 4 }}</div>
                        <div class="single_step-name" v-if="step < steps.PAYMENT">Персональные данные</div>
                        <div class="single_step-name" v-if="step > steps.PERSON">
                            <div class="ok_name">Персональные данные: </div>
                            <div class="ok_info payment">
                                {{ inputName }}, +7 {{ inputPhone }}
                            </div>
                        </div>
                        <div class="single_step-pipka" @click="toStepPerson"></div>
                    </div>
                    <div class="single_step-body" v-if="step === steps.PERSON">
                        <div class="order__form">
                            <form>
                                <div class="input-group">
                                    <input type="text" class="input__name" placeholder="Ваше имя" v-model="inputName">
                                </div>
                                <div class="input-group">
                                    <div class="input-group-addon"><div>+7</div></div>
                                    <input
                                        type="tel"
                                        class="input__phone"
                                        placeholder="(555) 555-5555"
                                        v-model="inputPhone"
                                        autocomplete="tel"
                                        maxlength="14"
                                        v-phone
                                        pattern="[(][0-9]{3}[)] [0-9]{3}-[0-9]{4}"
                                        required
                                    >
                                </div>
                                <button type="button" class="next-step" v-if="readyStepPerson()" @click="toStepPayment">Далее</button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="single_step single_step-05" :class="stepClass(6)">
                    <div class="single_step-header">
                        <div class="single_step-no" @click="toStepPayment">{{ isDual ? 6 : 5 }}</div>
                        <div class="single_step-name" v-if="step < steps.CONFIRMATION">Способ оплаты</div>
                        <div class="single_step-name" v-if="step > steps.PAYMENT">
                            <div class="ok_name">Способ оплаты: </div>
                            <div class="ok_info payment">
                                {{ payTypes[activePayment] }}
                                <span v-if="payNumTypes[activePayment] || false">№ {{ inputPaymentNum }}</span>
                                <div class="additional__info" v-if="activePayment === 4">Не забудьте взять карту с собой в салон</div>
                            </div>
                            <div class="summ__info">
                                <div class="summ__info-name">Итог:</div>
                                <div class="summ__info-price" :class="checkoutPriceClass()">{{ servicesSumFull }} руб.</div>
                            </div>
                            <div class="summ__info" v-if="discountValue > 0">
                                <div class="summ__info-name">Скидка:</div>
                                <div class="summ__info-price summ__info-price--discount">{{ discountValue }} руб.</div>
                            </div>
                            <div class="summ__info" v-if="discountValue > 0">
                                <div class="summ__info-name">Итог с учетом скидки:</div>
                                <div class="summ__info-price">{{ servicesSumDiscount }} руб.</div>
                            </div>
                        </div>
                        <div class="single_step-pipka" @click="toStepPayment"></div>
                    </div>
                    <div class="single_step-body" v-if="step === steps.PAYMENT">
                        <div class="order__payment-wrap">
                            <div class="order__payment-block abon">
                                <div class="order__payment-single" :class="{active: activePayment == 1}" @click.prevent="activePayment = 1; extraFee = null; lastErrorMessage = null;">
                                  <div class="order__payment-single--img">
                                    <img src="/upload/record/order__wallet.svg" alt="В салоне">
                                  </div>
                                  <div class="order__payment-single--info">В салоне (наличными/безналично)</div>
                                </div>
                                <div class="order__payment-single" :class="{active: activePayment == 2}" @click.prevent="activePayment = 2" v-if="bankCardAllow">
                                  <div class="order__payment-single--img">
                                    <img src="/upload/record/order__bank-card.svg" alt="Банковской картой онлайн">
                                  </div>
                                  <div class="order__payment-single--info">Банковской картой онлайн</div>
                                </div>
                                <div class="order__payment-single" :class="{active: activePayment == 3}" @click.prevent="activePayment = 3">
                                    <div class="order__payment-single--img">
                                        <img src="/upload/record/order__abonement.svg" alt="Оплата абонементом">
                                    </div>
                                    <div class="order__payment-single--info">Оплата <br>абонементом</div>
                                </div>
                                <div class="order__payment-single" :class="{active: activePayment == 4}" @click.prevent="activePayment = 4">
                                    <div class="order__payment-single--img">
                                        <img src="/upload/record/order__client-card.svg" alt="Оплата Картой Клиента">
                                    </div>
                                    <div class="order__payment-single--info">Оплата Картой <br>Клиента</div>
                                </div>
                                <div class="order__payment-single" :class="{active: activePayment == 5}" @click.prevent="activePayment = 5">
                                    <div class="order__payment-single--img">
                                        <img src="/upload/record/order__sert.svg" alt="Оплата Подарочным сертификатом">
                                    </div>
                                    <div class="order__payment-single--info">Оплата Подарочным сертификатом</div>
                                </div>
                            </div>
                            <div class="order__payment-card--number">
                                <div class="order__payment-card--number-info" v-if="payNumTypes[activePayment] || false">Введите номер {{ payNumTypes[activePayment] }}</div>
                                <div class="order__payment-card--number-input" v-if="payNumTypes[activePayment] || false">
                                    <input type="text" v-model="inputPaymentNum" maxlength="8">
                                </div>
                                <div class="next-step" v-if="readyStepPayment()" @click="toStepConfirmation">Далее</div>
                            </div>
                            <div class="order__payment-error" v-if="lastErrorMessage">{{ lastErrorMessage }}</div>
                            <div class="order__payment-additional-info" v-if="extraFee">На карте клиента {{ payDocBalance }} рублей, остаток {{ extraFee }} рублей нужно будет доплатить в салоне</div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="order__submit-sms" v-if="step === steps.CONFIRMATION">
            <div class="info">Для подтверждения записи мы отправили SMS сообщение с кодом на указанный номер, введите его и нажмите кнопку “Записаться”</div>
            <div class="error">{{ lastErrorMessage }}</div>
            <form action="">
                <div>
                    <input type="text" class="order__submit-sms--text-input" placeholder="Проверочный код" v-model="inputCheckCode">
                </div>
                <div class="order__submit-sms--checkbox-container">
                    <label class="order__submit-sms--checkbox-label" for="confirmPolicy">
                        <input type="checkbox" class="order__submit-sms--checkbox" id="confirmPolicy" v-model="personalDataPolicyConfirmed"/>
                        <span></span>
                        <div>Настоящим подтверждаю согласие на <a href="/agreement/" target="_blank">обработку персональных данных</a></div>
                    </label>
                </div>
                <button type="button" class="next-step" :class="submitClass()" :disabled="!readySubmit()" @click="submit">Записаться</button>
            </form>
        </div>




    </div>
</div>
