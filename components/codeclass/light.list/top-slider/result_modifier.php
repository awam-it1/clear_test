<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$cityId = ccHelpers::city('ID');
$items = [];
foreach ($arResult['ITEMS'] as $item) {
    if (!empty($item['PROPERTIES']['CITIES']['VALUE']) && !in_array($cityId, $item['PROPERTIES']['CITIES']['VALUE'])) continue;
    $items[] = $item;
}
$arResult['ITEMS'] = $items;

