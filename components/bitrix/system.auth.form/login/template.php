<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// echo '<script>var log_php_object = '.json_encode($arResult).';</script>';

?>
<div id="login-in-modal" class="modal-content-inner<?if($arResult["ERROR_MESSAGE"]["TYPE"] == "ERROR") echo " modal-self is-show";?>">
	<div class="auth-modal is-styled">
		<form data-is="formValidation" name="form_auth" method="post">
			<input type="hidden" name="AUTH_FORM" value="Y" />
			<input type="hidden" name="TYPE" value="AUTH" />
			<input type="hidden" name="USER_REMEMBER" value="Y" />
			<div class="title-h1">
				<span>Авторизация</span>
			</div>
			<?var_dump($_POST);?>
			<div class="input-element">
				<label>
					<span class="form-item-label">
						Телефон <span class="required">*</span>
					</span>
					<span class="form-item-error">Проверьте правильность ввода</span>
					<input type="phone" data-validation="phone" class="phone-mask" name="USER_PHONE" value="<?=@$_POST["USER_PHONE"];?>" />
				</label>
			</div>
			<div class="input-element<?if($arResult["ERROR_MESSAGE"]["TYPE"] == "ERROR") echo " has-error";?>">
				<label>
					<span class="form-item-label">
						Пароль <span class="required">*</span>
					</span>
					<span class="form-item-error">Сочетание телефон/пароль - неверно</span>
					<input type="password" name="USER_PASSWORD" />
				</label>
			</div>
			<div class="sign-in__btn">
				<input type="hidden" name="Login" value="Войти">
				<button class="button" type="submit">Войти</button>
			</div>
            <a class="gfr-link" href="/personal/?forgot_password=yes">Забыли пароль?</a>
		</form>
	</div>
</div>