<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
// print_r($arResult);

?>
<h1>Поиск</h1>
<div class="search-page">
    <form action="" method="get">
        <input type="hidden" name="tags" value="<?echo $arResult["REQUEST"]["TAGS"]?>" />
        <input type="hidden" name="how" value="<?echo $arResult["REQUEST"]["HOW"]=="d"? "d": "r"?>" />

        <div class="search-page__form">
            <div class="input-element is-search <?
            if (
                isset($_REQUEST["q"])
                    &&
                (strlen(trim($_REQUEST["q"])) != 0)
                    &&
                    (
                        ($arResult["REQUEST"]["QUERY"] === false && $arResult["REQUEST"]["TAGS"] === false)
                            ||
                        ($arResult["ERROR_CODE"] != 0)
                    )
                ) {
                echo " has-error";
            }?>">
                <label>
                    <span class="form-item-error">Проверьте правильность ввода</span>
                    <input type="text" name="q" placeholder="Введите слово для поиска" value="<?=$arResult["REQUEST"]["QUERY"]?>" size="40" />
                </label>
            </div>
            <button type="submit" value="<?=GetMessage("SEARCH_GO")?>" class="search-btn">
                <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 16 16">
                    <path fill="#999999" d="M15.7,14.3l-3.105-3.105C13.473,10.024,14,8.576,14,7c0-3.866-3.134-7-7-7S0,3.134,0,7s3.134,7,7,7 c1.576,0,3.024-0.527,4.194-1.405L14.3,15.7c0.184,0.184,0.38,0.3,0.7,0.3c0.553,0,1-0.447,1-1C16,14.781,15.946,14.546,15.7,14.3z M2,7c0-2.762,2.238-5,5-5s5,2.238,5,5s-2.238,5-5,5S2,9.762,2,7z"></path>
                </svg>
            </button>
        </div>

        <div class="cntr">
		    <input class="button is-big" type="submit" value="<?=GetMessage("SEARCH_GO")?>" />
        </div>
	</form>


    <?if ($arResult["REQUEST"]["QUERY"] === false && $arResult["REQUEST"]["TAGS"] === false) :?>
    <?elseif ($arResult["ERROR_CODE"]!=0) :?>
    	<?/*
        <p><?=GetMessage("SEARCH_ERROR")?></p>
        <?ShowError($arResult["ERROR_TEXT"]);?>
        <p><?=GetMessage("SEARCH_CORRECT_AND_CONTINUE")?></p>
        <br /><br />
        <p><?=GetMessage("SEARCH_SINTAX")?><br /><b><?=GetMessage("SEARCH_LOGIC")?></b></p>
        <table border="0" cellpadding="5">
            <tr>
                <td align="center" valign="top"><?=GetMessage("SEARCH_OPERATOR")?></td><td valign="top"><?=GetMessage("SEARCH_SYNONIM")?></td>
                <td><?=GetMessage("SEARCH_DESCRIPTION")?></td>
            </tr>
            <tr>
                <td align="center" valign="top"><?=GetMessage("SEARCH_AND")?></td><td valign="top">and, &amp;, +</td>
                <td><?=GetMessage("SEARCH_AND_ALT")?></td>
            </tr>
            <tr>
                <td align="center" valign="top"><?=GetMessage("SEARCH_OR")?></td><td valign="top">or, |</td>
                <td><?=GetMessage("SEARCH_OR_ALT")?></td>
            </tr>
            <tr>
                <td align="center" valign="top"><?=GetMessage("SEARCH_NOT")?></td><td valign="top">not, ~</td>
                <td><?=GetMessage("SEARCH_NOT_ALT")?></td>
            </tr>
            <tr>
                <td align="center" valign="top">( )</td>
                <td valign="top">&nbsp;</td>
                <td><?=GetMessage("SEARCH_BRACKETS_ALT")?></td>
            </tr>
        </table>
        */?>
    <?elseif (count($arResult["SEARCH"])>0) :
        $href_base = array();
        ?>
        <div id="results">
            <?foreach ($arResult["SEARCH"] as $arItem) :
                $href = preg_replace("/index\.php/", "",$arItem["URL"]);
                if(array_search($href, $href_base)===false) {
	                ?>
                    <a href="<?= $href; ?>">
                        <h3><?= $arItem["TITLE_FORMATED"] ?></h3>
                        <p><?= strip_tags($arItem["BODY_FORMATED"]) ?></p>
                    </a>
	                <?
                    array_push($href_base, $href);
                }
            endforeach;?>


            <?if ($arParams["DISPLAY_BOTTOM_PAGER"] != "N") {
                echo $arResult["NAV_STRING"];
            }?>
        </div>
        

        <?/*if ($arResult["REQUEST"]["HOW"]=="d") :?>
            <a href="<?=$arResult["URL"]?>&amp;how=r" class="is-link"><?=GetMessage("SEARCH_SORT_BY_RANK")?></a>&nbsp;|&nbsp;<span><?=GetMessage("SEARCH_SORTED_BY_DATE")?></span>
        <?else :?>
            <span><?=GetMessage("SEARCH_SORTED_BY_RANK")?></span>&nbsp;|&nbsp;<a href="<?=$arResult["URL"]?>&amp;how=d" class="is-link"><?=GetMessage("SEARCH_SORT_BY_DATE")?></a>
        <?endif;*/?>
        <!--</p>-->

    <?else :?>
        <h3>
            <?=GetMessage("SEARCH_NOTHING_TO_FOUND");?>
        </h3>
    <?endif;?>
</div>