<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// print_r($arResult);
use Bitrix\Main\Localization\Loc;
ob_start();
	if ($arResult["FILE"] <> '') include($arResult["FILE"]);
$file = ob_get_contents();
ob_end_clean();
?>
<div class="tra-phone-number">
    <span class="tb-inner-icon mobile-hidden"><svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 12 12"><path fill="#d4be6f" d="M3.55 4.57l.42-1.12c.1-.26.13-.54.07-.79L3.37 0 0 .49a11.93 11.93 0 0 0 3.55 7.78 12.36 12.36 0 0 0 8.08 3.5L12 8.3l-2.5-.56a1.34 1.34 0 0 0-.84.1l-1.25.53a11.05 11.05 0 0 1-3.86-3.8z"/><path fill="#d7c091" d="M3.55 4.57l.42-1.12c.1-.26.13-.54.07-.79L3.37 0 0 .49a11.93 11.93 0 0 0 3.55 7.78 12.36 12.36 0 0 0 8.08 3.5L12 8.3l-2.5-.56a1.34 1.34 0 0 0-.84.1l-1.25.53a11.05 11.05 0 0 1-3.86-3.8z"/></svg></span>
    <a href="tel:<?=preg_replace("/([^0-9]+)/", "", $file)?>" onclick="yaCounter38992940.reachGoal('click_nomer_telefona'); return true;"
       <? if(in_array($CITY_CODE, getCityPhoneIgnoreComagic())): ?>
       class="comagic_phone"
        <?endif;?>
    >
        <?=$file;?>
    </a>
    <div class="worktime-text">
        <?= Loc::getMessage("WORK_TIME_TEXT"); ?>
    </div>
</div>