<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// print_r($arResult);
ob_start();
	if ($arResult["FILE"] <> '') include($arResult["FILE"]);
$file = ob_get_contents();
ob_end_clean();
?>

<div class="gfp-number">
    <span class="gfp-number__icon">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20" height="20">
          <defs>
            <path id="gfp-number__icon__a" d="M107.91 98.62l.7-1.88c.17-.42.22-.89.13-1.31L107.6 91l-5.61.8c.3 4.9 2.4 9.5 5.91 12.99 3.61 3.53 8.4 5.57 13.48 5.83l.61-5.79-4.17-.94a2.23 2.23 0 0 0-1.4.17l-2.08.9a18.76 18.76 0 0 1-6.44-6.34z"/>
          </defs>
          <use fill="#d4be6f" transform="translate(-102 -91)" xlink:href="#gfp-number__icon__a"/>
          <use fill="#d7c091" transform="translate(-102 -91)" xlink:href="#gfp-number__icon__a"/>
        </svg>
    </span>
    <a href="tel:<?=preg_replace("/([^0-9]+)/", "", $file)?>"  onclick="yaCounter38992940.reachGoal('click_nomer_telefona'); return true;"
        <? if(in_array($CITY_CODE, getCityPhoneIgnoreComagic())): ?>
            class="comagic_phone"
        <?endif;?>
    >
        <?=$file;?>
    </a>
</div>