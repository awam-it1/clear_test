<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// echo '<script>var log_php_object = '.json_encode($arResult).';</script>';
?>
<div class="salon-services-list">
	<?for ($i=0; $i < 8; $i++):
		if (!strlen($arParams["FILE_".$i]) || !strlen($arParams["FILE_".$i."_NAME"])) continue;?>
	<div class="ss-l-item">
		<div class="ss-l-image">
			<img src="<?=$arParams["FILE_".$i];?>">
		</div>
		<div class="ss-l-label"><?=$arParams["FILE_".$i."_NAME"];?></div>
	</div>
	<?endfor;?>
</div>