<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}

use Bitrix\Main\Localization\Loc;

$APPLICATION->SetPageProperty("description",  'Салон Тайрай '.$arResult["DISPLAY_PROPERTIES"]["NAME"]["~VALUE"]);
$APPLICATION->SetPageProperty('og:title', $arResult["DISPLAY_PROPERTIES"]["NAME"]["~VALUE"]);
?>

<div class="salon-header-banner">
    <?$file = CFile::ResizeImageGet($arResult["DETAIL_PICTURE"], array('width'=>1980, "height"=>520), BX_RESIZE_IMAGE_PROPORTIONAL, true);
    //$file['src']?>
    <div class="shb-inner" style="background-image:url('<?=$file['src'];?>')"></div>
    <div class="shb-middle">
        <div class="shb-header">Салон Тайрай <?=$arResult["DISPLAY_PROPERTIES"]["NAME"]["~VALUE"];?></div>
        <div class="shb-order-link">
            <? if ($arResult['PROPERTIES']['COMING_SOON']['VALUE']) : ?>
            <a class="button"  disabled="">
                <?= $arResult['PROPERTIES']['COMING_SOON']['VALUE']; ?>
            </a>
            <? else: ?>
            <a class="button" href="/online-recording/?s=<?=$arResult["ID"];?>">
                Записаться в салон
            </a>
            <? endif; ?>
        </div>
    </div>
</div>

<div class="inner-wrapper min-width">

    <div class="salon-header-card" ref="salonInnerCard">
        <div class="shc-content"  v-cloak="">
            <div class="is-columns">
                <?if (strlen($arResult["DISPLAY_PROPERTIES"]["ADDRESS"]["VALUE"])) :?>
                    <div class="col-2">
                        <div class="shc-meta-a">
                            <img src="/dist/img/temp/icon7.png" />
                            <?=$arResult["DISPLAY_PROPERTIES"]["ADDRESS"]["VALUE"];?>
                            <br/>
                            <?if (strlen($arResult["DISPLAY_PROPERTIES"]["ADDRESS_SUB"]["VALUE"])) :?>
                        <span><?=$arResult["DISPLAY_PROPERTIES"]["ADDRESS_SUB"]["VALUE"];?></span>
                        <?endif;?>
                        </div>
                    </div>
                <?endif;?>
                <?if (strlen($arResult["METRO"])) :?>
                <div class="col-2">
                    <div class="shc-meta-a">
                        <img src="/dist/img/temp/icon5.png" />
                        <?=$arResult["METRO"];?>
                    </div>
                </div>
                <?endif;?>
            </div>
            <div class="is-columns">
                <?if (!empty($arResult["DISPLAY_PROPERTIES"]["PHONES"]["VALUE"]) && count($arResult["DISPLAY_PROPERTIES"]["PHONES"]["VALUE"])) :?>
                    <div class="col-2">
                        <div class="shc-meta-a">
                            <img src="/dist/img/temp/icon6.png" />
                            <?foreach ($arResult["DISPLAY_PROPERTIES"]["PHONES"]["VALUE"] as $phone) :?>
                                <a href="tel:<?=preg_replace("/([^0-9]+)/", "", $phone);?>" class="comagic_phone"><?=$phone;?></a>
                                <br/>
                            <?endforeach;?>
                        </div>
                    </div>
                <?endif;?>
                <?if (strlen($arResult["DISPLAY_PROPERTIES"]["TIME"]["VALUE"])) :?>
                <div class="col-2">
                    <div class="shc-meta-a">
                        <img src="/dist/img/temp/icon8.png" />
                        <?=str_replace('00:00 - 23:59', 'круглосуточно', $arResult['DISPLAY_PROPERTIES']['TIME']['VALUE']);?>
                    </div>
                </div>
                <?endif;?>
            </div>

            <?if (count($arResult["SERVICES"])) :?>
            <div class="salon-services-list">
                <?foreach ($arResult["SERVICES"] as $service) :?>
                <div class="ss-l-item">
                    <div class="ss-l-image">
                        <img src="<?=$service["IMAGE"];?>" />
                    </div>
                    <div class="ss-l-label"><?=$service["NAME"];?></div>
                </div>
                <?endforeach;?>
            </div>
            <?endif;?>

            <div class="shc-action">
                <div>
                <? if ($arResult['PROPERTIES']['COMING_SOON']['VALUE']) : ?>
                    <a class="button"  disabled="">
                        <?= $arResult['PROPERTIES']['COMING_SOON']['VALUE']?>
                    </a>
                <? else: ?>
                    <a class="button" href="/online-recording/?s=<?=$arResult["ID"];?>">
                        Записаться
                    </a>
                <? endif; ?>
                <a class="button solid" href="#review-modal" data-modal-open="">
                    Оставить отзыв
                </a>
                </div>
                <?php if ($arResult['IS_MOSCOW_CITY'] === 'Y'): ?>
                    <div class="shc-action--comment">
                        <?= Loc::getMessage('SALON:MOSCOW_CITY_COMMENT') ?>
                    </div>
                <?php endif; ?>

                <div id="review-modal" class="modal-content-inner">
                    <div class="simple-modal is-styled">
                        <form action="/ajax/reviewNew.php"
                              data-vv-scope="salon-card-form"
                              @submit.prevent="submit('salon-card-form', 'cardReviewForm')"
                              ref="cardReviewForm"
                              method="POST"
                              enctype="multipart/form-data">
                            <div class="title-h1">
                                <span>Добавить отзыв</span>
                            </div>
                            <div class="reviews-rating"  :class="{ 'has-error' : errors.has('salon-card-form.rating') }">
                                <span class="rating-error-msg">Поставьте оценку</span>
                                <star-rating :border-width="6"
                                             active-color="#ffbf35"
                                             border-color="#ffbf35"
                                             :increment="0.5"
                                             inactive-color="#fff"
                                             :padding="2"
                                             :star-size="18"
                                             :show-rating="false"
                                             :inline="true"
                                             v-model="rating"
                                ></star-rating>
                                <input type="hidden" name="rating"  v-model="rating"  v-validate="'required'" />
                            </div>
                            <div class="input-element" :class="{ 'has-error' : errors.has('salon-card-form.name') }">
                                <label>
                                <span class="form-item-label">
                                    Имя <span class="required">*</span>
                                </span>
                                    <span class="form-item-error">Проверьте правильность ввода</span>
                                    <input type="text" name="name" v-validate="'required|alpha_spaces'" v-model="form.name"/>
                                </label>
                            </div>
                            <div class="input-element" :class="{ 'has-error' : errors.has('salon-card-form.email') }">
                                <label>
                                <span class="form-item-label">
                                    E-mail <span class="required">*</span>
                                </span>
                                    <span class="form-item-error">Проверьте правильность ввода</span>
                                    <input type="text" name="email" v-validate="'required|email'" v-model="form.email"/>
                                </label>
                            </div>
                            <div class="input-element"  :class="{ 'has-error' : errors.has('salon-card-form.text') }">
                                <label>
                                <span class="form-item-label">
                                    Текст сообщения <span class="required">*</span>
                                </span>
                                    <span class="form-item-error">Проверьте правильность ввода</span>
                                    <textarea row="5" name="text" v-validate="'required'" v-model="form.msg"></textarea>
                                </label>
                            </div>
                            <div class="reviews-form-send" >
                                <div class="tairai__recaptcha" v-pre="">
                                    <div id="salons-recaptcha" data-recaptcha=""></div>
                                    <input data-recaptcha-id="salons-recaptcha" type="hidden" v-validate="'required|in:Y'" />
                                </div>
                                <input type="hidden" name="iblock_submit" value="Сохранить">
                                <button class="button" type="submit" name="iblock_submit" value="Сохранить">Отправить</button>
                            </div>
                            <input type="hidden" name="objectid" value="<?=(!isset($arParams["REVIEW_OBJ_ID"]) ? $arResult["ID"] : $arParams["REVIEW_OBJ_ID"]) ;?>">
                            <input type="hidden" name="type" value="<?=$arParams["REVIEW_TYPE"];?>">
                            <div class="form-required">
                                <span class="required">*</span> — поля обязательные для заполнения
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?if (strlen($arResult["DISPLAY_PROPERTIES"]["MAP"]["VALUE"])) :?>
        <div class="shc-map">
            <div class="shc-gmap" data-value='<?

            list($lat, $long) = explode(",", $arResult["DISPLAY_PROPERTIES"]["MAP"]["VALUE"]);

            echo json_encode(array("lat" => $lat, "lng" => $long, "zoom" => "17"));

            ?>'>
            </div>
        </div>
        <?endif;?>
    </div>

    <div class="salon-preview-text">
        <div class="spt-header">
            <div class="row">
                <div class="col">
                    <h2><?=$arResult["DISPLAY_PROPERTIES"]["TITLE"]["VALUE"];?></h2>
                </div>
                <div class="col">
                    <div class="salon-sharing-cnt" id="salon-sharing">
                        <a href="#" class="salon-sharing-link"  @click.prevent="showSocial">
                            <span class="salon-sharing-icon">
                                <svg xmlns:xlink="http://www.w3.org/1999/xlink" width="18" height="18">
                                    <defs>
                                        <path id="salon-sharing__a" d="M1109.69 1249.44a3.3 3.3 0 0 0-2.59 1.26l-5.61-2.85c.07-.28.12-.56.12-.85 0-.32-.06-.63-.15-.92l5.6-2.84a3.3 3.3 0 0 0 5.94-1.96 3.3 3.3 0 0 0-3.31-3.28 3.3 3.3 0 0 0-3.3 3.28c0 .3.05.58.12.85l-5.62 2.85a3.3 3.3 0 0 0-5.89 2.02 3.3 3.3 0 0 0 5.94 1.96l5.6 2.84c-.1.29-.16.6-.16.92a3.3 3.3 0 0 0 3.3 3.28 3.3 3.3 0 0 0 3.32-3.28 3.3 3.3 0 0 0-3.31-3.28z"/>
                                    </defs>
                                    <use fill="#c7b179" transform="translate(-1095 -1238)" xlink:href="#salon-sharing__a"/>
                                </svg>
                            </span>
                            <span class="salon-sharing-text">Поделиться</span>
                        </a>
                        <div class="salon-sharing-soc" v-show="isShowSocial" v-cloak="">
                            <social-sharing :url="url"
                                            :title="title"
                                            :description="description"
                                            inline-template>
                                <div class="lks-social">
                                    <div class="gfs-item">
                                        <network network="facebook">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32"><path fill="#8a007c" d="M13.65 16.55v8.62h3.74c.02-2.27 0-4.6 0-6.87v-2.26l.02-.03s2.37.02 2.45-.01l.05-.01.32-3.14H17.4v-1.41c0-.42-.01-.95.25-1.18l2.57-.26V6.85c-.49-.03-1.07-.01-1.57-.01-.51 0-1.04-.01-1.55.03-.97.06-1.75.35-2.31.83-.82 1.32-1.14.19-1.15 5.15h-1.87v3.16h1.86c.05.06.02.47.02.54zM32 16a16 16 0 1 1-32 0 16 16 0 0 1 32 0z"/></svg>
                                        </network>
                                    </div>
                                    <div class="gfs-item">
                                        <network network="vk">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32"><path fill="#8a007c" d="M7.25 12.3c.14-.04.27-.06.37-.06h.14l.35.01.48.02.56-.03c.16-.01.3-.02.43-.01l.34.01c.2.02.36.06.48.11.06.03.12.1.19.2s.13.19.17.28l.17.38.15.31c.37.82.84 1.62 1.39 2.42l.11.16.12.19.14.17c.05.07.1.12.15.16l.16.13c.05.04.11.07.17.08h.17c.28-.05.44-.63.46-1.73l.02-.52a3.5 3.5 0 0 0-.22-1.51.73.73 0 0 0-.23-.26c-.1-.07-.22-.13-.37-.18-.14-.06-.26-.11-.34-.16.14-.27.39-.46.74-.55a6 6 0 0 1 1.43-.13h.78a11.42 11.42 0 0 0 .79.05l.33.08.28.14c.2.09.26.16.19.21.09.17.14.38.16.62l.02.21c0 .19-.02.45-.05.77l-.06.77a5.07 5.07 0 0 0-.01 1.24c.04.44.19.74.44.91.09-.01.17-.03.25-.06a.8.8 0 0 0 .23-.17c.08-.07.13-.13.16-.17l.19-.24.15-.21c.47-.61.93-1.42 1.4-2.42l.13-.33.17-.39c.11-.26.17-.36.18-.29 0 .07.08 0 .23-.2.16-.07.34-.1.56-.1h.13l.69.03.69.03.41-.02.48-.02c.14-.01.29 0 .44.01.15.01.28.04.38.09.1.04.17.11.21.19a.4.4 0 0 1 .04.19c0 .29-.17.72-.52 1.29a8.8 8.8 0 0 1-.51.78l-.68.86-.54.69-.21.26-.26.36c-.06.08-.11.19-.17.33-.06.15-.08.28-.08.4 0 .08.02.15.05.22a1.75 1.75 0 0 0 .29.4l.2.19.18.17c.7.64 1.25 1.22 1.66 1.73.39.51.59.88.59 1.11 0 .33-.25.53-.76.61a4.93 4.93 0 0 1-1.42.01 5.5 5.5 0 0 0-.55-.03c-.25 0-.48.03-.66.08h-.09c-.66-.11-1.34-.57-2.06-1.4a2.8 2.8 0 0 1-.26-.3c-.13-.16-.23-.29-.32-.38a1.87 1.87 0 0 0-.34-.25.62.62 0 0 0-.4-.09c-.23.04-.38.19-.46.43a4.2 4.2 0 0 0-.13.88 1.6 1.6 0 0 1-.14.72c-.13.24-.52.36-1.17.36-.28 0-.6-.02-.98-.06a6.16 6.16 0 0 1-3.59-1.59 14.3 14.3 0 0 1-1.2-1.38c-1.03-1.31-2-2.9-2.92-4.77l-.16-.33-.18-.38a5.05 5.05 0 0 1-.26-.76c-.02-.14-.04-.27-.04-.4a.9.9 0 0 1 .37-.17zM16 0a16 16 0 1 0 0 32 16 16 0 0 0 0-32z"/></svg>
                                        </network>
                                    </div>
                                </div>
                            </social-sharing>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <p>
            <?=$arResult["~DETAIL_TEXT"];?>
        </p>
    </div>

    <? $APPLICATION->AddBufferContent(function ($arResult) {
        global $APPLICATION;

        $desktopTexts = [];
        $desktopTexts[] = $APPLICATION->GetViewContent('services');
        if (count($arResult["DISPLAY_PROPERTIES"]["PHOTO"]["~VALUE"])) {
            $desktopTexts[] = '<div class="shop__nav-link"><a href="#salon-photo">Фотографии салона</a></div>';
        }
        $desktopTexts[] = $APPLICATION->GetViewContent('promotions');
        $desktopTexts[] = $APPLICATION->GetViewContent('reviews');
        $desktopTexts[] = $APPLICATION->GetViewContent('masters');


        $mobileTexts = [];
        $mobileTexts[] = $APPLICATION->GetViewContent('services_mobile');
        if (count($arResult["DISPLAY_PROPERTIES"]["PHOTO"]["~VALUE"])) {
            $mobileTexts[] = '<option value="#salon-photo">Фотографии салона</option>';
        }
        $mobileTexts[] = $APPLICATION->GetViewContent('promotions_mobile');
        $mobileTexts[] = $APPLICATION->GetViewContent('reviews_mobile');
        $mobileTexts[] = $APPLICATION->GetViewContent('masters_mobile');

        //удаляем элементы с пустыми строками
        $desktopTexts = array_map('trim', $desktopTexts);
        $desktopTexts = array_filter($desktopTexts);

        $mobileTexts = array_map('trim', $mobileTexts);
        $mobileTexts = array_filter($mobileTexts);

        if (count($desktopTexts) <= 1 && count($mobileTexts) <= 1) {
            return '';
        }

        return '
            <div class="shop__nav is-purple" ref="pageNav">
                <div class="shop__nav-links mobile-hidden">
                    ' . join('', $desktopTexts) . '
                </div>
                <div class="mobile-select mobile-visible">
                    <select name="shop-nav">
                        ' . join('', $mobileTexts) . '
                    </select>
                    <span class="mobile-select__icon"></span>
                </div>
            </div>
        ';
    }, $arResult); ?>

    <?
	// POPULAR_SERVICES hidden 
	/*
	if (count($arResult["DISPLAY_PROPERTIES"]["POPULAR_SERVICES"]["LINK_SECTION_VALUE"])) :?>
    <div>
        <h2 class="cntr">Популярные услуги салона</h2>

        <div class="serivce-slider-container" ref="serviceSlider">
            <div class="swiper-container service-slider" >
                <div class="swiper-wrapper">
                    <?foreach ($arResult["DISPLAY_PROPERTIES"]["POPULAR_SERVICES"]["LINK_SECTION_VALUE"] as $arItem) :?>
                    <div class="swiper-slide">
                        <div class="service-slide">
                            <div class="sslide-image"<?
                            if (strlen($arItem["PICTURE"])) :
	                            //$file = CFile::ResizeImageGet($arItem["PICTURE"], array('width'=>325, "height"=>240), BX_RESIZE_IMAGE_PROPORTIONAL, true)
                                // CFile::GetPath($arItem["PICTURE"]);
                                ?>
                                 style="background-image: url('<?=CFile::ResizeImageGet($arItem["PICTURE"], ['width' => 360, 'height' => 240])['src'];?>');"
                            <? endif;?>></div>
                            <a class="sslide-title" href="<?=$arItem["SECTION_PAGE_URL"];?>"><?=$arItem["NAME"];?></a>
                            <div class="sslide-links">
                                <a href="/online-recording/?s0=<?=$arItem["ID"];?>" class="is-link">Записаться</a>
                                <?//<a href="#" class="is-link">Оформить сертификат</a>?>
                            </div>
                        </div>
                    </div>
                    <?endforeach;?>
                </div>
            </div>
            <div class="ig-arrow ig-left">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20" height="20" viewBox="0 0 20 20"><g transform="translate(-90 -402)"><image width="20" height="20" transform="translate(90 402)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAACa0lEQVQ4T5VUTWgTQRR+bybZbo3bNiWppggSiYhEkVoIQkGqoBgUiiAePSkoiodSGvCg4sGDKAiCeKkXvYigSK1W0R4qKEWlSvGnINEc2vrTRGPAEpPMk9nNJpvNZhvfYWdm571v5n3fe4PQpP34PHp0haoNeT1tawkRWgI9XqdQXA4vkxq771NDcQSFmb4C8qSu6q2srRgNAReTd0+0tqy+zEDlAKTHyK8egERqaCtHRGPDYo6A6S9jL1VPVy8JQh3BDDPnCLBUWBgJrhs4vCzgz9TjNBe+Tt3RAlBzRQIoQu5XZ3iP3xUwnXz0m4GqkShfSbIkAKDMlrwuAYEcEYgKWnZNMDgw75jy90/35pnwhYAIAA0miAgQZWgdVQYGz2eDkX0ddYALs7eectG+g4BqODVFcKDQYAQZlTD3KrThQMwExbmZm7s56xgXZBHAhLUj2giT6TNkRPAn2R09GNEPmXt3568ogmOR1tWoTSTrknEmiLLXMfX2Rh6EohicGfQ1osytCRALJYb5Efz4+spOL7U/AWJoVdEoZENROTaqIsaAiqXM+/Wxk5tMH5h9cXGUscBekjy6mKm4HMvBVMDM5MbYYH9FFHPy4fmlJFJb2A3QrpGAXDraNxhwrEP5c2byQoaB5q+mWC3kuo5gRK1+6g5Hj39tCCg33kyc/8ZxZZfbTY18lxY39yeCdQc5BU5PnH2GQuuTolefmFpPphSvbdmeONYUoHSaHj99pMSVqyA8HuPJqZrsxp5dw80/X9bgqYenbhMp+5E4ryjJBMXiZ/7vgbWnMvUgcUgQJgB4BBnHbfFzejPY7R+83t3zXtOZRwAAAABJRU5ErkJggg=="/></g></svg>
            </div>
            <div class="ig-arrow ig-right">
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"><defs><linearGradient id="a" x1="10" x2="10" y1="10" y2="20" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#d4be6f"/><stop offset="1" stop-color="#b3a05d"/></linearGradient><linearGradient id="b" x1="10" x2="10" y2="20" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#fff7b4" stop-opacity=".7"/><stop offset=".99" stop-color="#ccc370" stop-opacity=".7"/></linearGradient><filter id="c" width="200%" height="200%" x="-50%" y="-50%"><feGaussianBlur in="SourceGraphic" result="SvgjsFeGaussianBlur1067Out"/></filter><mask id="d"><path fill="#fff" d="M6.28 0C10.87.79 15.99 3.38 20 10c-4.01 6.62-9.13 9.22-13.72 10l-.01-.22c-.02-.85-.09-1.65-.22-2.39a10.78 10.78 0 0 0-1.55-3.91A8.83 8.83 0 0 0 0 10a8.83 8.83 0 0 0 4.51-3.5c.33-.48.6-1.01.91-1.76.25-.64.5-1.33.63-2.12.13-.75.2-1.55.22-2.39z"/></mask><linearGradient id="e" x1="10" x2="10" y2="19.78" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#ffeed9"/><stop offset="1" stop-color="#c7b179"/></linearGradient></defs><path fill="#d4be6f" d="M6.28 0C10.87.79 15.99 3.38 20 10c-4.01 6.62-9.13 9.22-13.72 10l-.01-.22c-.02-.85-.09-1.65-.22-2.39a10.78 10.78 0 0 0-1.55-3.91A8.83 8.83 0 0 0 0 10a8.83 8.83 0 0 0 4.51-3.5c.33-.48.6-1.01.91-1.76.25-.64.5-1.33.63-2.12.13-.75.2-1.55.22-2.39z"/><path fill="url(#a)" d="M6.28 0C10.87.79 15.99 3.38 20 10c-4.01 6.62-9.13 9.22-13.72 10l-.01-.22c-.02-.85-.09-1.65-.22-2.39a10.78 10.78 0 0 0-1.55-3.91A8.83 8.83 0 0 0 0 10a8.83 8.83 0 0 0 4.51-3.5c.33-.48.6-1.01.91-1.76.25-.64.5-1.33.63-2.12.13-.75.2-1.55.22-2.39z"/><path fill="url(#b)" d="M6.28 0C10.87.79 15.99 3.38 20 10c-4.01 6.62-9.13 9.22-13.72 10l-.01-.22c-.02-.85-.09-1.65-.22-2.39a10.78 10.78 0 0 0-1.55-3.91A8.83 8.83 0 0 0 0 10a8.83 8.83 0 0 0 4.51-3.5c.33-.48.6-1.01.91-1.76.25-.64.5-1.33.63-2.12.13-.75.2-1.55.22-2.39z"/><path fill="#fff" fill-opacity=".5" d="M0 21V-1h20v22zM6.27 1.23c-.02.84-.09 1.64-.22 2.39-.13.79-.38 1.48-.63 2.12a9.19 9.19 0 0 1-2.04 3.13A8.83 8.83 0 0 1 0 11a8.83 8.83 0 0 1 4.5 3.48 10.78 10.78 0 0 1 1.55 3.91c.13.74.2 1.54.22 2.39l.01.22c4.59-.78 9.71-3.38 13.72-10-4.01-6.62-9.13-9.21-13.72-10z" filter="url(#c)" mask="url(&quot;#d&quot;)"/><path fill="url(#e)" d="M6.28 0C10.87.79 15.99 3.38 20 10c-4.01 6.62-9.13 9.22-13.72 10l-.01-.22c-.02-.85-.09-1.65-.22-2.39a10.78 10.78 0 0 0-1.55-3.91A8.83 8.83 0 0 0 0 10a8.83 8.83 0 0 0 4.51-3.5c.33-.48.6-1.01.91-1.76.25-.64.5-1.33.63-2.12.13-.75.2-1.55.22-2.39z"/></svg>
            </div>
        </div>
    </div>
    <?endif;?>
	<?*/// POPULAR_SERVICES hidden?>

    <div class="sale-text"><?=$arResult["DISPLAY_PROPERTIES"]["SLOGAN"]["~VALUE"];?></div>
    <?

    // фото
    if (count($arResult["DISPLAY_PROPERTIES"]["PHOTO"]["~VALUE"])):?>
      <div id="salon-photo">
          <h2 class="cntr">
              Фотографии салона
          </h2>
      </div>
    <?endif;?>
</div>

<?

// фото
    if (count($arResult["DISPLAY_PROPERTIES"]["PHOTO"]["~VALUE"])):?>
        <div class="inner-wrapper">
            <div class="image-gallery" ref="imageGallery">
                <div class="swiper-container ig-main">
                    <div class="swiper-wrapper">
                        <?foreach($arResult["DISPLAY_PROPERTIES"]["PHOTO"]["~VALUE"] as $arImage):?>
                            <?$file = CFile::ResizeImageGet($arImage, ['width' => 1180, 'height' => 520]);
                            //$file['src']?>
                            <div class="swiper-slide">
                                <div class="gallery-image" style="background-image: url('<?=$file['src'];?>')"></div>
                            </div>
                        <?endforeach;?>
                    </div>
                </div>
                <div class="ig-arrow ig-left">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20" height="20" viewBox="0 0 20 20"><g transform="translate(-90 -402)"><image width="20" height="20" transform="translate(90 402)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAACa0lEQVQ4T5VUTWgTQRR+bybZbo3bNiWppggSiYhEkVoIQkGqoBgUiiAePSkoiodSGvCg4sGDKAiCeKkXvYigSK1W0R4qKEWlSvGnINEc2vrTRGPAEpPMk9nNJpvNZhvfYWdm571v5n3fe4PQpP34PHp0haoNeT1tawkRWgI9XqdQXA4vkxq771NDcQSFmb4C8qSu6q2srRgNAReTd0+0tqy+zEDlAKTHyK8egERqaCtHRGPDYo6A6S9jL1VPVy8JQh3BDDPnCLBUWBgJrhs4vCzgz9TjNBe+Tt3RAlBzRQIoQu5XZ3iP3xUwnXz0m4GqkShfSbIkAKDMlrwuAYEcEYgKWnZNMDgw75jy90/35pnwhYAIAA0miAgQZWgdVQYGz2eDkX0ddYALs7eectG+g4BqODVFcKDQYAQZlTD3KrThQMwExbmZm7s56xgXZBHAhLUj2giT6TNkRPAn2R09GNEPmXt3568ogmOR1tWoTSTrknEmiLLXMfX2Rh6EohicGfQ1osytCRALJYb5Efz4+spOL7U/AWJoVdEoZENROTaqIsaAiqXM+/Wxk5tMH5h9cXGUscBekjy6mKm4HMvBVMDM5MbYYH9FFHPy4fmlJFJb2A3QrpGAXDraNxhwrEP5c2byQoaB5q+mWC3kuo5gRK1+6g5Hj39tCCg33kyc/8ZxZZfbTY18lxY39yeCdQc5BU5PnH2GQuuTolefmFpPphSvbdmeONYUoHSaHj99pMSVqyA8HuPJqZrsxp5dw80/X9bgqYenbhMp+5E4ryjJBMXiZ/7vgbWnMvUgcUgQJgB4BBnHbfFzejPY7R+83t3zXtOZRwAAAABJRU5ErkJggg=="/></g></svg>
                </div>
                <div class="ig-arrow ig-right">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"><defs><linearGradient id="a" x1="10" x2="10" y1="10" y2="20" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#d4be6f"/><stop offset="1" stop-color="#b3a05d"/></linearGradient><linearGradient id="b" x1="10" x2="10" y2="20" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#fff7b4" stop-opacity=".7"/><stop offset=".99" stop-color="#ccc370" stop-opacity=".7"/></linearGradient><filter id="c" width="200%" height="200%" x="-50%" y="-50%"><feGaussianBlur in="SourceGraphic" result="SvgjsFeGaussianBlur1067Out"/></filter><mask id="d"><path fill="#fff" d="M6.28 0C10.87.79 15.99 3.38 20 10c-4.01 6.62-9.13 9.22-13.72 10l-.01-.22c-.02-.85-.09-1.65-.22-2.39a10.78 10.78 0 0 0-1.55-3.91A8.83 8.83 0 0 0 0 10a8.83 8.83 0 0 0 4.51-3.5c.33-.48.6-1.01.91-1.76.25-.64.5-1.33.63-2.12.13-.75.2-1.55.22-2.39z"/></mask><linearGradient id="e" x1="10" x2="10" y2="19.78" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#ffeed9"/><stop offset="1" stop-color="#c7b179"/></linearGradient></defs><path fill="#d4be6f" d="M6.28 0C10.87.79 15.99 3.38 20 10c-4.01 6.62-9.13 9.22-13.72 10l-.01-.22c-.02-.85-.09-1.65-.22-2.39a10.78 10.78 0 0 0-1.55-3.91A8.83 8.83 0 0 0 0 10a8.83 8.83 0 0 0 4.51-3.5c.33-.48.6-1.01.91-1.76.25-.64.5-1.33.63-2.12.13-.75.2-1.55.22-2.39z"/><path fill="url(#a)" d="M6.28 0C10.87.79 15.99 3.38 20 10c-4.01 6.62-9.13 9.22-13.72 10l-.01-.22c-.02-.85-.09-1.65-.22-2.39a10.78 10.78 0 0 0-1.55-3.91A8.83 8.83 0 0 0 0 10a8.83 8.83 0 0 0 4.51-3.5c.33-.48.6-1.01.91-1.76.25-.64.5-1.33.63-2.12.13-.75.2-1.55.22-2.39z"/><path fill="url(#b)" d="M6.28 0C10.87.79 15.99 3.38 20 10c-4.01 6.62-9.13 9.22-13.72 10l-.01-.22c-.02-.85-.09-1.65-.22-2.39a10.78 10.78 0 0 0-1.55-3.91A8.83 8.83 0 0 0 0 10a8.83 8.83 0 0 0 4.51-3.5c.33-.48.6-1.01.91-1.76.25-.64.5-1.33.63-2.12.13-.75.2-1.55.22-2.39z"/><path fill="#fff" fill-opacity=".5" d="M0 21V-1h20v22zM6.27 1.23c-.02.84-.09 1.64-.22 2.39-.13.79-.38 1.48-.63 2.12a9.19 9.19 0 0 1-2.04 3.13A8.83 8.83 0 0 1 0 11a8.83 8.83 0 0 1 4.5 3.48 10.78 10.78 0 0 1 1.55 3.91c.13.74.2 1.54.22 2.39l.01.22c4.59-.78 9.71-3.38 13.72-10-4.01-6.62-9.13-9.21-13.72-10z" filter="url(#c)" mask="url(&quot;#d&quot;)"/><path fill="url(#e)" d="M6.28 0C10.87.79 15.99 3.38 20 10c-4.01 6.62-9.13 9.22-13.72 10l-.01-.22c-.02-.85-.09-1.65-.22-2.39a10.78 10.78 0 0 0-1.55-3.91A8.83 8.83 0 0 0 0 10a8.83 8.83 0 0 0 4.51-3.5c.33-.48.6-1.01.91-1.76.25-.64.5-1.33.63-2.12.13-.75.2-1.55.22-2.39z"/></svg>
                </div>
                <div class="swiper-container ig-tumbnails">
                    <div class="swiper-wrapper">
                        <?foreach($arResult["DISPLAY_PROPERTIES"]["PHOTO"]["FILE_VALUE"] as $arImage):?>
                            <?$file = CFile::ResizeImageGet($arImage, array('width'=>72,"height"=>72), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                            //$file['src']?>
                            <div class="swiper-slide tumbs-slide">
                                <div class="tumbs-image" style="background-image: url('<?=$file['src'];?>')"></div>
                            </div>
                        <?endforeach;?>
                    </div>
                </div>
            </div>
        </div>
    <?endif;
?>

<?
// акции
    global $salon_action_filter;
    $salon_action_filter = [
        'PROPERTY_SALON' => $arResult['ID'],
        '!PROPERTY_HIDDEN_VALUE' => 'Да',
    ];
    $APPLICATION->IncludeComponent(
        "bitrix:news.list",
        "salon_action",
        array(
            "IBLOCK_TYPE" => IBLOCK_TYPE_salons,
            "IBLOCK_ID" => IBLOCK_salons_actions,
            "NEWS_COUNT" => "10000000",
            "SORT_BY1" => "SORT",
            "SORT_ORDER1" => "DESC",
            "SORT_BY2" => "",
            "SORT_ORDER2" => "",
            "FILTER_NAME" => "salon_action_filter",
            "FIELD_CODE" => array(),
            "PROPERTY_CODE" => array("LABEL"),
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "Y",
            "AJAX_OPTION_HISTORY" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "36000000",
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "Y",
            "PREVIEW_TRUNCATE_LEN" => "",
            "ACTIVE_DATE_FORMAT" => "d F Y",
            "SET_STATUS_404" => "N",
            "SET_TITLE" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "INCLUDE_SUBSECTIONS" => "Y",
            "DISPLAY_DATE" => "Y",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "Y",
            "DISPLAY_PREVIEW_TEXT" => "Y",
            "PAGER_TEMPLATE" => "pagination",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "PAGER_TITLE" => "",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "Y",
            "LINK" => "/promotions/?salon=".$arResult["ID"]
        )
    );
?>

<?

//услуги
$APPLICATION->IncludeComponent("bitrix:salon.services","",
    Array(
        "IBLOCK_ID" => IBLOCK_salons_services,
        "IBLOCK_TYPE" => IBLOCK_TYPE_salons,
        "SORT" => array("SORT"  => "ASC", "NAME" => "ASC"),
        "SECTION_USER_FIELDS" => array("UF_ICON", "UF_SHORT", "UF_ID1C"),
        "SHOW_PARENT_NAME" => "Y",
        "SALON_ID" => $arResult["ID"],
        "SERVICES" => $arResult["DISPLAY_PROPERTIES"]["ALL_SERVICES"]["VALUE"],
        "PRICE_TYPE" => $arResult["DISPLAY_PROPERTIES"]["PRICE_TYPE"]["VALUE"],
        "DEPTH_2_nPageSize" => 5,
        "DEPTH_2_iNumPage" => 1,
    )
);

?>

<div id="modal-msg" class="modal-self ">
    <div class="thanks-modal">
        <h2 class="js-msg"></h2>
    </div>
</div>
