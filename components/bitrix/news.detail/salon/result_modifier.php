<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CModule::IncludeModule("highloadblock");
use Bitrix\Highloadblock as HL; 
use Bitrix\Main\Entity;

\Bitrix\Main\Loader::includeModule('dev2fun.opengraph');
\Dev2fun\Module\OpenGraph::Show($arResult['ID'],'element');

// print_r($arResult);
// сервисы
	$arResult["SERVICES"] = Array();

	if (count($arResult["DISPLAY_PROPERTIES"]["SERVICES"]["VALUE"])) {
		$hlblock = HL\HighloadBlockTable::getById(1)->fetch(); 
		$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
		$main_query = new Entity\Query($entity);
		$main_query->setSelect(array('*'));
		$main_query->setFilter(array('UF_XML_ID' => $arResult["DISPLAY_PROPERTIES"]["SERVICES"]["VALUE"]));
		$result = $main_query->exec();
		$result = new CDBResult($result);

		while ($row = $result->Fetch()) {
			$arResult["SERVICES"][] = Array("NAME" => $row["UF_NAME"], "IMAGE" => CFile::GetPath($row["UF_FILE"]));
		}
	}
// метро
	$arResult["METRO"] = "";
	$rsMetro = CIBlockSection::GetByID($arResult["IBLOCK_SECTION_ID"]);
	if ($arMetro = $rsMetro->GetNext()) {
		if (strlen($arMetro["IBLOCK_SECTION_ID"])) {
			$arResult["METRO"] = $arMetro["NAME"];
		}
	}

	$popular_services_info = array();
	$rsPopularServices = CIBlockSection::GetList(
		array("SORT"=>"ASC"),
		array(
			"IBLOCK_ID"=> IBLOCK_salons_services
		),
		false,
		array("ID", "UF_PICTURES")
	);
	while ($arPopularServices = $rsPopularServices->GetNext()) {
		$popular_services_info[$arPopularServices['ID']] = $arPopularServices;
	}

	$popular_services = array();
	foreach($arResult["DISPLAY_PROPERTIES"]["POPULAR_SERVICES"]["LINK_SECTION_VALUE"] as $section) {

		$section['PICTURE'] = ($popular_services_info[$section['ID']]['UF_PICTURES'] ? $popular_services_info[$section['ID']]['UF_PICTURES'][0] : '/local/include/img/stub.jpg');
		//Если элемент с 0 сортировкой то в конец списка
        if($section['SORT'] == 0) $section['SORT'] = 500;
        $section['SECTION_PAGE_URL'] = '/salon-service/'.$arResult['CODE'].'/'.$section['CODE'].'/';
		$popular_services[] = $section;
	}
    //сортируем массив
    usort($popular_services, function($a, $b){
        return $a['SORT'] <=> $b['SORT'];
    });
    $comingSoondate = DateTime::createFromFormat('d.m.Y', $arResult['PROPERTIES']['COMING_SOON']['VALUE']);
    $comingSoon = ($comingSoondate)? 'открытие в '.getComingSoonMonth($comingSoondate->format('m')) : '';
    $arResult['PROPERTIES']['COMING_SOON']['VALUE'] = $comingSoon;
	$arResult["DISPLAY_PROPERTIES"]["POPULAR_SERVICES"]["LINK_SECTION_VALUE"] = $popular_services;

    //FILE_VALUE когда 1 фото то попадают значения фото, а если несколько, то массив значений для каждого фото
    // что бы в шаблоне не падал foeach
    if(count($arResult["DISPLAY_PROPERTIES"]["PHOTO"]["VALUE"]) == 1){
        $arResult["DISPLAY_PROPERTIES"]["PHOTO"]["FILE_VALUE"] = [$arResult["DISPLAY_PROPERTIES"]["PHOTO"]["FILE_VALUE"]];
    }

    global $APPLICATION;
    $curDir = $APPLICATION->GetCurDir();
    $arResult['IS_MOSCOW_CITY'] = (strpos($curDir, 'm-delovoy-tsentr') !== false) ? 'Y' : 'N';
