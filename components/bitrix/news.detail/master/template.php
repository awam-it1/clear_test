<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// echo '<script>var log_php_object = '.json_encode($arResult).';</script>';
$salon = array_shift($arResult["DISPLAY_PROPERTIES"]["SALON"]["LINK_ELEMENT_VALUE"]);
?>
<div id="master-app">
<div class="inner-wrapper">
    <div class="master">
        <div class="master-inner">
            <div class="master-img" style="background-image: url(<?=$arResult["PREVIEW_PICTURE"]["SRC"];?>);"></div>
            <div class="master-info">
                <div class="master-info__top">
                    <div class="master-info__top-item">
                        <div class="master-info__top-item--left">
                            <h2><?=$arResult["NAME"];?></h2>
                            <div class="master-info-salon"><?=$salon["NAME"];?></div>
                        </div>
                        <div class="master-info__top-item--right">
                            <div class="master-info-stars">
                                <star-rating :border-width="4"
                                             active-color="#ffbf35"
                                             border-color="#ffbf35"
                                             :increment="0.5"
                                             inactive-color="#fff"
                                             :padding="2"
                                             :star-size="6"
                                             :read-only="true"
                                             :show-rating="false"
                                             :rating="<?=$arResult["RATING"];?>"
                                ></star-rating>
                            </div>
                            <?if ($arResult["REVIEWS_COUNT"]):?>
                            <div class="master-info-reviews">
                                <a href="/reviews/?type=master&id=<?=$arResult["ID"];?>" class="is-link">Посмотреть отзывы (<?=$arResult["REVIEWS_COUNT"];?>)</a>
                            </div>
                            <?endif;?>
                        </div>
                    </div>
                    <div class="master-info__top-item">
                        <div class="master-info__top-item--left">
                            <div class="master-info-experience"><?=$arResult["DISPLAY_PROPERTIES"]["WORK_EXPERIENCE"]["~VALUE"];?></div>
                        </div>
                        <div class="master-info__top-item--right">
                            <a href="#review-modal" class="button solid" data-modal-open="">Оставить отзыв</a>
                            <a href="/online-recording/?m0=<?=$arResult["ID"];?>&s=<?=$salon["ID"];?>" class="button">Записаться</a>
                        </div>
                    </div>
                </div>
                <div class="master-info__bottom">
                    <?if (count($arResult["DISPLAY_PROPERTIES"]["ALL_SERVICES"]["VALUE"])):?>
                    <div class="master-info__bottom-item">
                        <div class="master-info__bottom--left">
                            <div class="master-info__bottom-title">Услуги</div>
                        </div>
                        <div class="master-info__bottom--right">
                            <?foreach ($arResult["DISPLAY_PROPERTIES"]["ALL_SERVICES"]["LINK_SECTION_VALUE"] as $arService):?>
                            <div class="master-info__bottom-content"><?=$arService["NAME"];?></div>
                            <?endforeach;?>
                        </div>
                    </div>
                    <?endif;?>
                    <?if (strlen($arResult["DISPLAY_PROPERTIES"]["TRAINING"]["~VALUE"])):?>
                    <div class="master-info__bottom-item">
                        <div class="master-info__bottom--left">
                            <div class="master-info__bottom-title">Карьера</div>
                        </div>
                        <div class="master-info__bottom--right">
                            <div class="master-info__bottom-content"><?=$arResult["DISPLAY_PROPERTIES"]["TRAINING"]["~VALUE"];?></div>
                        </div>
                    </div>
                    <?endif;?>
                </div>
            </div>
        </div>
    </div>
</div>

<br>
<br>
<br>
<br>
<br>

<div id="review-modal" class="modal-content-inner">
    <div class="simple-modal is-styled">
        <form action="/ajax/review.php" data-vv-scope="master-review-form" @submit.prevent="submit('master-review-form')" ref="reviewForm">
            <div class="title-h1">
                <span>Добавить отзыв</span>
            </div>
            <div class="reviews-rating">
                <star-rating :border-width="6"
                             active-color="#ffbf35"
                             border-color="#ffbf35"
                             :increment="0.5"
                             inactive-color="#fff"
                             :padding="2"
                             :star-size="18"
                             :show-rating="false"
                             :inline="true"
                             v-model="rating"
                ></star-rating>
                <input type="hidden" name="rate"  v-model="rating"/>
            </div>
            <input type="hidden" name="master" value="<?=$arResult["ID"];?>"/>
            <input type="hidden" name="block" value="<?=$arParams["IBLOCK_ID"];?>"/>
            <div class="input-element" :class="{ 'has-error' : errors.has('name') }">
                <label>
                    <span class="form-item-label">
                        Имя <span class="required">*</span>
                    </span>
                    <span class="form-item-error">Проверьте правильность ввода</span>
                    <input type="text" name="name" v-validate="'required|alpha_spaces'" v-model="form.name"/>
                </label>
            </div>
            <div class="input-element" :class="{ 'has-error' : errors.has('email') }">
                <label>
                    <span class="form-item-label">
                        E-mail <span class="required">*</span>
                    </span>
                    <span class="form-item-error">Проверьте правильность ввода</span>
                    <input type="text" name="email" v-validate="'required|email'"  v-model="form.email"/>
                </label>
            </div>
            <div class="input-element"  :class="{ 'has-error' : errors.has('message') }">
                <label>
                    <span class="form-item-label">
                        Текст сообщения <span class="required">*</span>
                    </span>
                    <span class="form-item-error">Проверьте правильность ввода</span>
                    <textarea row="5"  name="message"  v-validate="'required'"  v-model="form.msg"></textarea>
                </label>
            </div>
            <div class="reviews-form-send" >
                <div class="tairai__recaptcha">
                    <div id="reviews-recaptcha" data-recaptcha=""></div>
                    <input data-recaptcha-id="reviews-recaptcha" type="hidden" v-validate="'required|in:Y'" />
                </div>
                <button class="button" type="submit">Отправить</button>
            </div>
            <div class="form-required">
                <span class="required">*</span> — поля обязательные для заполнения
            </div>
        </form>
    </div>
</div>
</div>

<?
GLOBAL $salon_review_filter;
$salon_review_filter = Array("PROPERTY_MASTER" => $arResult["ID"]);

$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "salon_review",
    array(
        "IBLOCK_TYPE" => "salons",
        "IBLOCK_ID" => "18",
        "NEWS_COUNT" => "10000000",
        "SORT_BY1" => "SORT",
        "SORT_ORDER1" => "DESC",
        "SORT_BY2" => "",
        "SORT_ORDER2" => "",
        "FILTER_NAME" => "salon_news_filter",
        "FIELD_CODE" => array("ACTIVE_FROM"),
        "PROPERTY_CODE" => array(),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "PREVIEW_TRUNCATE_LEN" => "",
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "N",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => $_REQUEST["CODE"],
        "INCLUDE_SUBSECTIONS" => "Y",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "PAGER_TEMPLATE" => "pagination",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "PAGER_TITLE" => "",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "Y",
        "LINK" => "/reviews/?type=master&id=".$arResult["ID"],
        "REVIEW_TYPE" => IBLOCK_salons_masters,
        "REVIEW_OBJ_ID" => $arResult["ID"]
    )
);?>

<?
GLOBAL $salon_master_filter;
$salon_master_filter = Array("PROPERTY_SALON" => $salon["ID"]);

$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "salon_master",
    array(
        "IBLOCK_TYPE" => "salons",
        "IBLOCK_ID" => "10",
        "NEWS_COUNT" => "10000000",
        "SORT_BY1" => "SORT",
        "SORT_ORDER1" => "DESC",
        "SORT_BY2" => "",
        "SORT_ORDER2" => "",
        "FILTER_NAME" => "salon_master_filter",
        "FIELD_CODE" => array(),
        "PROPERTY_CODE" => array(),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "PREVIEW_TRUNCATE_LEN" => "",
        "ACTIVE_DATE_FORMAT" => "d F Y",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "N",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => $_REQUEST["CODE"],
        "INCLUDE_SUBSECTIONS" => "Y",
        "DISPLAY_DATE" => "Y",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "Y",
        "DISPLAY_PREVIEW_TEXT" => "Y",
        "PAGER_TEMPLATE" => "pagination",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "PAGER_TITLE" => "",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "Y",
        "SALON_NAME" => $salon["NAME"]
    )
);?>

<div class="inner-wrapper">
    <a href="/online-recording/?m0=<?=$arResult["ID"];?>" class="button">Записаться к мастеру</a>
    <a href="<?=$salon["DETAIL_PAGE_URL"];?>" class="button solid" style="margin: 0 70px 0 10px">Перейти к странице салона</a>
    <a href="/salons/" class="is-link">Посмотреть другие салоны</a>
</div>

<br>
<br>
<br>