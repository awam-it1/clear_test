<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// echo '<script>var log_php_object = '.json_encode($arResult).';</script>';


// отзывы
    $rsReviews = CIBlockElement::GetList(
        Array("SORT" => "ASC")
        , Array("IBLOCK_ID" => 18, "ACTIVE" => "Y", "PROPERTY_MASTER" => $arResult["ID"])
        , false
        , false
        , Array("PROPERTY_RATING")
    );

    $arResult["REVIEWS_COUNT"] = 0;
    $arResult["RATING"] = 0;
    while ($arReviews = $rsReviews->Fetch()) {
        $arResult["REVIEWS_COUNT"]++;
        $arResult["RATING"] += $arReviews["PROPERTY_RATING_VALUE"];
    }

    if ($arResult["REVIEWS_COUNT"]) {
        $arResult["RATING"] = floatval(number_format($arResult["RATING"] / $arResult["REVIEWS_COUNT"], 1, ".", ""));
    }
?>