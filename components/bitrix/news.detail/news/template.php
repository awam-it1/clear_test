<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
echo '<script>var log_php_object = '.json_encode($arResult).';</script>';
?>
<h1><?=$arResult["NAME"];?></h1>
<div class="separator">
	<span class="news-inner__date"><?=$arResult["DISPLAY_ACTIVE_FROM"];?></span>
</div>
<div class="anotation">
	<p></p>
</div>
<div class="news-inner__content is-styled">
	<div class="image">
		<?if (strlen($arResult["DETAIL_PICTURE"]["SRC"])):?>
		<div class="image-right">
			<img src="<?=$arResult["DETAIL_PICTURE"]["SRC"];?>" alt="<?=$arResult["NAME"];?>">
			<!-- <div class="image-descrption"></div> -->
		</div>
		<?endif;?>
		<p class="start-p"><?=$arResult["~DETAIL_TEXT"];?></p>
		<a href="../" class="button with-arrow">Вернуться ко всем новостям</a>
	</div>
</div>

<?
GLOBAL $news_slider_filter;
$news_slider_filter = Array("!ID" => $arResult["ID"]);

$APPLICATION->IncludeComponent("bitrix:news.list", "news_slider",
	Array(
		"IBLOCK_TYPE" => "blocks",
		"IBLOCK_ID" => "5",
		"NEWS_COUNT" => "8",
		"SORT_BY1" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "",
		"SORT_ORDER2" => "",
		"FILTER_NAME" => "news_slider_filter",
		"FIELD_CODE" => array("ACTIVE_FROM"),
		"PROPERTY_CODE" => array(),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d F Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"PAGER_TEMPLATE" => "pagination",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "Y"
	)
);?>