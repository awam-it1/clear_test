<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// echo '<script>var log_php_object = '.json_encode($arResult).';</script>';
?>

<div class="salon-header-banner">
	<div class="shb-inner" style="background-image:url('<?=$arResult["DETAIL_PICTURE"]["SRC"];?>')"></div>
	<div class="shb-middle">
		<div class="shb-header"><?=$arResult["DISPLAY_PROPERTIES"]["NAME"]["~VALUE"];?></div>
		<div class="shb-order-link">
			<a class="button" href="/online-recording/<?=(!empty($arResult["ID"])) ? '?s='.$arResult["ID"] : ''?>">
				Записаться в салон
			</a>
		</div>
	</div>
</div>

<div class="inner-wrapper min-width">

	<div class="salon-header-card" data-is="salonInnerCard">
		<div class="shc-content">
			<div class="is-columns">
                <?if (strlen($arResult["DISPLAY_PROPERTIES"]["ADDRESS"]["VALUE"])):?>
                    <div class="col-2">
                        <div class="shc-meta-a">
                            <img src="/dist/img/temp/icon7.png" />
                            <?=$arResult["DISPLAY_PROPERTIES"]["ADDRESS"]["VALUE"];?>
                            <br/>
                            <?if (strlen($arResult["DISPLAY_PROPERTIES"]["ADDRESS_SUB"]["VALUE"])):?>
                                <span><?=$arResult["DISPLAY_PROPERTIES"]["ADDRESS_SUB"]["VALUE"];?></span>
                            <?endif;?>
                        </div>
                    </div>
                <?endif;?>
                <?if (strlen($arResult["METRO"])):?>
				<div class="col-2">
					<div class="shc-meta-a">
						<img src="/dist/img/temp/icon5.png" />
						<?=$arResult["METRO"];?>
					</div>
				</div>
                <?endif;?>
			</div>
			<div class="is-columns">
                <?if (!empty($arResult["DISPLAY_PROPERTIES"]["PHONES"]["VALUE"]) && count($arResult["DISPLAY_PROPERTIES"]["PHONES"]["VALUE"])):?>
                    <div class="col-2">
                        <div class="shc-meta-a">
                            <img src="/dist/img/temp/icon6.png" />
                            <?foreach($arResult["DISPLAY_PROPERTIES"]["PHONES"]["VALUE"] as $phone):?>
                                <a href="tel:<?=preg_replace("/([^0-9]+)/", "", $phone);?>" class="comagic_phone"><?=$phone;?></a>
                                <br/>
                            <?endforeach;?>
                        </div>
                    </div>
                <?endif;?>
                <?if (strlen($arResult["DISPLAY_PROPERTIES"]["TIME"]["VALUE"])):?>
				<div class="col-2">
					<div class="shc-meta-a">
						<img src="/dist/img/temp/icon8.png" />
						<?=$arResult["DISPLAY_PROPERTIES"]["TIME"]["VALUE"];?>
					</div>
				</div>
                <?endif;?>
			</div>
			
			<?if (count($arResult["SERVICES"])):?>
			<div class="salon-services-list">
				<?foreach($arResult["SERVICES"] as $service):?>
				<div class="ss-l-item">
					<div class="ss-l-image">
						<img src="<?=$service["IMAGE"];?>" />
					</div>
					<div class="ss-l-label"><?=$service["NAME"];?></div>
				</div>
				<?endforeach;?>
			</div>
			<?endif;?>

			<div class="shc-action">
				<a class="button" href="/online-recording/<?=(!empty($arResult["ID"])) ? '?s='.$arResult["ID"] : ''?>">
					Записаться
				</a>
				<a class="button solid" href="<?=$arResult["DETAIL_PAGE_URL"];?>">
					Подробнее
				</a>
			</div>
		</div>
		<?if (strlen($arResult["DISPLAY_PROPERTIES"]["MAP"]["VALUE"])):?>
		<div class="shc-map">
			<div class="shc-gmap" data-value='<?

			list($lat, $long) = explode(",", $arResult["DISPLAY_PROPERTIES"]["MAP"]["VALUE"]);

			echo json_encode(Array("lat" => $lat, "lng" => $long, "zoom" => "8"));

			?>'>
				
			</div>
		</div>
		<?endif;?>
	</div>
</div>
