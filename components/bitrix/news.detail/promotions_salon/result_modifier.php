<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CModule::IncludeModule("highloadblock");
use Bitrix\Highloadblock as HL; 
use Bitrix\Main\Entity;

// print_r($arResult);
// сервисы
	$arResult["SERVICES"] = Array();

	if (count($arResult["DISPLAY_PROPERTIES"]["SERVICES"]["VALUE"])) {
		$hlblock = HL\HighloadBlockTable::getById(1)->fetch(); 
		$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
		$main_query = new Entity\Query($entity);
		$main_query->setSelect(array('*'));
		$main_query->setFilter(array('UF_XML_ID' => $arResult["DISPLAY_PROPERTIES"]["SERVICES"]["VALUE"]));
		$result = $main_query->exec();
		$result = new CDBResult($result);

		while ($row = $result->Fetch()) {
			$arResult["SERVICES"][] = Array("NAME" => $row["UF_NAME"], "IMAGE" => CFile::GetPath($row["UF_FILE"]));
		}
	}
// метро
	$arResult["METRO"] = "";
	$rsMetro = CIBlockSection::GetByID($arResult["IBLOCK_SECTION_ID"]);
	if ($arMetro = $rsMetro->GetNext()) {
		if (strlen($arMetro["IBLOCK_SECTION_ID"])) {
			$arResult["METRO"] = $arMetro["NAME"];
		}
	}
?>