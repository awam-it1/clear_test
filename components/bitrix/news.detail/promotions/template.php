<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>
<h1><?=$arResult['NAME'];?></h1>
<div class="separator">
	<span class="news-inner__date"><?=$arResult['PROPERTIES']['DATE_BEGIN']['VALUE']?></span>
</div>
<div class="anotation">
	<p></p>
</div>
<div class="news-inner__content is-styled">
	<div class="image">
		<?if (!empty($arResult['DETAIL_PICTURE']['SRC'])):?>
		<div class="image-right">
			<img src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" alt="<?=$arResult['NAME'];?>">
		</div>
		<?endif;?>
        <?=$arResult['~DETAIL_TEXT'];?>
    </div>
</div>
<?if (!empty($arResult['SALON_ID'])) {
	$APPLICATION->IncludeComponent('bitrix:news.detail', 'promotions_salon', [
        'USE_SHARE' => 'N',
        'SHARE_HIDE' => 'Y',
        'SHARE_TEMPLATE' => '',
        'SHARE_HANDLERS' => ['delicious'],
        'SHARE_SHORTEN_URL_LOGIN' => '',
        'SHARE_SHORTEN_URL_KEY' => '',
        'AJAX_MODE' => 'N',
        'IBLOCK_TYPE' => 'salons',
        'IBLOCK_ID' => '6',
        'ELEMENT_ID' => $arResult['SALON_ID'],
        'ELEMENT_CODE' => '',
        'CHECK_DATES' => 'N',
        'FIELD_CODE' => [],
        'PROPERTY_CODE' => ['ADDRESS', 'PHONES', 'ADDRESS_SUB', 'TIME', 'SERVICES', 'TITLE', 'NAME', 'MAP'],
        'IBLOCK_URL' => '',
        'DETAIL_URL' => '',
        'SET_TITLE' => 'N',
        'SET_CANONICAL_URL' => 'N',
        'SET_BROWSER_TITLE' => 'N',
        'BROWSER_TITLE' => '-',
        'SET_META_KEYWORDS' => 'N',
        'META_KEYWORDS' => '-',
        'SET_META_DESCRIPTION' => 'N',
        'META_DESCRIPTION' => '-',
        'SET_STATUS_404' => 'N',
        'SET_LAST_MODIFIED' => 'Y',
        'INCLUDE_IBLOCK_INTO_CHAIN' => 'N',
        'ADD_SECTIONS_CHAIN' => 'N',
        'ADD_ELEMENT_CHAIN' => 'N',
        'ACTIVE_DATE_FORMAT' => 'd.m.Y',
        'USE_PERMISSIONS' => 'N',
        'CACHE_TYPE' => 'A',
        'CACHE_TIME' => '3600',
        'CACHE_GROUPS' => 'Y',
        'DISPLAY_TOP_PAGER' => 'Y',
        'DISPLAY_BOTTOM_PAGER' => 'Y',
        'PAGER_TITLE' => 'Страница',
        'PAGER_TEMPLATE' => '',
        'PAGER_SHOW_ALL' => 'Y',
        'PAGER_BASE_LINK_ENABLE' => 'Y',
        'SHOW_404' => 'N',
        'MESSAGE_404' => '&nbsp',
        'STRICT_SECTION_CHECK' => 'Y',
        'PAGER_BASE_LINK' => '',
        'PAGER_PARAMS_NAME' => 'arrPager',
        'AJAX_OPTION_JUMP' => 'N',
        'AJAX_OPTION_STYLE' => 'Y',
        'AJAX_OPTION_HISTORY' => 'N'
	]);
}?>
<div class="promotion-bottom">
    <a href="/promotions/" class="button with-arrow is-bottom">Вернуться ко всем акциям</a>
</div>
