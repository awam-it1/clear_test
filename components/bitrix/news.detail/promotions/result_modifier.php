<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
\Bitrix\Main\Loader::includeModule('dev2fun.opengraph');
\Dev2fun\Module\OpenGraph::Show($arResult['ID'], 'element');

$arResult['SECTION'] = CIBlockSection::GetByID($arResult['IBLOCK_SECTION_ID'])->GetNext() ?? [];

$date = ParseDateTime($arResult['PROPERTIES']['DATE_BEGIN']['VALUE'], FORMAT_DATETIME);
$arResult['PROPERTIES']['DATE_BEGIN']['VALUE'] = $date['DD'] . ' ' . ToLower(GetMessage('MONTH_' . intval($date['MM']) . '_S')) . ' ' . $date['YYYY'];

$arResult['SALON_ID'] = empty($arResult['PROPERTIES']['SALON']['VALUE']) || count($arResult['PROPERTIES']['SALON']['VALUE']) > 1 ? 0 : $arResult['PROPERTIES']['SALON']['VALUE'][0];
