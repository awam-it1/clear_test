<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}

$APPLICATION->SetPageProperty("description",  'Салон Тайрай '.$arResult["DISPLAY_PROPERTIES"]["NAME"]["~VALUE"]);
$APPLICATION->SetPageProperty('og:title', $arResult["DISPLAY_PROPERTIES"]["NAME"]["~VALUE"]);
?>

<div class="salon-header-banner">
    <?$file = CFile::ResizeImageGet($arResult["DETAIL_PICTURE"], array('width'=>1980, "height"=>520), BX_RESIZE_IMAGE_PROPORTIONAL, true);
    //$file['src']?>
    <div class="shb-inner" style="background-image:url('<?=$file['src'];?>')"></div>
    <div class="shb-middle">
        <div class="shb-header">Салон Тайрай <?=$arResult["DISPLAY_PROPERTIES"]["NAME"]["~VALUE"];?></div>
        <div class="shb-order-link">
            <a class="button" href="/online-recording/?s=<?=$arResult["ID"];?>">
                Записаться в салон
            </a>
        </div>
    </div>
</div>

<div class="inner-wrapper min-width">

    <div class="salon-header-card" ref="salonInnerCard">
        <div class="shc-content"  v-cloak="">
            <div class="is-columns">
                <?if (strlen($arResult["DISPLAY_PROPERTIES"]["ADDRESS"]["VALUE"])) :?>
                    <div class="col-2">
                        <div class="shc-meta-a">
                            <img src="/dist/img/temp/icon7.png" />
                            <?=$arResult["DISPLAY_PROPERTIES"]["ADDRESS"]["VALUE"];?>
                            <br/>
                            <?if (strlen($arResult["DISPLAY_PROPERTIES"]["ADDRESS_SUB"]["VALUE"])) :?>
                        <span><?=$arResult["DISPLAY_PROPERTIES"]["ADDRESS_SUB"]["VALUE"];?></span>
                        <?endif;?>
                        </div>
                    </div>
                <?endif;?>
                <?if (strlen($arResult["METRO"])) :?>
                <div class="col-2">
                    <div class="shc-meta-a">
                        <img src="/dist/img/temp/icon5.png" />
                        <?=$arResult["METRO"];?>
                    </div>
                </div>
                <?endif;?>
            </div>
            <div class="is-columns">
                <?if (!empty($arResult["DISPLAY_PROPERTIES"]["PHONES"]["VALUE"]) && count($arResult["DISPLAY_PROPERTIES"]["PHONES"]["VALUE"])) :?>
                    <div class="col-2">
                        <div class="shc-meta-a">
                            <img src="/dist/img/temp/icon6.png" />
                            <?foreach ($arResult["DISPLAY_PROPERTIES"]["PHONES"]["VALUE"] as $phone) :?>
                                <a href="tel:<?=preg_replace("/([^0-9]+)/", "", $phone);?>" class="comagic_phone"><?=$phone;?></a>
                                <br/>
                            <?endforeach;?>
                        </div>
                    </div>
                <?endif;?>
                <?if (strlen($arResult["DISPLAY_PROPERTIES"]["TIME"]["VALUE"])) :?>
                <div class="col-2">
                    <div class="shc-meta-a">
                        <img src="/dist/img/temp/icon8.png" />
                        <?=$arResult["DISPLAY_PROPERTIES"]["TIME"]["VALUE"];?>
                    </div>
                </div>
                <?endif;?>
            </div>
            
            <?if (count($arResult["SERVICES"])) :?>
            <div class="salon-services-list">
                <?foreach ($arResult["SERVICES"] as $service) :?>
                <div class="ss-l-item">
                    <div class="ss-l-image">
                        <img src="<?=$service["IMAGE"];?>" />
                    </div>
                    <div class="ss-l-label"><?=$service["NAME"];?></div>
                </div>
                <?endforeach;?>
            </div>
            <?endif;?>

            <div class="shc-action">
                <a class="button" href="/online-recording/?s=<?=$arResult["ID"];?>">
                    Записаться
                </a>
                <a class="button solid" href="#review-modal" data-modal-open="">
                    Оставить отзыв
                </a>

                <div id="review-modal" class="modal-content-inner">
                    <div class="simple-modal is-styled">
                        <form action="/ajax/reviewNew.php"
                              data-vv-scope="salon-card-form"
                              @submit.prevent="submit('salon-card-form', 'cardReviewForm')"
                              ref="cardReviewForm"
                              method="POST"
                              enctype="multipart/form-data">
                            <div class="title-h1">
                                <span>Добавить отзыв</span>
                            </div>
                            <div class="reviews-rating"  :class="{ 'has-error' : errors.has('salon-card-form.rating') }">
                                <span class="rating-error-msg">Поставьте оценку</span>
                                <star-rating :border-width="6"
                                             active-color="#ffbf35"
                                             border-color="#ffbf35"
                                             :increment="0.5"
                                             inactive-color="#fff"
                                             :padding="2"
                                             :star-size="18"
                                             :show-rating="false"
                                             :inline="true"
                                             v-model="rating"
                                ></star-rating>
                                <input type="hidden" name="rating"  v-model="rating"  v-validate="'required'" />
                            </div>
                            <div class="input-element" :class="{ 'has-error' : errors.has('salon-card-form.name') }">
                                <label>
                                <span class="form-item-label">
                                    Имя <span class="required">*</span>
                                </span>
                                    <span class="form-item-error">Проверьте правильность ввода</span>
                                    <input type="text" name="name" v-validate="'required|alpha_spaces'" v-model="form.name"/>
                                </label>
                            </div>
                            <div class="input-element" :class="{ 'has-error' : errors.has('salon-card-form.email') }">
                                <label>
                                <span class="form-item-label">
                                    E-mail <span class="required">*</span>
                                </span>
                                    <span class="form-item-error">Проверьте правильность ввода</span>
                                    <input type="text" name="email" v-validate="'required|email'" v-model="form.email"/>
                                </label>
                            </div>
                            <div class="input-element"  :class="{ 'has-error' : errors.has('salon-card-form.text') }">
                                <label>
                                <span class="form-item-label">
                                    Текст сообщения <span class="required">*</span>
                                </span>
                                    <span class="form-item-error">Проверьте правильность ввода</span>
                                    <textarea row="5" name="text" v-validate="'required'" v-model="form.msg"></textarea>
                                </label>
                            </div>
                            <div class="reviews-form-send" >
                                <div class="tairai__recaptcha" v-pre="">
                                    <div id="salons-recaptcha" data-recaptcha=""></div>
                                    <input data-recaptcha-id="salons-recaptcha" type="hidden" v-validate="'required|in:Y'" />
                                </div>
                                <input type="hidden" name="iblock_submit" value="Сохранить">
                                <button class="button" type="submit" name="iblock_submit" value="Сохранить">Отправить</button>
                            </div>
                            <input type="hidden" name="objectid" value="<?=(!isset($arParams["REVIEW_OBJ_ID"]) ? $arResult["ID"] : $arParams["REVIEW_OBJ_ID"]) ;?>">
                            <input type="hidden" name="type" value="<?=$arParams["REVIEW_TYPE"];?>">
                            <div class="form-required">
                                <span class="required">*</span> — поля обязательные для заполнения
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?if (strlen($arResult["DISPLAY_PROPERTIES"]["MAP"]["VALUE"])) :?>
        <div class="shc-map">
            <div class="shc-gmap" data-value='<?

            list($lat, $long) = explode(",", $arResult["DISPLAY_PROPERTIES"]["MAP"]["VALUE"]);

            echo json_encode(array("lat" => $lat, "lng" => $long, "zoom" => "18"));

            ?>'>
            </div>
        </div>
        <?endif;?>
    </div>
    
    <div class="salon-preview-text">
        <div class="spt-header">
            <div class="row">
                <div class="col">
                    <h2><?=$arResult["DISPLAY_PROPERTIES"]["TITLE"]["VALUE"];?></h2>
                </div>
                <div class="col">
                    <div class="salon-sharing-cnt" id="salon-sharing">
                        <a href="#" class="salon-sharing-link"  @click.prevent="showSocial">
                            <span class="salon-sharing-icon">
                                <svg xmlns:xlink="http://www.w3.org/1999/xlink" width="18" height="18">
                                    <defs>
                                        <path id="salon-sharing__a" d="M1109.69 1249.44a3.3 3.3 0 0 0-2.59 1.26l-5.61-2.85c.07-.28.12-.56.12-.85 0-.32-.06-.63-.15-.92l5.6-2.84a3.3 3.3 0 0 0 5.94-1.96 3.3 3.3 0 0 0-3.31-3.28 3.3 3.3 0 0 0-3.3 3.28c0 .3.05.58.12.85l-5.62 2.85a3.3 3.3 0 0 0-5.89 2.02 3.3 3.3 0 0 0 5.94 1.96l5.6 2.84c-.1.29-.16.6-.16.92a3.3 3.3 0 0 0 3.3 3.28 3.3 3.3 0 0 0 3.32-3.28 3.3 3.3 0 0 0-3.31-3.28z"/>
                                    </defs>
                                    <use fill="#c7b179" transform="translate(-1095 -1238)" xlink:href="#salon-sharing__a"/>
                                </svg>
                            </span>
                            <span class="salon-sharing-text">Поделиться</span>
                        </a>
                        <div class="salon-sharing-soc" v-show="isShowSocial" v-cloak="">
                            <social-sharing :url="url"
                                            :title="title"
                                            :description="description"
                                            inline-template>
                                <div class="lks-social">
                                    <div class="gfs-item">
                                        <network network="facebook">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32"><path fill="#8a007c" d="M13.65 16.55v8.62h3.74c.02-2.27 0-4.6 0-6.87v-2.26l.02-.03s2.37.02 2.45-.01l.05-.01.32-3.14H17.4v-1.41c0-.42-.01-.95.25-1.18l2.57-.26V6.85c-.49-.03-1.07-.01-1.57-.01-.51 0-1.04-.01-1.55.03-.97.06-1.75.35-2.31.83-.82 1.32-1.14.19-1.15 5.15h-1.87v3.16h1.86c.05.06.02.47.02.54zM32 16a16 16 0 1 1-32 0 16 16 0 0 1 32 0z"/></svg>
                                        </network>
                                    </div>
                                    <div class="gfs-item">
                                        <network network="vk">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32"><path fill="#8a007c" d="M7.25 12.3c.14-.04.27-.06.37-.06h.14l.35.01.48.02.56-.03c.16-.01.3-.02.43-.01l.34.01c.2.02.36.06.48.11.06.03.12.1.19.2s.13.19.17.28l.17.38.15.31c.37.82.84 1.62 1.39 2.42l.11.16.12.19.14.17c.05.07.1.12.15.16l.16.13c.05.04.11.07.17.08h.17c.28-.05.44-.63.46-1.73l.02-.52a3.5 3.5 0 0 0-.22-1.51.73.73 0 0 0-.23-.26c-.1-.07-.22-.13-.37-.18-.14-.06-.26-.11-.34-.16.14-.27.39-.46.74-.55a6 6 0 0 1 1.43-.13h.78a11.42 11.42 0 0 0 .79.05l.33.08.28.14c.2.09.26.16.19.21.09.17.14.38.16.62l.02.21c0 .19-.02.45-.05.77l-.06.77a5.07 5.07 0 0 0-.01 1.24c.04.44.19.74.44.91.09-.01.17-.03.25-.06a.8.8 0 0 0 .23-.17c.08-.07.13-.13.16-.17l.19-.24.15-.21c.47-.61.93-1.42 1.4-2.42l.13-.33.17-.39c.11-.26.17-.36.18-.29 0 .07.08 0 .23-.2.16-.07.34-.1.56-.1h.13l.69.03.69.03.41-.02.48-.02c.14-.01.29 0 .44.01.15.01.28.04.38.09.1.04.17.11.21.19a.4.4 0 0 1 .04.19c0 .29-.17.72-.52 1.29a8.8 8.8 0 0 1-.51.78l-.68.86-.54.69-.21.26-.26.36c-.06.08-.11.19-.17.33-.06.15-.08.28-.08.4 0 .08.02.15.05.22a1.75 1.75 0 0 0 .29.4l.2.19.18.17c.7.64 1.25 1.22 1.66 1.73.39.51.59.88.59 1.11 0 .33-.25.53-.76.61a4.93 4.93 0 0 1-1.42.01 5.5 5.5 0 0 0-.55-.03c-.25 0-.48.03-.66.08h-.09c-.66-.11-1.34-.57-2.06-1.4a2.8 2.8 0 0 1-.26-.3c-.13-.16-.23-.29-.32-.38a1.87 1.87 0 0 0-.34-.25.62.62 0 0 0-.4-.09c-.23.04-.38.19-.46.43a4.2 4.2 0 0 0-.13.88 1.6 1.6 0 0 1-.14.72c-.13.24-.52.36-1.17.36-.28 0-.6-.02-.98-.06a6.16 6.16 0 0 1-3.59-1.59 14.3 14.3 0 0 1-1.2-1.38c-1.03-1.31-2-2.9-2.92-4.77l-.16-.33-.18-.38a5.05 5.05 0 0 1-.26-.76c-.02-.14-.04-.27-.04-.4a.9.9 0 0 1 .37-.17zM16 0a16 16 0 1 0 0 32 16 16 0 0 0 0-32z"/></svg>
                                        </network>
                                    </div>
                                </div>
                            </social-sharing>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <p>
            <?=$arResult["~DETAIL_TEXT"];?>
        </p>
    </div>


