<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
echo '<script>var log_php_object = '.json_encode($arResult).';</script>';

if (count($arResult["ITEMS"])>2):?>
    <div class="separator">
        <form action="" class="js-news-filter">
            <div class="simple-select ">
                <select name="salon" id="" class="simple-select js-news-salon" data-is="salonsCitySelect">
                    <?foreach($arResult["ITEMS"] as $arItem):?>
                        <option value="<?=$arItem["id"];?>" <?if ($arItem["id"] == $arParams["SELECT_SALON"]):?> selected<?endif;?>><?=$arItem["name"];?></option>
                    <?endforeach;?>
                </select>
            </div>
        </form>
    </div>
<? elseif (count($arResult["ITEMS"])): ?>
    <div class="separator">
        <div class="choices">
            <?= $arResult["ITEMS"][count($arResult["ITEMS"])-1]["name"]; ?>
        </div>
    </div>
<?endif;?>