<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// echo '<script>var log_php_object = '.json_encode($arResult).';</script>';
// сихронизируем с модулем услуг
global $services_count;
global $page;
?>
<div class="shop__nav-links mobile-hidden">
<?foreach($arResult as $arItem):
	if(($services_count[$arItem["TEXT"]]<1) && (strripos($page, 'lk')===false) && (strripos($page, 'style')===false))
        continue;
    ?>
	<div class="shop__nav-link<?if ($arItem["SELECTED"]) echo " is-active";?>">
		<?if($arItem["TEXT"] == 'СПА-программы для женщин и мужчин'){$link_name = 'СПА-программы';} else {$link_name = $arItem["TEXT"];}?>
		<?/*<a<?if (!$arItem["SELECTED"]) echo ' href="'.$arItem["LINK"].'"';?>><?=$arItem["TEXT"];?></a>*/?>
		<a<?if (!$arItem["SELECTED"]) echo ' href="'.$arItem["LINK"].'"';?>><?=$link_name;?></a>
	</div>
<?endforeach;?>
</div>
<div class="mobile-select mobile-visible" >
	<form action="">
		<select name="shop-nav" data-is="redirectSelect" ref="redirectSelect">
        <?if($arParams["HIDE_ITEM_ALL_SERVICES_ON_MOBILE"] !== "Y"):?>
            <option selected value="/services/">Все услуги</option>
        <?endif?>
		<?foreach($arResult as $arItem):
			if(($services_count[$arItem["TEXT"]]<1) && (strripos($page, 'lk')===false) && (strripos($page, 'style')===false))
				continue;
            ?>
			<option<?if ($arItem["SELECTED"]) echo " selected";?> value="<?=$arItem["LINK"];?>"><?=$arItem["TEXT"];?></option>
		<?endforeach;?>
		</select>
		<span class="mobile-select__icon"></span>
	</form>
</div>