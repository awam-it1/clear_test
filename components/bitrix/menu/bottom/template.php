<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="gf-bottom">
	<nav class="bottom-menu">
		<?foreach($arResult as $arItem):?>
		<a href="<?=$arItem["LINK"];?>"><?=$arItem["TEXT"];?></a>
		<?endforeach;?>
	</nav>
	<a href="https://itech-group.ru/" class="itech-group">Разработано в ITECH.group</a>
</div>