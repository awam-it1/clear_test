<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>
<nav class="tb-top-menu mobile-hidden">
	<?foreach($arResult as $arItem):
	if(!empty($arItem['PARAMS']['DONT_SHOW_MENU']))
	    continue;
        ?>
	<a href="<?=$arItem["LINK"];?>" class="tm-link"><?=$arItem["TEXT"];?></a>
	<?endforeach;?>
</nav>