<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$arResult["JS_FILTER"] = Array();
foreach ($arResult["ITEMS"] as $arItem) {
	if ($arItem["CODE"] == "BASE")
		continue;
	
	$values = Array();

	foreach ($arItem["VALUES"] as $value) {
		$values["name"] = Array(
			"name" => $value["VALUE"]
			, "text" => $value["VALUE"]
			, "isFilter" => false
			);
	}

	$arResult["JS_FILTER"][0][] = Array(
		"name" => $arItem["CODE"]
		, "text" => $arItem["NAME"]
		, "isFilter" => false
		, "isOpen" => $arItem["DISPLAY_EXPANDED"]
		, "values" => $values
		);
}
?>