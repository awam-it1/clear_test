<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die(); ?>

<div class="gfsl-item">
	<div class="gf-i-icon">
		<svg xmlns="http://www.w3.org/2000/svg"
			xmlns:xlink="http://www.w3.org/1999/xlink" width="19" height="22">
      <defs>
        <path id="gf-i-icon__a"
				d="M119.19 205.67a8.82 8.82 0 0 1 .05 12.87L112.5 225l-6.73-6.46a8.8 8.8 0 0 1 0-12.82l.05-.05a9.7 9.7 0 0 1 13.37 0c-.05-.04-.05-.04 0 0z" />
        <path id="gf-i-icon__b"
				d="M111.54 213.6c.24.53.32 1.07.34 1.4v.36s-.13.1-.36.2a3.9 3.9 0 0 1-1.87.38 2.5 2.5 0 0 1-1.05-.3c-.4-.22-.76-.56-1.08-.94a8.04 8.04 0 0 1-.73-1.07c-.32-.56-.56-1.09-.68-1.39l-.11-.27s.71-.12 1.62-.14h.25c.66 0 1.36.05 1.99.27.4.14.79.35 1.1.66.25.25.44.54.58.84zm2.82-1.41a6.19 6.19 0 0 1 2.27-.36 12.23 12.23 0 0 1 1.82.14s-.15.4-.4.94c-.25.48-.58 1.1-1 1.63-.3.38-.62.72-.98.96-.32.21-.67.35-1.04.41-.22.04-.46.06-.7.06a6.79 6.79 0 0 1-1.17-.11c-.22 0-.27 1.63-.28 2.54v.6h-.4c0-.54.01-1.07.03-1.61.01-.77.04-1.54.06-2.3.02-.68.03-1.53.84-2.28.27-.26.6-.48.95-.62zm-1.1-.2a4.44 4.44 0 0 1-1 .78s-.24-.12-.57-.36a4.27 4.27 0 0 1-.5-.45c-.5-.52-.84-1.2-.85-1.93-.01-1.1.7-2.4 1.24-3.22a12.12 12.12 0 0 1 .5-.72l.08-.09a9.13 9.13 0 0 0 .67.87c.63.91 1.3 2.04 1.28 3.19a2.88 2.88 0 0 1-.84 1.92z" />
      </defs>
      <use fill="#d7c091" transform="translate(-103 -203)"
				xlink:href="#gf-i-icon__a" />
      <use fill="#fff" transform="translate(-103 -203)"
				xlink:href="#gf-i-icon__b" />
    </svg>
	</div>
	<div class="tra-select is-dark">
		<select class="form-control" data-trigger data-is="headerSelect"
			name="choices-single-default">
			<? foreach ($arResult["CITIES_DATA"] as $id => $city) :
				if ($id == $arResult["CITY"]) {
					$_SESSION['CURRENT_CITY_CODE'] = $arResult["CITIES_ID_BY_CODE"][$id];
				} ?>
			<option value="<?=$city['URL'] ?>"<?if ($id == $arResult["CITY"]) echo " selected";?>><?=$city['NAME'];?></option>
			<?endforeach;?>
		</select>
	</div>
</div>