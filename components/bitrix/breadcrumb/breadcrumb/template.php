<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$r = '<div class="inner-wrapper full-width mobile-hidden"><div class="is-breadcrumbs">';
for ($i = 0; $i < count($arResult); $i++) {
	$item = $arResult[$i];
	
	if ($i != count($arResult) - 1) {
		$r .= '<a href="'.$item["LINK"].'">'.htmlspecialcharsex($item["TITLE"]).'</a><i></i>';
	} else {
		$r .= '<div>'.htmlspecialcharsex($item["TITLE"]).'</div>';
	}
}
$r .= '</div></div>';
return $r;