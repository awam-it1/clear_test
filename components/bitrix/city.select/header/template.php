<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>
<div class="tra-select mobile-hidden">
	<div class="tb-inner-icon">
		<svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 15 15">
            <path fill="#d4be6f" d="M12.08 1.82a6 6 0 0 1 .03 8.78L7.5 15l-4.61-4.4a6 6 0 0 1 0-8.74l.04-.04a6.66 6.66 0 0 1 9.15 0c-.04-.03-.04-.03 0 0z"/>
            <path fill="#3c0036" d="M6.79 7.18c.17.36.22.73.23.96l.01.24-.26.14c-.24.12-.65.27-1.11.27l-.16-.01a1.8 1.8 0 0 1-.72-.21 2.77 2.77 0 0 1-.74-.64 4.98 4.98 0 0 1-.5-.73 8.91 8.91 0 0 1-.47-.95L3 6.07s.49-.08 1.11-.09l.17-.01c.45 0 .93.04 1.36.19.28.1.54.24.75.45.18.17.31.37.4.57zm1.93-.96a4.27 4.27 0 0 1 1.55-.25c.45 0 .85.05 1.07.08l.18.02s-.1.27-.28.64c-.16.33-.39.75-.68 1.11-.2.26-.42.49-.67.66a2.07 2.07 0 0 1-1.19.32 4 4 0 0 1-.79-.08H7.9c-.15 0-.18 1.11-.19 1.73v.41h-.27l.01-1.09.05-1.57c.01-.46.01-1.05.57-1.56.19-.18.41-.32.65-.42zm-.17-1.45c.01-.78-.45-1.55-.88-2.17a1.8 1.8 0 0 0-.18-.25L7.22 2l-.05.06-.14.18-.21.31a4.43 4.43 0 0 0-.85 2.2c.01.5.24.96.58 1.31a3.12 3.12 0 0 0 .74.55s.18-.09.41-.28c.09-.07.18-.15.27-.25.29-.31.57-.75.58-1.31z"/>
            <path fill="#d7c091" d="M12.08 1.82a6 6 0 0 1 .03 8.78L7.5 15l-4.61-4.4a6 6 0 0 1 0-8.74l.04-.04a6.66 6.66 0 0 1 9.15 0c-.04-.03-.04-.03 0 0z"/>
            <path fill="#3c0036" d="M6.79 7.18c.17.36.22.73.23.96l.01.24-.26.14c-.24.12-.65.27-1.11.27l-.16-.01a1.8 1.8 0 0 1-.72-.21 2.77 2.77 0 0 1-.74-.64 4.98 4.98 0 0 1-.5-.73 8.91 8.91 0 0 1-.47-.95L3 6.07s.49-.08 1.11-.09l.17-.01c.45 0 .93.04 1.36.19.28.1.54.24.75.45.18.17.31.37.4.57zm1.93-.96a4.27 4.27 0 0 1 1.55-.25c.45 0 .85.05 1.07.08l.18.02s-.1.27-.28.64c-.16.33-.39.75-.68 1.11-.2.26-.42.49-.67.66a2.07 2.07 0 0 1-1.19.32 4 4 0 0 1-.79-.08H7.9c-.15 0-.18 1.11-.19 1.73v.41h-.27l.01-1.09.05-1.57c.01-.46.01-1.05.57-1.56.19-.18.41-.32.65-.42zm-.17-1.45c.01-.78-.45-1.55-.88-2.17a1.8 1.8 0 0 0-.18-.25L7.22 2l-.05.06-.14.18-.21.31a4.43 4.43 0 0 0-.85 2.2c.01.5.24.96.58 1.31a3.12 3.12 0 0 0 .74.55s.18-.09.41-.28c.09-.07.18-.15.27-.25.29-.31.57-.75.58-1.31z"/>
        </svg>
	</div>
    <div id="location"></div>
	<select class="form-control" data-trigger data-is="headerSelect" name="choices-single-default">
	<?foreach($arResult['CITIES_FOR_VIEW'] as $id => $name):?>
		<option value="<?=$arResult['CITIES_DATA'][$id]['URL'] . $GLOBALS['APPLICATION']->GetCurPageParam('CITY=' . $id, ['CITY', 'formresult'])?>"<?=$id==$arResult['CITY'] ? ' selected' : ''?>><?=$name?></option>
	<?endforeach;?>
	</select>
</div>
<?if ($arResult['CITY_ANOTHER_ALERT']):?>
    <div id="anotherCityAlert">
        <p id="textcity">
            Мы думаем, что ваш город - <span class="city"><?=$arResult['DETECTED_DATA']['NAME']?></span>,
            у нас нет салонов в этом городе и мы показываем информацию для Москвы.
            Если вы хотите посмотреть информацию для другого города, выберите его из списка.
        </p>
        <div class="buttonsblock">
            <div class="button" onclick="window.location='<?=$arResult['CITIES_DATA'][DEFAULT_CITY_ID]['URL'] . $GLOBALS['APPLICATION']->GetCurPageParam('CITY=' . DEFAULT_CITY_ID, ['CITY', 'formresult'])?>'">Оставить Москву</div>
            <div class="button" onclick="document.getElementById('anotherCityAlert').style.display='none'">Выбрать другой город</div>
        </div>
    </div>
<?endif;?>
<script>
	window.TAIRAI = {};
	window.TAIRAI.selectedCity = '<?=$_SESSION['CURRENT_CITY_CODE'];?>';
</script>
