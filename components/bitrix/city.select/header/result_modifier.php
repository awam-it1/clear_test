<?php
$_SESSION['CURRENT_CITY_CODE'] = $arResult['CITY_CODE'];
$_SESSION['CURRENT_CITY_NAME'] = $arResult['CITY_NAME'];
$detectId = $arResult['DETECTED_DATA']['ID'] ?? 0;
$arResult['CITY_ANOTHER_ALERT'] = !$arResult['CITY_APPLY'] && $arResult['CITY'] == DEFAULT_CITY_ID && $arResult['CITY'] != $detectId && !empty($arResult['DETECTED_DATA']['NAME']);
$arResult['CITY_ANOTHER_ALERT'] = false; //TODO: временно убираем выбор региона