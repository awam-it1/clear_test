<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// echo '<script>var log_php_object = '.json_encode($arResult).';</script>';
?>
<div class="separator">
	<form action="" class="js-news-filter">
		<div class="simple-select ">
			<select name="salon" id="" class="simple-select js-news-salon" data-is="salonsCitySelect">
				<?foreach($arResult["ITEMS"] as $arItem):?>
				<option value="<?=$arItem["ID"];?>" <?if ($arItem["ID"] == $arParams["SELECT_SALON"]):?> selected<?endif;?>><?=$arItem["NAME"];?></option>
				<?endforeach;?>
			</select>
		</div>
	</form>
</div>