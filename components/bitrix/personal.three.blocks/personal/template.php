<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// echo '<script>var log_php_object = '.json_encode($arResult).';</script>';
?>

<div class="lk-actions">
	<div class="lk-action">
		<div class="lka-title"><?=$arParams["~TITLE_1"];?></div>
		<div class="lka-info">У Вас на счете <br>978 бонусов</div>
		<p>начислено 500 балов за день рождения, срок действия 3 дня, потратить можно на услуги в салоне</p>
		<a href="#gift-bonus" class="is-link" data-modal-open="">Подарить бонусы</a>
		<div class="clarification"><?=$arParams["~TEXT_1"];?></div>
	</div>
	<div class="lk-action">
		<div class="lka-title"><?=$arParams["~TITLE_2"];?></div>
		<div class="lka-info">У вас пока нет сертификата</div>
		<a href="#" class="button solid">Подробнее о сертификатах</a>
		<a href="#" class="is-link" download="">Скачать сертификат</a>
	</div>
	<div class="lk-action">
		<div class="lka-title"><?=$arParams["~TITLE_3"];?></div>
		<div class="lka-info">У Вас на счете <br>26 885 рублей</div>
		<p><?=$arParams["~TEXT_3"];?></p>
	</div>
</div>