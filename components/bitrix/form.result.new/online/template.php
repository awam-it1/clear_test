<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Localization\Loc;
/*echo '<script>var log_php_object = '.json_encode($arResult).';</script>';*/

GLOBAL $USER;

$arUser = [ "NAME" => "", "PERSONAL_PHONE" => "", "EMAIL" => ""];
if ($USER->IsAuthorized()) {
	$rsUser = CUser::GetByID($USER->GetID());
	$arUser = $rsUser->Fetch();
}

function get_name($name, $arResult) {
	return "form_".$arResult["QUESTIONS"][$name]["STRUCTURE"][0]["FIELD_TYPE"]."_".$arResult["QUESTIONS"][$name]["STRUCTURE"][0]["ID"];
}

function get_value($name, $arResult) {
	return @$arResult["arrVALUES"][get_name($name, $arResult)];
}
if(isset($_REQUEST['salon'])){
    $salonId = $_REQUEST['salon'];
}
if (isset($_REQUEST["formresult"])):
	if ($_REQUEST["formresult"] == "addok"):?>
		<div id="modal-thanks" class="modal-self is-show">
			<div class="thanks-modal">
				<div class="sale-text">Спасибо!</div>
				<h2>Наш менеджер в ближайшее время свяжется с Вами для подтверждения бронирования!</h2>
			</div>
		</div>
	<?endif;
endif;?>
<form action="" name="<?=$arResult["arForm"]["SID"]?>" method="POST" enctype="multipart/form-data"  @submit.prevent="submit" ref="form">
    <div class="online-block" v-cloak="">
        <div  class="ob-col">
            <?=bitrix_sessid_post()?>
            <input type="hidden" name="WEB_FORM_ID" value="<?=$arResult["arForm"]["ID"]?>">

            <div class="input-element"  :class="{ 'has-error' : errors.has('procedure') }">
                <span class="form-item-label">
                    Название процедуры <span class="required">*</span>
                </span>
                <select name="procedure" v-validate="'required|not_in:-1'" v-model="activeProcedureId" @change="changeParam">
                    <option value="-1" disabled>Выберите из списка</option>
                    <option v-for="procedure in procedures" :value="procedure.id" v-html="procedure.name"></option>
                </select>
            </div>
            <input type="hidden" name="<?=get_name("procedure", $arResult);?>" v-model="activeProcedure.name" v-if="activeProcedure">

            <div class="input-element" v-if="procedureDurations.length > 0">
                <span class="form-item-label">
                  Длительность процедуры
                </span>
                <select name="duration" @change="changeDuration" v-model="activeProcedure.activeDuration">
                    <option value="-1" disabled>Выберите из списка</option>
                    <option v-for="duration in procedureDurations"  :value="duration.id" v-html="duration.name"></option>
                </select>
            </div>
            <input type="hidden" name="<?=get_name("duration", $arResult);?>" v-model="activeProcedureDuration.name" v-if="activeProcedureDuration">
            <input type="hidden" name="duration_id" v-model="activeProcedureDuration.id1c" v-if="activeProcedureDuration">

            <div class="input-element" :class="{ 'has-error' : errors.has('salon') }">
                <span class="form-item-label">
                    Выбор салона <span class="required">*</span>
                </span>
                <select name="salon" v-model="activeSalonId" @change="changeParam"   v-validate="'required|not_in:-1'">
                    <option value="-1" disabled >Выберите из списка</option>
                    <option v-for="salon in filterSalons" v-if = "!salon.coming_soon" :value="salon.id" v-html="salon.name"></option>
                    <option v-else = "salon.coming_soon" :value="salon.id" v-html="salon.name +' '+salon.coming_soon" disabled></option>
                </select>
            </div>
            <input type="hidden" name="<?=get_name("salon", $arResult);?>" v-model="activeSalon.name" v-if="activeSalon">
            <input type="hidden" name="salon_id" v-model="activeSalonId">

            <?/*<div class="input-element">
                <span class="form-item-label">
                    Выбор Мастера
                </span>
                <select name="<?=get_name("master", $arResult);?>" v-model="activeMasterId" @change="changeParam">
                    <option value="any">Свободный</option>
                    <option v-for="master in filterMaster" :value="master.id">{{ master.name }}</option>
                </select>
            </div>*/?>
            <div class="input-element"  :class="{ 'has-error' : errors.has('<?=get_name("date", $arResult);?>') }">
                <label>
                    <span class="form-item-label">
                        Дата бронирования <span class="required">*</span>
                    </span>
                    <span class="form-item-error">Проверьте правильность ввода</span>
                    <div class="date-input">
                        <flat-pickr v-model="date" :config="flatpickrOpt" ref="fp" name="<?=get_name("date", $arResult);?>" v-validate="'required'"></flat-pickr>
                    </div>
                </label>
            </div>
           <?/* <div class="input-element">
                <span class="form-item-label">
                    {{ datePlaceholder }}
                </span>
                <select name="<?=get_name("time", $arResult);?>" v-model="activeTime" :disabled="timeDisabled">
                    <option value="all" selected>Выберите из списка</option>
                    <option v-for="time in times" :value="time.id">{{ time.value }}</option>
                </select>
            </div>*/?>
        </div>

        <div class="ob-col with-map">
            <online-map :salons="salonsForMap" @choose-salon="chooseSalon"></online-map>
        </div>

        <div  class="ob-col">
            <div class="input-element" :class="{ 'has-error' : errors.has('<?=get_name("name", $arResult);?>') }">
                <label>
                    <span class="form-item-label">
                        Имя <span class="required">*</span>
                    </span>
                    <span class="form-item-error">Проверьте правильность ввода</span>
                    <input type="text" name="<?=get_name("name", $arResult);?>" data-validation="text" v-validate="'required|alpha_spaces'"/>
                </label>
            </div>
            <div class="input-element" :class="{ 'has-error' : errors.has('<?=get_name("phone", $arResult);?>') }">
                <label>
                    <span class="form-item-label">
                        Телефон <span class="required">*</span>
                    </span>
                    <span class="form-item-error">Проверьте правильность ввода</span>
                    <input type="tel" name="<?=get_name("phone", $arResult);?>" placeholder="+7 (___) ___-__-__" data-validation="text" v-validate="{ required: true }" data-mask="phone" autocomplete="off">
                    <? /*<the-mask mask="+7 (###) ###-##-##"
                            :masked="true"
                            v-model="phone"
                            v-validate="{ required: true, regex: /^\+7\s\(([0-9]{3})\)\s([0-9]{3})-[0-9]{2}-[0-9]{2}$/ }"
                            type="tel"
                            name="<?=get_name("phone", $arResult);?>"
                            placeholder="Введите ваш номер телефона">
                    </the-mask> */?>
                </label>
            </div>
            <div class="input-element" :class="{ 'has-error' : errors.has('<?=get_name("email", $arResult);?>') }">
                <label>
                    <span class="form-item-label">
                        E-mail
                    </span>
                    <span class="form-item-error">Проверьте правильность ввода</span>
                    <input type="text" name="<?=get_name("email", $arResult);?>" v-validate="'email'" />
                </label>
            </div>
        </div>
        <div  class="ob-col">
            <div class="input-element">
                <span class="form-item-label">
                    Комментарий
                </span>
                <textarea rows="10" name="<?=get_name("comment", $arResult);?>" placeholder="Введите ваш комментарий"></textarea>
            </div>
        </div>
        <div class="ob-btn-cnt">
            <input type="hidden" name="web_form_apply" value="Y" />
            <div class="sub-text">
                <?= Loc::getMessage("ONLINE_CALLBACK_INFO"); ?>
            </div>
            <button type="submit"
                    name="web_form_apply"
                    value="Забронировать"
                    class="button"
                    :class="{ 'load' : isLoad }"
                    :disabled="isLoad">Забронировать</button>
        </div>
    </div>
    <script>
    	setTimeout(function() {
    		var input = $("[data-mask=\"phone\"]");
    		input.mask("+7 (999) 999-99-99");
    		input.on("click", function() {
		    	$(this).focus();
		    	var tmpStr = $(this).val();
		    	$(this).val("");
		    	$(this).val(tmpStr);
    		});
    	}, 750);
    </script>
</form>