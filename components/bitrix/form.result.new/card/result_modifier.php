<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// echo '<script>var log_php_object = '.json_encode($arResult).';</script>';
$arResult["CITIES"] = Array();
if (CModule::IncludeModule("iblock")) {
	$rsCity = CIBlockElement::GetList(
		Array('SORT' => 'ASC'),
		Array("IBLOCK_ID" => IBLOCK_salons_cities, "ACTIVE" => "Y"),
		false,
		false,
		Array("ID", "NAME")
	);
	while ($arCity = $rsCity->GetNext()) {
		$arResult["CITIES"][] = $arCity;
	}
}
?>

