<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// echo '<script>var log_php_object = '.json_encode($arResult).';</script>';

function get_name($name, $arResult) {
	return "form_".$arResult["QUESTIONS"][$name]["STRUCTURE"][0]["FIELD_TYPE"]."_".$arResult["QUESTIONS"][$name]["STRUCTURE"][0]["ID"];
}

function get_value($name, $arResult) {
	return @$arResult["arrVALUES"][get_name($name, $arResult)];
}

if (isset($_REQUEST["formresult"])):
	if ($_REQUEST["formresult"] == "addok"):?>
		<div id="modal-thanks" class="modal-self is-show">
			<div class="thanks-modal">
				<div class="sale-text">Спасибо!</div>
				<h2>Наш менеджер в ближайшее время свяжется с Вами для подтверждения заказа!</h2>
			</div>
		</div>
	<?endif;
endif;?>
<form action="" class="js-order-card-form" name="<?=$arResult["arForm"]["SID"]?>" novalidate ref="form" @submit.prevent="submit('form')">
	<?=bitrix_sessid_post()?>
	<input type="hidden" name="WEB_FORM_ID" value="<?=$arResult["arForm"]["ID"]?>">


	<div class="input-element" :class="{ 'has-error': errors.has('<?=get_name("price", $arResult);?>') }">
		<label>
			<span class="form-item-label">
				Введите сумму в рублях
			</span>
			<span class="form-item-error">Проверьте правильность ввода</span>
			<input placeholder="Введите сумму в рублях" type="number" min="1" name="<?=get_name("price", $arResult);?>" v-validate="'required|numeric|min_value:1'" v-model="summa" />
		</label>
	</div>
	<div class="input-element" :class="{ 'has-error': errors.has('<?=get_name("city", $arResult);?>') }">
		<span class="form-item-label">
		  Город
		</span>
		<select name="<?=get_name("city", $arResult);?>" v-validate="'required|not_in:-1'"  v-model="city">
			<option :value="-1" disabled selected>Выберите из списка</option>
			<?foreach($arResult["CITIES"] as $city):?>
			<option value="<?=$city["ID"];?>"><?=$city["NAME"];?></option>
			<?endforeach;?>
		</select>
	</div>


    <transition name="slide-fade--m">
        <div v-if="deliveryWays.length > 0" v-cloak="" class="lk-card__delivery">
            <div class="cart__delivery"  :class="{ 'is-incorrect': errors.has('delivery-way') }">
                <h4>Выберите способ доставки</h4>
                <div class="cart__delivery-inner">
                    <div class="cart__delivery-way" v-for="(way, index) in deliveryWays" >
                        <input v-model="deliveryWay" type="radio" name="delivery-way" :value="way.id" :id="'delivery-way-'+index" v-validate="'required'" />
                        <label :for="'delivery-way-'+index">
                            {{ way.name }}
                        </label>
                    </div>

                    <transition name="slide-fade--m" mode="out-in">
                        <div v-if="deliveryWay === couriertDeliveryId" key="delivery-courier" class="cart__delivery-way-more cart__delivery__courier">
                            <div v-for="item in activeDeliveryWay.list" class="element-radio"  :class="{ 'has-error': errors.has('DELIVERY_ID') }">
                                <label>
                                    <input  v-model="deliveryByCourierValue" type="radio" name="DELIVERY_ID" :value="item.id"  v-validate="'required'" />
                                    <i></i>
                                    {{ item.text }}
                                    <span class="clarification" v-if="item.clarification !== ''">{{ item.clarification }}</span>
                                </label>
                            </div>
                        </div>
                        <div v-else-if="deliveryWay" key="delivery-pickup" class="cart__delivery-way-more cart__delivery__pickup">
                            <input type="radio" name="DELIVERY_ID" :value="activeDeliveryWay.id" checked="checked" class="is-hidden" />
                        </div>
                    </transition>
                </div>
            </div>
        </div>
    </transition>

	<div class="input-element":class="{ 'has-error': errors.has('<?=get_name("street", $arResult);?>') }">
		<label>
			<span class="form-item-label">
				Улица
			</span>
			<span class="form-item-error">Проверьте правильность ввода</span>
			<input placeholder="Введите улицу" type="text" name="<?=get_name("street", $arResult);?>" v-validate="'required'" v-model="street"/>
		</label>
	</div>
	<div class="row">
		<div class="col">
			<div class="input-element" :class="{ 'has-error': errors.has('<?=get_name("home", $arResult);?>') }">
				<label>
					<span class="form-item-label">
						Дом
					</span>
					<input placeholder="Введите дом" type="text" name="<?=get_name("home", $arResult);?>" v-validate="'required'" v-model="home"/>
				</label>
			</div>
		</div>
		<div class="col">
			<div class="input-element" :class="{ 'has-error': errors.has('<?=get_name("apartment", $arResult);?>') }">
				<label>
					<span class="form-item-label">
						Квартира
					</span>
					<input placeholder="Введите квартиру" type="text" name="<?=get_name("apartment", $arResult);?>" v-model="apartment"/>
				</label>
			</div>
		</div>
	</div>
    <div class="input-element" :class="{ 'has-error' : errors.has('<?=get_name("PHONE", $arResult);?>') }">
        <label>
            <span class="form-item-label">
                Телефон <span class="required">*</span>
            </span>
            <span class="form-item-error">Проверьте правильность ввода</span>
            <the-mask mask="+7 (###) ###-##-##"
                      :masked="true"
                      v-model="phone"
                      v-validate="{ required: true, regex: /^\+7\s\(([0-9]{3})\)\s([0-9]{3})-[0-9]{2}-[0-9]{2}$/ }"
                      type="tel"
                      name="<?=get_name("PHONE", $arResult);?>"
                      placeholder="Введите ваш номер телефона">
            </the-mask>
        </label>
    </div>
	<div class="input-element" :class="{ 'has-error': errors.has('<?=get_name("message", $arResult);?>') }">
		 <span class="form-item-label">
		  Дополнительная информация к адресу
		</span>
		<textarea rows="5" placeholder="Введите ваше сообщение" name="<?=get_name("message", $arResult);?>" v-model="message"></textarea>
	</div>
	<div class="cntr">
		<input type="hidden" name="web_form_apply" value="Y" />
		<button type="submit" name="web_form_apply" value="Заказать карту" class="button" :class="{ 'load': isLoad }" :disabled="isLoad">Заказать карту</button>
	</div>
</form>
