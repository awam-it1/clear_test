<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// echo '<script>var log_php_object = '.json_encode($arResult).';</script>';

function get_name($name, $arResult) {
	return "form_".$arResult["QUESTIONS"][$name]["STRUCTURE"][0]["FIELD_TYPE"]."_".$arResult["QUESTIONS"][$name]["STRUCTURE"][0]["ID"];
}

function get_value($name, $arResult) {
	return @$arResult["arrVALUES"][get_name($name, $arResult)];
}

if (isset($_REQUEST["formresult"])):
	if ($_REQUEST["formresult"] == "addok"):?>
		<div id="modal-thanks" class="modal-self is-show">
			<div class="thanks-modal">
				<div class="sale-text">Спасибо!</div>
				<h2>Наш менеджер в ближайшее время свяжется с Вами для подтверждения заказа!</h2>
			</div>
		</div>
	<?endif;
endif;?>
<form action="/ajax" data-is="formValidation"  name="<?=$arResult["arForm"]["SID"]?>" method="POST" enctype="multipart/form-data">
	<?=bitrix_sessid_post()?>
	<input type="hidden" name="WEB_FORM_ID" value="<?=$arResult["arForm"]["ID"]?>">
    <div class="input-element">
      <label>
        <span class="form-item-label">
          Ваше имя
        </span>
        <span class="form-item-error">Проверьте правильность ввода</span>
        <input placeholder="Введите имя" type="text" name="form_text_27" data-validation="text"/>
      </label>
    </div>
    <div class="input-element">
        <label>
            <span class="form-item-label">
                Телефон <span class="required">*</span>
            </span>
            <div class="input-element__complex">
                <input type="tel" data-validation="phone" class="phone-mask" name="form_text_26" />
                <span class="form-item-error">Проверьте правильность ввода</span>
            </div>
        </label>
    </div>
	<div class="input-element">
		<label>
			<span class="form-item-label">
				Введите сумму в рублях
			</span>
			<span class="form-item-error">Проверьте правильность ввода</span>
			<input placeholder="Введите сумму в рублях" type="number" name="<?=get_name("price", $arResult);?>" data-validation="summa"/>
		</label>
	</div>
	<div class="input-element">
		<span class="form-item-label">
		  Город
		</span>
		<select name="<?=get_name("city", $arResult);?>" data-validation="select">
			<option value="-1" disabled selected>Выберите из списка</option>
			<?foreach($arResult["CITIES"] as $city):?>
			<option value="<?=$city;?>"><?=$city;?></option>
			<?endforeach;?>
		</select>
	</div>
	<div class="input-element">
		<label>
			<span class="form-item-label">
				Улица
			</span>
			<span class="form-item-error">Проверьте правильность ввода</span>
			<input placeholder="Введите улицу" type="text" name="<?=get_name("street", $arResult);?>" data-validation="text"/>
		</label>
	</div>
	<div class="row">
		<div class="col">
			<div class="input-element">
				<label>
					<span class="form-item-label">
						Дом
					</span>
					<input placeholder="Введите дом" type="text" name="<?=get_name("home", $arResult);?>" data-validation="text"/>
				</label>
			</div>
		</div>
		<div class="col">
			<div class="input-element">
				<label>
					<span class="form-item-label">
						Квартира
					</span>
					<input placeholder="Введите квартиру" type="text" name="<?=get_name("apartment", $arResult);?>" data-validation="text"/>
				</label>
			</div>
		</div>
	</div>
	<div class="input-element">
		 <span class="form-item-label">
		  Дополнительная информация к адресу
		</span>
		<textarea rows="5" placeholder="Введите ваше сообщение" name="<?=get_name("message", $arResult);?>"></textarea>
	</div>
	<div class="cntr">
		<input type="hidden" name="web_form_apply" value="Y" />
		<button type="submit" name="web_form_apply" value="Заказать карту" class="button">Заказать карту</button>
	</div>
</form>