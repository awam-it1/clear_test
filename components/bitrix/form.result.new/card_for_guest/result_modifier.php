<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// echo '<script>var log_php_object = '.json_encode($arResult).';</script>';
$arResult["CITIES"] = Array();
if (CModule::IncludeModule("iblock")) {
	$rsCity = CIBlockSection::GetList(Array('SORT' => 'ASC'), Array("IBLOCK_ID" => 6, "ACTIVE" => "Y", "DEPTH_LEVEL" => 1), false, Array("ID", "NAME"));
	while ($arCity = $rsCity->GetNext()) {
		$arResult["CITIES"][] = $arCity["NAME"];
	}
}
?>

