<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
GLOBAL $CITY_CODE;
GLOBAL $PRODUCT_ID;
$PRODUCT_ID = $arResult["ID"];
// echo '<script>var log_php_object = '.CUtil::PhpToJSObject($arResult).';</script>';
echo '<script>window.TAIRAI = {}; window.TAIRAI.offers = '.CUtil::PhpToJSObject($arResult["OFFERS_SIMPLE"]).';window.TAIRAI.productid = "'.$PRODUCT_ID.'";</script>';

?>
<div class="content is-styled"  id="salon-app">
    <div class="inner-wrapper">
        <div class="breadcrumbs-additional-nav">
            <div class="bn-left-link">
                <a class="is-link" href="../">
                    Назад на страницу каталога
                </a>
            </div>
            <?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "breadcrumb",
                Array(
                    "START_FROM" => "0",
                    "PATH" => "",
                    "SITE_ID" => "s1"
                )
            );?>
        </div>
        <h1><?=$arResult["NAME"];?></h1>

        <?$APPLICATION->IncludeComponent("bitrix:news.list", "service_catalog",
            Array(
                "IBLOCK_TYPE" => "salons",
                "IBLOCK_ID" => IBLOCK_salons_services,
                "NEWS_COUNT" => "10000",
                "SORT_BY1" => "SORT",
                "SORT_ORDER1" => "DESC",
                "SORT_BY2" => "ACTIVE_FROM",
                "SORT_ORDER2" => "DESC",
                "FILTER_NAME" => "",
                "FIELD_CODE" => array(),
                "PROPERTY_CODE" => array("TIME", "ID1C"),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AJAX_OPTION_HISTORY" => "N",
                "CACHE_TYPE" => "N",
                "CACHE_TIME" => "1",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "d F Y",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => $arResult['PARENT']['CODE'],
                "INCLUDE_SUBSECTIONS" => "Y",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "PAGER_TEMPLATE" => "pagination",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "Y",
                "CITY_CODE" => $CITY_CODE
            )
        );

        /*?>

        <form action="" class="js-form js-to-cart-selection-form">
            <div class="row service-row">
                <?if(true): //if (count($arResult["SALONS"])):?>
                    <div class="col">
                        <div class="service-descr">
                            <div class="service-descr-top">
                                    <div class="is-styled">
                                        <div class="input-element">
                                            <span class="form-item-label">
                                              Выберите салон
                                            </span>
                                            <select name="SALON_TYPE" class="js-salon-select">
                                                <?
                                                $j = 0;
                                                foreach ($arResult["SALONS"] as $key => $arItem):
                                                    if($j==0)
                                                        $salon_id = $arItem["PROPERTY_PRICE_TYPE_VALUE"];
                                                    $j++;
                                                    ?>
                                                    <option value="<?=$arItem["ID"];?>:<?=$arItem["PROPERTY_PRICE_TYPE_VALUE"];?>"<?if (!$key){ echo " selected";}?>><?=$arItem["NAME"];?></option>
                                                <?endforeach;?>
                                            </select>
                                        </div>
                                    </div>
                                    <?
                                    if (count($arResult["ITEMS"])):?>
                                        <div class="is-styled">
                                            <div class="input-element"><span class="form-item-label">Выберите длительность сеанса</span></div>
                                        </div>
                                        <div class="service-radio-buttons">
                                            <?
                                            $duration = array();

                                            foreach($arResult["ITEMS"] as $item) {
                                                $item['DURATION'] = getMinutesByString($item["DISPLAY_PROPERTIES"]["TIME"]["VALUE"]);
                                                $duration[] = $item;
                                            }

                                            function cmp($a, $b)
                                            {
                                                if ($a["DURATION"] == $b["DURATION"]) {
                                                    return 0;
                                                }
                                                return ($a["DURATION"] < $b["DURATION"]) ? -1 : 1;
                                            }
                                            usort($duration, "cmp");
                                            $i = 0;
                                            foreach ($duration as $key => $arItem):
                                                ?>
                                                <div class="service-radio">
                                                    <label>
                                                        <input class="js-duration" type="radio" name="DURATION" value="<?=$arItem["ID"];?>:<?=$arItem["DISPLAY_PROPERTIES"]["ID1C"]["VALUE"];?>"<?if ($i==0){ $duration_id = $arItem["DISPLAY_PROPERTIES"]["ID1C"]["VALUE"]; echo " checked";}?>/>
                                                        <span><?=make_time($arItem["DISPLAY_PROPERTIES"]["TIME"]["VALUE"]);?></span>
                                                    </label>
                                                </div>
                                                <?
                                                $i++;
                                            endforeach;?>
                                        </div>
                                    <?endif;?>

                                    <input type="hidden" name="SERVICE_ID" value="<?=$arResult["PARENT"]["ID"];?>">
                                    <input type="hidden" name="SERVICE" value="<?=$arResult["PARENT"]["UF_ID1C"];?>">
                                <div class="service-price">Стоимость услуги: <span class="js-price"><?if (!is_null($arResult["PRICE"])) echo GetPrice($salon_id, $duration_id)." рублей";?></span></div>
                                <div class="service-price-description">Цена услуги зависит от выбранного города<?if (count($arResult["ITEMS"])):?> и продолжительности процедуры<?endif;?></div>
                            </div>
                            <div class="service-button-head">
                                <a href="#" class="button js-link js-to-cart-selection" href="#" data-product-id="<?=$arResult["ID"];?>">В корзину</a>
                                <!-- <a href="#" class="button solid">Купить сертификат</a> -->
                            </div>
                        </div>
                    </div>
                <?else:?>
                    <div class="col">
                        <div class="service-descr">
                            <div class="service-price">Для данного города сертификат недоступен. Ознакомьтесь со списком сертификатов по <a href="/catalog/">ссылке</a>.</div>
                        </div>
                    </div>
                <?endif;?>
                <div class="col">
                    <?if (count($arResult["MORE_PHOTO"])):?>
                        <div class="image-gallery service-gallery" ref="imageGallery">
                            <div class="swiper-container ig-main">
                                <div class="swiper-wrapper">
                                    <?foreach ($arResult["MORE_PHOTO"] as $arImage):?>
                                        <div class="swiper-slide" data-design="<?=$arImage["SRC"];?>">
                                            <div class="gallery-image" style="background-image: url('<?=$arImage["SRC"];?>')"></div>
                                        </div>
                                    <?endforeach;?>
                                    <!-- <div class="swiper-slide">
                                        <div class="gallery-video video" data-is="video">
                                            <video src="/dist/video/mainvideo.mp4" poster="/dist/img/temp/temp13.jpg"></video>
                                            <div class="play">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" viewBox="0 0 80 80"><path fill="#8a007c" d="M0 40a40 40 0 1 1 80 0 40 40 0 0 1-80 0z"/><path fill="#fff" d="M36 32l12 8-12 8z"/></svg>
                                            </div>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                            <input type="hidden" name="design" data-design-input="" value="<?=$arResult["MORE_PHOTO"][0]['SRC']?>">
                            <div class="ig-arrow ig-left">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20" height="20" viewBox="0 0 20 20"><g transform="translate(-90 -402)"><image width="20" height="20" transform="translate(90 402)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAACa0lEQVQ4T5VUTWgTQRR+bybZbo3bNiWppggSiYhEkVoIQkGqoBgUiiAePSkoiodSGvCg4sGDKAiCeKkXvYigSK1W0R4qKEWlSvGnINEc2vrTRGPAEpPMk9nNJpvNZhvfYWdm571v5n3fe4PQpP34PHp0haoNeT1tawkRWgI9XqdQXA4vkxq771NDcQSFmb4C8qSu6q2srRgNAReTd0+0tqy+zEDlAKTHyK8egERqaCtHRGPDYo6A6S9jL1VPVy8JQh3BDDPnCLBUWBgJrhs4vCzgz9TjNBe+Tt3RAlBzRQIoQu5XZ3iP3xUwnXz0m4GqkShfSbIkAKDMlrwuAYEcEYgKWnZNMDgw75jy90/35pnwhYAIAA0miAgQZWgdVQYGz2eDkX0ddYALs7eectG+g4BqODVFcKDQYAQZlTD3KrThQMwExbmZm7s56xgXZBHAhLUj2giT6TNkRPAn2R09GNEPmXt3568ogmOR1tWoTSTrknEmiLLXMfX2Rh6EohicGfQ1osytCRALJYb5Efz4+spOL7U/AWJoVdEoZENROTaqIsaAiqXM+/Wxk5tMH5h9cXGUscBekjy6mKm4HMvBVMDM5MbYYH9FFHPy4fmlJFJb2A3QrpGAXDraNxhwrEP5c2byQoaB5q+mWC3kuo5gRK1+6g5Hj39tCCg33kyc/8ZxZZfbTY18lxY39yeCdQc5BU5PnH2GQuuTolefmFpPphSvbdmeONYUoHSaHj99pMSVqyA8HuPJqZrsxp5dw80/X9bgqYenbhMp+5E4ryjJBMXiZ/7vgbWnMvUgcUgQJgB4BBnHbfFzejPY7R+83t3zXtOZRwAAAABJRU5ErkJggg=="/></g></svg>
                            </div>
                            <div class="ig-arrow ig-right">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"><defs><linearGradient id="a" x1="10" x2="10" y1="10" y2="20" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#d4be6f"/><stop offset="1" stop-color="#b3a05d"/></linearGradient><linearGradient id="b" x1="10" x2="10" y2="20" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#fff7b4" stop-opacity=".7"/><stop offset=".99" stop-color="#ccc370" stop-opacity=".7"/></linearGradient><filter id="c" width="200%" height="200%" x="-50%" y="-50%"><feGaussianBlur in="SourceGraphic" result="SvgjsFeGaussianBlur1067Out"/></filter><mask id="d"><path fill="#fff" d="M6.28 0C10.87.79 15.99 3.38 20 10c-4.01 6.62-9.13 9.22-13.72 10l-.01-.22c-.02-.85-.09-1.65-.22-2.39a10.78 10.78 0 0 0-1.55-3.91A8.83 8.83 0 0 0 0 10a8.83 8.83 0 0 0 4.51-3.5c.33-.48.6-1.01.91-1.76.25-.64.5-1.33.63-2.12.13-.75.2-1.55.22-2.39z"/></mask><linearGradient id="e" x1="10" x2="10" y2="19.78" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#ffeed9"/><stop offset="1" stop-color="#c7b179"/></linearGradient></defs><path fill="#d4be6f" d="M6.28 0C10.87.79 15.99 3.38 20 10c-4.01 6.62-9.13 9.22-13.72 10l-.01-.22c-.02-.85-.09-1.65-.22-2.39a10.78 10.78 0 0 0-1.55-3.91A8.83 8.83 0 0 0 0 10a8.83 8.83 0 0 0 4.51-3.5c.33-.48.6-1.01.91-1.76.25-.64.5-1.33.63-2.12.13-.75.2-1.55.22-2.39z"/><path fill="url(#a)" d="M6.28 0C10.87.79 15.99 3.38 20 10c-4.01 6.62-9.13 9.22-13.72 10l-.01-.22c-.02-.85-.09-1.65-.22-2.39a10.78 10.78 0 0 0-1.55-3.91A8.83 8.83 0 0 0 0 10a8.83 8.83 0 0 0 4.51-3.5c.33-.48.6-1.01.91-1.76.25-.64.5-1.33.63-2.12.13-.75.2-1.55.22-2.39z"/><path fill="url(#b)" d="M6.28 0C10.87.79 15.99 3.38 20 10c-4.01 6.62-9.13 9.22-13.72 10l-.01-.22c-.02-.85-.09-1.65-.22-2.39a10.78 10.78 0 0 0-1.55-3.91A8.83 8.83 0 0 0 0 10a8.83 8.83 0 0 0 4.51-3.5c.33-.48.6-1.01.91-1.76.25-.64.5-1.33.63-2.12.13-.75.2-1.55.22-2.39z"/><path fill="#fff" fill-opacity=".5" d="M0 21V-1h20v22zM6.27 1.23c-.02.84-.09 1.64-.22 2.39-.13.79-.38 1.48-.63 2.12a9.19 9.19 0 0 1-2.04 3.13A8.83 8.83 0 0 1 0 11a8.83 8.83 0 0 1 4.5 3.48 10.78 10.78 0 0 1 1.55 3.91c.13.74.2 1.54.22 2.39l.01.22c4.59-.78 9.71-3.38 13.72-10-4.01-6.62-9.13-9.21-13.72-10z" filter="url(#c)" mask="url(&quot;#d&quot;)"/><path fill="url(#e)" d="M6.28 0C10.87.79 15.99 3.38 20 10c-4.01 6.62-9.13 9.22-13.72 10l-.01-.22c-.02-.85-.09-1.65-.22-2.39a10.78 10.78 0 0 0-1.55-3.91A8.83 8.83 0 0 0 0 10a8.83 8.83 0 0 0 4.51-3.5c.33-.48.6-1.01.91-1.76.25-.64.5-1.33.63-2.12.13-.75.2-1.55.22-2.39z"/></svg>
                            </div>
                            <div class="swiper-container ig-tumbnails">
                                <div class="swiper-wrapper">
                                    <?foreach ($arResult["MORE_PHOTO"] as $arImage):?>
                                        <div class="swiper-slide tumbs-slide">
                                            <div class="tumbs-image" style="background-image: url('<?=$arImage["SRC"];?>')"></div>
                                        </div>
                                    <?endforeach;?>
                                </div>
                            </div>
                        </div>
                    <?endif;?>
                </div>
            </div>
        </form>
        <?
        */





        /*
    // prev version (catalog for other products)
        <form class="js-to-cart-selection-form">
            <div class="certificate-tune-card">
                <div class="ctc-params">

                    <div class="ctc-meta-a">
                        <!-- Шаг 1. Выберите длительность сеанса -->
                    </div>


                        <div class="ctc-step-item item-step-1">
                            <div class="service-form-name">Шаг 1. Выберите длительность сеанса</div>
                            <?foreach($arResult["P"]["TIME"] as $key => $value):?>
                            <div class="element-radio">
                                <label>
                                    <input type="radio" name="TIME" value="<?=$value?>"<?if (!$key) echo " checked";?>/>
                                    <i></i>
                                    <?=$value?>
                                </label>
                            </div>
                            <?endforeach;?>
                        </div>
                        <div class="ctc-step-item item-step-2">
                            <div class="service-form-name">Шаг 2. Выберите тип сертификата</div>
                            <?foreach($arResult["P"]["FORMAT"] as $key => $value):?>
                            <div class="element-radio">
                                <label>
                                    <input type="radio" name="FORMAT" value="<?=$value?>"<?if (!$key) echo " checked";?>/>
                                    <i></i>
                                    <?=$value?>
                                </label>
                            </div>
                            <?endforeach;?>
                        </div>
                        <div class="ctc-step-item item-step-3">
                            <a class="button js-to-cart-selection" href="#" data-product-id="<?=$arResult["ID"];?>">
                                В корзину
                            </a>
                            <!-- <a class="button-like" href="#">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 93 81">
                                    <path d="M86 7c-9.3578827-9.3558509-24.5114345-9.3558509-34 0l-6 6-5-6C31.5095646-2.35384014 16.3560128-2.35384014 7 7c-9.3368582 9.4919616-9.3368582 24.7686732 0 34l6 6 33 34 34-34 6-6c9.3369828-9.2313268 9.3369828-24.5090437 0-34zM69 47L46 70 24 47 13 36c-6.55690468-6.6764293-6.55690468-16.9308127 0-23 5.9003262-6.58102328 15.9962849-6.58102328 22 0l11 11 12-11c6.0037151-6.58102328 16.1016673-6.58102328 22 0 6.5569047 6.0691873 6.5569047 16.3215463 0 23L69 47z"></path>
                                </svg>
                                В список желаемого
                            </a> -->
                        </div>
                </div>

                <?if (count($arResult["MORE_PHOTO"])):?>
                <div class="ctc-gallery">
                    <div class="ctc-meta-a cntr">
                        Выберите дизайн сертификата
                    </div>
                    <?$sFirst = false;?>
                    <div class="image-gallery service-gallery" ref="imageGallery">
                        <div class="swiper-container ig-main">
                            <div class="swiper-wrapper">
                                <?foreach($arResult["MORE_PHOTO"] as $arImage):?>
                                    <?if (strlen($arImage["DESCRIPTION"])):?>
                                        <div class="swiper-slide" data-design="test">
                                            <div class="gallery-video video" data-is="<?=$arImage["SRC"];?>">
                                                <video src="<?=$arImage["DESCRIPTION"];?>" poster="<?=$arImage["SRC"];?>"></video>
                                                <div class="play">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" viewBox="0 0 80 80"><path fill="#8a007c" d="M0 40a40 40 0 1 1 80 0 40 40 0 0 1-80 0z"/><path fill="#fff" d="M36 32l12 8-12 8z"/></svg>
                                                </div>
                                            </div>
                                        </div>
                                    <?else:?>
                                        <div class="swiper-slide" data-design="<?=$arImage["SRC"];?>">
                                            <div class="gallery-image" style="background-image: url('<?=$arImage["SRC"];?>')"></div>
                                        </div>
                                    <?endif;?>
                                    <?if(!$sFirst):?>
                                    <?$sFirst = $arImage["SRC"];?>
                                    <?endif;?>
                                <?endforeach;?>
                            </div>
                        </div>
                        <input type="hidden" name="design" data-design-input="" value="<?=$sFirst?>">
                        <div class="ig-arrow ig-left">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20" height="20" viewBox="0 0 20 20"><g transform="translate(-90 -402)"><image width="20" height="20" transform="translate(90 402)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAACa0lEQVQ4T5VUTWgTQRR+bybZbo3bNiWppggSiYhEkVoIQkGqoBgUiiAePSkoiodSGvCg4sGDKAiCeKkXvYigSK1W0R4qKEWlSvGnINEc2vrTRGPAEpPMk9nNJpvNZhvfYWdm571v5n3fe4PQpP34PHp0haoNeT1tawkRWgI9XqdQXA4vkxq771NDcQSFmb4C8qSu6q2srRgNAReTd0+0tqy+zEDlAKTHyK8egERqaCtHRGPDYo6A6S9jL1VPVy8JQh3BDDPnCLBUWBgJrhs4vCzgz9TjNBe+Tt3RAlBzRQIoQu5XZ3iP3xUwnXz0m4GqkShfSbIkAKDMlrwuAYEcEYgKWnZNMDgw75jy90/35pnwhYAIAA0miAgQZWgdVQYGz2eDkX0ddYALs7eectG+g4BqODVFcKDQYAQZlTD3KrThQMwExbmZm7s56xgXZBHAhLUj2giT6TNkRPAn2R09GNEPmXt3568ogmOR1tWoTSTrknEmiLLXMfX2Rh6EohicGfQ1osytCRALJYb5Efz4+spOL7U/AWJoVdEoZENROTaqIsaAiqXM+/Wxk5tMH5h9cXGUscBekjy6mKm4HMvBVMDM5MbYYH9FFHPy4fmlJFJb2A3QrpGAXDraNxhwrEP5c2byQoaB5q+mWC3kuo5gRK1+6g5Hj39tCCg33kyc/8ZxZZfbTY18lxY39yeCdQc5BU5PnH2GQuuTolefmFpPphSvbdmeONYUoHSaHj99pMSVqyA8HuPJqZrsxp5dw80/X9bgqYenbhMp+5E4ryjJBMXiZ/7vgbWnMvUgcUgQJgB4BBnHbfFzejPY7R+83t3zXtOZRwAAAABJRU5ErkJggg=="/></g></svg>
                        </div>
                        <div class="ig-arrow ig-right">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"><defs><linearGradient id="a" x1="10" x2="10" y1="10" y2="20" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#d4be6f"/><stop offset="1" stop-color="#b3a05d"/></linearGradient><linearGradient id="b" x1="10" x2="10" y2="20" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#fff7b4" stop-opacity=".7"/><stop offset=".99" stop-color="#ccc370" stop-opacity=".7"/></linearGradient><filter id="c" width="200%" height="200%" x="-50%" y="-50%"><feGaussianBlur in="SourceGraphic" result="SvgjsFeGaussianBlur1067Out"/></filter><mask id="d"><path fill="#fff" d="M6.28 0C10.87.79 15.99 3.38 20 10c-4.01 6.62-9.13 9.22-13.72 10l-.01-.22c-.02-.85-.09-1.65-.22-2.39a10.78 10.78 0 0 0-1.55-3.91A8.83 8.83 0 0 0 0 10a8.83 8.83 0 0 0 4.51-3.5c.33-.48.6-1.01.91-1.76.25-.64.5-1.33.63-2.12.13-.75.2-1.55.22-2.39z"/></mask><linearGradient id="e" x1="10" x2="10" y2="19.78" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#ffeed9"/><stop offset="1" stop-color="#c7b179"/></linearGradient></defs><path fill="#d4be6f" d="M6.28 0C10.87.79 15.99 3.38 20 10c-4.01 6.62-9.13 9.22-13.72 10l-.01-.22c-.02-.85-.09-1.65-.22-2.39a10.78 10.78 0 0 0-1.55-3.91A8.83 8.83 0 0 0 0 10a8.83 8.83 0 0 0 4.51-3.5c.33-.48.6-1.01.91-1.76.25-.64.5-1.33.63-2.12.13-.75.2-1.55.22-2.39z"/><path fill="url(#a)" d="M6.28 0C10.87.79 15.99 3.38 20 10c-4.01 6.62-9.13 9.22-13.72 10l-.01-.22c-.02-.85-.09-1.65-.22-2.39a10.78 10.78 0 0 0-1.55-3.91A8.83 8.83 0 0 0 0 10a8.83 8.83 0 0 0 4.51-3.5c.33-.48.6-1.01.91-1.76.25-.64.5-1.33.63-2.12.13-.75.2-1.55.22-2.39z"/><path fill="url(#b)" d="M6.28 0C10.87.79 15.99 3.38 20 10c-4.01 6.62-9.13 9.22-13.72 10l-.01-.22c-.02-.85-.09-1.65-.22-2.39a10.78 10.78 0 0 0-1.55-3.91A8.83 8.83 0 0 0 0 10a8.83 8.83 0 0 0 4.51-3.5c.33-.48.6-1.01.91-1.76.25-.64.5-1.33.63-2.12.13-.75.2-1.55.22-2.39z"/><path fill="#fff" fill-opacity=".5" d="M0 21V-1h20v22zM6.27 1.23c-.02.84-.09 1.64-.22 2.39-.13.79-.38 1.48-.63 2.12a9.19 9.19 0 0 1-2.04 3.13A8.83 8.83 0 0 1 0 11a8.83 8.83 0 0 1 4.5 3.48 10.78 10.78 0 0 1 1.55 3.91c.13.74.2 1.54.22 2.39l.01.22c4.59-.78 9.71-3.38 13.72-10-4.01-6.62-9.13-9.21-13.72-10z" filter="url(#c)" mask="url(&quot;#d&quot;)"/><path fill="url(#e)" d="M6.28 0C10.87.79 15.99 3.38 20 10c-4.01 6.62-9.13 9.22-13.72 10l-.01-.22c-.02-.85-.09-1.65-.22-2.39a10.78 10.78 0 0 0-1.55-3.91A8.83 8.83 0 0 0 0 10a8.83 8.83 0 0 0 4.51-3.5c.33-.48.6-1.01.91-1.76.25-.64.5-1.33.63-2.12.13-.75.2-1.55.22-2.39z"/></svg>
                        </div>
                        <div class="swiper-container ig-tumbnails">
                            <div class="swiper-wrapper">
                                <?foreach($arResult["MORE_PHOTO"] as $arImage):?>
                                <div class="swiper-slide tumbs-slide">
                                    <div class="tumbs-image" style="background-image: url('<?=$arImage["SRC"];?>')"></div>
                                </div>
                                <?endforeach;?>
                            </div>
                        </div>
                    </div>
                </div>
                <?endif;?>
            </div>
        </form>*/?>

		<?if (strlen($arResult["~DETAIL_TEXT"])):?>
		<div class="certificate-preview-text">
			<h2>
				Описание
			</h2>
			<p><?=nl2br($arResult["~DETAIL_TEXT"]);?></p>
		</div>
		<?endif;?>

		<?if (count($arResult["TOGETHER_PRODUCT"]) && strlen($arResult["DISPLAY_PROPERTIES"]["NAME"]["VALUE"])):?>
		<div class="certificate-similar-stuff">
			<h2>
				Вместе с этим товаром покупают
			</h2>
			<div class="cr-ss-list">

				<div class="cr-ss-item item-state-card">
          <a href="<?=$arResult["DETAIL_PAGE_URL"]?>">
            <img class="cr-ss-card" src="<?=$arResult["DISPLAY_PROPERTIES"]["IMAGE"]["FILE_VALUE"]["SRC"];?>" />
            <div class="cr-ss-details"><?=$arResult["DISPLAY_PROPERTIES"]["NAME"]["VALUE"];?></div>
          </a>
				</div>
        <div class="cr-ss-item item-state-card">
          <a href="<?=$arResult["TOGETHER_PRODUCT"]["DETAIL_PAGE_URL"]?>">
            <img class="cr-ss-card" src="<?=$arResult["TOGETHER_PRODUCT"]["IMAGE"];?>" />
            <div class="cr-ss-details"><?=$arResult["TOGETHER_PRODUCT"]["NAME"];?></div>
          </a>
        </div>

				<div class="cr-ss-item item-state-result">
					<div class="cr-ss-amount">
						<?echo $arResult["TOGETHER_PRODUCT"]["PRICE"]+$arResult["MIN_PRICE"];?> руб
					</div>
					<div class="cr-ss-order">
						<a class="button js-to-cart-together" href="/cart/" data-together-product-id="<?=$arResult["TOGETHER_PRODUCT"]["ID"];?>">
							В корзину
						</a>
					</div>
				</div>
			</div>
		</div>
		<?endif;?>
	</div>

	<?
	GLOBAL $salon_review_filter;
	$salon_review_filter = Array("PROPERTY_PRODUCT" => $arResult["ID"]);

	$APPLICATION->IncludeComponent("bitrix:news.list", "salon_review",
		Array(
			"IBLOCK_TYPE" => "catalog",
			"IBLOCK_ID" => IBLOCK_reviews_products,
			"NEWS_COUNT" => "10000",
			"SORT_BY1" => "SORT",
			"SORT_ORDER1" => "DESC",
			"SORT_BY2" => "",
			"SORT_ORDER2" => "",
			"FILTER_NAME" => "salon_review_filter",
			"FIELD_CODE" => array("ACTIVE_FROM"),
			"PROPERTY_CODE" => array(),
			"CHECK_DATES" => "Y",
			"DETAIL_URL" => "",
			"AJAX_MODE" => "N",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_STYLE" => "Y",
			"AJAX_OPTION_HISTORY" => "N",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "36000000",
			"CACHE_FILTER" => "N",
			"CACHE_GROUPS" => "Y",
			"PREVIEW_TRUNCATE_LEN" => "",
			"ACTIVE_DATE_FORMAT" => "d.m.Y",
			"SET_STATUS_404" => "N",
			"SET_TITLE" => "N",
			"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
			"ADD_SECTIONS_CHAIN" => "N",
			"HIDE_LINK_WHEN_NO_DETAIL" => "N",
			"PARENT_SECTION" => "",
			"PARENT_SECTION_CODE" => "",
			"INCLUDE_SUBSECTIONS" => "Y",
			"DISPLAY_DATE" => "Y",
			"DISPLAY_NAME" => "Y",
			"DISPLAY_PICTURE" => "Y",
			"DISPLAY_PREVIEW_TEXT" => "Y",
			"PAGER_TEMPLATE" => "pagination",
			"DISPLAY_TOP_PAGER" => "N",
			"DISPLAY_BOTTOM_PAGER" => "N",
			"PAGER_TITLE" => "",
			"PAGER_SHOW_ALWAYS" => "N",
			"PAGER_DESC_NUMBERING" => "N",
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
			"PAGER_SHOW_ALL" => "Y",
			"REVIEW_TYPE" => IBLOCK_catalog_catalog,
			"REVIEW_OBJ_ID" => $arResult["ID"]
		)
	);?>

	<?
	if (count($arResult["DISPLAY_PROPERTIES"]["RELATED_PRODUCTS"]["VALUE"])) {
		GLOBAL $related_products_filter;
		
		foreach ($arResult["DISPLAY_PROPERTIES"]["RELATED_PRODUCTS"]["VALUE"] as $key => $value) {
			if ($value == $arResult["ID"]) {
				unset($arResult["DISPLAY_PROPERTIES"]["RELATED_PRODUCTS"]["VALUE"][$key]);
			}
		}

		$related_products_filter = Array(
			"ID" => $arResult["DISPLAY_PROPERTIES"]["RELATED_PRODUCTS"]["VALUE"],
			"!ID" => $arResult["ID"]
		);

		$APPLICATION->IncludeComponent("bitrix:news.list", "related_products",
			Array(
				"IBLOCK_TYPE" => "catalog",
				"IBLOCK_ID" => IBLOCK_catalog_catalog,
				"NEWS_COUNT" => "10000000",
				"SORT_BY1" => "SORT",
				"SORT_ORDER1" => "DESC",
				"SORT_BY2" => "",
				"SORT_ORDER2" => "",
				"FILTER_NAME" => "related_products_filter",
				"FIELD_CODE" => array(),
				"PROPERTY_CODE" => array("LABEL"),
				"CHECK_DATES" => "Y",
				"DETAIL_URL" => "",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "36000000",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "Y",
				"PREVIEW_TRUNCATE_LEN" => "",
				"ACTIVE_DATE_FORMAT" => "d F Y",
				"SET_STATUS_404" => "N",
				"SET_TITLE" => "N",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
				"ADD_SECTIONS_CHAIN" => "N",
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",
				"PARENT_SECTION" => "",
				"PARENT_SECTION_CODE" => "",
				"INCLUDE_SUBSECTIONS" => "Y",
				"DISPLAY_DATE" => "Y",
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => "Y",
				"DISPLAY_PREVIEW_TEXT" => "Y",
				"PAGER_TEMPLATE" => "pagination",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "N",
				"PAGER_TITLE" => "",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "Y"
			)
		);
	}
	?>

    <div id="modal-msg" class="modal-self ">
        <div class="thanks-modal">
            <h2 class="js-msg"></h2>
        </div>
    </div>

    <div id="adding-msg" class="modal-self ">
        <div class="simple-modal">
            <h2>Товар успешно добавлен в корзину</h2>
            <div class="adding-msg__actions">
                <a href="/catalog/" class="button js-close-modal">Вернуться к покупкам</a>
                <a href="/cart/" class="button solid">Перейти в корзину</a>
            </div>
        </div>
    </div>
</div>
