<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

// echo '<script>var log_php_object = '.json_encode($arResult).';</script>';
use Bitrix\Iblock\InheritedProperty;

// получаем цены по услуге
global $CITY_CODE;
// получаем список салонов по услуге

// идентификатор услуги и типы цены
$service = $arResult['PROPERTIES']['ID1C']['VALUE'];
if (count($arResult["ITEMS"])) {
	$service = $arResult["ITEMS"][0]["DISPLAY_PROPERTIES"]["ID1C"]["VALUE"];
}

// id услуги дочерней
$rsServices = CIBlockElement::GetList(
	Array("SORT" => "ASC")
	, Array("IBLOCK_ID" => IBLOCK_salons_services, "ACTIVE" => "Y", "INCLUDE_SUBSECTIONS" => "Y") // услуги
	, false
	, false
	, Array("ID", "IBLOCK_ID", "IBLOCK_SECTION_ID", "PROPERTY_ID1C")
);
while ($arServices = $rsServices->Fetch()) {

	if ($arServices["PROPERTY_ID1C_VALUE"] == $service) {
		$service_section_id = $arServices["IBLOCK_SECTION_ID"];
		$service_id = $arServices['ID'];
	}
}

$ipropValues = new InheritedProperty\ElementValues(20, $service_id);
//Получить значения
$values = $ipropValues->getValues();
//Сбросить кеш
$ipropValues->clearValues();

$price_types = GetPriceTypesByService($service);

$arResult['PARENT']['UF_ID1C'] = $service;
// id услуги секции
$rsResult = CIBlockSection::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => IBLOCK_salons_services, "ID" => $service_section_id), false, array("UF_ID1C", "CODE"));
while ($arRez = $rsResult->GetNext()) {
	$service = $arRez["UF_ID1C"];
	$service_section_code = $arRez["CODE"];
}

$arResult['PARENT']['ID'] = $service_section_id;
$arResult['PARENT']['CODE'] = $service_section_code;


// салоны в которых доступна услуга
$arResult["SALONS"] = [];
$rsSalon = CIBlockElement::GetList(
	Array("SORT" => "ASC")
	, Array("IBLOCK_ID" => IBLOCK_salons_salons, "SECTION_CODE" => $CITY_CODE, "ACTIVE" => "Y", "INCLUDE_SUBSECTIONS" => "Y")
	, false
	, false
	, Array("ID", "NAME", "PROPERTY_ID1C", "PROPERTY_PRICE_TYPE", "PROPERTY_ALL_SERVICES", "PROPERTY_TIME")
);
$i = 0;

while ($arSalon = $rsSalon->Fetch()) {
	$salon_id = $arSalon['PROPERTY_PRICE_TYPE_VALUE'];
	if (in_array($arSalon["PROPERTY_PRICE_TYPE_VALUE"], $price_types)) {
		$arResult["SALONS"][$arSalon['ID']] = $arSalon;
	} elseif ($arSalon['PROPERTY_ALL_SERVICES_VALUE']['service'] == $arResult['PARENT']['ID']) {
		$arResult["SALONS"][$arSalon['ID']] = $arSalon;
	}
	$i++;
}

// начальная цена
$arResult["PRICE"] = null;
//var_dump("GetPrice($salon_id, $service)");
if (count($arResult["SALONS"])) {
	$arResult["PRICE"] = GetPrice(null, $service);
}

// инфа о длительности
$arResult["ITEMS"] = null;
$rsServices = CIBlockElement::GetList(
	Array("SORT" => "ASC")
	, Array("IBLOCK_ID" => IBLOCK_salons_services, "ACTIVE" => "Y", "INCLUDE_SUBSECTIONS" => "Y") // услуги
	, false
	, false
	, Array("ID", "NAME", "PROPERTY_ID1C", "PROPERTY_TYPE", "PROPERTY_PRICE_TYPE", "PROPERTY_ALL_SERVICES", "PROPERTY_TIME")
);

$unique_duration = array();
while ($arServices = $rsServices->Fetch()) {

	if ((array_search($arServices['PROPERTY_TIME_VALUE'], $unique_duration) === false) && (!is_null(GetPrice($salon_id, $arServices['PROPERTY_ID1C_VALUE'])))) {
		$unique_duration[] = $arServices['PROPERTY_TIME_VALUE'];
		$item = [
			"ID" => $arServices['ID'],
			"PROPERTIES" => [
				"ID1C" => [
					"VALUE" => $service //$arServices['PROPERTY_ID1C_VALUE']
				],
				"TIME" => [
					"VALUE" => $arServices['PROPERTY_TIME_VALUE']
				]
			],
			"DISPLAY_PROPERTIES" => [
				"ID1C" => [
					"VALUE" => $arServices['PROPERTY_ID1C_VALUE']
				],
				"TIME" => [
					"VALUE" => $arServices['PROPERTY_TIME_VALUE']
				]
			]
		];
		$arResult["ITEMS"][] = $item;
	}
}

$arResult["TOGETHER_PRODUCT"] = Array();
if (strlen($arResult["DISPLAY_PROPERTIES"]["TOGETHER_PRODUCT"]["VALUE"]) && strlen($arResult["DISPLAY_PROPERTIES"]["IMAGE"]["VALUE"]) && strlen($arResult["DISPLAY_PROPERTIES"]["NAME"]["VALUE"])) {
	$rsTogetherProduct = CIBlockElement::GetByID($arResult["DISPLAY_PROPERTIES"]["TOGETHER_PRODUCT"]["VALUE"]);
	if ($arTogetherProduct = $rsTogetherProduct->GetNext()) {
		$rsPrice = CPrice::GetList(Array(), Array("=PRODUCT_ID" => $arTogetherProduct["ID"]));
		if ($arPrice = $rsPrice->Fetch()) {
			$arResult["TOGETHER_PRODUCT"]["ID"] = $arTogetherProduct["ID"];
			$arResult["TOGETHER_PRODUCT"]["DETAIL_PAGE_URL"] = $arTogetherProduct["DETAIL_PAGE_URL"];
			$arResult["TOGETHER_PRODUCT"]["NAME"] = $arTogetherProduct["NAME"];
			$arResult["TOGETHER_PRODUCT"]["IMAGE"] = CFile::GetPath($arTogetherProduct["PREVIEW_PICTURE"]);
			$arResult["TOGETHER_PRODUCT"]["PRICE"] = (double)$arPrice["PRICE"];
			$arResult["TOGETHER_PRODUCT"]["PRICE_SUMM"] = (double)$arResult["PRICES"]["BASE"]["VALUE"] + (double)$arPrice["PRICE"];
		}
	}
}

$arResult["P"] = Array();
$arResult["P"]["TIME"] = Array();

foreach ($arResult["OFFERS"] as $arOffer) {
	$prices[] = $arOffer["PRICES"]["BASE"]["VALUE"];

	$arResult["P"]["TIME"][$arOffer["DISPLAY_PROPERTIES"]["TIME"]["VALUE"]] = $arOffer["DISPLAY_PROPERTIES"]["TIME"]["VALUE"];
}
$arResult["MIN_PRICE"] = min($prices);
$arResult["P"]["TIME"] = array_values($arResult["P"]["TIME"]);

$arResult["P"]["FORMAT"] = Array();
foreach ($arResult["OFFERS"] as $arOffer) {
	$arResult["P"]["FORMAT"][$arOffer["DISPLAY_PROPERTIES"]["FORMAT"]["VALUE"]] = $arOffer["DISPLAY_PROPERTIES"]["FORMAT"]["VALUE"];
}
$arResult["P"]["FORMAT"] = array_values($arResult["P"]["FORMAT"]);


$arResult["OFFERS_SIMPLE"] = array_map(function ($arOffer) {
	return [
		"ID" => $arOffer["ID"]
		, "FORMAT" => $arOffer["DISPLAY_PROPERTIES"]["FORMAT"]["VALUE"]
		, "TIME" => $arOffer["DISPLAY_PROPERTIES"]["TIME"]["VALUE"]
	];
}, $arResult["OFFERS"]);

?>