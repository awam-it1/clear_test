<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// echo '<script>var log_php_object = '.json_encode($arResult).';</script>';

$errors = implode(",", $arResult["ERRORS"]);
?>

<?if (strlen($arResult["MESSAGE"])):?>
	<div id="reviews_form_message" class="modal-self is-show">
		<div class="thanks-modal">
			<div class="sale-text">Отправлено</div>
			<h2>Отзыв появится на сайте после модерации</h2>
		</div>
	</div>
<?endif;?>

<div id="review-modal" class="modal-content-inner modal-self<?if (strlen($errors)):?> is-show<?endif;?>">
	<div class="simple-modal is-styled">
		<form action=""  @submit.prevent="submit" ref="reviewForm" name="iblock_add" method="POST" enctype="multipart/form-data">
			<?=bitrix_sessid_post()?>
			<div class="title-h1">
				<span>Добавить отзыв</span>
			</div>
			<div class="reviews-rating"  :class="{ 'has-error' : errors.has('PROPERTY[40][0]') }">
                <span class="rating-error-msg">Поставьте оценку</span>
				<star-rating :border-width="6"
							 active-color="#ffbf35"
							 border-color="#ffbf35"
							 :increment="0.5"
							 inactive-color="#fff"
							 :padding="2"
							 :star-size="18"
							 :show-rating="false"
							 :inline="true"
							 @rating-selected="setRating"
				></star-rating>
				<input type="hidden" name="PROPERTY[40][0]" value="<?=@$_REQUEST["PROPERTY"][40][0];?>" v-model="rating"  v-validate="'required'" />
			</div>
			<input type="hidden" name="section" v-model="section"/>
			<input type="hidden" name="subcat" v-if="activeSubCat"  :value="activeSubCat"/>
			<div class="input-element" :class="{ 'has-error' : errors.has('PROPERTY[NAME][0]') }">
				<label>
					<span class="form-item-label">
						Имя <span class="required">*</span>
					</span>
					<span class="form-item-error">Проверьте правильность ввода</span>
					<input type="text" name="PROPERTY[NAME][0]" value="<?=@$_REQUEST["PROPERTY"]["NAME"][0];?>" v-validate="'required|alpha_spaces'"/>
				</label>
			</div>
			<div class="input-element" :class="{ 'has-error' : errors.has('PROPERTY[43][0]') }">
				<label>
					<span class="form-item-label">
						E-mail <span class="required">*</span>
					</span>
					<span class="form-item-error">Проверьте правильность ввода</span>
					<input type="text" name="PROPERTY[43][0]" value="<?=@$_REQUEST["PROPERTY"][43][0];?>" v-validate="'required|email'"/>
				</label>
			</div>
			<div class="input-element"  :class="{ 'has-error' : errors.has('PROPERTY[PREVIEW_TEXT][0]') }">
				<label>
					<span class="form-item-label">
						Текст сообщения <span class="required">*</span>
					</span>
					<span class="form-item-error">Проверьте правильность ввода</span>
					<textarea row="5" name="PROPERTY[PREVIEW_TEXT][0]" v-validate="'required'"><?=@$_REQUEST["PROPERTY"]["PREVIEW_TEXT"][0];?></textarea>
				</label>
			</div>
			<div class="reviews-form-send" >
				<div class="tairai__recaptcha">
					<div id="reviews-recaptcha" data-recaptcha=""></div>
					<input data-recaptcha-id="reviews-recaptcha" type="hidden" v-validate="'required|in:Y'" />
				</div>
				<input type="hidden" name="iblock_submit" value="Сохранить">
				<button class="button" type="submit" name="iblock_submit" value="Сохранить">Отправить</button>
			</div>
			<div class="form-required">
				<span class="required">*</span> — поля обязательные для заполнения
			</div>
		</form>
	</div>
</div>



<?/*

<div class="is-styled">
	<form method="POST" name="iblock_add" enctype="multipart/form-data">
		<?=bitrix_sessid_post()?>
		<div class="input-element<?if (strpos($errors, "'Оценка'") !== false) echo " has-error";?>">
			<label>
				<span class="form-item-label">&nbsp;</span>
				<span class="form-item-error">Проверьте правильность ввода</span>
				<input placeholder="Рейтинг" name="PROPERTY[40][0]" value="<?=$_REQUEST["PROPERTY"][40][0];?>">
			</label>
		</div>
		<div class="input-element<?if (strpos($errors, "'Название'") !== false) echo " has-error";?>">
			<label>
				<span class="form-item-label">&nbsp;</span>
				<span class="form-item-error">Проверьте правильность ввода</span>
				<input placeholder="Ваше имя*" name="PROPERTY[NAME][0]" value="<?=$_REQUEST["PROPERTY"]["NAME"][0];?>">
			</label>
		</div>
		<div class="input-element<?if (strpos($errors, "'E-mail'") !== false) echo " has-error";?>">
			<label>
				<span class="form-item-label">&nbsp;</span>
				<span class="form-item-error">Проверьте правильность ввода</span>
				<input type="email" placeholder="E-mail*" name="PROPERTY[43][0]" value="<?=$_REQUEST["PROPERTY"][43][0];?>">
			</label>
		</div>
		<div class="input-element<?if (strpos($errors, "'Описание для анонса'") !== false) echo " has-error";?>">
			<span class="form-item-label">&nbsp;</span>
			<textarea rows="5" placeholder="Текст отзыва*" name="PROPERTY[PREVIEW_TEXT][0]"><?=$_REQUEST["PROPERTY"]["PREVIEW_TEXT"][0];?></textarea>
		</div>
		<button class="button" type="submit" name="iblock_submit" value="Сохранить">Отправить</button>
	</form>
</div>


<div id="reviews_form" class="modal-self<?if (strlen($errors)):?> is-show<?endif;?>">
	<div class="thanks-modal">
		<div class="sale-text">Добавить отзыв</div>
		<div class="is-styled">
			<form method="POST" name="iblock_add" enctype="multipart/form-data">
				<?=bitrix_sessid_post()?>
				<div class="input-element<?if (strpos($errors, "'Оценка'") !== false) echo " has-error";?>">
					<label>
						<span class="form-item-label">&nbsp;</span>
						<span class="form-item-error">Проверьте правильность ввода</span>
						<input placeholder="Рейтинг" name="PROPERTY[40][0]" value="<?=$_REQUEST["PROPERTY"][40][0];?>">
					</label>
				</div>
				<div class="input-element<?if (strpos($errors, "'Название'") !== false) echo " has-error";?>">
					<label>
						<span class="form-item-label">&nbsp;</span>
						<span class="form-item-error">Проверьте правильность ввода</span>
						<input placeholder="Ваше имя*" name="PROPERTY[NAME][0]" value="<?=$_REQUEST["PROPERTY"]["NAME"][0];?>">
					</label>
				</div>
				<div class="input-element<?if (strpos($errors, "'E-mail'") !== false) echo " has-error";?>">
					<label>
						<span class="form-item-label">&nbsp;</span>
						<span class="form-item-error">Проверьте правильность ввода</span>
						<input type="email" placeholder="E-mail*" name="PROPERTY[43][0]" value="<?=$_REQUEST["PROPERTY"][43][0];?>">
					</label>
				</div>
				<div class="input-element<?if (strpos($errors, "'Описание для анонса'") !== false) echo " has-error";?>">
					<span class="form-item-label">&nbsp;</span>
					<textarea rows="5" placeholder="Текст отзыва*" name="PROPERTY[PREVIEW_TEXT][0]"><?=$_REQUEST["PROPERTY"]["PREVIEW_TEXT"][0];?></textarea>
				</div>
				<div class="row">
					<div class="col" style="padding-top: 34px;">
						<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"];?>">
						<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"];?>">
					</div>
					<div class="col">
						<div class="input-element<?if (strpos($errors, "слово с картинки") !== false) echo " has-error";?>">
							<label>
								<span class="form-item-label">&nbsp;</span>
								<span class="form-item-error">Неверно введено слово с картинки</span>
								<input type="text" name="captcha_word" placeholder="Текст с картинки*">
							</label>
						</div>
					</div>
				</div>
				<button class="button" type="submit" name="iblock_submit" value="Сохранить">Отправить</button>
			</form>
		</div>
	</div>
</div>

<div id="review-modal" class="modal-content-inner">
	<div class="simple-modal is-styled">
		<form action=""  @submit.prevent="submit" ref="reviewForm">
			<div class="title-h1">
				<span>Добавить отзыв</span>
			</div>
			<div class="reviews-rating">
				<star-rating :border-width="6"
							 active-color="#ffbf35"
							 border-color="#ffbf35"
							 :increment="0.5"
							 inactive-color="#fff"
							 :padding="2"
							 :star-size="18"
							 :show-rating="false"
							 :inline="true"
							 @rating-selected="setRating"
				></star-rating>
				<input type="hidden" name="rate"  v-model="rating"/>
			</div>
			<input type="hidden" name="section" v-model="section"/>
			<div class="input-element" :class="{ 'has-error' : errors.has('name') }">
				<label>
					<span class="form-item-label">
						Имя <span class="required">*</span>
					</span>
					<span class="form-item-error">Проверьте правильность ввода</span>
					<input type="text" name="name" v-validate="'required|alpha_spaces'"/>
				</label>
			</div>
			<div class="input-element" :class="{ 'has-error' : errors.has('email') }">
				<label>
					<span class="form-item-label">
						E-mail <span class="required">*</span>
					</span>
					<span class="form-item-error">Проверьте правильность ввода</span>
					<input type="text" name="email" v-validate="'required|email'"/>
				</label>
			</div>
			<div class="input-element"  :class="{ 'has-error' : errors.has('message') }">
				<label>
					<span class="form-item-label">
						Текст сообщения <span class="required">*</span>
					</span>
					<span class="form-item-error">Проверьте правильность ввода</span>
					<textarea row="5"  name="message"  v-validate="'required'"></textarea>
				</label>
			</div>
			<div class="reviews-form-send" >
				<div class="tairai__recaptcha">
					<div id="reviews-recaptcha" data-recaptcha=""></div>
					<input data-recaptcha-id="reviews-recaptcha" type="hidden" v-validate="'required|in:Y'" />
				</div>
				<button class="button">Отправить</button>
			</div>
			<div class="form-required">
				<span class="required">*</span> — поля обязательные для заполнения
			</div>
		</form>
	</div>
</div>


*/?>