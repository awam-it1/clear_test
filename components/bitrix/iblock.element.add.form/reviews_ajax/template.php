<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (strlen($arResult["MESSAGE"])) {
    echo json_encode(Array(
        'SUCCESS' => true,
        'TITLE' => 'Спасибо за отзыв!',
        'MESSAGE' => 'Отзыв будет опубликован после модерации',
    ));
}

