<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arResult["JSON_LIST"] = array_map(function($arItem) {
    

    $salon = array_shift($arItem["DISPLAY_PROPERTIES"]["SALON"]["LINK_ELEMENT_VALUE"]);
    $arMaster = [
        "id" => $arItem["ID"]
        , "img" => $arItem["PREVIEW_PICTURE"]["SRC"]
        , "name" => $arItem["NAME"]
        , "salon" => $salon["NAME"]
        , "experience" => "Стаж работы: ".$arItem["DISPLAY_PROPERTIES"]["WORK_EXPERIENCE"]["~VALUE"]
        , "reviewsLink" => "/reviews/?type=master&id=".$arItem["ID"]
        , "enrollLink" => "/online-recording/?m0=".$arItem["ID"]
        , "servicesAll" => false // Нужно для "Покзазать все", всегда false
        , "services" => []
        , "careerAll" => false // Нужно для "Покзазать все", всегда false
        , "career" => [ $arItem["DISPLAY_PROPERTIES"]["TRAINING"]["~VALUE"] ]
        , "link" => $arItem["DETAIL_PAGE_URL"]
    ];


    // услуги
        foreach ($arItem["DISPLAY_PROPERTIES"]["ALL_SERVICES"]["LINK_SECTION_VALUE"] as $service) {
            $arMaster["services"][] = $service["NAME"];
        }

    // отзывы
        $rsReviews = CIBlockElement::GetList(
            Array("SORT" => "ASC")
            , Array("IBLOCK_ID" => 18, "ACTIVE" => "Y", "PROPERTY_MASTER" => $arItem["ID"])
            , false
            , false
            , Array("PROPERTY_RATING")
        );

        $reviews_count = 0;
        $master_rating = 0;
        while ($arReviews = $rsReviews->Fetch()) {
            $reviews_count++;
            $master_rating += $arReviews["PROPERTY_RATING_VALUE"];
        }

        if ($reviews_count) {
            $arMaster["reviews"] = $reviews_count." отзыв".word_end($reviews_count);
            $arMaster["rating"] = floatval(number_format($master_rating / $reviews_count, 1, ".", ""));
        }

    return $arMaster;
}, $arResult["ITEMS"]);
$arResult["JSON_LIST"] = array();
?>