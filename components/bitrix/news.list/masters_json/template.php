<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// echo '<script>var log_php_object = '.json_encode($arResult).';</script>';
if ($arParams["IS_AJAX"]):
    echo json_encode(Array(
        'SUCCESS' =>  true,
        'MASTERS' => $arResult["JSON_LIST"],
        'NEXT_PAGE' => ($arResult["NAV_RESULT"]->NavPageNomer < $arResult["NAV_RESULT"]->NavPageCount)
    ));
else:?>
    <script>
     /**
         *
         * Если у мастера нет отзывов,
         * то reviews должен быть false,
         * или отсутсовать.
         * reviewsLink при этом может быть
         * любой или не быть вовсе.
         *
         * Могут отсутствовать
         * experience и career
         */

        window.TAIRAI.masters = <?=json_encode($arResult["JSON_LIST"]);?>;

        window.TAIRAI.nextPage = <?if ($arResult["NAV_RESULT"]->NavPageNomer < $arResult["NAV_RESULT"]->NavPageCount) {
                echo "true";
            } else {
                echo "false";
            }?>;
    </script>
<?endif;?>
