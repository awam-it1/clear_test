<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// print_r($arResult);
?>
<div class="index-icon-grid revealer">
	<?foreach($arResult["ITEMS"] as $arItem):?>
	<a href="<?=$arItem["DISPLAY_PROPERTIES"]["LINK"]["VALUE"];?>" class="ii-item">
		<div class="ii-icon">
			<?=file_get_contents($_SERVER["DOCUMENT_ROOT"].$arItem["DISPLAY_PROPERTIES"]["ICON"]["FILE_VALUE"]["SRC"]);?>
		</div>
		<div class="ii-title"><?=$arItem["NAME"];?></div>
		<div class="ii-descr"><?=$arItem["~PREVIEW_TEXT"];?></div>
	</a>
	<?endforeach;?>
</div>