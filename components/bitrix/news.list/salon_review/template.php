<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// print_r($arResult);
if (count($arResult["ITEMS"])):
?>
  <?$this->SetViewTarget("reviews");//дальше контент который буферизируется?>
    <div class="shop__nav-link"><a href="#salon-reviews">Отзывы</a></div>
  <?$this->EndViewTarget();//конец буферизации?>
  <?$this->SetViewTarget("reviews_mobile");//дальше контент который буферизируется?>
    <option value="#salon-reviews">Отзывы</option>
  <?$this->EndViewTarget();//конец буферизации?>
<div class="salon-reviews-list-parent" id="salon-reviews">
	<h2 class="cntr">
		Отзывы
	</h2>
	<div class="srl-all-reviews">
		<a class="is-link" href="/reviews/<?/*=$arParams["LINK"];*/?>">
			Все отзывы
		</a>
	</div>

	<div class="srl-inner">
		<div class="salon-reviews-slider" ref="salonReviewsSlider">
			<div class="swiper-container" >
				<div class="swiper-wrapper">
					<?foreach($arResult["ITEMS"] as $arItem):?>
					<div class="swiper-slide">
						<div class="salon-quote-box">
							<div class="sqb-content"><?=$arItem["~PREVIEW_TEXT"];?></div>
							<div class="sqb-author"><?=$arItem["NAME"];?></div>
							<div class="sqb-date"><?=$arItem["DISPLAY_ACTIVE_FROM"];?></div>
						</div>
					</div>
					<?endforeach;?>
				</div>
			</div>
			<div class="ig-arrow ig-left">
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20" height="20" viewBox="0 0 20 20"><g transform="translate(-90 -402)"><image width="20" height="20" transform="translate(90 402)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAACa0lEQVQ4T5VUTWgTQRR+bybZbo3bNiWppggSiYhEkVoIQkGqoBgUiiAePSkoiodSGvCg4sGDKAiCeKkXvYigSK1W0R4qKEWlSvGnINEc2vrTRGPAEpPMk9nNJpvNZhvfYWdm571v5n3fe4PQpP34PHp0haoNeT1tawkRWgI9XqdQXA4vkxq771NDcQSFmb4C8qSu6q2srRgNAReTd0+0tqy+zEDlAKTHyK8egERqaCtHRGPDYo6A6S9jL1VPVy8JQh3BDDPnCLBUWBgJrhs4vCzgz9TjNBe+Tt3RAlBzRQIoQu5XZ3iP3xUwnXz0m4GqkShfSbIkAKDMlrwuAYEcEYgKWnZNMDgw75jy90/35pnwhYAIAA0miAgQZWgdVQYGz2eDkX0ddYALs7eectG+g4BqODVFcKDQYAQZlTD3KrThQMwExbmZm7s56xgXZBHAhLUj2giT6TNkRPAn2R09GNEPmXt3568ogmOR1tWoTSTrknEmiLLXMfX2Rh6EohicGfQ1osytCRALJYb5Efz4+spOL7U/AWJoVdEoZENROTaqIsaAiqXM+/Wxk5tMH5h9cXGUscBekjy6mKm4HMvBVMDM5MbYYH9FFHPy4fmlJFJb2A3QrpGAXDraNxhwrEP5c2byQoaB5q+mWC3kuo5gRK1+6g5Hj39tCCg33kyc/8ZxZZfbTY18lxY39yeCdQc5BU5PnH2GQuuTolefmFpPphSvbdmeONYUoHSaHj99pMSVqyA8HuPJqZrsxp5dw80/X9bgqYenbhMp+5E4ryjJBMXiZ/7vgbWnMvUgcUgQJgB4BBnHbfFzejPY7R+83t3zXtOZRwAAAABJRU5ErkJggg=="/></g></svg>
			</div>
			<div class="ig-arrow ig-right">
				<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"><defs><linearGradient id="a" x1="10" x2="10" y1="10" y2="20" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#d4be6f"/><stop offset="1" stop-color="#b3a05d"/></linearGradient><linearGradient id="b" x1="10" x2="10" y2="20" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#fff7b4" stop-opacity=".7"/><stop offset=".99" stop-color="#ccc370" stop-opacity=".7"/></linearGradient><filter id="c" width="200%" height="200%" x="-50%" y="-50%"><feGaussianBlur in="SourceGraphic" result="SvgjsFeGaussianBlur1067Out"/></filter><mask id="d"><path fill="#fff" d="M6.28 0C10.87.79 15.99 3.38 20 10c-4.01 6.62-9.13 9.22-13.72 10l-.01-.22c-.02-.85-.09-1.65-.22-2.39a10.78 10.78 0 0 0-1.55-3.91A8.83 8.83 0 0 0 0 10a8.83 8.83 0 0 0 4.51-3.5c.33-.48.6-1.01.91-1.76.25-.64.5-1.33.63-2.12.13-.75.2-1.55.22-2.39z"/></mask><linearGradient id="e" x1="10" x2="10" y2="19.78" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#ffeed9"/><stop offset="1" stop-color="#c7b179"/></linearGradient></defs><path fill="#d4be6f" d="M6.28 0C10.87.79 15.99 3.38 20 10c-4.01 6.62-9.13 9.22-13.72 10l-.01-.22c-.02-.85-.09-1.65-.22-2.39a10.78 10.78 0 0 0-1.55-3.91A8.83 8.83 0 0 0 0 10a8.83 8.83 0 0 0 4.51-3.5c.33-.48.6-1.01.91-1.76.25-.64.5-1.33.63-2.12.13-.75.2-1.55.22-2.39z"/><path fill="url(#a)" d="M6.28 0C10.87.79 15.99 3.38 20 10c-4.01 6.62-9.13 9.22-13.72 10l-.01-.22c-.02-.85-.09-1.65-.22-2.39a10.78 10.78 0 0 0-1.55-3.91A8.83 8.83 0 0 0 0 10a8.83 8.83 0 0 0 4.51-3.5c.33-.48.6-1.01.91-1.76.25-.64.5-1.33.63-2.12.13-.75.2-1.55.22-2.39z"/><path fill="url(#b)" d="M6.28 0C10.87.79 15.99 3.38 20 10c-4.01 6.62-9.13 9.22-13.72 10l-.01-.22c-.02-.85-.09-1.65-.22-2.39a10.78 10.78 0 0 0-1.55-3.91A8.83 8.83 0 0 0 0 10a8.83 8.83 0 0 0 4.51-3.5c.33-.48.6-1.01.91-1.76.25-.64.5-1.33.63-2.12.13-.75.2-1.55.22-2.39z"/><path fill="#fff" fill-opacity=".5" d="M0 21V-1h20v22zM6.27 1.23c-.02.84-.09 1.64-.22 2.39-.13.79-.38 1.48-.63 2.12a9.19 9.19 0 0 1-2.04 3.13A8.83 8.83 0 0 1 0 11a8.83 8.83 0 0 1 4.5 3.48 10.78 10.78 0 0 1 1.55 3.91c.13.74.2 1.54.22 2.39l.01.22c4.59-.78 9.71-3.38 13.72-10-4.01-6.62-9.13-9.21-13.72-10z" filter="url(#c)" mask="url(&quot;#d&quot;)"/><path fill="url(#e)" d="M6.28 0C10.87.79 15.99 3.38 20 10c-4.01 6.62-9.13 9.22-13.72 10l-.01-.22c-.02-.85-.09-1.65-.22-2.39a10.78 10.78 0 0 0-1.55-3.91A8.83 8.83 0 0 0 0 10a8.83 8.83 0 0 0 4.51-3.5c.33-.48.6-1.01.91-1.76.25-.64.5-1.33.63-2.12.13-.75.2-1.55.22-2.39z"/></svg>
			</div>
		</div>
	</div>

	<div class="srl-send-review">
		<a class="button" href="#review-modal" data-modal-open="">
			Оставить отзыв
		</a>


        <div id="review-modal" class="modal-content-inner">
            <div class="simple-modal is-styled">
                <form action="/ajax/reviewNew.php"
                      data-vv-scope="salon-review-form"
                      @submit.prevent="submit('salon-review-form', 'reviewForm')"
                      ref="reviewForm"
                      method="POST"
                      enctype="multipart/form-data">
                    <div class="title-h1">
                        <span>Добавить отзыв</span>
                    </div>
                    <div class="reviews-rating"  :class="{ 'has-error' : errors.has('salon-review-form.rating') }">
                        <span class="rating-error-msg">Поставьте оценку</span>
                        <star-rating :border-width="6"
                                     active-color="#ffbf35"
                                     border-color="#ffbf35"
                                     :increment="0.5"
                                     inactive-color="#fff"
                                     :padding="2"
                                     :star-size="18"
                                     :show-rating="false"
                                     :inline="true"
                                     v-model="rating"
                        ></star-rating>
                        <input type="hidden" name="rating"  v-model="rating"  v-validate="'required'" />
                    </div>
                    <div class="input-element" :class="{ 'has-error' : errors.has('salon-review-form.name') }">
                        <label>
                                <span class="form-item-label">
                                    Имя <span class="required">*</span>
                                </span>
                            <span class="form-item-error">Проверьте правильность ввода</span>
                            <input type="text" name="name" v-validate="'required|alpha_spaces'" v-model="form.name"/>
                        </label>
                    </div>
                    <div class="input-element" :class="{ 'has-error' : errors.has('salon-review-form.email') }">
                        <label>
                                <span class="form-item-label">
                                    E-mail <span class="required">*</span>
                                </span>
                            <span class="form-item-error">Проверьте правильность ввода</span>
                            <input type="text" name="email" v-validate="'required|email'" v-model="form.email"/>
                        </label>
                    </div>
                    <div class="input-element"  :class="{ 'has-error' : errors.has('salon-review-form.text') }">
                        <label>
                                <span class="form-item-label">
                                    Текст сообщения <span class="required">*</span>
                                </span>
                            <span class="form-item-error">Проверьте правильность ввода</span>
                            <textarea row="5" name="text" v-validate="'required'" v-model="form.msg"></textarea>
                        </label>
                    </div>
                    <div class="reviews-form-send" >
                        <div class="tairai__recaptcha">
                            <div id="salon-reviews-recaptcha" data-recaptcha=""></div>
                            <input data-recaptcha-id="salon-reviews-recaptcha" type="hidden" v-validate="'required|in:Y'" />
                        </div>
                        <input type="hidden" name="iblock_submit" value="Сохранить">
                        <button class="button" type="submit" name="iblock_submit" value="Сохранить">Отправить</button>
                    </div>
                    <input type="hidden" name="objectid" value="<?=(!isset($arParams["REVIEW_OBJ_ID"]) ? $arResult["ID"] : $arParams["REVIEW_OBJ_ID"]) ;?>">
                    <input type="hidden" name="type" value="<?=$arParams["REVIEW_TYPE"];?>">
                    <div class="form-required">
                        <span class="required">*</span> — поля обязательные для заполнения
                    </div>
                </form>
            </div>
        </div>
	</div>
</div>

<?endif;?>