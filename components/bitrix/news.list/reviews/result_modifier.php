<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// echo '<script>var log_php_object = '.json_encode($arResult).';</script>';

$sections_id = array_filter(
	array_map(function($arItem) {
		return $arItem["IBLOCK_SECTION_ID"];
	}, $arResult["ITEMS"])
);

$SECTIONS = Array();

if (count($sections_id)) {
	$rsSection = CIBlockSection::GetList(Array('SORT' => 'ASC'), Array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "ACTIVE" => "Y", "ID" => $sections_id), false, Array("ID", "NAME", "CODE"));
	while ($arSection = $rsSection->GetNext()) {
		$SECTIONS[$arSection["ID"]] = $arSection;
	}
}

$IBLOCK_ID = $arParams["IBLOCK_ID"];
$arResult["ITEMS"] = array_map(function($arItem) use ($SECTIONS, $IBLOCK_ID) {
	if (isset($SECTIONS[$arItem["IBLOCK_SECTION_ID"]])) {
		$arItem["SECTION"] = $SECTIONS[$arItem["IBLOCK_SECTION_ID"]];
		switch ($IBLOCK_ID) {
			case 18:
				$arItem["SECTION"]["URL"] = $arItem["SECTION"]["CODE"]; 
				break;
			case 11:
				$arItem["SECTION"]["URL"] = "/salons/".$arItem["SECTION"]["CODE"]."/"; 
				break;
		}
	}
	return $arItem;
}, $arResult["ITEMS"]);


$arResult["ITEMS"] = array_map(function($arItem) {
	$_arItem = Array(
		"title" => ""//$arItem["NAME"]
		, "rating" => $arItem["DISPLAY_PROPERTIES"]["RATING"]["VALUE"]
		, "info" => $arItem["NAME"].", ".mb_strtolower($arItem["DISPLAY_ACTIVE_FROM"])
		, "text" => $arItem["~PREVIEW_TEXT"]
	);
	if (strlen($arItem["~DETAIL_TEXT"])) {
		$_arItem["answer"] = Array(
			"from" => "Тайрай"
			, "text" => $arItem["~DETAIL_TEXT"]
		);
	}
	return $_arItem;
}, $arResult["ITEMS"]);

?>
