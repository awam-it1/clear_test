<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// echo '<script>var log_php_object = '.json_encode($arResult).';</script>';
if (isset($arParams["IS_AJAX"])):
	echo json_encode(Array(
		'SUCCESS' => true,
		'NEXT_PAGE' => (strlen($arResult["NAV_STRING"]) > 0),
		'REVIEWS' => $arResult["ITEMS"]
	));
elseif (count($arResult["ITEMS"])):?>
<script>
	window.TAIRAI.reviewsData.push({
		catId: 'IBLOCK_ID_<?=empty($arParams["IBLOCK_ID"]) ? $arParams["DEFAULT_IBLOCK_ID"] : $arParams["IBLOCK_ID"];?>',
		catName: '<?=$arParams["NAME"];?>',
		nextPage: <?=(strlen($arResult["NAV_STRING"]) > 0 ? "true" : "false");?>,
		isLoad: false,
		currentPage: 1,
		activeSubCat: -1,
		reviews: <?=json_encode($arResult["ITEMS"])?>
	});
</script>
<?endif;?>