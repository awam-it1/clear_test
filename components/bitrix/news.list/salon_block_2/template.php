<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// echo '<script>var log_php_object = '.json_encode($arResult).';</script>';

if (count($arResult["ITEMS"])):
?>
<div class="salon-card-list">
	<div class="scl-inner">
		<?foreach($arResult["ITEMS"] as $arItem):?>
		<div class="scl-card-item">
			<a class="scl-card-link" href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"];?>">
				<img class="scl-card-image" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"];?>" />
			</a>
			<div class="scl-card-header"><?=$arItem["NAME"];?></div>
			<div class="scl-card-preview"><?=$arItem["~PREVIEW_TEXT"];?></div>
			<div class="scl-card-details-link">
				<a class="is-link" href="<?=$arItem["PROPERTIES"]["LINK"]["~VALUE"];?>">
					Подробнее
				</a>
			</div>
		</div>
		<?endforeach;?>
	</div>
</div>
<?endif;?>
