<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
 //print_r($arResult["JS_LIST"]);
?>
<script>
window.TAIRAI.shop.push({
	name: '<?=$arResult["SECTION"]["PATH"][0]["CODE"];?>'
	, text: '<?=$arResult["SECTION"]["PATH"][0]["NAME"];?>'
	, filterParam: <?
		$APPLICATION->IncludeComponent("bitrix:catalog.smart.filter", "shop_filter", 
			Array(
				"COMPONENT_TEMPLATE" => ".default",
				"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"SECTION_ID" => $arResult["SECTION"]["PATH"][0]["ID"],
				"SECTION_CODE" => "",
				"FILTER_NAME" => "",
				"HIDE_NOT_AVAILABLE" => "N",
				"TEMPLATE_THEME" => "blue",
				"FILTER_VIEW_MODE" => "horizontal",
				"DISPLAY_ELEMENT_COUNT" => "Y",
				"SEF_MODE" => "N",
				"CACHE_TYPE" => "A",
				"CACHE_TIME" => "36000000",
				"CACHE_GROUPS" => "Y",
				"SAVE_IN_SESSION" => "N",
				"INSTANT_RELOAD" => "Y",
				"PAGER_PARAMS_NAME" => "",
				"PRICE_CODE" => array(
					0 => "BASE",
				),
				"CONVERT_CURRENCY" => "Y",
				"XML_EXPORT" => "N",
				"SECTION_TITLE" => "-",
				"SECTION_DESCRIPTION" => "-",
				"POPUP_POSITION" => "left",
				"SEF_RULE" => "/examples/books/#SECTION_ID#/filter/#SMART_FILTER_PATH#/apply/",
				"SECTION_CODE_PATH" => "",
				"SMART_FILTER_PATH" => "",
				"CURRENCY_ID" => "RUB"
				),
			false
		);
		?>
	, list: <?=json_encode(array_values($arResult["JS_LIST"]));?>
});
</script>
