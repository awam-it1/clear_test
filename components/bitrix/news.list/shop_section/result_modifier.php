<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// print_r($arResult);
CModule::IncludeModule("catalog");

// получаем список салонов
global $CITY_CODE;
$salon_ids = [];
$sDb = CIBlockElement::GetList(
    [],
    [
        "IBLOCK_ID" => IBLOCK_salons_salons,
        "ACTIVE" => "Y",
        "SECTION_CODE" => $CITY_CODE,
        "INCLUDE_SUBSECTIONS" => "Y"
    ],
    false,
    false,
    [
        "ID",
        "NAME",
        "PROPERTY_ALL_SERVICES",
        "PROPERTY_PRICE_TYPE"
    ]
);
while($sOb = $sDb->GetNext()) {
	$salon_ids[$sOb['PROPERTY_PRICE_TYPE_VALUE']] = 1;
}

// получаем список секций услуг (для сортировки)
// формируем массив вида idсекции => значение_сортировки
$services_sorts = [];
$sDb = CIBlockSection::GetMixedList(
	[],
	[
		"IBLOCK_ID" => IBLOCK_salons_services,
		"ACTIVE" => "Y",
		"INCLUDE_SUBSECTIONS" => "Y"
	],
	false,
	false,
	[
		"ID",
		"NAME",
		"PROPERTY_ID1C",
		"ID1C",
		"SORT"
	]
);
while($sOb = $sDb->GetNext()) {
	if($sOb["SORT"]!=500)
		$services_sorts[$sOb['ID']] = $sOb["SORT"];
}

// получаем список услуг (для сортировки)
// чтобы получить id секции по услуге
$services_section_ids = [];
$sDb = CIBlockElement::GetList(
	[],
	[
		"IBLOCK_ID" => IBLOCK_salons_services,
		"ACTIVE" => "Y",
		"INCLUDE_SUBSECTIONS" => "Y"
	],
	false,
	false,
	[
		"ID",
		"NAME",
		"PROPERTY_ID1C",
		"ID1C",
		"SORT",
		"IBLOCK_SECTION_ID"
	]
);
while($sOb = $sDb->GetNext()) {
	$services_section_ids[$sOb['PROPERTY_ID1C_VALUE']] = $sOb["IBLOCK_SECTION_ID"];
}


// получаем список ценовых предложений
$prices = [];
$sDb = CIBlockElement::GetList([], [
	"IBLOCK_ID" => IBLOCK_salons_prices,
	"ACTIVE" => "Y",
	"INCLUDE_SUBSECTIONS" => "Y" 
], false, false, [
	"ID",
	"NAME",
	"PROPERTY_SERVICE",
	"PROPERTY_TYPE",
	"PROPERTY_VALUE",
]);
while ( $sOb = $sDb->GetNext() ) {
	
	$prices[$sOb["PROPERTY_SERVICE_VALUE"].':'.$sOb["PROPERTY_TYPE_VALUE"]] = intval($sOb["PROPERTY_VALUE_VALUE"]);
}

$elDb = CIBlockElement::GetList([], [
	"PROPERTY_ID1C" => $id1c
]);

// формируем из товаров массив объектов для json и их фильтра
$arResult["JS_LIST"] = Array();
foreach ($arResult["ITEMS"] as $arItem) {
	$id1c = $arItem["DISPLAY_PROPERTIES"]["ID1C"]["VALUE"];
	$sort = $services_sorts[$services_section_ids[$id1c]];
	$filter = Array();
	foreach ($arItem["DISPLAY_PROPERTIES"] as $property) {
		$value = $property["VALUE"];
		if (!is_array($value)) {
			$value = Array($value);
		}
		$filter[$property["CODE"]] = $value;
	}
	
	// поиск минимальной цены на услугу в текущем городе
	$price_text = '';
	$price = 0;
	$price_min = 0;
	$mustAdd = false;
	foreach (array_keys($salon_ids) as $salon_id) {
		
		
		if(!empty($prices[$id1c.':'.$salon_id])) {
			
			$price_temp = $prices[$id1c.':'.$salon_id];
			
			if ($price_min == 0) {
				$price_min = $price_temp;
				$price = $price_temp;
				$mustAdd = true;
			}
			if ($price_temp < $price_min) {
				$price_min = $price_temp;
				$price = $price_temp;
				$mustAdd = true;
			}
		}
	}
	if (empty($price_min)) {
		
		$price_text = 'недоступно в выбранном городе';
		continue;
	} else {
		
		$price_text = 'от ' . $price_min . ' руб.';
	}
	
	if($mustAdd) {
		
		if(!empty($arResult["JS_LIST"][$arItem["NAME"]]) && $arResult["JS_LIST"][$arItem["NAME"]]['price'] < $price) {
			
			continue;
		}
		
		$img = $arItem['PREVIEW_PICTURE']['SRC'];
		if(!is_file($_SERVER["DOCUMENT_ROOT"].$img))
			$img = '/local/include/img/stub.jpg';
		$sort = (!empty($sort)) ? (int)$sort : 0;
		$arResult["JS_LIST"][$arItem["NAME"]] = Array(
			"id" => $arItem["ID"],
			"sort_id" => $arItem["ID"],
			"img" => $img,
			"link" => $arItem["DETAIL_PAGE_URL"],
			"price" => $price,
			"priceText" => $price_text,
			"name" => $arItem["~NAME"],
			"text" => "<p>" . $arItem["~PREVIEW_TEXT"] . "</p>",
			"filterParam" => $filter,
			"inCart" => false,
			"sort" =>  $sort
		);
	}
}

?>