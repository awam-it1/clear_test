<?php
use Bitrix\Iblock\InheritedProperty;
$ipropValues = new InheritedProperty\SectionValues(IBLOCK_salons_services, $arResult['ITEMS'][0]['IBLOCK_SECTION_ID']);
//Получить значения
$values = $ipropValues->getValues();
//Сбросить кеш
$ipropValues->clearValues();