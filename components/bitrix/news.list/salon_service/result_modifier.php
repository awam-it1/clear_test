<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// echo '<script>var log_php_object = '.json_encode($arResult).';</script>';
use Bitrix\Iblock\InheritedProperty;
$ipropValues = new InheritedProperty\SectionValues(IBLOCK_salons_services, $arResult['ITEMS'][0]['IBLOCK_SECTION_ID']);
//Получить значения
$values = $ipropValues->getValues();
//Сбросить кеш
$ipropValues->clearValues();

if ($arResult["SECTION"]) {

    // прямой родитель
        $arResult["PARENT"] = array_pop($arResult["SECTION"]["PATH"]);

    // если секция содержит детей, значит это не услуга, а раздел, и нам сюда нельзя.
        $rsChildSection = CIBlockSection::GetList(
            Array('SORT' => 'ASC')
            , Array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "SECTION_ID" => $arResult["PARENT"]["ID"])
            , false
            , Array("ID")
            , Array('nPageSize' => 1)
        );
        if ($rsChildSection->Fetch()) {
            $APPLICATION->RestartBuffer();
            CHTTP::SetStatus("404 Not Found");
            @define("ERROR_404","Y");
            include($_SERVER['DOCUMENT_ROOT']."/404.php");
            die();
        }

        $rsSection = CIBlockSection::GetList(
            Array('SORT' => 'ASC')
            , Array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "ID" => $arResult["PARENT"]["ID"])
            , false
            , Array("ID", "UF_PICTURES", "UF_ID1C")
        );
        while ($arSection = $rsSection->GetNext()) {
            $arResult["PARENT"] = array_merge($arResult["PARENT"], $arSection);
        }

        GLOBAL $APPLICATION, $CITY;
        $APPLICATION->AddChainItem($arResult["PARENT"]["NAME"], "#");

    // идентификатор услуги и типы цены
        $service = $arResult["PARENT"]["UF_ID1C"];
        if (count($arResult["ITEMS"])) {
            $service = $arResult["ITEMS"][0]["DISPLAY_PROPERTIES"]["ID1C"]["VALUE"];
        }
        $price_types = GetPriceTypesByService($service);
        $metro = parse_url($_SERVER["REQUEST_URI"]);
        $metro = explode('/', $metro['path']);
        $metro = $metro[2];

    // салоны в которых доступна услуга
        $arResult["SALONS"] = [];
        $rsSalon = CIBlockElement::GetList(
            Array("SORT" => "ASC")
            , Array("IBLOCK_ID" => IBLOCK_salons_salons, "CODE" => $metro, "ACTIVE" => "Y", "INCLUDE_SUBSECTIONS" => "Y")
            , false
            , false
            , Array("ID", "NAME", "PROPERTY_ID1C", "PROPERTY_PRICE_TYPE", "PROPERTY_ALL_SERVICES")
        );
        while ($arSalon = $rsSalon->Fetch()) {
            if($arSalon['PROPERTY_ALL_SERVICES_VALUE']['service'] == $arResult['PARENT']['ID']) {
            	$arResult["SALONS"][$arSalon['ID']] = $arSalon;
                $arResult["SALONS"]['NAME'] = $arSalon['NAME'];
            }
        }

		if (count($arResult["SALONS"])<1) {
			$rsSalon = CIBlockElement::GetList(
				Array("SORT" => "ASC")
				, Array("IBLOCK_ID" => IBLOCK_salons_salons, "CODE" => $metro, "ACTIVE" => "Y", "INCLUDE_SUBSECTIONS" => "Y")
				, false
				, false
				, Array("ID", "NAME", "CODE", "PROPERTY_ID1C", "PROPERTY_PRICE_TYPE", "PROPERTY_ALL_SERVICES")
			);
			while ($arSalon = $rsSalon->Fetch()) {
				if (in_array($arSalon["PROPERTY_PRICE_TYPE_VALUE"], $price_types)) {
					$arResult["SALONS"][$arSalon['ID']] = $arSalon;
					$arResult["SALONS"]['NAME'] = $arSalon['NAME'];
				}
			}
		}
        
    // начальная цена
        $arResult["PRICE"] = null;
        if (count($arResult["SALONS"])) {
            $arResult["PRICE"] = GetPrice($arResult["SALONS"][0]["PROPERTY_PRICE_TYPE_VALUE"], $service);
        }
        //var_dump("GetPrice(".$arResult["SALONS"][0]["PROPERTY_PRICE_TYPE_VALUE"].", $service)");

        $arResult["PARENT"]["DESCRIPTION"] = str_replace('[город]', $CITY, $arResult["PARENT"]["DESCRIPTION"]);

    //настройка og-тегов
    \Bitrix\Main\Loader::includeModule('dev2fun.opengraph');
    \Dev2fun\Module\OpenGraph::Show($arResult['PARENT']['ID'], 'section');
}
//dump($arResult);
?>