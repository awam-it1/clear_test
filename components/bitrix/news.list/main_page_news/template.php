<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// echo '<script>var log_php_object = '.json_encode($arResult).';</script>';
?>
<div class="index-news">
	<div class="inews-swiper-container">
		<div class="swiper-wrapper index-news-inner">
			<?foreach($arResult["ITEMS"] as $arItem):?>
			<div class="inews-item revealer swiper-slide">
				<div class="inews-image" style="background-image: url('<?=$arItem["PREVIEW_PICTURE"]["SRC"];?>')">
				<?
				if (strlen($arItem["DISPLAY_ACTIVE_FROM"])) {
					$date = $arItem["DISPLAY_ACTIVE_FROM"];
				} else {
					$date = CIBlockFormatProperties::DateFormat($arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($arItem["DISPLAY_PROPERTIES"]["DATE_BEGIN"]["VALUE"], CSite::GetDateFormat()));
				}

				if (strlen($date) && (!empty($arItem["DISPLAY_PROPERTIES"]["DATE_BEGIN"]["VALUE"]))):?>
					<div class="inews-date"><?
						$paths = explode(" ", $date);
						echo $paths[0];
						echo " <span>".$paths[1]."</span>";
					?></div>
				<?endif;?>
				</div>
				<a href="<?=$arItem["DETAIL_PAGE_URL"];?>" class="inews-title"><?=$arItem["NAME"];?></a>
				<div class="inews-text">
					<p><?
						if (strlen($arItem["~PREVIEW_TEXT"])) {
							echo $arItem["~PREVIEW_TEXT"];		
						} else {
							echo TruncateText(HTMLToTxt($arItem["~DETAIL_TEXT"]), 120);
						}
					?></p>
				</div>
			</div>
			<?endforeach;?>
		</div>
		<div class="inews-slider-pagination mobile-visible"></div>
	</div>
</div>