<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// echo '<script>var log_php_object = '.json_encode($arResult).';</script>';
?>
<?if (count($arResult["ITEMS"])):?>
<div class="reviews-cnt">
	<?foreach($arResult["ITEMS"] as $key => $arItem):?>
	<div class="review">
		<div class="image">
			<div class="image-left">
				<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"];?>" alt="<?=$arItem["NAME"];?>">
			</div>
			<?if (strlen($arItem["DISPLAY_PROPERTIES"]["LINK"]["VALUE"])):?>
			<a href="<?=$arItem["DISPLAY_PROPERTIES"]["LINK"]["VALUE"];?>" target="_blank" rel="nofollow">
				<div class="title-h2"><?=$arItem["NAME"];?></div>
			</a>
			<?else:?>
				<div class="title-h2"><?=$arItem["NAME"];?></div>
			<?endif;?>
			<div>
				<p><?=$arItem["~PREVIEW_TEXT"];?></p>
			</div>
		</div>
	</div>
	<?endforeach;?>
</div>
<?endif;?>
