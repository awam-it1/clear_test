<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// echo '<script>var log_php_object = '.json_encode($arResult).';</script>';
// $rsPrice = CPrice::GetList(Array(), Array("=PRODUCT_ID" => $arTogetherProduct["ID"])); 
$arResult["ITEMS"] = array_filter($arResult["ITEMS"], function($arItem) {
    return $arItem["ID"] != BONUSES_ID;
});

$arResult["ITEMS"] = array_map(function($arItem) {
    $arItem["PRICE"] = CPrice::GetBasePrice($arItem["ID"]);
    return $arItem;
}, $arResult["ITEMS"]);
?>