<?
global $usercerts;
global $designs;
if (!empty($usercerts)):?>
    <div class="lkc-purchased" v-once>
        <h2> Сертификаты, которые вы купили в подарок</h2>
        <div class="lkcp-cnt">
            <?
            foreach($usercerts as $cert) {
                $data = json_decode($cert['DATA']);
                $design = $data->DESIGN;
                if(!is_file($_SERVER["DOCUMENT_ROOT"].$design))
                    $design = CFile::GetPath($designs[$design]);

                $image = (!empty($design)) ? $design : '/dist/img/temp/lk-certitficate.jpg';
                switch($cert['TYPE']) {
                    case 'el':
                        $cert_name = 'Электронный сертификат';
                        break;
                    case 'paper':
                        $cert_name = 'Cертификат';
                        break;
                    default:
                        $cert_name = 'Cертификат';
                }
                /*
                 // get image id by image path BITRIX can?
                $newWidth = 260;
                $newHeight = 162;
                var_dump($image);
                $subdir = explode('/', $image);
                unset($subdir[count($subdir)-1]);
                $subdir = implode($subdir, '/');
                $subdir = $subdir.'/';
                var_dump($subdir);


                $image_to_render = CFile::MakeFileArray($image);
                    //array($image, $subdir, $newWidth, $newHeight, 'image/jpeg');
                var_dump($image_to_render);
                //var_dump("CFile::ResizeImageGet(".$image.", Array(\"width\" => ".$newWidth.", \"height\" => ".$newHeight."), ".BX_RESIZE_IMAGE_PROPORTIONAL.", true)");
                if($renderImage = CFile::ResizeImageGet($image_to_render, Array("width" => $newWidth, "height" => $newHeight), BX_RESIZE_IMAGE_PROPORTIONAL, true))
                    $renderImage = $renderImage['src'];
                else {
                    $renderImage = $image;
                }*/

                ?>
                <div class="lkcp-item">
                    <div class="lkcp-title"><?=$cert_name;?> <?=$cert['CERTIFICATE_NUMBER'];?></div>
                    <div class="row">
                        <div class="col">
                            <img src="<?=$image;?>" class="certificate-preview" alt="">
                        </div>
                        <div class="col">
                            <div class="lkcp-info">
                                <span class="lkcpi-name">Баланс</span>
                                <span class="lkcpi-value"><?=$cert['SUMM'];?> руб</span>
                            </div>
                            <?php
                            if(!empty($cert["DATE_EXPIRED"])):
                                ?>
                                <div class="lkcp-info">
                                    <span class="lkcpi-name">Срок действия</span>
                                    <span class="lkcpi-value"><?=$cert["DATE_EXPIRED"]->format("d.m.Y");?></span>
                                </div>
                            <?php
                            endif;
                            ?>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>

            <?if ($arResult['CERTIFICATES_INFO']['CERTS_COUNT'] > 10):?>
                <div class="news-show-more js-show-more-cnt" >
                    <button ref="showMore" type="button" data-href="/ajax/personalCertificatesShowMore.php" class="button js-show-more" data-page="2" data-skip-id="">Показать еще</button>
                </div>
            <?endif;?>
        </div>
    </div>
<?else:?>
    <div class="lkc-purchased" v-once>
        <h2>Вы еще не приобрели ни одного сертификата</h2>
    </div>
<?endif;?>