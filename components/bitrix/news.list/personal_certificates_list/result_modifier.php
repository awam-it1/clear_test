<?php
use \Bitrix\Main\Loader,
    \Itech\Certificates\Orm\CertificateTable,
    \Bitrix\Main\Application,
    \Bitrix\Main\Type\DateTime;
include $_SERVER["DOCUMENT_ROOT"].'/local/modules/itech.certificates/lib/orm/certificate.php';

//дизайны сертификатов
$arSelect = Array("ID", "IBLOCK_ID", "ID");
global $arFilter;
$arFilter = Array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "ACTIVE" => "Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
$designs_data = array();
while($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();
    $arFields["PROPERTIES"] = $ob->GetProperties();
    $designs_data[] = $arFields;
}

global $designs;
$designs = array();
foreach($designs_data as $design) {
    $designs[$design["PROPERTIES"]["ID1C"]["VALUE"]] = $design["PROPERTIES"]["PICTURES"]["VALUE"][0];
}

$USER_ID = null;
GLOBAL $USER;
if ($USER->IsAuthorized()) {
    $USER_ID = $USER->GetID();
}

$certs_data = CertificateTable::getList(
    array(
        'select' => array('ID', 'SUMM', 'DATA', 'TYPE', 'DATE_EXPIRED', 'CERTIFICATE_NUMBER'),
        'filter' => array('=USER_ID' => $USER_ID, '!=CERTIFICATE_NUMBER' => ''),
        'limit' => 10, // количество записей
        'offset' => 0, // смещение для limit
        'count_total' => true,
        'order' => array('ID' => 'DESC') // параметры сортировки
        //'group'   => ... // явное указание полей, по которым нужно группировать результат
        //'runtime' => ... // динамически определенные поля
    )
);
$certs_count = $certs_data->getCount();
$arResult['CERTIFICATES_INFO']['CERTS_COUNT'] = $certs_count;
global $usercerts;
$usercerts = array();
while ($row = $certs_data->fetch()) {
    $usercerts[] = $row;
}