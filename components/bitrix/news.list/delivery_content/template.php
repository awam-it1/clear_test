<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
foreach ($arResult["ITEMS"] as $arItem):?>
    <h3>Оплата</h3>
    <?= $arItem['PROPERTIES']['PAYMENT']['~VALUE']['TEXT']; ?>
    <h3>Доставка подарочного сертификата</h3>
    <div class="row">
        <div class="col">
            <?= $arItem['PROPERTIES']['DELIVERY1']['~VALUE']['TEXT']; ?>
        </div>
        <div class="col">
            <?= $arItem['PROPERTIES']['DELIVERY2']['~VALUE']['TEXT']; ?>
        </div>
    </div>
<? endforeach; ?>

