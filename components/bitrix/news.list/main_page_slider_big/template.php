<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// print_r($arResult);
?>
<div class="index-services" >
	<div class="inner-wrapper full-width">
		<div class="iservices-slider" data-is="indexServicesSlider">
			<div class="swiper-container is-main">
				<div class="swiper-wrapper">
					<?foreach($arResult["ITEMS"] as $key => $arItem):?>
					<div class="swiper-slide">
						<div class="is-main__slide" style="background-image: url('<?=$arItem["PREVIEW_PICTURE"]["SRC"];?>')">
							<div class="is-main__content<?if (!$key) echo " revealer";?>">
								<div class="is-main__title">
									<span><?=$arItem["DISPLAY_PROPERTIES"]["TITLE"]["VALUE"];?></span>
								</div>
								<div class="is-main__text">
									<span><?=$arItem["~PREVIEW_TEXT"];?></span>
								</div>
							</div>
						</div>
					</div>
					<?endforeach;?>
				</div>
			</div>

			<div class="swiper-container is-tumbs revealer">
				<div class="swiper-wrapper">
					<?foreach($arResult["ITEMS"] as $arItem):?>
					<div class="swiper-slide">
						<div class="is-tumbs__slide iservice-link">
							<div class="is-tumbs__icon">
								<?=file_get_contents($_SERVER["DOCUMENT_ROOT"].$arItem["DISPLAY_PROPERTIES"]["ICON"]["FILE_VALUE"]["SRC"]);?>
							</div>
							<div class="is-tumbs__title">
								<span><?=$arItem["NAME"];?></span>
							</div>
						</div>
					</div>
					<?endforeach;?>
				</div>
			</div>
		</div>
	</div>
</div>