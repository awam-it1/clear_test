<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
//echo '<script>var log_php_object = ' . json_encode($arResult) . ';</script>';

// забираем метро
$metro = Array();
$rsMetro = CIBlockSection::GetList(Array('SORT' => 'ASC', "NAME" => "ASC"), Array("IBLOCK_ID" => $arParams["IBLOCK_ID"]), false, Array("ID", "NAME"));
while ($arMetro = $rsMetro->GetNext()) {
	$metro[$arMetro["ID"]] = $arMetro["NAME"];
}

// забираем мастеров
$masters = Array();
$rsMaster = CIBlockElement::GetList(
	Array("SORT" => "ASC")
	, Array("IBLOCK_ID" => IBLOCK_salons_masters, "PROPERTY_SALON" => array_map(function ($salon) {
		return $salon["ID"];
	}, $arResult["ITEMS"]), "ACTIVE" => "Y")
	, false
	, false
	, Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_SALON", "PROPERTY_ID1C")
);
while ($arMaster = $rsMaster->GetNextElement()) {
	$fields = $arMaster->GetFields();
	$properties = $arMaster->GetProperties();

	$masters[] = Array(
		"id" => $fields["PROPERTY_ID1C_VALUE"]
	, "name" => $fields["NAME"]
	, "SECTION_ID" => $fields["PROPERTY_SALON_VALUE"]
	, "procedures" => []//$properties["SERVICES"]["VALUE"]
	);
}

// получаем древо услуг $services_sections
$services_sections = array();
$rsServices = CIBlockSection::GetTreeList(Array("IBLOCK_ID" => IBLOCK_salons_services), Array("ID", "NAME", "DEPTH_LEVEL", "IBLOCK_SECTION_ID"));
while ($arServices = $rsServices->GetNext()) {
	$services_sections[] = $arServices;
}
$parent_id = 1;
$services = array();
foreach ($services_sections as $services_section) {
	if($services_section["DEPTH_LEVEL"]>1) {
		$services[$parent_id]['ITEMS'][] = $services_section;
	} else {
		$parent_id = $services_section["ID"];
	}
}

global $services_list;
$services_list = [];
function getAllChildrenIDs($arr) {
	global $services_list;
	$return = [];
	foreach ($arr as $key => $value) {
		if(is_array($value) || ($key=='ITEMS')) {
			$re = getAllChildrenIDs($value);
			if(!empty($re)) {
				if(empty($value['ID'])) {
					$services_list[] = $value['ID'];
					$return[] = $re;
				}
			}
		} else if($key=='ID') {
			$services_list[] = $value;
			$return[] = $value;
		}
	}
	return $return;
}

function recarray($ar, $searchfor) {
	global $services_list;
	$result = array();

	foreach($ar as $k => $v) {
		// 1 lvl
		if($k==$searchfor) {
			$services_list[] = $v['ID'];
			$result[] = getAllChildrenIDs($v);
		} else {

			if (($k == 'ID') && $v==$searchfor){
				$services_list[] = $v;
				$result[] = $v;
				$result[] = getAllChildrenIDs($v);
			}
			else if (is_array($ar[$k])) {
				$ret = recarray($v, $searchfor);
				if(count($ret)) $result[] = $ret;
			}
		}
	}
	return $result;
}

function searchForID($id, $array) {
	$rez = array();
	foreach ($array as $key => $val) {
		foreach ($val as $key2 => $val2) {
			if ( ($key2=='ID') && ($val2 == $id)) {
				$rez[] = $val2;
			}
		}

	}
	if(!empty($rez))
		return $rez;
	return false;
}

// формируем структуру салонов
$arResult["JSON_SALONS"] = [];
foreach ($arResult["ITEMS"] as $arItem) {
	list($lat, $lng) = explode(",", $arItem["DISPLAY_PROPERTIES"]["MAP"]["VALUE"]);
    $comingSoondate = DateTime::createFromFormat('d.m.Y', $arItem['PROPERTIES']['COMING_SOON']['VALUE']);
    $comingSoon = ($comingSoondate)? 'открытие в '.getComingSoonMonth($comingSoondate->format('m')) : '';
	$services_list = array();
	foreach ($arItem["DISPLAY_PROPERTIES"]["ALL_SERVICES"]["VALUE"] as $service) {
		$rez = recarray($services, $service['service']);
	}

	$salon_id = $arItem["ID"];
	if ($salon_id == $arParams["SELECTED"]) {
		$arResult["SELECTED_SALON"] = $arItem["DISPLAY_PROPERTIES"]["ID1C"]["VALUE"];
	}
	$salon_descr = (!empty($arItem["DISPLAY_PROPERTIES"]["ADDRESS_SUB"]["~VALUE"])) ? $arItem["DISPLAY_PROPERTIES"]["ADDRESS_SUB"]["~VALUE"] : '';
	$salon = Array(
		"id" => $arItem["DISPLAY_PROPERTIES"]["ID1C"]["VALUE"]
	, "name" => $arItem["NAME"]
	, "sort" => $arItem["SORT"]
	, "lat" => $lat
	, "lng" => $lng
	, "address" => Array(
			"streetAddress" => $arItem["DISPLAY_PROPERTIES"]["ADDRESS"]["~VALUE"]
		, "descr" => $salon_descr
		)
    , "coming_soon" => $comingSoon
	, "phoneNumbers" => $arItem["DISPLAY_PROPERTIES"]["PHONES"]["VALUE"]
	, "time" => $arItem["DISPLAY_PROPERTIES"]["TIME"]["~VALUE"]
	, "readmore" => $arItem["DETAIL_PAGE_URL"]
	, "procedures" => $services_list//$all_services//array_map(function($data) {return $data['service'];}, $arItem["DISPLAY_PROPERTIES"]["ALL_SERVICES"]["VALUE"])//$arItem["DISPLAY_PROPERTIES"]["ALL_SERVICES"]["VALUE"]
	, "masters" => array_filter($masters, function ($master) use ($salon_id) {
			return $master["SECTION_ID"] == $salon_id;
		})
	);

	if (isset($metro[$arItem["IBLOCK_SECTION_ID"]])) {
		$salon["metro"] = $metro[$arItem["IBLOCK_SECTION_ID"]];
	}

	$arResult["JSON_SALONS"][] = $salon;
}

$salons = customMultiSort($arResult["JSON_SALONS"], 'name', true);
$arResult["JSON_SALONS"] = $salons;
?>