<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
// echo '<script>var log_php_object = '.json_encode($arResult).';</script>';
global $service_section;
global $service_section_id;
$json = array();
if ($arResult["SECTION"]):
	if (count($arResult["SALONS"])):
		$j = 0;
		foreach ($arResult["SALONS"] as $key => $arItem):
			if ($j == 0)
				$salon_id = $arItem["PROPERTY_PRICE_TYPE_VALUE"];
			$j++;
			$json['SALONS'][] = array('id' => $arItem["ID"] . ':' . $arItem["PROPERTY_PRICE_TYPE_VALUE"], 'name' => $arItem["NAME"]);
		endforeach;
		if (count($arResult["ITEMS"])):
			$duration = array();
			foreach ($arResult["ITEMS"] as $item) {
				$item['DURATION'] = getMinutesByString($item["DISPLAY_PROPERTIES"]["TIME"]["VALUE"]);
				$duration[] = $item;
			}

			function cmp($a, $b)
			{
				if ($a["DURATION"] == $b["DURATION"]) {
					return 0;
				}
				return ($a["DURATION"] < $b["DURATION"]) ? -1 : 1;
			}

			usort($duration, "cmp");
			$i = 0;
			foreach ($duration as $key => $arItem):
				if ($i == 0)
					$duration_id = $arItem["DISPLAY_PROPERTIES"]["ID1C"]["VALUE"];
				$json['DURATIONS'][] = array(
				    'id' => $arItem["ID"] . ':' . $arItem["DISPLAY_PROPERTIES"]["ID1C"]["VALUE"],
                    'name' => make_time($arItem["DISPLAY_PROPERTIES"]["TIME"]["VALUE"])
                );
				$i++;
			endforeach;
		endif;
		//<input type="hidden" name="SERVICE_ID" value="<?=$arResult["PARENT"]["ID"];">
		//<input type="hidden" name="SERVICE" value="$arResult["PARENT"]["UF_ID1C"];">
		$json['PRICE'] = (!is_null($arResult["PRICE"])) ? GetPrice($salon_id, $duration_id) : '';
	else:?>
        Для данного города услуга недоступна. Ознакомьтесь со списком услуг по <a href="/services/">ссылке</a>.
	<?endif;
	if (count($arResult["DESIGNS"])):
		foreach ($arResult["DESIGNS"] as $arPicture):
			$json["DESIGNS"][] = CFile::GetPath($arPicture);
		endforeach;
	endif;
endif;
echo json_encode(Array(
	'SUCCESS' => true,
	'SALON_ID' => $json['SALONS'][0]['id'],
	//'SALONS' => $json['SALONS'], //'SALONS' => Array(Array('id' => 's001', 'name' => 'salon 001'), Array('id' => 's002', 'name' => 'salon 002')),
	'DURATIONS' => $json["DURATIONS"], //Array(Array('id' => 'd001', 'name' => '1 h'), Array('id' => 'd002', 'name' => '2 h')),
	'DESIGNS' => $json["DESIGNS"], //Array('/dist/img/temp/1.jpg', '/dist/img/temp/3.jpg'),
	'PRICE_TEXT' => $json["PRICE"] . ' рублей',
	'SERVICE' => $service_section,
	'SERVICE_ID' => $service_section_id,
    'SELECTED_DURATION' => $arParams['SELECTED_DURATION']
));
?>