<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// echo '<script>var log_php_object = '.json_encode($arResult).';</script>';


if ($arResult["SECTION"]) {

    // прямой родитель
        $arResult["PARENT"] = array_pop($arResult["SECTION"]["PATH"]);
        $rsSection = CIBlockSection::GetList(
            Array('SORT' => 'ASC')
            , Array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "ID" => $arResult["PARENT"]["ID"])
            , false
            , Array("ID", "UF_PICTURES", "UF_ID1C")
        );
        while ($arSection = $rsSection->GetNext()) {
            $arResult["PARENT"] = array_merge($arResult["PARENT"], $arSection);
        }

        GLOBAL $APPLICATION;
        $APPLICATION->AddChainItem($arResult["PARENT"]["NAME"], "#");

    // идетификатор услуги и типы цены
        $service = $arResult["PARENT"]["UF_ID1C"];
        if (count($arResult["ITEMS"])) {
            $service = $arResult["ITEMS"][0]["DISPLAY_PROPERTIES"]["ID1C"]["VALUE"];
        }
        $price_types = GetPriceTypesByService($service);

    // салоны в которых доступна услуга
        $arResult["SALONS"] = [];
        $rsSalon = CIBlockElement::GetList(
            Array("SORT" => "ASC")
            , Array("IBLOCK_ID" => 6, "SECTION_CODE" => $arParams["CITY_CODE"], "ACTIVE" => "Y", "INCLUDE_SUBSECTIONS" => "Y")
            , false
            , false
            , Array("ID", "NAME", "PROPERTY_ID1C", "PROPERTY_PRICE_TYPE", "PROPERTY_ALL_SERVICES")
        );
        while ($arSalon = $rsSalon->Fetch()) {
            if (in_array($arSalon["PROPERTY_PRICE_TYPE_VALUE"], $price_types)) {
                $arResult["SALONS"][$arSalon['ID']] = $arSalon;
            } elseif($arSalon['PROPERTY_ALL_SERVICES_VALUE']['service'] == $arResult['PARENT']['ID']) {
            	$arResult["SALONS"][$arSalon['ID']] = $arSalon;
            }
        }
        
    // начальная цена
        $arResult["PRICE"] = null;
        if (count($arResult["SALONS"])) {
            $arResult["PRICE"] = GetPrice($arResult["SALONS"][0]["PROPERTY_PRICE_TYPE_VALUE"], $service);
        }
        //var_dump("GetPrice(".$arResult["SALONS"][0]["PROPERTY_PRICE_TYPE_VALUE"].", $service)");


// получаем дизайны сертификатов
	$arSelect = Array("ID", "IBLOCK_ID", "ID");
	global $arFilter;
	$arFilter = Array("IBLOCK_ID" => IBLOCK_catalog_design, "ACTIVE" => "Y");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
	$designs_data = array();
	while ($ob = $res->GetNextElement()) {
		$arFields = $ob->GetFields();
		$arFields["PROPERTIES"] = $ob->GetProperties();
		$designs_data[] = $arFields;
	}

	$designs = array();
	foreach ($designs_data as $design) {
		$designs[$design["PROPERTIES"]["ID1C"]["VALUE"]] = $design["PROPERTIES"]["PICTURES"]["VALUE"][0];
	}
	$arResult["DESIGNS"] = $designs;
	$APPLICATION->RestartBuffer();
}
?>