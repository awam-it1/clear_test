<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// echo '<script>var log_php_object = '.json_encode($arResult).';</script>';
?>
<?if($arResult["ITEMS"]):?>
<?foreach($arResult["ITEMS"] as $arItem):
	list($day, $month) = explode(" ", $arItem["DISPLAY_ACTIVE_FROM"]);
    if (strlen($arItem["PREVIEW_PICTURE"]["SRC"]))
        $img = $arItem["PREVIEW_PICTURE"]["SRC"];
    else
		$img = '/local/include/img/stub.jpg';
	?>
	<div class="news-item">
		<div class="news-image" style="background-image: url('<?=$img;?>')">
			<?if (strlen($arItem["DISPLAY_ACTIVE_FROM"])):?>
			<div class="inews-date"><?=$day;?> <span><?=$month;?></span></div>
			<?endif;?>
		</div>
		<a href="<?=$arItem["DETAIL_PAGE_URL"];?>" class="shop__good-name"><?=$arItem["NAME"];?></a>
		<div class="inews-text">
			<p><?=$arItem["~PREVIEW_TEXT"];?></p>
		</div>
	</div>
<?endforeach;?>
<?if ($arResult["NAV_RESULT"]->NavPageNomer < $arResult["NAV_RESULT"]->NavPageCount):?>	
<div class="news-show-more js-show-more-cnt">
	<button type="button" data-href="/ajax/newsShowMoreTest.php" class="button js-show-more" data-page="<?=$arResult["NAV_RESULT"]->NavPageNomer+1?>" data-skip-id="<?=$arResult["SKIP_ID"]?>" data-salon="<?=$arParams["SALON"]?>">Показать еще</button>
</div>
<?endif;?>
<?else:?>
<div class="news-item">
    <p>По данному салону нет новостей</p>
</div>
<?endif;?>
