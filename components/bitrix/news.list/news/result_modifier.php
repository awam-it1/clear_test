<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// echo '<script>var log_php_object = '.json_encode($arResult).';</script>';

if (strlen($arParams["SKIP_ID"])) {
	$arResult["SKIP_ID"] = $arParams["SKIP_ID"];
} else {
	$arResult["SKIP_ID"] = Array();
	foreach ($arResult["ITEMS"] as $arItem) {
		$arResult["SKIP_ID"][] = $arItem["ID"];
	}
	$arResult["SKIP_ID"] = explode(",", $arResult["SKIP_ID"]);
}
?>
