<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// print_r($arResult);
?>
<div class="icons-404">
	<?foreach($arResult["ITEMS"] as $arItem):?>
	<div class="item-404">
		<div class="i404">
			<?=file_get_contents($_SERVER["DOCUMENT_ROOT"].$arItem["DISPLAY_PROPERTIES"]["ICON"]["FILE_VALUE"]["SRC"]);?>
		</div>
		<a href="<?=$arItem["DISPLAY_PROPERTIES"]["LINK"]["VALUE"];?>" class="i404-text"><?=$arItem["NAME"];?></a>
	</div>
	<?endforeach;?>
</div>