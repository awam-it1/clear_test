$(document).ready(function() {
    $(document).on('change', '.js-salon-select', function () {
        let $this = $(this);
        let value = $this.val();
        let $comment = $('.js-service-price-comment');
        if (value.indexOf('8776') >= 0) {
            $comment.show();
        } else {
            $comment.hide();
        }
    });
});
