<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Iblock\InheritedProperty;
$ipropValues = new InheritedProperty\SectionValues(IBLOCK_salons_services, $arResult['ITEMS'][0]['IBLOCK_SECTION_ID']);
$values = $ipropValues->getValues();
$ipropValues->clearValues();

if ($arResult['SECTION']) {
    $arResult['PARENT'] = array_pop($arResult['SECTION']['PATH']);

    //Если есть дочерние разделы, то это не услуга, а раздел - ошибка 404
    $child = CIBlockSection::GetList(
        ['SORT' => 'ASC'],
        ['IBLOCK_ID' => $arParams['IBLOCK_ID'], 'SECTION_ID' => $arResult['PARENT']['ID']],
        '',
        ['ID'],
        ['nPageSize' => 1]
    )->fetch();
    if (!empty($child)) ccHelpers::showError404();

    //Выбираем данные по родительской секции
    $list = CIBlockSection::GetList(
        ['SORT' => 'ASC'],
        ['IBLOCK_ID' => $arParams['IBLOCK_ID'], 'ID' => $arResult['PARENT']['ID']],
        '',
        ['ID', 'UF_PICTURES', 'UF_ID1C']
    );
    while ($item = $list->GetNext()) $arResult['PARENT'] = array_merge($arResult['PARENT'], $item);

    //Хлебные крошки
    $GLOBALS['APPLICATION']->AddChainItem($arResult['PARENT']['NAME'], '#');

    //Идентификатор услуги и типы цены
    $service = $arResult['PARENT']['UF_ID1C'];
    if (count($arResult['ITEMS'])) $service = $arResult['ITEMS'][0]['DISPLAY_PROPERTIES']['ID1C']['VALUE'];
    $price_types = GetPriceTypesByService($service);

    //Салоны в которых доступна услуга
    $arResult['SALONS'] = [];
    $salonsByPrice = [];
    $list = CIBlockElement::GetList(
        ['SORT' => 'ASC'],
        ['IBLOCK_ID' => IBLOCK_salons_salons, 'SECTION_CODE' => $arParams['CITY_CODE'], 'ACTIVE' => 'Y', 'INCLUDE_SUBSECTIONS' => 'Y'],
        '', '',
        ['ID', 'NAME', 'PROPERTY_ID1C', 'PROPERTY_PRICE_TYPE', 'PROPERTY_ALL_SERVICES']
    );
    while ($item = $list->fetch()) {
        if ($item['PROPERTY_ALL_SERVICES_VALUE']['service'] == $arResult['PARENT']['ID']) {
            $arResult['SALONS'][$item['ID']] = $item;
        }
        if (in_array($item['PROPERTY_PRICE_TYPE_VALUE'], $price_types)) {
            $salonsByPrice[$item['ID']] = $item;
        }
    }
    if (!count($arResult['SALONS'])) $arResult['SALONS'] = $salonsByPrice;

    //Цены для первого салона
    $arResult['PRICE'] = null;
    if (count($arResult['SALONS'])) {
        reset($arResult['SALONS']);
        $firstSalon = current($arResult['SALONS']);
        $arResult['PRICE'] = GetPrice($firstSalon['PROPERTY_PRICE_TYPE_VALUE'], $service);
        $items = [];
        foreach ($arResult['ITEMS'] as $item) {
            $item['PRICE'] = GetPrice($firstSalon['PROPERTY_PRICE_TYPE_VALUE'], $item['DISPLAY_PROPERTIES']['ID1C']['VALUE']);
            if ($item['PRICE']) $items[] = $item;
        }
        $arResult['ITEMS'] = $items;
    }
    $arResult['PARENT']['DESCRIPTION'] = str_replace('[город]', $GLOBALS['CITY'], $arResult['PARENT']['DESCRIPTION']);

    //Сортируем, оставляя "метро" в начале
    usort($arResult['SALONS'], function ($a, $b) {
        $a['SORT_METRO'] = substr($a['NAME'], 0, 2) == 'м.' ? 0 : 1;
        $b['SORT_METRO'] = substr($b['NAME'], 0, 2) == 'м.' ? 0 : 1;
        if ($a['SORT_METRO'] !== $b['SORT_METRO']) return $a['SORT_METRO'] <=> $b['SORT_METRO'];
        if ($a['SORT'] !== $b['SORT']) return $a['SORT'] <=> $b['SORT'];
        return $a['NAME'] <=> $b['NAME'];
    });

    //Настройка og-тегов
    if (\Bitrix\Main\Loader::includeModule('dev2fun.opengraph')) {
        \Dev2fun\Module\OpenGraph::Show($arResult['PARENT']['ID'], 'section');
    }

}
