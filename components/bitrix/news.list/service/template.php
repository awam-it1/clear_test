<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

if ($arResult['SECTION']):?>
<div class="inner-wrapper min-width">
    <h1><?=$arResult['PARENT']['NAME'];?></h1>
    <div class="row service-row">
        <?if (count($arResult['SALONS'])):?>
        <div class="col">
            <div class="service-descr">
                <div class="service-descr-top">
                    <form action="" class="js-form" autocomplete="off">
                        <div class="is-styled">
                            <div class="input-element">
                                <span class="form-item-label">Выберите салон</span>
                                <select name="SALON_TYPE" class="js-salon-select">
                                <?
                                $first = true;
                                foreach ($arResult['SALONS'] as $key => $item):
                                    if ($first) {
                                        $salon_id = $item['PROPERTY_PRICE_TYPE_VALUE'];
                                        $salon = $item['ID'];
                                        $first = false;
                                    }
                                ?>
                                    <option value="<?=$item['ID'];?>:<?=$item['PROPERTY_PRICE_TYPE_VALUE'];?>"<?=$key ? '' : ' selected'?>><?=$item['NAME'];?></option>
                                <?endforeach;?>
                                </select>
                            </div>
                        </div>
                        <?if (count($arResult['ITEMS'])):?>
                            <div class="is-styled">
                                <div class="input-element"><span class="form-item-label">Выберите длительность сеанса</span></div>
                            </div>
                            <div class="service-radio-buttons">
                                <?
                                $duration = [];
                                foreach ($arResult['ITEMS'] as $item) {
                                    $item['DURATION'] = getMinutesByString($item['DISPLAY_PROPERTIES']['TIME']['VALUE']);
                                    $duration[] = $item;
                                }
                                usort($duration, function ($a, $b) {
                                    return $a['DURATION'] <=> $b['DURATION'];
                                });
                                $i = 0;
                                foreach ($duration as $key => $arItem):
                                    ?>
                                <div class="service-radio">
                                    <label>
                                        <input class="js-duration <?if ($i==0) echo "is-active";?>" type="radio" name="DURATION" value="<?=$arItem["ID"];?>:<?=$arItem["DISPLAY_PROPERTIES"]["ID1C"]["VALUE"];?>"<?if ($i==0){ $duration_id = $arItem["DISPLAY_PROPERTIES"]["ID1C"]["VALUE"]; echo " checked";}?>/>
                                        <span><?=make_time($arItem["DISPLAY_PROPERTIES"]["TIME"]["VALUE"]);?></span>
                                    </label>
                                </div>
                                <?
                                $i++;
                                endforeach;?>
                            </div>
                        <?endif;?>
                        <input type="hidden" name="SERVICE_ID" value="<?=$arResult["PARENT"]["ID"];?>">
                        <input type="hidden" name="SERVICE" value="<?=$arResult["PARENT"]["UF_ID1C"];?>">
                    </form>
                    <div class="service-price">Стоимость услуги: <span class="js-price"><?
                            $pricev = GetPrice($salon_id, $duration_id);
                            if (!empty($pricev))
                                echo $pricev." рублей";
                            else
                                echo 'не указана';
                            ?></span></div>
                    <div class="service-price-description">Цена услуги зависит от выбранного города<?if (count($arResult["ITEMS"])):?> и продолжительности процедуры<?endif;?></div>
                    <div class="service-price--comment js-service-price-comment">
                        <?= Loc::getMessage('SERVICE:MOSCOW_CITY_COMMENT') ?>
                    </div>
                </div>
                <div class="service-button-head">
                    <a href="/online-recording/?s=<?= $salon; ?>&s0=<?= $arResult["PARENT"]["ID"];?>&duration=<?= $duration[0]['ID']; ?>" class="button js-link">Записаться</a>
                    <!-- <a href="#" class="button solid">Купить сертификат</a> -->
                </div>
            </div>

        </div>
        <?else:?>

		<div class="col">
			<div class="service-descr">
				<div class="service-price">Для данного города услуга недоступна. Ознакомьтесь со списком услуг по <a href="/services/">ссылке</a>.</div>
			</div>
		</div>
		<?endif;?>
        <div class="col">
        <?if (count($arResult["PARENT"]["UF_PICTURES"])):?>
            <div class="image-gallery service-gallery" data-is="imageGallery">
                <div class="swiper-container ig-main">
                    <div class="swiper-wrapper">
                        <?foreach ($arResult["PARENT"]["UF_PICTURES"] as $arPicture):?>
                        <div class="swiper-slide">
                            <div class="gallery-image" style="background-image: url('<?=CFile::GetPath($arPicture);?>')"></div>
                        </div>
                        <?endforeach;?>
                        <!-- <div class="swiper-slide">
                            <div class="gallery-video video" data-is="video">
                                <video src="/dist/video/mainvideo.mp4" poster="/dist/img/temp/temp13.jpg"></video>
                                <div class="play">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" viewBox="0 0 80 80"><path fill="#8a007c" d="M0 40a40 40 0 1 1 80 0 40 40 0 0 1-80 0z"/><path fill="#fff" d="M36 32l12 8-12 8z"/></svg>
                                </div>
                            </div>
                        </div> -->
                    </div>
                </div>
                <div class="ig-arrow ig-left">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20" height="20" viewBox="0 0 20 20"><g transform="translate(-90 -402)"><image width="20" height="20" transform="translate(90 402)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAACa0lEQVQ4T5VUTWgTQRR+bybZbo3bNiWppggSiYhEkVoIQkGqoBgUiiAePSkoiodSGvCg4sGDKAiCeKkXvYigSK1W0R4qKEWlSvGnINEc2vrTRGPAEpPMk9nNJpvNZhvfYWdm571v5n3fe4PQpP34PHp0haoNeT1tawkRWgI9XqdQXA4vkxq771NDcQSFmb4C8qSu6q2srRgNAReTd0+0tqy+zEDlAKTHyK8egERqaCtHRGPDYo6A6S9jL1VPVy8JQh3BDDPnCLBUWBgJrhs4vCzgz9TjNBe+Tt3RAlBzRQIoQu5XZ3iP3xUwnXz0m4GqkShfSbIkAKDMlrwuAYEcEYgKWnZNMDgw75jy90/35pnwhYAIAA0miAgQZWgdVQYGz2eDkX0ddYALs7eectG+g4BqODVFcKDQYAQZlTD3KrThQMwExbmZm7s56xgXZBHAhLUj2giT6TNkRPAn2R09GNEPmXt3568ogmOR1tWoTSTrknEmiLLXMfX2Rh6EohicGfQ1osytCRALJYb5Efz4+spOL7U/AWJoVdEoZENROTaqIsaAiqXM+/Wxk5tMH5h9cXGUscBekjy6mKm4HMvBVMDM5MbYYH9FFHPy4fmlJFJb2A3QrpGAXDraNxhwrEP5c2byQoaB5q+mWC3kuo5gRK1+6g5Hj39tCCg33kyc/8ZxZZfbTY18lxY39yeCdQc5BU5PnH2GQuuTolefmFpPphSvbdmeONYUoHSaHj99pMSVqyA8HuPJqZrsxp5dw80/X9bgqYenbhMp+5E4ryjJBMXiZ/7vgbWnMvUgcUgQJgB4BBnHbfFzejPY7R+83t3zXtOZRwAAAABJRU5ErkJggg=="/></g></svg>
                </div>
                <div class="ig-arrow ig-right">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"><defs><linearGradient id="a" x1="10" x2="10" y1="10" y2="20" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#d4be6f"/><stop offset="1" stop-color="#b3a05d"/></linearGradient><linearGradient id="b" x1="10" x2="10" y2="20" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#fff7b4" stop-opacity=".7"/><stop offset=".99" stop-color="#ccc370" stop-opacity=".7"/></linearGradient><filter id="c" width="200%" height="200%" x="-50%" y="-50%"><feGaussianBlur in="SourceGraphic" result="SvgjsFeGaussianBlur1067Out"/></filter><mask id="d"><path fill="#fff" d="M6.28 0C10.87.79 15.99 3.38 20 10c-4.01 6.62-9.13 9.22-13.72 10l-.01-.22c-.02-.85-.09-1.65-.22-2.39a10.78 10.78 0 0 0-1.55-3.91A8.83 8.83 0 0 0 0 10a8.83 8.83 0 0 0 4.51-3.5c.33-.48.6-1.01.91-1.76.25-.64.5-1.33.63-2.12.13-.75.2-1.55.22-2.39z"/></mask><linearGradient id="e" x1="10" x2="10" y2="19.78" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#ffeed9"/><stop offset="1" stop-color="#c7b179"/></linearGradient></defs><path fill="#d4be6f" d="M6.28 0C10.87.79 15.99 3.38 20 10c-4.01 6.62-9.13 9.22-13.72 10l-.01-.22c-.02-.85-.09-1.65-.22-2.39a10.78 10.78 0 0 0-1.55-3.91A8.83 8.83 0 0 0 0 10a8.83 8.83 0 0 0 4.51-3.5c.33-.48.6-1.01.91-1.76.25-.64.5-1.33.63-2.12.13-.75.2-1.55.22-2.39z"/><path fill="url(#a)" d="M6.28 0C10.87.79 15.99 3.38 20 10c-4.01 6.62-9.13 9.22-13.72 10l-.01-.22c-.02-.85-.09-1.65-.22-2.39a10.78 10.78 0 0 0-1.55-3.91A8.83 8.83 0 0 0 0 10a8.83 8.83 0 0 0 4.51-3.5c.33-.48.6-1.01.91-1.76.25-.64.5-1.33.63-2.12.13-.75.2-1.55.22-2.39z"/><path fill="url(#b)" d="M6.28 0C10.87.79 15.99 3.38 20 10c-4.01 6.62-9.13 9.22-13.72 10l-.01-.22c-.02-.85-.09-1.65-.22-2.39a10.78 10.78 0 0 0-1.55-3.91A8.83 8.83 0 0 0 0 10a8.83 8.83 0 0 0 4.51-3.5c.33-.48.6-1.01.91-1.76.25-.64.5-1.33.63-2.12.13-.75.2-1.55.22-2.39z"/><path fill="#fff" fill-opacity=".5" d="M0 21V-1h20v22zM6.27 1.23c-.02.84-.09 1.64-.22 2.39-.13.79-.38 1.48-.63 2.12a9.19 9.19 0 0 1-2.04 3.13A8.83 8.83 0 0 1 0 11a8.83 8.83 0 0 1 4.5 3.48 10.78 10.78 0 0 1 1.55 3.91c.13.74.2 1.54.22 2.39l.01.22c4.59-.78 9.71-3.38 13.72-10-4.01-6.62-9.13-9.21-13.72-10z" filter="url(#c)" mask="url(&quot;#d&quot;)"/><path fill="url(#e)" d="M6.28 0C10.87.79 15.99 3.38 20 10c-4.01 6.62-9.13 9.22-13.72 10l-.01-.22c-.02-.85-.09-1.65-.22-2.39a10.78 10.78 0 0 0-1.55-3.91A8.83 8.83 0 0 0 0 10a8.83 8.83 0 0 0 4.51-3.5c.33-.48.6-1.01.91-1.76.25-.64.5-1.33.63-2.12.13-.75.2-1.55.22-2.39z"/></svg>
                </div>
                <div class="swiper-container ig-tumbnails">
                    <div class="swiper-wrapper">
                        <?foreach ($arResult["PARENT"]["UF_PICTURES"] as $arPicture):?>
                        <div class="swiper-slide tumbs-slide">
                            <div class="tumbs-image" style="background-image: url('<?=CFile::GetPath($arPicture);?>')"></div>
                        </div>
                        <?endforeach;?>
                    </div>
                </div>
            </div>
        <?endif;?>
        </div>
    </div>
</div>
<?if (strlen($arResult["PARENT"]["DESCRIPTION"])):?>
<div class="inner-wrapper min-width is-styled">
    <h2>Описание</h2>
    <p><?=$arResult["PARENT"]["DESCRIPTION"];?></p>
    <!-- <div class="accordion" data-is="accordion" data-singleton="true">
        <div class="accordion-item">
            <a href="#" class="acr-head opened">
                <div class="acr-head-title">Перечень противопоказаний</div>
                <div class="acr-head-elm"></div>
            </a>
            <div class="acr-body">
                <div class="row">
                    <div class="col">
                        <ul>
                            <li>Беременность</li>
                            <li>Период менструации</li>
                            <li>Острые лихорадочные состояния, высокая температура тела, воспалительные процессы</li>
                            <li>Аллергия различного характера</li>
                            <li>Заболевания кожи, ногтей инфекционной, грибковой и невыясненной этиологии</li>
                            <li>Сердечно-сосудистые заболевания и повышенное артериальное давление</li>
                            <li>Доброкачественные и злокачественные опухоли различной локализации</li>
                            <li>Ревматические заболевания с воспалением суставов и мышц</li>
                            <li>Болезни крови, атеросклероз периферических сосудов, тромбангиит, аневризмы сосудов, аорты, сердца, тромбозы, тромбофлебиты</li>
                            <li>Психические заболевания с чрезмерным возбуждением, значительно изменённой психикой</li>
                        </ul>
                    </div>
                    <div class="col">
                        <ul>
                            <li>Почечная и печеночная недостаточность </li>
                            <li>Грыжи различной этиологии</li>
                            <li>Желчнокаменная болезнь</li>
                            <li>Камни в почках</li>
                            <li>Венерические заболевания</li>
                            <li>Воспаление лимфатических узлов</li>
                            <li>Общие тяжёлые состояния при различных заболеваниях и травмах</li>
                            <li>Недавно перенесенные операции и переломы</li>
                            <li>Желудочно-кишечные заболевания в стадии обострения</li>
                            <li>Я готов (а) к проявлениям красноты на теле и жжения от воздействия крема с чили или красным перцем, а также к возможному образованию гематом</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- <a href="#" class="button solid">Получить консультацию</a> -->
</div>
<?endif;?>

<div class="inner-wrapper">
    <?$APPLICATION->IncludeComponent("bitrix:main.include", "", 
        Array(
            "AREA_FILE_SHOW" => "file", 
            "PATH" => "/local/templates/tairai/paths/disclimer.php" 
        )
    );?>
</div>

<div class="service-detail-slider">
    <div class="inner-wrapper min-width">
        <?$APPLICATION->IncludeComponent(
            "bitrix:catalog.section.list",
            "services_three",
            array(
                "ADD_SECTIONS_CHAIN" => "N",
                "CACHE_GROUPS" => "Y",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "A",
                "COUNT_ELEMENTS" => "Y",
                "IBLOCK_ID" => IBLOCK_salons_services,
                "IBLOCK_TYPE" => IBLOCK_TYPE_salons,
                "SECTION_CODE" => "",
                "SECTION_FIELDS" => array(),
                "SECTION_ID" => "",
                "SECTION_URL" => "",
                "SECTION_USER_FIELDS" => array("UF_ICON", "UF_SHORT", "UF_ID1C", "UF_PICTURES"),
                "SHOW_PARENT_NAME" => "Y",
                "TOP_DEPTH" => "5",
                "VIEW_MODE" => "TEXT",
                "CURRENT_ID" => $arResult["PARENT"]["ID"]
            )
        );?>
    </div>
</div>
<div id="modal-msg" class="modal-self ">
    <div class="thanks-modal">
        <h2 class="js-msg"></h2>
    </div>
</div>
<?else:
    
endif;?>