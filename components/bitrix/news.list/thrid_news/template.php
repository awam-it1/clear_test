<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// print_r($arResult);
if (count($arResult["ITEMS"])):?>
<div class="salon-news-list-parent">
    <h2 class="cntr">
        Новости
    </h2>
    <div class="salon-news-list inner-wrapper">
        <div class="snl-inner">
            <?foreach($arResult["ITEMS"] as $arItem):?>
            <div class="snl-item">
                <div class="snl-item-link">
                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"];?></a>
                </div>
                <div class="snl-item-content"><?=$arItem["~PREVIEW_TEXT"];?></div>
            </div>
            <?endforeach;?>
        </div>
    </div>
    <div class="sce-all-news">
        <a href="/news/" class="button">
            Все новости
        </a>
    </div>
</div>
<?endif;?>
