<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// echo '<script>var log_php_object = '.json_encode($arResult).';</script>';

GLOBAL $PRODUCT_ID;
if ($arResult["SECTION"]):?>
    <form action="" class="js-to-cart-selection-form js-form">
        <div class="row service-row ">
            <?if (count($arResult["SALONS"])):?>
            <div class="col">

                <div class="service-descr">
                    <div class="service-descr-top">
                        <div class="is-styled">
                            <div class="input-element">
                                <span class="form-item-label">
                                  Выберите салон
                                </span>
                                <select name="SALON_TYPE" class="js-salon-select">
                                <?
                                $j = 0;
                                foreach ($arResult["SALONS"] as $key => $arItem):
                                    if($j==0)
                                        $salon_id = $arItem["PROPERTY_PRICE_TYPE_VALUE"];
                                    $j++;
                                    ?>
                                    <option value="<?=$arItem["ID"];?>:<?=$arItem["PROPERTY_PRICE_TYPE_VALUE"];?>"<?if (!$key){ echo " selected";}?>><?=$arItem["NAME"];?></option>
                                <?endforeach;?>
                                </select>
                            </div>
                        </div>
                        <?if (count($arResult["ITEMS"])):?>
                            <div class="is-styled">
                                <div class="input-element"><span class="form-item-label">Выберите длительность сеанса</span></div>
                            </div>
                            <div class="service-radio-buttons">
                                <?
                                $duration = array();
                                foreach($arResult["ITEMS"] as $item) {
                                    $item['DURATION'] = getMinutesByString($item["DISPLAY_PROPERTIES"]["TIME"]["VALUE"]);
                                    $duration[] = $item;
                                }

                                function cmp($a, $b)
                                {
                                    if ($a["DURATION"] == $b["DURATION"]) {
                                        return 0;
                                    }
                                    return ($a["DURATION"] < $b["DURATION"]) ? -1 : 1;
                                }
                                usort($duration, "cmp");
                                $i = 0;
                                foreach ($duration as $key => $arItem):
                                    ?>
                                <div class="service-radio">
                                    <label>
                                        <input class="js-duration <?if ($i==0) echo "is-active";?>" type="radio" name="DURATION" value="<?=$arItem["ID"];?>:<?=$arItem["DISPLAY_PROPERTIES"]["ID1C"]["VALUE"];?>"<?if ($i==0){ $duration_id = $arItem["DISPLAY_PROPERTIES"]["ID1C"]["VALUE"]; echo " checked";}?>/>
                                        <span><?=make_time($arItem["DISPLAY_PROPERTIES"]["TIME"]["VALUE"]);?></span>
                                    </label>
                                </div>
                                <?
                                $i++;
                                endforeach;?>
                            </div>
                        <?endif;?>
                        <input type="hidden" name="SERVICE_ID" value="<?=$arResult["PARENT"]["ID"];?>">
                        <input type="hidden" name="SERVICE" value="<?=$arResult["PARENT"]["UF_ID1C"];?>">
                        <div class="service-price">Стоимость услуги: <span class="js-price"><?if (!is_null($arResult["PRICE"])) echo GetPrice($salon_id, $duration_id)." рублей";?></span></div>
                        <div class="service-price-description">Цена услуги зависит от выбранного города<?if (count($arResult["ITEMS"])):?> и продолжительности процедуры<?endif;?></div>
                    </div>
                    <div class="service-button-head">
                        <a href="#" class="button js-link js-to-cart-selection" data-product-id="<?=$PRODUCT_ID;?>">В корзину</a>
                    </div>
                </div>

            </div>
            <?else:?>

            <div class="col">
                <div class="service-descr">
                    <div class="service-price">Для данного города услуга недоступна. Ознакомьтесь со списком услуг по <a href="/services/">ссылке</a>.</div>
                </div>
            </div>
            <?endif;?>
            <div class="col">
            <?if (count($arResult["PARENT"]["UF_PICTURES"])):?>
                <div class="image-gallery service-gallery" ref="imageGallery">
                    <div class="swiper-container ig-main">
                        <div class="swiper-wrapper">
                            <?foreach ($arResult["PARENT"]["UF_PICTURES"] as $arPicture):?>
                            <div class="swiper-slide">
                                <div class="gallery-image" style="background-image: url('<?=CFile::GetPath($arPicture);?>')"></div>
                            </div>
                            <?endforeach;?>
                            <!-- <div class="swiper-slide">
                                <div class="gallery-video video" data-is="video">
                                    <video src="/dist/video/mainvideo.mp4" poster="/dist/img/temp/temp13.jpg"></video>
                                    <div class="play">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80" viewBox="0 0 80 80"><path fill="#8a007c" d="M0 40a40 40 0 1 1 80 0 40 40 0 0 1-80 0z"/><path fill="#fff" d="M36 32l12 8-12 8z"/></svg>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                    <div class="ig-arrow ig-left">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20" height="20" viewBox="0 0 20 20"><g transform="translate(-90 -402)"><image width="20" height="20" transform="translate(90 402)" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAACa0lEQVQ4T5VUTWgTQRR+bybZbo3bNiWppggSiYhEkVoIQkGqoBgUiiAePSkoiodSGvCg4sGDKAiCeKkXvYigSK1W0R4qKEWlSvGnINEc2vrTRGPAEpPMk9nNJpvNZhvfYWdm571v5n3fe4PQpP34PHp0haoNeT1tawkRWgI9XqdQXA4vkxq771NDcQSFmb4C8qSu6q2srRgNAReTd0+0tqy+zEDlAKTHyK8egERqaCtHRGPDYo6A6S9jL1VPVy8JQh3BDDPnCLBUWBgJrhs4vCzgz9TjNBe+Tt3RAlBzRQIoQu5XZ3iP3xUwnXz0m4GqkShfSbIkAKDMlrwuAYEcEYgKWnZNMDgw75jy90/35pnwhYAIAA0miAgQZWgdVQYGz2eDkX0ddYALs7eectG+g4BqODVFcKDQYAQZlTD3KrThQMwExbmZm7s56xgXZBHAhLUj2giT6TNkRPAn2R09GNEPmXt3568ogmOR1tWoTSTrknEmiLLXMfX2Rh6EohicGfQ1osytCRALJYb5Efz4+spOL7U/AWJoVdEoZENROTaqIsaAiqXM+/Wxk5tMH5h9cXGUscBekjy6mKm4HMvBVMDM5MbYYH9FFHPy4fmlJFJb2A3QrpGAXDraNxhwrEP5c2byQoaB5q+mWC3kuo5gRK1+6g5Hj39tCCg33kyc/8ZxZZfbTY18lxY39yeCdQc5BU5PnH2GQuuTolefmFpPphSvbdmeONYUoHSaHj99pMSVqyA8HuPJqZrsxp5dw80/X9bgqYenbhMp+5E4ryjJBMXiZ/7vgbWnMvUgcUgQJgB4BBnHbfFzejPY7R+83t3zXtOZRwAAAABJRU5ErkJggg=="/></g></svg>
                    </div>
                    <div class="ig-arrow ig-right">
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"><defs><linearGradient id="a" x1="10" x2="10" y1="10" y2="20" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#d4be6f"/><stop offset="1" stop-color="#b3a05d"/></linearGradient><linearGradient id="b" x1="10" x2="10" y2="20" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#fff7b4" stop-opacity=".7"/><stop offset=".99" stop-color="#ccc370" stop-opacity=".7"/></linearGradient><filter id="c" width="200%" height="200%" x="-50%" y="-50%"><feGaussianBlur in="SourceGraphic" result="SvgjsFeGaussianBlur1067Out"/></filter><mask id="d"><path fill="#fff" d="M6.28 0C10.87.79 15.99 3.38 20 10c-4.01 6.62-9.13 9.22-13.72 10l-.01-.22c-.02-.85-.09-1.65-.22-2.39a10.78 10.78 0 0 0-1.55-3.91A8.83 8.83 0 0 0 0 10a8.83 8.83 0 0 0 4.51-3.5c.33-.48.6-1.01.91-1.76.25-.64.5-1.33.63-2.12.13-.75.2-1.55.22-2.39z"/></mask><linearGradient id="e" x1="10" x2="10" y2="19.78" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#ffeed9"/><stop offset="1" stop-color="#c7b179"/></linearGradient></defs><path fill="#d4be6f" d="M6.28 0C10.87.79 15.99 3.38 20 10c-4.01 6.62-9.13 9.22-13.72 10l-.01-.22c-.02-.85-.09-1.65-.22-2.39a10.78 10.78 0 0 0-1.55-3.91A8.83 8.83 0 0 0 0 10a8.83 8.83 0 0 0 4.51-3.5c.33-.48.6-1.01.91-1.76.25-.64.5-1.33.63-2.12.13-.75.2-1.55.22-2.39z"/><path fill="url(#a)" d="M6.28 0C10.87.79 15.99 3.38 20 10c-4.01 6.62-9.13 9.22-13.72 10l-.01-.22c-.02-.85-.09-1.65-.22-2.39a10.78 10.78 0 0 0-1.55-3.91A8.83 8.83 0 0 0 0 10a8.83 8.83 0 0 0 4.51-3.5c.33-.48.6-1.01.91-1.76.25-.64.5-1.33.63-2.12.13-.75.2-1.55.22-2.39z"/><path fill="url(#b)" d="M6.28 0C10.87.79 15.99 3.38 20 10c-4.01 6.62-9.13 9.22-13.72 10l-.01-.22c-.02-.85-.09-1.65-.22-2.39a10.78 10.78 0 0 0-1.55-3.91A8.83 8.83 0 0 0 0 10a8.83 8.83 0 0 0 4.51-3.5c.33-.48.6-1.01.91-1.76.25-.64.5-1.33.63-2.12.13-.75.2-1.55.22-2.39z"/><path fill="#fff" fill-opacity=".5" d="M0 21V-1h20v22zM6.27 1.23c-.02.84-.09 1.64-.22 2.39-.13.79-.38 1.48-.63 2.12a9.19 9.19 0 0 1-2.04 3.13A8.83 8.83 0 0 1 0 11a8.83 8.83 0 0 1 4.5 3.48 10.78 10.78 0 0 1 1.55 3.91c.13.74.2 1.54.22 2.39l.01.22c4.59-.78 9.71-3.38 13.72-10-4.01-6.62-9.13-9.21-13.72-10z" filter="url(#c)" mask="url(&quot;#d&quot;)"/><path fill="url(#e)" d="M6.28 0C10.87.79 15.99 3.38 20 10c-4.01 6.62-9.13 9.22-13.72 10l-.01-.22c-.02-.85-.09-1.65-.22-2.39a10.78 10.78 0 0 0-1.55-3.91A8.83 8.83 0 0 0 0 10a8.83 8.83 0 0 0 4.51-3.5c.33-.48.6-1.01.91-1.76.25-.64.5-1.33.63-2.12.13-.75.2-1.55.22-2.39z"/></svg>
                    </div>
                    <div class="swiper-container ig-tumbnails">
                        <div class="swiper-wrapper">
                            <?foreach ($arResult["PARENT"]["UF_PICTURES"] as $arPicture):?>
                            <div class="swiper-slide tumbs-slide">
                                <div class="tumbs-image" style="background-image: url('<?=CFile::GetPath($arPicture);?>')"></div>
                            </div>
                            <?endforeach;?>
                        </div>
                    </div>
                </div>
            <?endif;?>
            </div>
        </div>
    </form>
<?else:

endif;?>