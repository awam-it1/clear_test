<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$cityId = ccHelpers::city('ID');
$items = [];
foreach ($arResult['ITEMS'] as $item) {
    if (!empty($item['PROPERTIES']['CITIES']['VALUE']) && !in_array($cityId, $item['PROPERTIES']['CITIES']['VALUE'])) continue;
    $item['USE_PICTURE'] = CFile::ResizeImageGet($item['PREVIEW_PICTURE'], ['width' => 1180, 'height' => 440])['src'];

    $small_src =  CFile::GetPath($item['PROPERTIES']['RESPONSIVE_IMG']["VALUE"]);
    $item['SMALL_PICTURE'] = $small_src;

    $items[] = $item;

}
$arResult['ITEMS'] = $items;

