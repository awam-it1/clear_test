<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}

if (count($arResult['BLOKS'])) :?>
    <a href="<?= $arResult['BLOKS']['LINK']; ?>">
        <div style="font-family: 'Open Sans', sans-serif;
                    width: 100%;
                    background: <?= $arResult['BLOKS']['COLOR_BACKGROUND']; ?>;
                    color: <?= $arResult['BLOKS']['COLOR_TEXT']; ?>;
                    text-align: center;
                    font-size: <?= $arResult['BLOKS']['FONT_SIZE']; ?>pt;
                    vertical-align: middle;"><div style="padding: 10px">
            <?= $arResult['BLOKS']['NAME']; ?>
            </div>
        </div>
    </a>
<?endif;?>