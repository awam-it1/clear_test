<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$cityId = ccHelpers::city('ID');


if(count($arResult['ITEMS'])){
    foreach ($arResult['ITEMS'] as  $item){
        $block['ID'] = $item['ID'];
        $block['NAME'] = $item['NAME'];
        $block['COLOR_BACKGROUND'] = $item['PROPERTIES']['COLOR_BACKGROUND']['VALUE'];
        $block['COLOR_TEXT'] = $item['PROPERTIES']['COLOR_TEXT']['VALUE'];
        $block['CITIES'] = $item['PROPERTIES']['CITIES']['VALUE'];
        $block['LINK'] = $item['PROPERTIES']['LINK']['VALUE'];
        $block['FONT_SIZE'] = ($item['PROPERTIES']['FONT_SIZE']['VALUE'])?$item['PROPERTIES']['FONT_SIZE']['VALUE']:18;
        if(in_array($cityId, $block['CITIES'])){
            $arResult['BLOKS'] = $block;
            break;
        }
    }
}

