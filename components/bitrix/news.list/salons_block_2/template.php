<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// print_r($arResult);
if (count($arResult["ITEMS"])):
?>
<div class="sale-card-grid">
	<?foreach($arResult["ITEMS"] as $arItem):?>
	<div class="sale-card">
		<div class="sc-image">
			<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"];?>" alt="<?=$arItem["NAME"];?>">
		</div>
		<div class="sc-title"><?=$arItem["NAME"];?></div>
		<div class="sc-descr"><?=$arItem["~PREVIEW_TEXT"];?></div>
		<a href="<?=$arItem["DETAIL_PAGE_URL"];?>" class="is-link sc-link">Подробнее</a>
	</div>
	<?endforeach;?>
</div>
<?endif;?>