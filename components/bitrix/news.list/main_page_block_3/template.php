<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// print_r($arResult);
?>

<div class="index-sertificate revealer">
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<?if ($arItem["DISPLAY_PROPERTIES"]["TYPE"]["VALUE"] == "Изображение"):?>
			<div class="iser-item">
				<img src="<?=CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], ['width' => 280, 'height' => 200])['src'];?>" alt="<?=$arItem["NAME"];?>">
			</div>
		<?else:?>
			<div class="iser-item">
				<div class="iser-title"><?=$arItem["NAME"];?></div>
				<div class="iser-descr"><?=$arItem["~PREVIEW_TEXT"];?></div>

				<?if (strlen($arItem["DISPLAY_PROPERTIES"]["LINK"]["VALUE"]) && strlen($arItem["DISPLAY_PROPERTIES"]["LINK"]["DESCRIPTION"])):?>
				<a href="<?=$arItem["DISPLAY_PROPERTIES"]["LINK"]["VALUE"];?>" class="iser-link"><?=$arItem["DISPLAY_PROPERTIES"]["LINK"]["DESCRIPTION"];?></a>
				<?endif;?>
			</div>
		<?endif;?>
	<?endforeach;?>
</div>