<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// echo '<script>var log_php_object = '.json_encode($arResult).';</script>';
?>

<?foreach($arResult["ITEMS"] as $arItem):?>
	<div class="news-item">
		<div class="news-image"<?if (strlen($arItem["PREVIEW_PICTURE"]["SRC"])):?> style="background-image: url('<?=$arItem["PREVIEW_PICTURE"]["SRC"];?>')"<?endif;?>>
			<? if (strlen($arItem["DISPLAY_PROPERTIES"]["DATE_BEGIN"]["VALUE"])):
				list($day, $month) = explode(" ", 
					CIBlockFormatProperties::DateFormat("j F", MakeTimeStamp($arItem["DISPLAY_PROPERTIES"]["DATE_BEGIN"]["VALUE"], CSite::GetDateFormat()))
					);?>
				<div class="inews-date"><?=$day;?> <span><?=$month;?></span></div>
			<?endif;?>
		</div>
		<a href="<?=$arItem["DETAIL_PAGE_URL"];?>" class="shop__good-name"><?=$arItem["NAME"];?></a>
		<div class="inews-text">
			<p><?=$arItem["~PREVIEW_TEXT"];?></p>
		</div>
	</div>
<?endforeach;?>
<?if ($arResult["NAV_RESULT"]->NavPageNomer < $arResult["NAV_RESULT"]->NavPageCount):
    $salonlink = '';
    if(!empty($_GET['salon'])) {
        if($_GET['salon']!='-1') {
            $salonlink = '?SALON='.htmlspecialcharsbx($_GET['salon']);
        }
    }?>
<div class="news-show-more js-show-more-cnt">
	<button type="button" data-href="/ajax/promoutionsShowMoreTest.php<?=$salonlink;?>" class="button js-show-more" data-page="<?=$arResult["NAV_RESULT"]->NavPageNomer+1?>" data-skip-id="<?=$arResult["SKIP_ID"]?>" data-salon="<?=$arParams["SALON"]?>" data-section="<?=$arParams["SALON"]?>">Показать еще</button>
</div>
<?endif;?>
<?if ((count($arResult["ITEMS"]) == 0) && (($arResult["NAV_RESULT"]->NavPageNomer == 1) || (is_null($arResult["NAV_RESULT"]->NavPageNomer)))):?>	
	<div class="news-item">
	</div>
	<div class="news-item">
	</div>
	<div>
		<center>
			<h3>Акций по вашему запросу не найдено</h3>
		</center>
	</div>
<?endif;?>
