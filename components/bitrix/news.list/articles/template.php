<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<br>
<br>
	<div class="news-cnt articles-cnt">
		<?foreach($arResult["ITEMS"] as $arItem):
			if (strlen($arItem["PREVIEW_PICTURE"]["SRC"])) {
				$img = $arItem["PREVIEW_PICTURE"]["SRC"];
				if(!is_file($_SERVER["DOCUMENT_ROOT"].$img))
					$img = '/local/include/img/stub.jpg';
			} else
				$img = '/local/include/img/stub.jpg';
            ?>
		<div class="news-item">
            <div class="news-image" style="background-image: url('<?=$img;?>')">
			</div>
			<a href="<?=$arItem["DETAIL_PAGE_URL"];?>" class="shop__good-name"><?=$arItem["NAME"];?></a>
			<div class="inews-text">
				<p><?=$arItem["~PREVIEW_TEXT"];?></p>
			</div>
		</div>
		<?endforeach;?>
	</div>
	<?=$arResult["NAV_STRING"];?>

