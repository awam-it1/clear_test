<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// print_r($arParams);

$icons = Array(
	"PDF" => '<svg xmlns="http://www.w3.org/2000/svg" width="31" height="29" viewBox="0 0 31 29"><path fill="#fff" d="M0 28.67V0h30.02v28.67zm2.88-2.89h24.25V2.89H2.88z"/><path fill="none" stroke="#fff" stroke-width="1.57" stroke-miterlimit="50" d="M15.14 4.26c.43.19.33.46.19 1.76a27.2 27.2 0 0 1-1.53 6.22 33.1 33.1 0 0 1-3.47 6.54 17.87 17.87 0 0 1-2.97 3.56c-.78.66-1.32.83-1.67.89-.35.06-.5 0-.6-.17a1.2 1.2 0 0 1-.04-.82c.07-.36.26-.77.8-1.29.54-.53 1.43-1.17 2.66-1.77a28.44 28.44 0 0 1 7.31-2.28 24.9 24.9 0 0 1 2.39-.34 13.34 13.34 0 0 1 4.2.32c.6.15 1.11.32 1.55.6.44.28.82.66 1.01 1.09.19.42.19.89.04 1.22a1.3 1.3 0 0 1-.84.66c-.37.12-.78.15-1.27 0a6.06 6.06 0 0 1-1.66-.89c-.6-.42-1.24-.95-2.06-1.77a35.97 35.97 0 0 1-2.69-3.01 22.52 22.52 0 0 1-2.03-3.03c-.47-.9-.73-1.63-.93-2.36-.21-.73-.37-1.46-.43-2.12a7.88 7.88 0 0 1 .03-1.71c.07-.47.16-.82.32-1.05.16-.24.38-.36.54-.41.16-.06.25-.06.35-.08.09-.01.18-.04.33 0 .14.04.32.16.47.24z" stroke-linejoin="round"/></svg>'
	, "DOCx" => '<svg xmlns="http://www.w3.org/2000/svg" width="30" height="29" viewBox="0 0 30 29"><path fill="#fff" d="M20.93 21.89h-3.65l-2.5-7.72-2.62 7.72H8.53L4.43 8.57h3.73l2.39 8.61 2.9-8.61h2.84l2.7 8.61 2.6-8.61h3.72zM30 0H0v28.65h30zM2.88 2.89h24.23v22.88H2.88z"/></svg>'
	, "RTF" => '<svg xmlns="http://www.w3.org/2000/svg" width="31" height="29" viewBox="0 0 31 29"><path fill="#fff" d="M4.58 18.95v2.95H16.4v-2.95zm0-2.96h20.68v-2.95H4.58zm0-5.91h20.68V7.13H4.58zm25.44 18.59H0V0h30.02zM2.88 25.78h24.25V2.88H2.88z"/></svg>'
	, "PPTx" => '<svg xmlns="http://www.w3.org/2000/svg" width="30" height="29" viewBox="0 0 30 29"><path fill="#fff" d="M16.81 11.63c-.32-.31-.89-.47-1.68-.47h-1.32v3.57h1.31c.85 0 1.43-.17 1.75-.53.29-.31.44-.74.44-1.28s-.17-.96-.5-1.29zm4 1.25c0 1.18-.3 2.16-.89 2.93-.77 1.01-2.05 1.51-3.84 1.51h-2.27v4.57H10.3V8.57h5.72c1.62 0 2.85.45 3.7 1.35a4.17 4.17 0 0 1 1.09 2.96zM30 0H0v28.65h30zM2.88 2.88h24.23v22.89H2.88z"/></svg>'
	, "JPEG" => '<svg xmlns="http://www.w3.org/2000/svg" width="30" height="29" viewBox="0 0 30 29"><path fill="#fff" d="M0 28.58V0h29.93v28.58zm2.88-2.87h24.17V2.88H2.88zm5.34-18.6c1.61 0 2.91 1.21 2.91 2.71s-1.3 2.72-2.91 2.72S5.3 11.32 5.3 9.82c0-1.5 1.31-2.71 2.92-2.71zM4.37 21.02h20.68v-7.28l-2.2 1.6-3.24-2.4-8.72 6.45-2.2-1.61z"/></svg>'
	, "JPG" => '<svg xmlns="http://www.w3.org/2000/svg" width="30" height="29" viewBox="0 0 30 29"><path fill="#fff" d="M0 28.58V0h29.93v28.58zm2.88-2.87h24.17V2.88H2.88zm5.34-18.6c1.61 0 2.91 1.21 2.91 2.71s-1.3 2.72-2.91 2.72S5.3 11.32 5.3 9.82c0-1.5 1.31-2.71 2.92-2.71zM4.37 21.02h20.68v-7.28l-2.2 1.6-3.24-2.4-8.72 6.45-2.2-1.61z"/></svg>'
	, "PNG" => '<svg xmlns="http://www.w3.org/2000/svg" width="30" height="29" viewBox="0 0 30 29"><path fill="#fff" d="M0 28.58V0h29.93v28.58zm2.88-2.87h24.17V2.88H2.88zm5.34-18.6c1.61 0 2.91 1.21 2.91 2.71s-1.3 2.72-2.91 2.72S5.3 11.32 5.3 9.82c0-1.5 1.31-2.71 2.92-2.71zM4.37 21.02h20.68v-7.28l-2.2 1.6-3.24-2.4-8.72 6.45-2.2-1.61z"/></svg>'
	, "ZIP" => '<svg xmlns="http://www.w3.org/2000/svg" width="31" height="29" viewBox="0 0 31 29"><path fill="#fff" d="M7 5V3h3v2zM10 7V5h3v2zM7 9V7h3v2zM10 11V9h3v2zM7 13v-2h3v2zM10 15v-2h3v2zM7 17v-2h3v2z"/><path fill="#fff" d="M30.02 28.67H0V0h30.02zM2.88 25.78h24.25V2.88H2.88z"/><path fill="#fff" d="M7 22v-4h6v4z"/><path fill="#8a007c" d="M8 21v-1h4v1z"/></svg>'
	);

for ($i = 1; $i <= 8; $i++):
	if (strlen($arParams["FILE_".$i])):?>
		<div class="row">
			<div class="col">
				<?for ($i = 1; $i <= 4; $i++):
					if (strlen($arParams["FILE_".$i])):
						$file_data = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$arParams["FILE_".$i]);
						list($_type, $type) = explode("/", $file_data["type"]);
						$type = mb_strtoupper($type);
					?>
					<a href="<?=$arParams["FILE_".$i];?>" class="is-file" download="download">
						<span class="ifile-icon">
							<?=$icons[$type];?>
						</span>
						<span class="ifile-info">
							<span class="ifile-name"><?=$file_data["name"];?></span>
							<span class="ifile-context"><?=$type;?>, <?=number_format($file_data["size"] / 1024 / 1024, 2);?> Мб</span>
						</span>
					</a>
					<?
					endif;
				endfor;?>
			</div>
			<div class="col">
				<?for ($i = 1; $i <= 4; $i++):
					if (strlen($arParams["FILE_".$i])):
						$file_data = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].$arParams["FILE_".$i]);
						list($_type, $type) = explode("/", $file_data["type"]);
						list($name,) = explode(".", $file_data["name"]);
						$type = mb_strtoupper($type);
					?>
					<a href="<?=$arParams["FILE_".$i];?>" class="is-file" download="download">
						<span class="ifile-icon">
							<?=$icons[$type];?>
						</span>
						<span class="ifile-info">
							<span class="ifile-name"><?=$name;?></span>
							<span class="ifile-context"><?=$type;?>, <?=number_format($file_data["size"] / 1024 / 1024, 2);?> Мб</span>
						</span>
					</a>
					<?
					endif;
				endfor;?>
			</div>
		</div>
	<? break;
	endif;?>
<?endfor;?>
