<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
// print_r($arResult);

if (count($arResult["MESSAGE"])) {
	if (CModule::IncludeModule("subscribe")) {
		// автоподтверждение подписки
		$subscr = new CSubscription;
		$ID = $subscr->Update($arResult["ID"], array("CONFIRMED" => "Y"), "s1");
	}
    echo json_encode(array("SUCCESS" => true, "MESSAGE" => "Вы подписаны на рассылку!"));
} elseif (count($arResult["ERROR"])) {
    echo json_encode(array("SUCCESS" => false, "MESSAGE" => strip_tags(implode(",", $arResult["ERROR"]))));
}
