<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) {
    die();
}
// print_r($arResult);

if (count($arResult["MESSAGE"])) :?>
    <div class="modal-self is-show">
        <div class="thanks-modal">
            <div class="sale-text">Спасибо! Вы подписаны на рассылку!</div>
        </div>
    </div>
<?endif;
if (count($arResult["ERROR"])) :?>
    <div class="modal-self is-show">
        <div class="thanks-modal">
            <div class="sale-text">Произошла ошибка</div>
            <div class="is-styled"><?=implode(",", $arResult["ERROR"]);?></div>
        </div>
    </div>
<?endif;?>

<div class="news-item news-subscription-cnt">
    <div class="news-subscription" data-is="subscribe">
        <form action="/ajax/subscribe.php">
            <?=bitrix_sessid_post();?>
            <input type="hidden" name="PostAction" value="<?echo ($arResult["ID"] > 0 ? "Update" : "Add")?>" />
            <input type="hidden" name="ID" value="<?echo $arResult["SUBSCRIPTION"]["ID"];?>" />
            <input type="hidden" name="RUB_ID[]" value="0" />
            <input type="hidden" name="RUB_ID[]" value="1" />
            <input type="hidden" name="footer_subsribe" value="1">
         
            <div class="ns-action"><?=$arParams["~TITLE"];?></div>
            <div class="ns-description"><?=$arParams["~DESCRIPTION"];?></div>
              <div class="input-element">
                <label>
                    <input placeholder="Email" name="EMAIL" data-validation="email">
                    <span class="form-item-error">Укажите email</span>
                </label>
                <button class="isubscrip-submit" type="submit" name="Save">
                    <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25"><path fill="#e6e6e6" d="M1.65 12.62c-.9.47-.86 1.16.08 1.53l2.78 1.1c.94.37 2.36.17 3.16-.45l12.08-9.53c.8-.63.88-.53.19.2L10.39 15.7c-.69.73-.49 1.64.46 2l.32.13 3.42 1.36 3.08 1.26c.94.38 1.94-.07 2.21-1.05l4.7-17.29c.27-.97-.25-1.39-1.14-.92zM9.06 24.08c-.05.17 1.91-2.83 1.91-2.83.55-.85.24-1.86-.7-2.24l-2.13-.87c-.94-.39-1.39.06-1 1 0 0 1.98 4.77 1.92 4.94z"></path></svg>
                </button>
              </div>
            <div class="ns-footnote"><?=$arParams["~SUB_DESCRIPTION"];?></div>
        </form>
        <div class="hidden-content">
            <div id="subscribe-modal" class="modal-content-inner">
                <div class="simple-modal is-styled">
                    <h2 class="js-subscribe-title"></h2>
                    <div class="anotation">
                        <p class="js-subscribe-text"></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
