<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// echo '<script>var log_php_object = '.json_encode($arResult).';</script>';
?>

<div class="lk-sharing">
	<h2>Поделитесь ссылкой с друзьями <br>и получите бонус</h2>
	<div class="lks-steps">
	<?foreach($arParams["STEP"] as $key => $step):
			if (empty($step)) continue;?>
		<div class="lks-step">
			<div class="lks-number"><?=($key + 1);?></div>
			<div class="lks-text"><?=$step;?></div>
		</div>
	<?endforeach;?>
	</div>
	<div class="lks-link">
		<div class="input-element">
			<label>
				<input id="tagret" value="https://www.Реферальная сслыка.рф" type="text" readonly/>
			</label>
		</div>
		<button type="button" class="button solid js-clipboard" data-clipboard-target="#tagret">Скопировать ссылку</button>
	</div>
	<h3>Или разместите ссылку в соцсетях</h3>
	<social-sharing url="https://vuejs.org/"
					title="The Progressive JavaScript Framework"
					description="Intuitive, Fast and Composable MVVM for building interactive interfaces."
					quote="Vue is a progressive framework for building user interfaces."
					hashtags="vuejs,javascript,framework"
					twitter-user="vuejs"
					inline-template>
		<div class="lks-social">
			<div class="gfs-item">
				<network network="facebook">
				<svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32"><path fill="#8a007c" d="M13.65 16.55v8.62h3.74c.02-2.27 0-4.6 0-6.87v-2.26l.02-.03s2.37.02 2.45-.01l.05-.01.32-3.14H17.4v-1.41c0-.42-.01-.95.25-1.18l2.57-.26V6.85c-.49-.03-1.07-.01-1.57-.01-.51 0-1.04-.01-1.55.03-.97.06-1.75.35-2.31.83-.82 1.32-1.14.19-1.15 5.15h-1.87v3.16h1.86c.05.06.02.47.02.54zM32 16a16 16 0 1 1-32 0 16 16 0 0 1 32 0z"/></svg>
				</network>
			</div>
			<div class="gfs-item">
				<network network="vk">
				<svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32"><path fill="#8a007c" d="M7.25 12.3c.14-.04.27-.06.37-.06h.14l.35.01.48.02.56-.03c.16-.01.3-.02.43-.01l.34.01c.2.02.36.06.48.11.06.03.12.1.19.2s.13.19.17.28l.17.38.15.31c.37.82.84 1.62 1.39 2.42l.11.16.12.19.14.17c.05.07.1.12.15.16l.16.13c.05.04.11.07.17.08h.17c.28-.05.44-.63.46-1.73l.02-.52a3.5 3.5 0 0 0-.22-1.51.73.73 0 0 0-.23-.26c-.1-.07-.22-.13-.37-.18-.14-.06-.26-.11-.34-.16.14-.27.39-.46.74-.55a6 6 0 0 1 1.43-.13h.78a11.42 11.42 0 0 0 .79.05l.33.08.28.14c.2.09.26.16.19.21.09.17.14.38.16.62l.02.21c0 .19-.02.45-.05.77l-.06.77a5.07 5.07 0 0 0-.01 1.24c.04.44.19.74.44.91.09-.01.17-.03.25-.06a.8.8 0 0 0 .23-.17c.08-.07.13-.13.16-.17l.19-.24.15-.21c.47-.61.93-1.42 1.4-2.42l.13-.33.17-.39c.11-.26.17-.36.18-.29 0 .07.08 0 .23-.2.16-.07.34-.1.56-.1h.13l.69.03.69.03.41-.02.48-.02c.14-.01.29 0 .44.01.15.01.28.04.38.09.1.04.17.11.21.19a.4.4 0 0 1 .04.19c0 .29-.17.72-.52 1.29a8.8 8.8 0 0 1-.51.78l-.68.86-.54.69-.21.26-.26.36c-.06.08-.11.19-.17.33-.06.15-.08.28-.08.4 0 .08.02.15.05.22a1.75 1.75 0 0 0 .29.4l.2.19.18.17c.7.64 1.25 1.22 1.66 1.73.39.51.59.88.59 1.11 0 .33-.25.53-.76.61a4.93 4.93 0 0 1-1.42.01 5.5 5.5 0 0 0-.55-.03c-.25 0-.48.03-.66.08h-.09c-.66-.11-1.34-.57-2.06-1.4a2.8 2.8 0 0 1-.26-.3c-.13-.16-.23-.29-.32-.38a1.87 1.87 0 0 0-.34-.25.62.62 0 0 0-.4-.09c-.23.04-.38.19-.46.43a4.2 4.2 0 0 0-.13.88 1.6 1.6 0 0 1-.14.72c-.13.24-.52.36-1.17.36-.28 0-.6-.02-.98-.06a6.16 6.16 0 0 1-3.59-1.59 14.3 14.3 0 0 1-1.2-1.38c-1.03-1.31-2-2.9-2.92-4.77l-.16-.33-.18-.38a5.05 5.05 0 0 1-.26-.76c-.02-.14-.04-.27-.04-.4a.9.9 0 0 1 .37-.17zM16 0a16 16 0 1 0 0 32 16 16 0 0 0 0-32z"/></svg>
				</network>
			</div>
		</div>
	</social-sharing>
</div>