<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $services_section;
	/**
	 * Collect services and their groups from saloons from current city
	 */
	global $CITY_CODE;
	$allowed_services_sections = [];
    $salon_price_types = [];
    //Получаем урл откуда пришел запрос и парсим
    $urlParse = (parse_url($_SERVER['HTTP_REFERER']));
	$sDb = CIBlockElement::GetList(
		['SORT' => 'ASC'],
		[
            "IBLOCK_ID" => 6,
            "SECTION_CODE" => $CITY_CODE,
            "ACTIVE" => "Y",
			"INCLUDE_SUBSECTIONS" => "Y"
		],
		false,
		false,
		[
			"ID",
			"NAME",
			"PROPERTY_ALL_SERVICES",
            "PROPERTY_PRICE_TYPE"
		]
	);
//>Cобираем данные для фильтрации по фильтру
    //Получаем id секции СПА процедур на основании 1C ID, который не изменится никогда, в отличие от названия или симв.кода
    $spaSection = \ccHelpers::serviceById('5c88f118-f966-11e0-ac7d-c80aa95078cb');
    $spaSectionID = $spaSection['ID'];
    $spaSectionCode = $spaSection['CODE'];
    //Если нашли то получаем дочерние элементы с границами
    if ($spaSectionID) {
       $forWhomElements = CIBlockSection::GetList([],["SECTION_ID" => $spaSectionID],false);
        while($forWhomElem = $forWhomElements->Fetch()) {
          $arForWhom[] = [
              'NAME'=> $forWhomElem['NAME'],
              'CODE'=> $forWhomElem['CODE'],
              'LEFT_MARGIN' => $forWhomElem['LEFT_MARGIN'],
              'RIGHT_MARGIN' => $forWhomElem['RIGHT_MARGIN']];
        }
    }
//<
    CModule::includeModule('trdata');

    $cityFields = trServiceData::getSectionsList(
        6,
        array(),
        array('CODE'=>$CITY_CODE)
    );
    $priceType = trServiceData::getPriceType(6, $cityFields[0]['ID']);
	while($sOb = $sDb->Fetch()) {
        $allowed_services_sections[] = $sOb['PROPERTY_ALL_SERVICES_VALUE']['service'];
        $salon_price_types[] = $priceType ?: $sOb['PROPERTY_PRICE_TYPE_VALUE'];
	}

	// варианты используемых услуг
    $rsItem = CIBlockElement::GetList(
        Array("SORT" => "ASC")
        , Array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "SECTION_ID" => array_map(function($section) { return $section["ID"]; }, $arResult["SECTIONS"]), "ACTIVE" => "Y")
        , false
        , false
        , Array("ID", "IBLOCK_SECTION_ID", "PROPERTY_ID1C", "PROPERTY_TIME")
    );
    while ($arItem = $rsItem->Fetch()) { 
        foreach ($arResult["SECTIONS"] as &$arSection) {
            if ($arItem["IBLOCK_SECTION_ID"] == $arSection["ID"]) {
                if (!isset($arSection["ITEMS"])) {
                    $arSection["ITEMS"] = [];
                }
                //Определяем по границам к какой категории относится элемент
                foreach ($arForWhom as $elem){
                    if ($arSection['LEFT_MARGIN'] >= $elem['LEFT_MARGIN'] && $arSection['RIGHT_MARGIN']<=$elem['RIGHT_MARGIN']) {
                        $arItem['forwhom'] = $elem['CODE'];
                    }
                }
                    $arSection["ITEMS"][] = $arItem;
                    break;
                }
        }
    }

    //Получаем все цены
    $all_prices = [];
    $sDb = CIBlockElement::GetList([], [
        "IBLOCK_ID" => IBLOCK_salons_prices,
        "ACTIVE" => "Y",
        "INCLUDE_SUBSECTIONS" => "Y"
    ], false, false, [
        "ID",
        "NAME",
        "PROPERTY_SERVICE",
        "PROPERTY_TYPE",
        "PROPERTY_VALUE",
    ]);
    while ($sOb = $sDb->GetNext()) {
        $all_prices[$sOb["PROPERTY_SERVICE_VALUE"].':'.$sOb["PROPERTY_TYPE_VALUE"]] = intval($sOb["PROPERTY_VALUE_VALUE"]);
    }

// проставляем цены
    foreach ($arResult["SECTIONS"] as &$arSection) {
        $arSection['PRICE_TEXT'] = 'не указана';
        $arSection['PRICE'] = 0;
        $price_max = $price_min = null;
        foreach ($arSection["ITEMS"] as $arItem) {
            $id1c = $arItem["PROPERTY_ID1C_VALUE"];
            foreach ($salon_price_types as $salon_price_type) {
                if(!empty($all_prices[$id1c.':'.$salon_price_type])) {
                    if (is_null($price_min) || $all_prices[$id1c.':'.$salon_price_type] < $price_min) {
                        $price_min = $all_prices[$id1c.':'.$salon_price_type];
                    }
                    if (is_null($price_max) || $all_prices[$id1c.':'.$salon_price_type] > $price_max) {
                        $price_max = $all_prices[$id1c.':'.$salon_price_type];
                    }
                }
            }
        }
        if ($price_min) {
            $arSection['PRICE'] = $price_min;
            $arSection['PRICE_TEXT'] = ($price_min == $price_max ? '' : 'от ') . $price_min . ' руб.';
        }
    }


    function PrepareItemForJson($item) {
    	$res = [
    		"id" => $item["UF_ID1C"],
		    "sort_id" => $item["ID"]
    		, "section_id" => $item["ID"]
    		, "img" => ($item["UF_PICTURES"] ? $item["UF_PICTURES"][0] : '/local/include/img/stub.jpg')
    		, "link" => $item["SECTION_PAGE_URL"]
    		, "priceText" => $item["PRICE_TEXT"]
            , "price" => $item["PRICE"]
    		, "name" => $item["~NAME"]
    		, "text" => $item["~UF_SHORT"]
    		, "filterParam" => [ "duration" => [array("value" => -1)] ]
            , "sort" => (int)$item["SORT"]
    	];
    	
    	if ($item["UF_PICTURES"]) {
    		$res["img"] = CFile::ResizeImageGet($res["img"], ['width' => 335, 'height' => 240])['src'];
    	}
    	else {
    		$res["img"] = '/local/include/img/stub.jpg';
    	}
    	
    	if (count($item["ITEMS"])) {
    		$res["filterParam"]["duration"] =
    		    array_map(function($duration) {
                    return array(
    		            'value' => $duration["PROPERTY_TIME_VALUE"],
    		            'enroll' => $duration["ID"] // ID
                    );
                }, $item["ITEMS"]);
        }

        $res["filterParam"]["forwhom"] = array(
            array(
                'value' => $item['ITEMS'][0]['forwhom'],
            )
        );
        return $res;
    }

    // найти все дочерние
    function get_childrens($current_item, $list, $needFilter = false, $allowed_services_sections = []) {
        $children = [];

        foreach ($list as $item) {
            if ($item["IBLOCK_SECTION_ID"] == $current_item["ID"]) {
                $children[] = $item;
            }
        }

        if (!empty($children)) {
            $childServices = [];
            foreach ($children as $child) {
                $childServices = array_merge(
                    $childServices,
                    get_childrens(
                        $child,
                        $list,
                        $needFilter && !in_array((int) $child['ID'], $allowed_services_sections),
                        $allowed_services_sections
                    )
                );
            }
            return $childServices;
        }
        
        if(!$needFilter) {
        	$res = PrepareItemForJson($current_item);
        	return [$res];
    	} else {
    		return [];
    	}
    }

// формируем основной объект
    $arResult["JSON_OBJECT"] = Array();
    foreach ($arResult["SECTIONS"] as $arSection) {
    	/**
    	 * Проходим по первому уровню категорий
    	 */
        if ($arSection["IBLOCK_SECTION_ID"] == null) {
        	
        	$needFilter = !in_array($arSection['ID'], $allowed_services_sections);

            $list = get_childrens($arSection, $arResult["SECTIONS"], $needFilter, $allowed_services_sections);

	        global $services_count;
	        $services_count[$arSection["NAME"]] = count($list);

        	// Скрываем данный таб, если нет детишек
        	if(empty($list) || (!empty($services_section) && $arSection['CODE'] != $services_section)) {
        		continue;
        	}
        	
            $res = [
                'name' => $arSection["UF_ID1C"]
                , 'text' => $arSection["NAME"]
                , 'filterParam' => []
                , 'list' => $list
            ];

	        $duration = array_unique(
		        call_user_func_array(
			        'array_merge'
			        , array_map(
				        function($item) { return array_column($item["filterParam"]["duration"], "value"); }
				        , $list
			        )
		        )
	        );

            if (count($duration) > 1) {
                sort($duration);
                $duration = array_map(function($time) {
                    return [
                        "name" => $time
                        , "text" => make_time($time)
                        , "isFilter" => false
                    ];
                }, $duration);

                $res["filterParam"][] = [
                    "name" => "duration"
                    , "text" => "Продолжительность"
                    , "isFilter" => false
                    , "isOpen" => false
                    , "values" => $duration
                ];
            }
            //Включение отображения фильтра
            $isFilter = false;
            //Фильтр добавляется только на странице 'СПА-программы для женщин и мужчин'
            if (!empty($arForWhom) && $services_section == $spaSectionCode) {
                foreach ($arForWhom as $elem){
                    //если пришли с главной на "СПА для двоих" то выделям у фильтра значение
                    if($urlParse['path'] == '/' && $elem['CODE'] == 'spa-dlya-dvoikh') {
                        $forwhom[] = ["name" => $elem['CODE'], "text" => $elem['NAME'], "isFilter" => true];
                        $isFilter = true;
                    }else{
                        $forwhom[] = ["name" => $elem['CODE'], "text" => $elem['NAME'], "isFilter" => false];
                    }
                }

                $res["filterParam"][] = [
                    "name" => "forwhom"
                    , "text" => "Для кого"
                    , "isFilter" => $isFilter
                    , "isOpen" => false
                    , "values" => $forwhom
                ];

            }

            if (count($list)) {
                $arResult["JSON_OBJECT"][] = $res;
            }
        }
    }

    if(empty($arResult["JSON_OBJECT"])) {
    	$APPLICATION->RestartBuffer();
    	LocalRedirect('/services/');
    }

    // для общей страницы услуг
    if(empty($services_section)) {
        $arr = array();
        foreach($arResult["JSON_OBJECT"] as $category) {
            $arr = array_merge($arr, $category['list']);
        }
        $arResult["JSON_OBJECT"][0]['list'] = $arr;

        // фильтры
	    $arr = array();
	    foreach($arResult["JSON_OBJECT"] as $category) {
		    foreach ($category['filterParam'][0]['values'] as $newvalue) {

			    if(is_null(searchForName($newvalue['name'], $arr))) {
					array_push($arr, $newvalue);
			    }
		    }
	    }

		// сортировка по времени
	    sort($arr);
	    $duration = array_map(function($time) {
		    return $time;
	    }, $arr);

	    $arResult["JSON_OBJECT"][0]['filterParam'][0]['values'] = $duration;
    }

?>