<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// echo '<script>var log_php_object = '.json_encode($arResult).';</script>';

foreach ($arResult["SECTIONS"] as $key => $arSection) {
	$rsElement = CIBlockElement::GetList(
		Array("SORT" => "ASC")
		, Array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "SECTION_ID" => $arSection["ID"], "ACTIVE" => "Y")
		, Array()
		, false
		, Array("ID", "NAME")
	);
	if ($rsElement == 0) {
		unset($arResult["SECTIONS"][$key]);
	}
}

$arResult["SECTIONS"] = array_map(function($arSection) {
	return Array(
		"id" => $arSection["ID"]
		, "name" => $arSection["NAME"]
	);
}, $arResult["SECTIONS"]);


array_unshift($arResult["SECTIONS"], Array(
	"id" => "-1"
	, "name" => "Все"
));
?>