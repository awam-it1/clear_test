<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// echo '<script>var log_php_object_Params = '.json_encode($arParams).';</script>';

// найти все дочерние
function get_childrens($current_item, $list, $needFilter = false, $allowed_services_sections = []) {
    $children = [];

    foreach ($list as $item) {
        if ($item["IBLOCK_SECTION_ID"] == $current_item["ID"]) {
            $children[] = $item;
        }
    }

    if (!empty($children)) {
        $childServices = [];
        foreach ($children as $child) {
            $childServices = array_merge(
                $childServices,
                get_childrens(
                    $child,
                    $list,
                    $needFilter && !in_array((int) $child['ID'], $allowed_services_sections),
                    $allowed_services_sections
                )
            );
        }
        return $childServices;
    }

    if(!$needFilter) {
        return [$current_item];
    } else {
        return [];
    }
}

$allowed_services_sections = Lib::getServiceIDs();

$arResult["JSON_PROCEDURES"] = [];
foreach ($arResult["SECTIONS"] as $arSection) {
    if($arSection["ID"] == $arParams["SELECTED"]){
        $arResult["SELECTED_SERVICE"] = $arSection["ID"];
    }
    if ($arSection["IBLOCK_SECTION_ID"] == null) {
        $needFilter = !in_array($arSection['ID'], $allowed_services_sections);
        $arResult["JSON_PROCEDURES"] = array_merge( // смешиваем с предыдущим
            $arResult["JSON_PROCEDURES"], array_map(
                function($children) {
                	return Array('id' => $children["ID"], "id1c" => $children["UF_ID1C"], 'name' => $children["NAME"], 'sort' => $children["SORT"]);
                }, // оставляем только id,name
                get_childrens($arSection, $arResult["SECTIONS"], $needFilter, $allowed_services_sections) // получаем самые глубокие
            )
        );
    }
}
    
// варианты используемых услуг
    if (count($arResult["JSON_PROCEDURES"])) {
        $rsServiceVariant = CIBlockElement::GetList(
            Array("PROPERTY_TIME" => "ASC")
            , Array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "SECTION_ID" => array_map(function($service) { return $service["id"]; }, $arResult["JSON_PROCEDURES"]), "ACTIVE" => "Y")
            , false
            , false
            , Array("ID", "IBLOCK_SECTION_ID", "PROPERTY_ID1C", "PROPERTY_TIME")
        );
        echo '</script>';

        while ($arServiceVariant = $rsServiceVariant->Fetch()) { 
            foreach ($arResult["JSON_PROCEDURES"] as &$arService) {
                if ($arService["id"] == $arServiceVariant["IBLOCK_SECTION_ID"]) {
                    if (!isset($arService["durations"])) {
                        $arService["activeDuration"] = (!empty($_GET['duration'])) ? (int)$_GET['duration']: (int)$arServiceVariant["ID"];
                        $arService["durations"] = [];
                    }

                    $arService["durations"][] = [ "id" => (int)$arServiceVariant["ID"], "name" => make_time($arServiceVariant["PROPERTY_TIME_VALUE"]),  "id1c" => $arServiceVariant["PROPERTY_ID1C_VALUE"]];
                    break;
                }
            }
        }

        // ручная сортировка
	    function procedure_sort_sort($a, $b) {
	    	if((int)$a['sort'] < (int)$b['sort'])
	    		return '-1';
	    	else
	    		return 1;
	    }
	    function procedure_sort_id($a, $b) {
		    if($a['id'] < $b['id'])
			    return '-1';
		    else
			    return 1;
	    }

	    $unsorted = array();
	    $sorted = array();
	    foreach ($arResult["JSON_PROCEDURES"] as &$arService) {
        	if($arService['sort']=='500') {
		        $arService['sort'] = $arService['id'];
		        $unsorted[] = $arService;
	        } else {
        		$sorted[] = $arService;
	        }
	    }
	    usort($sorted, 'procedure_sort_sort');
	    usort($unsorted, 'procedure_sort_id');

	    $arResult["JSON_PROCEDURES"] = array_merge($sorted, $unsorted);

	    // сортировка в обратном порядке
	    $arResult["JSON_PROCEDURES"] = array_reverse($arResult["JSON_PROCEDURES"]);
    }
?>