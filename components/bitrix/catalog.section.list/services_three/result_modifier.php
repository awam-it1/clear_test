<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

// id текущей услуги
$current_section_id = $arResult["CURRENT_ID"];

// echo '<script>var log_php_object= '.json_encode($arResult).';</script>';

// найти все дочерние
    function get_childrens($current_item, $list, $needFilter = false, $allowed_services_sections = []) {
        $children = [];

        foreach ($list as $item) {
            if ($item["IBLOCK_SECTION_ID"] == $current_item["ID"]) {
                $children[] = $item;
            }
        }

        if (!empty($children)) {
            $childServices = [];
            foreach ($children as $child) {
                $childServices = array_merge(
                    $childServices,
                    get_childrens(
                        $child,
                        $list,
                        $needFilter && !in_array((int) $child['ID'], $allowed_services_sections),
                        $allowed_services_sections
                    )
                );
            }
            return $childServices;
        }

        if(!$needFilter) {
            if (count($current_item["UF_PICTURES"])) {
                $current_item["IMG"] = CFile::GetPath(array_shift($current_item["UF_PICTURES"]));
            }

            return [$current_item];
        } else {
            return [];
        }
    }

    global $CITY_CODE;

    //расчет минимальной цены
    //получаем список салонов
    $sDb = CIBlockElement::GetList(
        [],
        [
            "IBLOCK_ID" => IBLOCK_salons_salons,
            "ACTIVE" => "Y",
            "SECTION_CODE" => $CITY_CODE,
            "INCLUDE_SUBSECTIONS" => "Y"
        ],
        false,
        false,
        [
            "ID",
            "NAME",
            "PROPERTY_ALL_SERVICES",
            "PROPERTY_PRICE_TYPE"
        ]
    );
    $salon_ids = [];
    while($sOb = $sDb->GetNext()) {
        $allowed_services_sections[] = $sOb['PROPERTY_ALL_SERVICES_VALUE']['service'];
        $salon_ids[] = $sOb['PROPERTY_PRICE_TYPE_VALUE'];
    }

// формируем основной список
    $arResult["LIST"] = Array();
    foreach ($arResult["SECTIONS"] as $arSection) {
        if ($arSection["IBLOCK_SECTION_ID"] == null) {
            $needFilter = !in_array($arSection['ID'], $allowed_services_sections);
            $arResult["LIST"] = array_merge(
                $arResult["LIST"],
                get_childrens($arSection, $arResult["SECTIONS"], $needFilter, $allowed_services_sections)
            );
        }
    }


// убираем текущиую услугу и выбираем только 6
    $arResult["LIST"] = array_filter($arResult["LIST"], function($arSection) use ($current_section_id) {
        return $arSection["ID"] != $current_section_id;
    });

    $arResult["LIST"] = array_slice($arResult["LIST"], 0, 6);


// варианты используемых услуг
    $ID1C_list = array_map(function($arSection) { return $arSection["UF_ID1C"]; }, $arResult["LIST"]);

    $rsItem = CIBlockElement::GetList(
        Array("SORT" => "ASC")
        , Array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "SECTION_ID" => array_map(function($section) { return $section["ID"]; }, $arResult["LIST"]), "ACTIVE" => "Y")
        , false
        , false
        , Array("ID", "IBLOCK_SECTION_ID", "PROPERTY_ID1C", "PROPERTY_TIME")
    );
    while ($arItem = $rsItem->Fetch()) { 
        foreach ($arResult["LIST"] as &$arSection) {
            if ($arItem["IBLOCK_SECTION_ID"] == $arSection["ID"]) {
                if (!isset($arSection["ITEMS"])) {
                    $arSection["ITEMS"] = [];
                }

                $arSection["ITEMS"][] = $arItem;
                $ID1C_list[] = $arItem["PROPERTY_ID1C_VALUE"];
                break;
            }
        }
    }

    //Получаем все цены
    $prices = [];
    $sDb = CIBlockElement::GetList([], [
        "IBLOCK_ID" => IBLOCK_salons_prices,
        "ACTIVE" => "Y",
        "INCLUDE_SUBSECTIONS" => "Y"
    ], false, false, [
        "ID",
        "NAME",
        "PROPERTY_SERVICE",
        "PROPERTY_TYPE",
        "PROPERTY_VALUE",
    ]);
    while ($sOb = $sDb->GetNext()) {
        $prices[$sOb["PROPERTY_SERVICE_VALUE"].':'.$sOb["PROPERTY_TYPE_VALUE"]] = intval($sOb["PROPERTY_VALUE_VALUE"]);
    }



// проставляем цены
    foreach ($arResult["LIST"] as &$arSection) {
        $price_min = null;
        foreach ($arSection["ITEMS"] as $arItem) {
            $id1c = $arItem["PROPERTY_ID1C_VALUE"];
            foreach ($salon_ids as $salon_id) {
                if(!empty($prices[$id1c.':'.$salon_id])) {
                    if (is_null($price_min) || $prices[$id1c.':'.$salon_id] < $price_min) {
                        $price_min = $prices[$id1c.':'.$salon_id];
                    }
                }
            }
        }


        if (!is_null($price_min)) {
            $arSection["PRICE_TEXT"] = "от ".$price_min.' руб.';
        } else {
            $arSection["PRICE_TEXT"] = "не указана";
        }
    }
?>