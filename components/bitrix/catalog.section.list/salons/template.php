<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
//die('<pre>' . print_r($arResult, true) . '</pre>');
global $CITY;
?>
<script>
	window.TAIRAI = {};
	window.TAIRAI.selectedCity = '<?=$arResult["CURRENT_CITY"];?>';
	window.TAIRAI.cityList = <?=json_encode($arResult["cityList"]);?>;
	window.TAIRAI.salons = <?=json_encode($arResult["salons"]);?>;
</script>

<div class="inner-wrapper">
	<div data-is="salonsApp">
		<div class="salons-city-select"  v-cloak="">
			<div class="salons-city-select-text">Спа салоны в <span>г. <?=$CITY;?></span></div>
            <?/*<div class="scs-select city-select">
                <multiselect v-model="city"
                    :options="cities"
                    :searchable="false"
                    :show-labels="false"
                    :value="city"
                    track-by="value"
                    label="label"
                    placeholder="Выберите город"
                    :close-on-select="true">
                </multiselect>
            </div>*/?>
        </div>

        <div class="salons-list" v-cloak="">
            <section class="top-tool-service">
                <div class="scs-select top-tool-service-select"  v-if="false"> <? /* metroStations.length > 0 */?>
                    <multiselect v-model="metroActive"
                    :options="metroStations"
                    :searchable="false"
                    :show-labels="false"
                    track-by="value"
                    label="label"
                    placeholder="Выберите станцию"
                    :close-on-select="true">
                </multiselect>
            </div>
            <a href="#" class="top-tool-service-link" :class="{ active: previewState == 'map' }" @click.prevent="setPreviewMap">
                <span class="tts-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="11" height="16" viewBox="0 0 11 16"><path fill="#8a007c" d="M5.48 0A5.5 5.5 0 0 0 0 5.6C0 9.76 5.48 16 5.48 16s5.49-6.24 5.49-10.4c0-3.12-2.43-5.6-5.49-5.6zm0 7.6a1.96 1.96 0 0 1-1.95-2 1.96 1.96 0 1 1 3.91 0c0 1.12-.86 2-1.96 2z"/></svg>
                </span>
                <span class="tts-text">
                    Карта
                </span>
            </a>
            <a href="#" class="top-tool-service-link" :class="{ active: previewState == 'list' }" @click.prevent="setPreviewList">
                <span class="tts-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="16" viewBox="0 0 18 16"><path fill="#2b2b2b" d="M2 4a2 2 0 0 0 2-2 2 2 0 0 0-2-2 2 2 0 0 0-2 2c0 1.1.9 2 2 2zm.01-3a1 1 0 0 1 .99.99 1 1 0 0 1-.99 1 1 1 0 0 1 0-1.99zM2 10a2 2 0 0 0 2-2 2 2 0 0 0-2-2 2 2 0 0 0-2 2c0 1.1.9 2 2 2zm.01-3a1 1 0 0 1 .99.99 1 1 0 0 1-.99 1 1 1 0 0 1 0-1.99zM2 16a2 2 0 0 0 2-2 2 2 0 0 0-2-2 2 2 0 0 0-2 2c0 1.1.9 2 2 2zm.01-3a1 1 0 0 1 .99.99 1 1 0 0 1-.99 1 1 1 0 0 1 0-1.99zM6.45 2.4h11.08c.25 0 .45-.45.45-.7 0-.25-.2-.7-.45-.7H6.45C6.2 1 6 1.45 6 1.7c0 .25.2.7.45.7zM6.45 8.4h11.08c.25 0 .45-.5.45-.76 0-.25-.2-.64-.45-.64H6.45C6.2 7 6 7.39 6 7.64c0 .26.2.76.45.76zM6.45 14.4h11.08c.25 0 .45-.47.45-.73 0-.25-.2-.67-.45-.67H6.45c-.25 0-.45.42-.45.67 0 .26.2.73.45.73z"/></svg>
                </span>
                <span class="tts-text">
                    Список
                </span>
            </a>
        </section>
        <section class="displayed-area">
            <section class="filter-area">
                <form action="/" class="salons-filter" @input="changeFilter">
                    <?
                    foreach($arResult["SERVICES"] as $arService):
                        if($arService["DESCRIPTION"]=='1'):
                            ?>
                            <div class="element-checkbox">
                                <label>
                                    <span class="radio-icon">
                                        <img src="<?=$arService["IMAGE"];?>" alt="<?=$arService["NAME"];?>">
                                    </span>
                                    <input type="checkbox" name="" value="<?=$arService["VALUE"];?>" v-model="filterState"/>
                                    <i></i>
                                    <?=$arService["NAME"];?>
                                </label>
                            </div>
                        <?endif;
                    endforeach;?>
                </form>
            </section>
            <salons-content :states="filterState"
            v-show="previewState === 'list'"
            :salonsit="itemsArray"
            :metro="metro"
            :city="city"></salons-content>

            <salons-map v-show="previewState === 'map'"
            :states="filterState"
            :salonsit="itemsArray"
            :metro="metro"
            :city="city"></salons-map>
        </section>
        </div>
<?/*
        <section class="all-salons" v-cloak="">
            <a href="#" class="all-salons-link" @click.prevent="getAllSalons">{{ allSalonsBtnText }}</a>
            <div class="all-salons-content" ref="allSalons">
                <div class="all-salons-inner"  v-if="allSalons.length > 0">
                    <salon-item v-for="salon in allSalons"
                    :key="salon.id"
                    :city="salon.city"
                    :metro="salon.metro"
                    :street="salon.address.streetAddress"
                    :streetdescr="salon.address.descr"
                    :time="salon.time"
                    :readmore="salon.readmore"
                    :enroll="salon.enroll"
                    :phones="salon.phoneNumbers"></salon-item>
                </div>
            </div>
        </section>*/?>
    </div>
</div>


<script type="text/x-template" id="salons">
	<section class="content-area">
		<transition name="fade">
			<div class="salons-error" v-if="salons.length === 0">
				К сожалению, по вашему запросу ничего не найдено
			</div>
		</transition>
		<salon-item v-for="salon in salons"
                    :key="salon.id"
                    :city="salon.city"
                    :metro="salon.metro"
                    :distance="salon.distance"
		            :street="salon.address.streetAddress"
                    :streetdescr="salon.address.descr"
                    :time="salon.time"
                    :readmore="salon.readmore"
                    :enroll="salon.enroll"
                    :phones="salon.phoneNumbers"
                    :coming_soon="salon.coming_soon">
        </salon-item>
	</section>
</script>
<script type="text/x-template" id="salonItem">
	<transition name="fade">
		<div class="salons-item">
            <div class="salons-item__top">
                <span v-if="distance > 0" class="nearest"></span>
                <div class="salon-item-metro salon-item-info" v-if="metro" >
                    <a :href="readmore" class="salon-item-info__link">
                        <div class="salon-item-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="12" height="10" viewBox="0 0 12 10"><path fill="#8a007c" d="M5.97 0A5.95 5.95 0 0 0 0 5.94c0 1.78.78 3.73.78 3.73h10.37s.78-2.06.78-3.73A5.95 5.95 0 0 0 5.97 0zm4.74 8.95H1.24S.73 7.21.73 5.92A5.24 5.24 0 0 1 5.99.71c2.9 0 5.25 2.34 5.25 5.21 0 1.3-.53 3.03-.53 3.03z"/><path fill="#8a007c" d="M7.81 2.03h-.08L5.97 5.77 4.14 2 1.65 8.81H1v.55h3.05v-.55h-.7l1.19-2.96 1.43 3.51 1.45-3.34 1.16 2.79h-.7v.55h2.97v-.55h-.61z"/></svg>
                        </div>
                        {{ streetdescr }}
                        <small v-if="distance > 0">(~ {{ (distance / 1000).toFixed(1) }} км)</small>
                    </a>
                </div>
                <div class="salon-item-address salon-item-info">
                    <div v-if="metro">
                        <div class="salon-item-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17" viewBox="0 0 14 17"><path fill="#8a007c" d="M11.93 2.94a6.36 6.36 0 0 1 .03 9.36L7 17l-4.96-4.7a6.34 6.34 0 0 1 0-9.32l.04-.04a7.21 7.21 0 0 1 9.85 0c-.04-.03-.04-.03 0 0z"/><path fill="#fff" d="M6.45 9.43c.19.45.26.9.27 1.19.02.18.01.3.01.3s-.11.08-.3.17c-.29.15-.77.33-1.31.33l-.19-.01a2 2 0 0 1-.84-.26 3.07 3.07 0 0 1-.87-.79c-.22-.29-.42-.6-.59-.91-.26-.47-.44-.91-.55-1.17L2 8.05s.57-.1 1.3-.12h.2c.53 0 1.1.05 1.6.23a2.28 2.28 0 0 1 1.35 1.27zm2.27-1.19a4.65 4.65 0 0 1 1.82-.31c.53 0 1 .06 1.25.09l.21.03s-.12.34-.33.79c-.19.42-.46.93-.79 1.39-.24.31-.5.6-.79.81a2.17 2.17 0 0 1-1.4.39c-.32 0-.64-.03-.93-.09h-.01c-.18 0-.21 1.38-.22 2.15V14h-.32l.02-1.36.05-1.95c.02-.57.02-1.3.67-1.93.22-.22.48-.4.77-.52zm-.21-1.81a5.06 5.06 0 0 0-1.02-2.69l-.22-.31L6.95 3l-.06.08-.16.22-.25.38c-.43.7-1.01 1.79-.99 2.73 0 .62.27 1.19.68 1.63a3.48 3.48 0 0 0 .86.69s.22-.12.49-.35l.32-.32c.34-.38.66-.93.67-1.63z"/></svg>
                        </div>
                        {{street}}
                    </div>
                    <a :href="readmore" class="salon-item-info__link" v-else="">
                        <div class="salon-item-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17" viewBox="0 0 14 17"><path fill="#8a007c" d="M11.93 2.94a6.36 6.36 0 0 1 .03 9.36L7 17l-4.96-4.7a6.34 6.34 0 0 1 0-9.32l.04-.04a7.21 7.21 0 0 1 9.85 0c-.04-.03-.04-.03 0 0z"/><path fill="#fff" d="M6.45 9.43c.19.45.26.9.27 1.19.02.18.01.3.01.3s-.11.08-.3.17c-.29.15-.77.33-1.31.33l-.19-.01a2 2 0 0 1-.84-.26 3.07 3.07 0 0 1-.87-.79c-.22-.29-.42-.6-.59-.91-.26-.47-.44-.91-.55-1.17L2 8.05s.57-.1 1.3-.12h.2c.53 0 1.1.05 1.6.23a2.28 2.28 0 0 1 1.35 1.27zm2.27-1.19a4.65 4.65 0 0 1 1.82-.31c.53 0 1 .06 1.25.09l.21.03s-.12.34-.33.79c-.19.42-.46.93-.79 1.39-.24.31-.5.6-.79.81a2.17 2.17 0 0 1-1.4.39c-.32 0-.64-.03-.93-.09h-.01c-.18 0-.21 1.38-.22 2.15V14h-.32l.02-1.36.05-1.95c.02-.57.02-1.3.67-1.93.22-.22.48-.4.77-.52zm-.21-1.81a5.06 5.06 0 0 0-1.02-2.69l-.22-.31L6.95 3l-.06.08-.16.22-.25.38c-.43.7-1.01 1.79-.99 2.73 0 .62.27 1.19.68 1.63a3.48 3.48 0 0 0 .86.69s.22-.12.49-.35l.32-.32c.34-.38.66-.93.67-1.63z"/></svg>
                        </div>
                        {{street}}
                    </a>
                    <?/*<span>{{streetdescr}}</span>*/?>
                </div>
                <div class="salon-item-phone salon-item-info">
                    <div class="salon-item-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 12 12"><path fill="#8a007c" d="M3.55 4.66l.42-1.15c.1-.26.13-.54.07-.8L3.37 0 0 .49c.18 3 1.43 5.81 3.55 7.94A12.3 12.3 0 0 0 11.63 12L12 8.46l-2.5-.57a1.34 1.34 0 0 0-.84.1l-1.25.55a11.73 11.73 0 0 1-2.17-1.69 11.66 11.66 0 0 1-1.69-2.19z"/></svg>
                    </div>
                    <a :href="'tel:' + phone" v-for="phone in phones" :class="{'comagic_phone': !noComagic }">{{phone}}</a>
                </div>
                <div class="salon-item-time salon-item-info">
                    <div class="salon-item-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14"><path fill="#8a007c" d="M7 0a7 7 0 1 0 .02 14.02A7 7 0 0 0 7 0zm0 12.51A5.51 5.51 0 1 1 7 1.48a5.51 5.51 0 0 1 0 11.03z"/><path fill="#900c83" d="M6 4.5a.5.5 0 0 1 1 0v3a.5.5 0 0 1-1 0z"/><path fill="#900c83" d="M9.51 7a.5.5 0 0 1 .49.5.5.5 0 0 1-.49.5H6.49A.5.5 0 0 1 6 7.5c0-.28.22-.5.49-.5z"/></svg>
                    </div>
                    <div v-if="coming_soon"><strong>{{coming_soon}}</strong></div>
                    <div v-else>{{time}}</div>
                </div>
            </div>
            <div class="salons-item__bottom">
                <div class="salon-item-more">
                    <a :href="readmore">Подробнее</a>
                </div>
                <div class="salon-item-button">
                    <a v-if="!coming_soon" :href="enroll" class="button" >Записаться</a>
                    <a v-else  class="button" disabled="">Записаться</a>
                </div>
            </div>
		</div>
	</transition>
</script>
<script type="text/x-template" id="salonsMap">
	<div class="salons-map">
	</div>
</script>