<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// print_r($arResult);
// сервисы
	CModule::IncludeModule("highloadblock");
	use Bitrix\Highloadblock as HL; 
	use Bitrix\Main\Entity;
    use Bitrix\Main\IO, Bitrix\Main\Application;

    $includePhone = new IO\File(Application::getDocumentRoot() . '/local/templates/tairai/paths/phone.php');
    $phone = $includePhone->getContents();

	$arResult["SERVICES"] = Array();

	$hlblock = HL\HighloadBlockTable::getById(1)->fetch(); 
	$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
	$main_query = new Entity\Query($entity);
	$main_query->setSelect(array('*'));
	$result = $main_query->exec();
	$result = new CDBResult($result);

	while ($row = $result->Fetch()) {
		$arResult["SERVICES"][] = Array("NAME" => $row["UF_NAME"], "VALUE" => $row["UF_XML_ID"], "IMAGE" => CFile::GetPath($row["UF_FILE_WHITE"]), "DESCRIPTION" => $row["UF_DESCRIPTION"]);
	}

// города
	$arResult["cityList"] = Array();
	GLOBAL $CITY_CODE;
	$arResult["CURRENT_CITY"] = $CITY_CODE;
	foreach ($arResult["SECTIONS"] as $key => $arSection) {
		if (!strlen($arSection["IBLOCK_SECTION_ID"])) {
			$arResult["cityList"][] = Array(
				"id" => $arSection["ID"]
				, "value" => $arSection["CODE"]
				, "label" => $arSection["NAME"]
				//, "selected" => ($key == 0)
				, "metro" => Array()
				);
			if (is_null($arResult["CURRENT_CITY"])) {
				$arResult["CURRENT_CITY"] = $arSection["CODE"];
			}
		}
	}

	function get_city($id, $city)
	{
		$metro = -1;
		foreach ($city as $city_item) {
			if ($id == $city_item["ID"]) {
				if (strlen($city_item["IBLOCK_SECTION_ID"])) {
					$metro = $city_item["IBLOCK_SECTION_ID"];
				} else {
					return $city_item["CODE"];
				}
			}
		}

		foreach ($city as $city_item) {
			if ($metro == $city_item["ID"]) {
				return $city_item["CODE"];
			}
		}
		
		return "";
	}

// metro
	foreach ($arResult["SECTIONS"] as $arSection) {
		foreach ($arResult["cityList"] as &$arCity) {
			if ($arCity["id"] == $arSection["IBLOCK_SECTION_ID"]) {
				$arCity["metro"][] = Array(
					"id" => $arSection["ID"]
					, "value" => $arSection["CODE"]
					, "label" => $arSection["NAME"]
					);
			}
		}
	}

	$arResult["salons"] = Array();
	$rsSalon = CIBlockElement::GetList(
		Array("SORT" => "ASC", "NAME" => "ASC")
		, Array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "ACTIVE" => "Y")
		, false
		, false
		, Array("ID", "DETAIL_PAGE_URL", "IBLOCK_SECTION_ID", "NAME")
	);

	function get_metro($id, $metro)
	{
		foreach ($metro as $metro_item) {
			if (($id == $metro_item["ID"]) && (strlen($metro_item["IBLOCK_SECTION_ID"]))) {
				return $metro_item["CODE"];
			}
		}

		return "";
	}

// салоны
	while ($arSalon = $rsSalon->GetNextElement()) {

		$properties = $arSalon->GetProperties();
		$fields = $arSalon->GetFields();
		list($lat, $long) = explode(',', $properties['MAP']['~VALUE']);
        $comingSoondate = DateTime::createFromFormat('d.m.Y', $properties['COMING_SOON']['~VALUE']);
        $comingSoon = ($comingSoondate)? 'открытие в '.getComingSoonMonth($comingSoondate->format('m')) : '';
		$arResult['salons'][] = [
			'id' => $fields['ID'],
            'metro' => get_metro($fields['IBLOCK_SECTION_ID'], $arResult['SECTIONS']),
            'address' => [
                'streetAddress' =>($CITY_CODE == 'moscow' && strripos($fields['~NAME'], 'м.') === false)? $fields['~NAME'].', '.$properties['ADDRESS']['~VALUE']: $properties['ADDRESS']['~VALUE'],
                'descr' => $fields['~NAME'],
            ],
            'params' => $properties['SERVICES']['~VALUE'],
            'phoneNumbers' => ($CITY_CODE == 'moscow' ||  $CITY_CODE == 'khabarovsk') ? $properties['PHONES']['~VALUE'] : [$phone],
            'lat' => $lat,
            'lng' => $long,
            'distance' => 0,
            'time' => str_replace('00:00 - 23:59', 'круглосуточно', $properties['TIME']['~VALUE']),
            'readmore' => $fields['DETAIL_PAGE_URL'],
            'enroll' => '/online-recording/?s=' . $fields['ID'],
            'city' => get_city($fields['IBLOCK_SECTION_ID'], $arResult['SECTIONS']),
            'regionPhone' => $phone,
            'coming_soon' => $comingSoon,
        ];
	}
