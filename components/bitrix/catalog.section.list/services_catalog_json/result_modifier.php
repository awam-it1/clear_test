<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

	/**
	 * Collect services and their groups from saloons from current city
	 */
	global $CITY_CODE;
	$services = [];
	$sDb = CIBlockElement::GetList(
		[],
		[
			"IBLOCK_ID" => 6, // Saloons 
			"ACTIVE" => "Y",
			"SECTION_CODE" => $CITY_CODE,
			"INCLUDE_SUBSECTIONS" => "Y"
		],
		false,
		false,
		[
			"ID",
			"NAME",
			"PROPERTY_ALL_SERVICES",
            "PROPERTY_PRICE_TYPE"
		]
	);
	while($sOb = $sDb->GetNext()) {
        $salon_id = $sOb['PROPERTY_PRICE_TYPE_VALUE'];
		$services[$sOb['PROPERTY_ALL_SERVICES_VALUE']['service']][] = $sOb; // !!!
	}
	
	// варианты используемых услуг
    $rsItem = CIBlockElement::GetList(
        Array("SORT" => "ASC")
        , Array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "SECTION_ID" => array_map(function($section) { return $section["ID"]; }, $arResult["SECTIONS"]), "ACTIVE" => "Y")
        , false
        , false
        , Array("ID", "IBLOCK_SECTION_ID", "PROPERTY_ID1C", "PROPERTY_TIME")
    );
    while ($arItem = $rsItem->Fetch()) { 
        foreach ($arResult["SECTIONS"] as &$arSection) {
            if ($arItem["IBLOCK_SECTION_ID"] == $arSection["ID"]) {
                if (!isset($arSection["ITEMS"])) {
                    $arSection["ITEMS"] = [];
                }

                $arSection["ITEMS"][] = $arItem;
                break;
            }
        }
    }

	// запрашиваем все цены (в идеале нужен фильтр по салонам текущего города)
    $all_price = [];
    $rsPrice = CIBlockElement::GetList(
        Array("SORT" => "ASC")
        , Array("IBLOCK_ID" => 21, "ACTIVE" => "Y")
        , false
        , false
        , Array("ID", "PROPERTY_VALUE", "PROPERTY_SERVICE")
    );
    while ($arPrice = $rsPrice->Fetch()) {
        if (isset($all_price[$arPrice["PROPERTY_SERVICE_VALUE"]])) {
            if (intval($all_price[$arPrice["PROPERTY_SERVICE_VALUE"]]) > intval($arPrice["PROPERTY_VALUE_VALUE"])) {
                $all_price[$arPrice["PROPERTY_SERVICE_VALUE"]] = $arPrice["PROPERTY_VALUE_VALUE"];
            }
        } else {
            $all_price[$arPrice["PROPERTY_SERVICE_VALUE"]] = $arPrice["PROPERTY_VALUE_VALUE"];
        }
    }

// проставляем цены
    foreach ($arResult["SECTIONS"] as &$arSection) {
        $arSection["PRICE_TEXT"] = "не указана";
        if (isset($all_price[$arSection["UF_ID1C"]])) {
           // $arSection["PRICE_TEXT"] = number_format($all_price[$arSection["UF_ID1C"]], 0, "", " ")." руб.";
            $price = GetPrice($services[0]["PROPERTY_PRICE_TYPE_VALUE"], $arItem["PROPERTY_ID1C_VALUE"]);
            $arSection["PRICE_TEXT"] = $price." руб.";
            $price = str_replace(' ', '', $price);
            $arSection["PRICE"] = (int)$price;
        } else {
            if (count($arSection["ITEMS"])) {
                $price = 1000000;
                $arSection["PRICE_TEXT"] = '';
                $duration = array();
                if (!function_exists('getMinutesByString')) {
                    function getMinutesByString($string)
                    {
                        $minutes = explode(':', $string);
                        return 60 * $minutes[0] + $minutes[1];
                    }
                }
                foreach($arSection["ITEMS"] as $item) {
                    $item['DURATION'] = getMinutesByString($item["PROPERTY_TIME_VALUE"]);
                    $duration[] = $item;
                }
                if (!function_exists('cmp')) {
                    function cmp($a, $b)
                    {
                        if ($a["DURATION"] == $b["DURATION"]) {
                            return 0;
                        }
                        return ($a["DURATION"] < $b["DURATION"]) ? -1 : 1;
                    }
                }
                usort($duration, "cmp");
                $i = 0;
                foreach ($duration as $key => $arItem):
                    $duration_id = $arItem["PROPERTY_ID1C_VALUE"];
                    break;
                endforeach;


                // берем id салона и первый duration,
                $price = GetPrice($salon_id, $duration_id);
                $price_text = (!empty($price)) ? "от ".$price.' руб.' : 'не указана';

                $arSection["PRICE_TEXT"] = $price_text;
                $price = str_replace(' ', '', $price);
                $arSection["PRICE"] = (int)$price;
            }
        }
    }
    if(!function_exists('PrepareItemForCatalog')) {
        function PrepareItemForCatalog($item) {
            $res = $item["PRICE_TEXT"];

            return [$item["UF_ID1C"] => $res];
        }
    }
// найти все дочерние
    if(!function_exists('get_childrens_services_catalog')) {
        global $children_services;
        if(empty($children_services))
            $children_services = array();
        function get_childrens_services_catalog($current_item, $list, $needSort = false, $services = []) {
            global $children_services;

            $childrens = [];
            /**
             * @TODO Добавить фильтрацию по списку ID доступных для салона
             */

            foreach ($list as $item) {
                if ($needSort) {

                    // В этом случае просто собирает список доступных услуг, без рекурсивного прохода
                    if (!empty($services[(int)$item['ID']])) {
                        $childrens[] = PrepareItemForCatalog($item);
                    } else {

                        foreach($item['ITEMS'] as $item2) {
                            $children_services[$item2['PROPERTY_ID1C_VALUE']] = 'от '.$item['PRICE'].' руб.';
                        }

                    }
                } else {

                    if ($item["IBLOCK_SECTION_ID"] == $current_item["ID"]) {
                        $childrens = array_merge($childrens, get_childrens_services_catalog($item, $list, $needSort));
                    }
                }
            }

            if (count($childrens)) {
                return $childrens;
            }

            if (!$needSort) {
                $res = PrepareItemForCatalog($current_item);
                return [$res];
            } else {
                return [];
            }
        }

// формируем основной объект
        $arResult["JSON_OBJECT"] = Array();
        foreach ($arResult["SECTIONS"] as $arSection) {

            /**
             * Проходим по первому уровню категорий
             */
            if ($arSection["IBLOCK_SECTION_ID"] == null) {

                $needFilter = !array_key_exists($arSection['ID'], $services);

                if (!$needFilter) {
                    unset($services[$arSection['ID']]);
                }

                $list = get_childrens_services_catalog($arSection, $arResult["SECTIONS"], $needFilter, $services);

                // Скрываем данный таб, если нет детишек
                if (empty($list)) {
                    continue;
                }

                if (count($list)) {
                    $arResult["JSON_OBJECT"][] = $list;
                }
            }
        }
        $arResult["JSON_OBJECT"][] = $children_services;
    }
?>