<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// echo '<script>var log_php_object = '.json_encode($arResult).';</script>';

if (count($arResult["TREE"])):?>
<div class="inner-wrapper">
    <h2 class="cntr">Услуги</h2>

    <div class="salon-services-tabs">

        <div class="tabs-cnt" data-tabs-list>
            <div class="tabs__list mobile-hidden" data-tabs data-target="_tabs_">
                <?foreach($arResult["TREE"] as $key => $arSection):?>
                <a href="#_tab_<?=$arSection["ID"];?>" class="tabs__item<?if (!$key) echo " active";?>" data-tabs-trigger>
                    <?if (strlen($arSection["UF_ICON"])):?>
                    <div class="sst-icon">
                        <img src="<?=CFile::GetPath($arSection["UF_ICON"])?>" alt="<?=$arSection["NAME"];?>" />
                    </div>
                    <?endif;?>
                    <div class="sst-label">
                        <?=$arSection["NAME"];?>
                    </div>
                </a>
                <?endforeach;?>
            </div>
            <div class="mobile-select mobile-visible" data-tabs="" data-target="_tabs_">
                <select name="mobile-tab" data-tabs-select="">
                    <?foreach($arResult["TREE"] as $key => $arSection):?>
                    <option value="#_tab_<?=$arSection["ID"];?>" selected=""><?=$arSection["NAME"];?></option>
                    <?endforeach;?>
                </select>
                <span class="mobile-select__icon"></span>
            </div>
        </div>

        <div class="inner-wrapper min-width">

            <div class="tabs__content" data-tabs-content id="_tabs_">
                <?foreach($arResult["TREE"] as $key => $arSection):?>
                <div class="tabs__pane<?if (!$key) echo " active";?>" data-tabs-pane id="_tab_<?=$arSection["ID"];?>">
                    <p><?=$arSection["DESCRIPTION"];?></p>
                    <?if (count($arSection["CHILDREN"])):?>
                        <div class="table-container">
                            <table class="salon-table">
                                <thead>
                                    <tr>
                                        <td>
                                            Название услуги
                                        </td>
                                        <td>
                                            Краткое описание
                                        </td>
                                        <td rowspan="2">
                                            Стоимость, ₽
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                <?foreach($arSection["CHILDREN"] as $arItem):?>
                                    <tr>
                                        <td>
                                            <a href="<?=$arItem["DETAIL_PAGE_URL"];?>" class="is-link"><?=$arItem["NAME"];?></a>
                                        </td>
                                        <td>
                                            <p><?=$arItem["~UF_SHORT"];?></p>
                                        </td>
                                        <td>
                                            <p><?=$arItem["PRICE"];?></p>
                                        </td>
                                        <td>
                                            <a href="/online-recording/?s=<?=$arParams["SALON_ID"];?>&s0=<?=$arItem["ID"];?>" class="is-link">Записаться</a>
                                        </td>
                                    </tr>
                                <?endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    <?/*<div class="table-container">
                        <table>
                            <thead>
                                <tr>
                                    <td>
                                        Название услуги
                                    </td>
                                    <td>
                                        Описание
                                    </td>
                                    <td>
                                        Продолжительность
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <?foreach($arSection["ITEMS"] as $arItem):?>
                                <tr>
                                    <td>
                                        <p><?=$arItem["NAME"];?></p>
                                    </td>
                                    <td>
                                        <p><?=$arItem["~PREVIEW_TEXT"];?></p>
                                    </td>
                                    <td>
                                        <p><?=$arItem["PROPERTY_TIME_VALUE"];?></p>
                                    </td>
                                </tr>
                                <?endforeach;?>
                            </tbody>
                        </table>
                    </div>
                    */?>
                    <?endif;?>
                </div>
                <?endforeach;?>
            </div>
        </div>
    </div>
</div>
<?endif;?>