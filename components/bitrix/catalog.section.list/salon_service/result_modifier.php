<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// echo '<script>var log_php_object_Params = '.json_encode($arParams).';</script>';

// найти все дочерние
	function get_children($current_item, $list)
	{
		$children = [];

		foreach ($list as $item) {
			if ($item["IBLOCK_SECTION_ID"] == $current_item["ID"]) {
				$children = array_merge($children, get_children($item, $list));
			}
		}

		if (count($children)) {
			return $children;
		}

		return [$current_item];
	}


$arResult["TREE"] = Array();
foreach ($arResult["SECTIONS"] as $arSection) {
	if ($arSection["IBLOCK_SECTION_ID"] == null) {
		$arSection["CHILDREN"] = get_children($arSection, $arResult["SECTIONS"]);

		if (count($arParams["SERVICES"])) {
			foreach ($arSection["CHILDREN"] as $key => $childSection) {
				if (!in_array($childSection["ID"], $arParams["SERVICES"])) {
					unset($arSection["CHILDREN"][$key]);
				}
			}
		}

		if (count($arSection["CHILDREN"])) {
			$arResult["TREE"][] = $arSection;
		}
	}
}

	
if (count($arResult["TREE"])) {
	// проставляем цены
		// все дочернии разделы
            $all_children = [];
			foreach ($arResult["TREE"] as $arSection) {
                $all_children = array_merge($all_children, $arSection["CHILDREN"]);
			}
			$sections_id = array_map(function($arSection) { return $arSection["ID"]; }, $all_children);

		// варианты используемых услуг
			$services_variant = [];
			$rsServiceVariant = CIBlockElement::GetList(
				Array("SORT" => "ASC")
				, Array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "SECTION_ID" => $sections_id, "ACTIVE" => "Y")
				, false
				, false
				, Array("ID", "IBLOCK_SECTION_ID", "PROPERTY_ID1C", "PROPERTY_TIME")
			);
			while ($arServiceVariant = $rsServiceVariant->Fetch()) {
				// строка время - в число
					$paths = explode(":", $arServiceVariant["PROPERTY_TIME_VALUE"]);
					$arServiceVariant["TIME"] = intval($paths[0]) * 3600 + intval($paths[1]) * 60 + intval($paths[2]);

				if (isset($services_variant[$arServiceVariant["IBLOCK_SECTION_ID"]])) {
					if ($services_variant[$arServiceVariant["IBLOCK_SECTION_ID"]]["TIME"] > $arServiceVariant["TIME"]) {
						$services_variant[$arServiceVariant["IBLOCK_SECTION_ID"]] = $arServiceVariant; 
					}
				} else {
					$services_variant[$arServiceVariant["IBLOCK_SECTION_ID"]] = $arServiceVariant; 
				}
			}


		// все идентификаторы (можно убрать те что имеют варианты)
			$ID1C_list = array_merge(
				array_map(function($arSection) { return $arSection["UF_ID1C"]; }, $all_children)
				, array_map(function($arVariant) { return $arVariant["PROPERTY_ID1C_VALUE"]; }, $services_variant)
			);


		// запрашиваем все цены по используемым услугам
			$all_price = [];
			$rsPrice = CIBlockElement::GetList(
				Array("SORT" => "ASC")
				, Array("IBLOCK_ID" => 21, "ACTIVE" => "Y", "PROPERTY_SERVICE" => $ID1C_list, "PROPERTY_TYPE" => $arParams["PRICE_TYPE"])
				, false
				, false
				, Array("ID", "PROPERTY_VALUE", "PROPERTY_SERVICE")
			);
			while ($arPrice = $rsPrice->Fetch()) {
				$all_price[$arPrice["PROPERTY_SERVICE_VALUE"]] = $arPrice["PROPERTY_VALUE_VALUE"];
			}

		// проставляем цены
			foreach ($arResult["TREE"] as &$arSection) {
				foreach ($arSection["CHILDREN"] as &$arService) {
					// без вариантов
					if (isset($all_price[$arService["UF_ID1C"]])) {
						$arService["PRICE"] = $all_price[$arService["UF_ID1C"]];
					// с вариантов
					} else {
						if (isset($services_variant[$arService["ID"]])) {
							if (isset($all_price[$services_variant[$arService["ID"]]["PROPERTY_ID1C_VALUE"]])) {
								$arService["PRICE"] = $all_price[$services_variant[$arService["ID"]]["PROPERTY_ID1C_VALUE"]];
							}
						}
					}
				}
			}
}



?>