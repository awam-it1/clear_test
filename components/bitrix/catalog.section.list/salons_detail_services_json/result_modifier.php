<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
// echo '<script>var log_php_object= '.json_encode($arResult).';</script>';
if (!CModule::includeModule('trdata')) return;

//Список разделов
$sections = [];

$allowed_services_sections = Lib::getServiceIDs();

// запрашиваем id салона
$rsSalon = CIBlockElement::GetList(
    Array("SORT" => "ASC")
    , Array("IBLOCK_ID" => 6, "ACTIVE" => "Y", "CODE" => $_REQUEST["CODE"])
    , false
    , false
    , Array()
);
while ($arSalon = $rsSalon->Fetch()) {
    $salon_id = $arSalon["ID"];
    $prop = CIBlockElement::GetByID($salon_id)->GetNextElement()->GetProperties();
    $salon_price_type = $prop['PRICE_TYPE']['VALUE'];
}

$services = trServiceData::getAllServices();
//Получим самые верхние категории
foreach ($services as $item) if ($item['DEPTH_LEVEL'] == 1) {
    $sertIds[$item['ID']] = $item['ID'];
}
//услуги салона
$services = ccHelpers::getSalonServicesList($salon_id);

foreach ($services as $item)  $sertIds[$item['ID']] = $item['ID'];

// варианты используемых услуг
    $rsItem = CIBlockElement::GetList(
        Array("SORT" => "ASC")
        , Array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "SECTION_ID" => array_map(function($section) { return $section["ID"]; }, $arResult["SECTIONS"]), "ACTIVE" => "Y")
        , false
        , false
        , Array("ID", "IBLOCK_SECTION_ID", "PROPERTY_ID1C", "PROPERTY_TIME")
    );
    while ($arItem = $rsItem->Fetch()) {
        foreach ($arResult["SECTIONS"] as &$arSection) {
            if ($arItem["IBLOCK_SECTION_ID"] == $arSection["ID"]) {
                if (!isset($arSection["ITEMS"])) {
                    $arSection["ITEMS"] = [];
                }

                $arSection["ITEMS"][] = $arItem;
                break;
            }
        }
    }

 // запрашиваем все цены салона
    $all_price = [];
    $rsPrice = CIBlockElement::GetList(
        Array("SORT" => "ASC")
        , Array("IBLOCK_ID" => 21, "ACTIVE" => "Y", 'PROPERTY_TYPE' => $salon_price_type)
        , false
        , false
        , Array("ID", "PROPERTY_VALUE", "PROPERTY_SERVICE")
    );
    while ($arPrice = $rsPrice->Fetch()) {
        if (isset($all_price[$arPrice["PROPERTY_SERVICE_VALUE"]])) {
            if (intval($all_price[$arPrice["PROPERTY_SERVICE_VALUE"]]) < intval($arPrice["PROPERTY_VALUE_VALUE"])) {
                $all_price[$arPrice["PROPERTY_SERVICE_VALUE"]] = $arPrice["PROPERTY_VALUE_VALUE"];
            }
        } else {
            $all_price[$arPrice["PROPERTY_SERVICE_VALUE"]] = $arPrice["PROPERTY_VALUE_VALUE"];
        }
    }


// найти все дочерние
    function get_childrens($salon_id, $current_item, $list, $sertIds, $all_price)
    {
        $childrens = [];
        foreach ($list as $item) {
            if ($item["IBLOCK_SECTION_ID"] == $current_item["ID"]) {
                $childrens = array_merge($childrens, get_childrens($salon_id, $item, $list, $sertIds, $all_price));
            }
        }

        if (count($childrens)) {
            foreach ($childrens as $item){
                if(isset($sertIds[$item["element_id"]])){
                    $container[] = $item;
                }
            }
            return $container;
        }

        foreach ($current_item['ITEMS'] as &$duration){
            $duration['PRICE'] = $all_price[$duration['PROPERTY_ID1C_VALUE']];
        }

        $res = [
            "element_id" => $current_item["ID"]
            ,"id" => $current_item["UF_ID1C"]
            , "section_id" => $current_item["ID"]
            , "img" => ($current_item["UF_PICTURES"] ?  CFile::ResizeImageGet($current_item["UF_PICTURES"][0],['width' => 180, 'height' => 94])['src'] : '/local/include/img/stub.jpg')
            , "link" => $current_item["SECTION_PAGE_URL"]
            , "price" => $current_item['ITEMS'][0]['PRICE']
            , "name" => $current_item["~NAME"]
            , "text" => $current_item["~UF_SHORT"]
            //, 'enroll' => '/online-recording/?s0='.$current_item['ITEMS'][0]['ID'].'&s='.$salon_id
			, 'enroll' => '/online-recording/?s0=' . $current_item['ID'].'&s='.$salon_id
            , "duration" =>  $current_item['ITEMS']
            , "sort" =>  $current_item['SORT']
        ];


        return [$res];
    }

// формируем основной объект
    $arResult["JSON_OBJECT"] = Array();
    foreach ($arResult["SECTIONS"] as $arSection) {
        if ($arSection["IBLOCK_SECTION_ID"] == null) {
            $list = get_childrens($salon_id, $arSection, $arResult["SECTIONS"], $sertIds, $all_price);

            $res = [
                'salon_id' => $salon_id
                ,'id' => $arSection["UF_ID1C"]
                , 'name' => $arSection["NAME"]
                , 'filterParam' => [$arSection["ITEMS"]]
                , 'list' => customMultiSort($list, 'name', true)
            ];

            if (count($list)>1) {
                 $arResult["JSON_OBJECT"][] = $res;
            }
        }
    }

?>